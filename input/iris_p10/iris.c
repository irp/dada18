#include "open.h"

struct dada_data* detector_open(struct dada_data* data, struct dada_data_path* path, __attribute__((unused))struct dada_data_modi* modi)
{
	int fd = -1;
	struct stat stat;
	void* so_handle = NULL;
	struct image* (*so_image_create)(int, int*) = NULL;
	int dims[2] = {0, 0};
	struct image* image = NULL;
	const char* IMG   = NULL;
	const unsigned short* FIRST = NULL;
	const unsigned short* LAST  = NULL;
	int headerlen = 0;
	int binsize   = 0;

	dada_status success = DADA_ERROR;
	char err[512];
	int errloop = 0;

	void* so_handle_filename = NULL;
	char* (*so_filename)(void*) = NULL;
	void* so_handle_dumpname = NULL;
	char* (*so_dumpname)(void*) = NULL;
	char* filename = NULL;
	char* dumpname = NULL;

	/* data -> filename */
	DL_LOAD_SYMBOL(DL_PATH_DATA(data, "filename"),  "detector_filename",  so_filename,  so_handle_filename);
	filename = so_filename(data);
	if (filename == NULL || strlen(filename) == 0)
	{
		RINGBUFFER_PRINTF("empty filename from %p", data);
		snprintf(err, sizeof(err)-1, "cannot deduce filename from [%s/%s/%s]",
				data->instrument, data->experiment, data->detector);
		goto error;
	}


	/* try to open and access file */
	CHECK_FILE_NAME(filename);
	fd = open(filename, O_RDONLY);
	if (fd < 0)
	{
		RINGBUFFER_PRINTF("cannot open %s: %s", filename, strerror(errno));
		snprintf(err, sizeof(err)-1, "cannot open [%s]: %s",
				filename, strerror(errno));
		switch (errno)
		{
			case ENOENT: success = DADA_NOTFOUND; break;
			case EACCES: success = DADA_NOACCESS; break;
			default:     success = DADA_ERROR; break;
		}
		goto error;
	}
	if (fstat(fd, &stat) != 0)
	{
		RINGBUFFER_PRINTF("cannot access %s: %s", filename, strerror(errno));
		snprintf(err, sizeof(err)-1, "cannot access [%s]: %s",
				filename, strerror(errno));
		switch (errno)
		{
			case EACCES: success = DADA_NOACCESS; break;
			default:     success = DADA_ERROR; break;
		}
		goto error;
	}

	RINGBUFFER_PRINTF("file %s size %.3f MB", filename, 1.0*stat.st_size/1048576);

	IMG = (const char*) mmap(NULL, stat.st_size, PROT_READ, MAP_PRIVATE, fd, 0);
	if (IMG == MAP_FAILED)
	{
		RINGBUFFER_PRINTF("cannot mmap %s: %s", filename, strerror(errno));
		snprintf(err, sizeof(err)-1, "cannot mmap [%s]: %s",
				filename, strerror(errno));
		switch (errno)
		{
			case EACCES: success = DADA_NOACCESS; break;
			default:     success = DADA_ERROR; break;
		}
		goto error;
	}

	if (iris_parse_header(IMG, &headerlen, &binsize, dims) != 0)
	{
		RINGBUFFER_PRINTF("cannot parse header of %s", filename);
		snprintf(err, sizeof(err)-1, "cannot parse header of [%s]", filename);
		success = DADA_BADHEADER;
		goto error;
	}


	/* create image struct */
	DL_LOAD_SYMBOL(DL_PATH_CONST("./image"),  "image_create",  so_image_create,  so_handle);
	image = so_image_create(2, dims);
	DL_CLOSE(so_handle);

	if (image == NULL)
		goto cleanup;

	/* load detector data into image */
	FIRST = (unsigned short*) ( (unsigned char*)IMG + headerlen );
	LAST  = (unsigned short*) ( (unsigned char*)IMG + stat.st_size - 1 );
	if (iris_extract_data(FIRST, image->data, binsize, LAST) != 0)
	{
		RINGBUFFER_PRINTF("cannot extract data from %s", filename);
		snprintf(err, sizeof(err)-1, "cannot extract data from [%s]", filename);
		success = DADA_BADDATA;
		goto error;
	}
	image->success = success = DADA_SUCCESS;


	/* try to obtain some meta data */
	DL_LOAD_SYMBOL(DL_PATH_DATA(data, "filename"),  "detector_dumpname",  so_dumpname,  so_handle_dumpname);
	dumpname = so_dumpname(data);
	if (dumpname == NULL || strlen(dumpname) == 0)
	{
		RINGBUFFER_PRINTF("empty dumpname from %p", data);
		snprintf(err, sizeof(err)-1, "empty dumpname from [%s/%s/%s]",
				data->instrument, data->experiment, data->detector);
		goto error;
	}
	{
		FILE* f = NULL;
		char buffer[64];
		double ctime = -1;
		int att = -1;

		FILE* mem = NULL;
		size_t memlen;

		FREE(image->meta);
		mem = open_memstream(&image->meta, &memlen);

		f = fopen(dumpname, "r");
		if (f == NULL)
		{
			fprintf(mem, "{\"status\":\"nodump\",\"reason\":\"%s\"}", strerror(errno));
			goto skipdump;
		}

		/* parse header */
		_READLINE(f, buffer); /* skip time stamp */
		_READLINE(f, buffer); /* Counting time */
		sscanf(buffer, "# Counting time %lf", &ctime);
		_READLINE(f, buffer); /* skip Couting time */
		_READLINE(f, buffer); /* att */
		sscanf(buffer, "# att %d", &att);
		_READLINE(f, buffer); /* skip three lines */
		_READLINE(f, buffer); /* skip three lines */
		_READLINE(f, buffer); /* skip three lines */

		fprintf(mem, "{\"status\":\"okay\",\"dump\":{");
		fprintf(mem, "\"ct\": %.6f, ", ctime);
		fprintf(mem, "\"att\": %d ", att);

		/* loop over motor positions */
		while (!feof(f))
		{
			char mne[16];
			double user, dial;
			_READLINE(f, buffer);
			sscanf(buffer, "%s %lf %lf\n", mne, &user, &dial);
			fprintf(mem, ", \"%s\": %f", mne, user);
		}

skipdump:
		if (f) fprintf(mem, "}}");
		fclose(mem);
		if (f) fclose(f); f=NULL;
	}

cleanup:
	if (IMG)
	{
		if (munmap((void*)IMG, stat.st_size) != 0)
			RINGBUFFER_PRINTF("cannot munmap %s: %s", filename, strerror(errno));
	}
	if (fd) close(fd); fd=-1;

	DL_CLOSE(so_handle_filename);
	FREE(filename);
	DL_CLOSE(so_handle_dumpname);
	FREE(dumpname);

	return image;

error:
	IMAGE_DESTROY(image);
	if (strlen(err) == 0)
		snprintf(err, sizeof(err)-1, "unknown error inside %s:%s", __FILE__, __FUNCTION__);

	/* empty image with negative success code and error message */
	dims[0] = 0;
	dims[1] = 0;

	/* check for goto error loop, then create empty image and copy error message */
	errloop++;
	if (errloop < 2)
	{
		DL_LOAD_SYMBOL(DL_PATH_CONST("./image"),  "image_create",  so_image_create,  so_handle);
		image = so_image_create(2, dims);
		DL_CLOSE(so_handle);
		image->success = success;
		snprintf(image->err, sizeof(image->err)-1, err);
	}

	goto cleanup;
forbidden:
	snprintf(err, sizeof(err)-1, "forbidden filename: [%s]", filename);
	goto error;
}

int iris_parse_header(const char* IMG, int* headerlen, int* binsize, int* dims)
{
	int ret = 0;

	int height = 2048;
	int width  = 2048;

	// const char* end_of_header    = "UserComment=\"\"";
	// const char* head_binsize     = "X-Binary-Size:";
	// const char* head_dimension_1 = "X-Binary-Size-Fastest-Dimension:";
	// const char* head_dimension_2 = "X-Binary-Size-Second-Dimension:";

	// const char* pos_eoh         = NULL;
	// const char* pos_binsize     = NULL;
	// const char* pos_dimension_1 = NULL;
	// const char* pos_dimension_2 = NULL;

	if (IMG == NULL)
	{
		RINGBUFFER_PRINTF("invalid image data pointer (%p)", IMG);
		ret = -1;
		goto cleanup;
	}

	// pos_eoh         = strstr(IMG+66, end_of_header   );
	// pos_binsize     = strstr(IMG, head_binsize    );
	// pos_dimension_1 = strstr(IMG, head_dimension_1);
	// pos_dimension_2 = strstr(IMG, head_dimension_2);

	/*
	if (pos_eoh == NULL)
	{
		RINGBUFFER_PRINTF("cannot find end of header (%p)", pos_eoh);
		ret = -1;
		goto cleanup;
	}
	if (pos_binsize == NULL)
	{
		RINGBUFFER_PRINTF("cannot find binary size (%p)", pos_binsize);
		ret = -1;
		goto cleanup;
	}
	if (pos_dimension_1 == NULL || pos_dimension_2 == NULL)
	{
		RINGBUFFER_PRINTF("cannot find number of pixels (%p,%p)", pos_dimension_1, pos_dimension_2);
		ret = -1;
		goto cleanup;
	}
	*/

	*headerlen = 10240;
	// *binsize   = atoi(pos_binsize     + strlen(head_binsize));
	// dims[0]    = atoi(pos_dimension_1 + strlen(head_dimension_1));
	// dims[1]    = atoi(pos_dimension_2 + strlen(head_dimension_2));
	*binsize = 2*height*width;
	dims[0]  = height;
	dims[1]  = width;

cleanup:
	return ret;
}

int iris_extract_data(const unsigned short* FIRST, double* image, size_t binsize, const unsigned short* LAST)
{
	int ret = 0;

	size_t parsed = 0;
	const unsigned short* parser;
	unsigned short cur  = 0;

	parser = FIRST;

	if (image == NULL)
	{
		ret = -1;
		goto cleanup;
	}

	while (likely(parsed<binsize))
	{
		if (unlikely(parser > LAST))
		{
			ret = -1;
			goto cleanup;
		}

		cur = *parser++;
		parsed += 2;
		*image++ = (double)cur;
	}

cleanup:
	return ret;
}

