#include "open.h"

struct dada_data* detector_open(struct dada_data* data, struct dada_data_path* path, __attribute__((unused))struct dada_data_modi* modi)
{
	const int timing = 1;

	int fd = -1;
	struct stat stat;
	int dims[2];
	const char* IMG   = NULL;
	const unsigned short* FIRST = NULL;
	const unsigned short* LAST  = NULL;
	size_t headerlen = 0;
	size_t binsize   = 0;

	struct timespec t0, t1, t2, t3, t4;
	double tt1, tt2, tt3, tt4;
	clock_gettime(CLOCK_REALTIME, &t0);

	void* so_handle = NULL;
	char* (*so_filename)(void*) = NULL;
	char* filename = NULL;

	DADA_DL_LOAD_SYMBOL(DADA_DL_PATH_DATA(path, "filename"),  "detector_filename",  so_filename,  so_handle);
	filename = so_filename(path);
	DADA_DL_CLOSE(so_handle);
	if (filename == NULL || strlen(filename) == 0)
	{
		DADA_RING_PRINTF("empty filename from %p", path);
		goto error;
	}


	// CHECK_FILE_NAME(filename);
	fd = open(filename, O_RDONLY);
	if (fd < 0)
	{
		DADA_RING_PRINTF("cannot open %s: %s", filename, strerror(errno));
		goto error;
	}
	if (fstat(fd, &stat) != 0)
	{
		DADA_RING_PRINTF("cannot access %s: %s", filename, strerror(errno));
		goto error;
	}

	DADA_RING_PRINTF("file %s size %.3f MB", filename, 1.0*stat.st_size/1048576);

	clock_gettime(CLOCK_REALTIME, &t1);
	IMG = (const char*) mmap(NULL, stat.st_size, PROT_READ, MAP_PRIVATE, fd, 0);
	if (IMG == MAP_FAILED)
	{
		DADA_RING_PRINTF("cannot mmap %s: %s", filename, strerror(errno));
		goto error;
	}

	if (iris_parse_header(IMG, &headerlen, &binsize, dims) != 0)
	{
		DADA_RING_PRINTF("cannot parse header of %s", filename);
		goto error;
	}

	if (!data || data->d[0] != dims[1] || data->d[1] != dims[0])
	{
		if (data) {dada_data_destroy(data); free(data); data=NULL;}
		data = dada_data_init(DADA_DATA_INT, 2, dims[0], dims[1]);
	}
	fprintf(stdout, "dada_data_init(DADA_DATA_INT, 2, %d, %d): %p\n", dims[0], dims[1], data);
	if (data == NULL)
	{
		DADA_RING_PRINTF("dada_data_init(INT, 2, %d,%d) failed", dims[0], dims[1]);
		goto error;
	}

	/* load detector data into image */
	clock_gettime(CLOCK_REALTIME, &t2);
	FIRST = (unsigned short*) ( (unsigned char*)IMG + headerlen );
	LAST  = (unsigned short*) ( (unsigned char*)IMG + stat.st_size - 1 );

	{
		int* writer = data->buffer.i;
		size_t parsed = 0;
		const unsigned short* parser;
		unsigned short cur  = 0;
		parser = FIRST;

		while (parsed<binsize)
		{
			if (parser > LAST)
			{
				DADA_RING_PRINTF("parser %p ran out of bounds!", parser);
				goto error;
			}

			cur = *parser++;
			parsed += 2;
			*writer++ = cur;
		}
	}

	clock_gettime(CLOCK_REALTIME, &t3);


cleanup:
	if (IMG) {munmap((void*)IMG, stat.st_size); IMG=NULL;}
	if (fd)  {close(fd);       fd=-1;}
	FREE(filename);
	clock_gettime(CLOCK_REALTIME, &t4);

	tt1 = (t1.tv_sec-t0.tv_sec)+(t1.tv_nsec-t0.tv_nsec)*1e-9;
	tt2 = (t2.tv_sec-t1.tv_sec)+(t2.tv_nsec-t1.tv_nsec)*1e-9;
	tt3 = (t3.tv_sec-t2.tv_sec)+(t3.tv_nsec-t2.tv_nsec)*1e-9;
	tt4 = (t4.tv_sec-t3.tv_sec)+(t4.tv_nsec-t3.tv_nsec)*1e-9;
	if (timing) DADA_RING_PRINTF("iris-timing: %.1f ms+%.1f ms+%.1f ms+%.1f ms", tt1*1e3, tt2*1e3, tt3*1e3, tt4*1e3);

	return data;

error:
	if (data) {dada_data_destroy(data); free(data); data=NULL;}

	goto cleanup;
}

int iris_parse_header(const char* IMG, size_t* headerlen, size_t* binsize, int* dims)
{
	int ret = 0;

	int height = 2048;
	int width  = 2048;

	if (IMG == NULL)
	{
		DADA_RING_PRINTF("invalid image data pointer (%p)", IMG);
		ret = -1;
		goto cleanup;
	}

	*headerlen = 10240;
	*binsize = 2*height*width;
	dims[0]  = height;
	dims[1]  = width;

cleanup:
	return ret;
}

