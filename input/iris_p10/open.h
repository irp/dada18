#ifndef _IRIS_OPEN_H_
#define _IRIS_OPEN_H_

#include "../../dada.h"

struct dada_data* detector_open(struct dada_data* data, struct dada_data_path* path, struct dada_data_modi* modi);

int iris_parse_header(const char* IMG, size_t* headerlen, size_t* binsize, int* dims);

#endif

