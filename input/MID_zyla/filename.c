/* filename.c for pirra @ P10
 */
#include "filename.h"

char* detector_filename(struct dada_data_path* path)
{
	char* buffer = NULL;
	FILE* mem = NULL;
	size_t memlen;
	mem = open_memstream(&buffer, &memlen);

	if (path->len==5 && strcmp(path->path[0], "MID")==0 && path->path[2] != NULL)
	{
		int run;
		if (path->path[3][0] == 'r')
			run = atoi(path->path[3]+1);
		else
			run = atoi(path->path[3]  );
		fprintf(mem, "config/%s/%s/data/raw/r%04d/RAW-R%04d-DA%02d-S%05d.h5",
				path->path[0], path->path[1],
				run, run, 4, 0);
		goto cleanup;
	}

cleanup:
	fclose(mem);
	// DADA_RING_PRINTF("filename: %s", buffer);
	// fprintf(stdout, "filename: [%s]\n", buffer);
	return buffer;
}

