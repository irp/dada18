/* pixelmask.c for Merlin @ PXS */
#include "pixelmask.h"

int detector_pixelmask(struct dada_data* data, __attribute__((unused))struct dada_data_modi* modi)
{
	int ret = 0;

	if (data == NULL)
	{
		DADA_RING_PRINTF("invalid data %p", data);
		ret = -1;
		goto cleanup;
	}

cleanup:
	return ret;
}

