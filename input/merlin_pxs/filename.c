/* filename.c for Merlin @ PXS
 *
 * parse struct dada_detector_data* and build actual filename
 * for the requested file;
 */
#include "filename.h"

char* detector_filename(struct dada_data_path* path)
{
	char* buffer = NULL;
	FILE* mem = NULL;
	size_t memlen;
	mem = open_memstream(&buffer, &memlen);

	if (path->len==5 && strcmp(path->path[0], "PXS")==0 && path->path[3] != NULL && path->path[4] != NULL)
	{
		fprintf(mem, "config/%s/%s/data/%s/%s1.mib",
				path->path[0], path->path[1], path->path[2],
				path->path[3]); // , atoi(path->path[4]));
		goto cleanup;
	}

cleanup:
	fclose(mem);
	DADA_RING_PRINTF("filename: %s:%s len %ld", buffer, path->path[path->len-1], memlen);
	// fprintf(stdout, "filename: [%s:%s] (memlen: %ld)\n", buffer, path->path[path->len-1], memlen);
	return buffer;
}

