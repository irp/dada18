/* open.c for Merlin @ PXS */
#include "open.h"

const int debug = 0;

struct dada_data* detector_open(struct dada_data* data, struct dada_data_path* path, __attribute__((unused))struct dada_data_modi* modi)
{
	struct timespec t0, t1, t2, t3, t4;
	double tt1, tt2, tt3, tt4;
	double t;
	clock_gettime(CLOCK_REALTIME, &t0);

	int fd = -1;
	struct stat stat;
	int dims[2] = {0, 0};

	void* so_handle = NULL;
	char* (*so_filename)(void*) = NULL;
	char* filename = NULL;

	DADA_DL_LOAD_SYMBOL(DADA_DL_PATH_DATA(path, "filename"),  "detector_filename",  so_filename,  so_handle);
	filename = so_filename(path);
	DADA_DL_CLOSE(so_handle);
	if (filename == NULL || strlen(filename) == 0)
	{
		DADA_RING_PRINTF("empty filename from %p", path);
		goto error;
	}

	int frame = atoi(path->path[path->len-1])-1; /* first frame is called MQ1, but we count from 0 here */

	/* try to open and access file */
	// CHECK_FILE_NAME(filename);
	fd = open(filename, O_RDONLY);
	if (fd < 0)
	{
		DADA_RING_PRINTF("cannot open %s: %s", filename, strerror(errno));
		goto error;
	}
	if (fstat(fd, &stat) != 0)
	{
		DADA_RING_PRINTF("cannot access %s: %s", filename, strerror(errno));
		goto error;
	}

	DADA_RING_PRINTF("file %s size %.3f MB frame %d", filename, 1.0*stat.st_size/1048576, frame);

	clock_gettime(CLOCK_REALTIME, &t1);
	t = (t1.tv_sec-t0.tv_sec)+(t1.tv_nsec-t0.tv_nsec)*1e-9;

	{
		off_t offset;
		char tmp[768];
		int parsed;
		int imno, headerlen, vier, dim1, dim2, bitsize;
		unsigned char* buffer = NULL;
		ssize_t imglen;
		int i, j;

		/* parse header of first frame */
		offset = 0x00;
		if (debug) DADA_RING_PRINTF("parsing header @ %ld", offset);
		lseek(fd, offset, SEEK_SET);
		memset(tmp, 0, sizeof(tmp));
		if (read(fd, tmp, sizeof(tmp)) != sizeof(tmp))
		{
			DADA_RING_PRINTF("canot read: %s", strerror(errno));
			goto errheader;
		}
		parsed = sscanf(tmp, "MQ1,%d,%d,%d,%d,%d,U%d,",
				&imno, &headerlen, &vier, &dim1, &dim2, &bitsize);
		if (parsed    !=   6) { DADA_RING_PRINTF("header error: %d=parsed    !=   6", parsed   ); goto errheader; }
		if (imno      !=   1) { DADA_RING_PRINTF("header error: %d=imno      !=   1", imno     ); goto errheader; }
		if (headerlen != 768) { DADA_RING_PRINTF("header error: %d=headerlen != 768", headerlen); goto errheader; }

		/* jump to desired frame HEADER */
		imglen = dim1*dim2 * sizeof(unsigned char);
		offset = 0x00 + frame*headerlen + frame*imglen;
		if (debug) DADA_RING_PRINTF("parsing header @ %ld", offset);
		lseek(fd, offset, SEEK_SET);
		memset(tmp, 0, sizeof(tmp));
		if (read(fd, tmp, sizeof(tmp)) != sizeof(tmp))
		{
			DADA_RING_PRINTF("canot read: %s", strerror(errno));
			goto errheader;
		}
		parsed = sscanf(tmp, "MQ1,%d,%d,%d,%d,%d,U%d,",
				&imno, &headerlen, &vier, &dim1, &dim2, &bitsize);
		if (parsed    !=     6) { DADA_RING_PRINTF("header error: %d=parsed    !=     6", parsed   ); goto errheader; }
		if (imno-1    != frame) { DADA_RING_PRINTF("header error: %d=imno      != frame", imno-1   ); goto errheader; }
		if (headerlen !=   768) { DADA_RING_PRINTF("header error: %d=headerlen !=   768", headerlen); goto errheader; }
		clock_gettime(CLOCK_REALTIME, &t2);

		dims[0] = dim1;
		dims[1] = dim2;

		if (!data || data->d[0] != dims[0] || data->d[1] != dims[1])
		{
			if (data) {dada_data_destroy(data); free(data); data=NULL;}
			data = dada_data_init(DADA_DATA_INT, 2, dims[1], dims[0]);
		}
		if (data == NULL)
		{
			DADA_RING_PRINTF("dada_data_init(INT, 2, %d,%d) failed", dims[1], dims[0]);
			goto error;
		}
		buffer = (unsigned char*) malloc(imglen);
		if (buffer == NULL)
		{
			DADA_RING_PRINTF("malloc failed: %s", strerror(errno));
			goto error;
		}
		clock_gettime(CLOCK_REALTIME, &t3);

		/* jump to desired frame DATA */
		offset = 0x00 + frame*headerlen + frame*imglen + headerlen;
		if (debug) DADA_RING_PRINTF("reading image DATA @ %ld", offset);
		lseek(fd, offset, SEEK_SET);
		memset(buffer, 0, imglen);
		if (read(fd, buffer, imglen) != imglen)
		{
			FREE(buffer);
			DADA_RING_PRINTF("canot read: %s", strerror(errno));
			goto error;
		}

		for (i=0; i<dims[0]; i++)
			for (j=0; j<dims[1]; j++)
				data->buffer.i[j*dims[0]+i] = buffer[j*dims[0]+i];


		clock_gettime(CLOCK_REALTIME, &t4);

		t   = (t4.tv_sec-t0.tv_sec)+(t4.tv_nsec-t0.tv_nsec)*1e-9;
		tt1 = (t1.tv_sec-t0.tv_sec)+(t1.tv_nsec-t0.tv_nsec)*1e-9;
		tt2 = (t2.tv_sec-t1.tv_sec)+(t2.tv_nsec-t1.tv_nsec)*1e-9;
		tt3 = (t3.tv_sec-t2.tv_sec)+(t3.tv_nsec-t2.tv_nsec)*1e-9;
		tt4 = (t4.tv_sec-t3.tv_sec)+(t4.tv_nsec-t3.tv_nsec)*1e-9;

		if (debug) DADA_RING_PRINTF("Merlin: %s frame %d @0x%08lx %.1fms", filename, frame, offset, t*1e3);
		if (debug) DADA_RING_PRINTF("Merlin-timing: %.1f ms+%.1f ms+%.1f ms+%.1f ms", tt1*1e3, tt2*1e3, tt3*1e3, tt4*1e3);
		FREE(buffer);
	}



cleanup:
	FREE(filename);
	if (fd>0) {close(fd); fd=-1;}

	return data;

error:
	if (data) {dada_data_destroy(data); free(data); data=NULL;}
	goto cleanup;

errheader:
	DADA_RING_PRINTF("cannot parse header for %s:%d", filename, frame);
	goto error;
}

