#ifndef _MERLIN_PXS_OPEN_H_
#define _MERLIN_PXS_OPEN_H_

#include "../../dada.h"

struct dada_data* detector_open(struct dada_data* data, struct dada_data_path* path, struct dada_data_modi* modi);

#endif

