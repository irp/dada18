#ifndef _MERLIN_PXS_PIXELMASK_H_
#define _MERLIN_PXS_PIXELMASK_H_

#include "../../dada.h"

int detector_pixelmask(struct dada_data* data, struct dada_data_modi* modi);

#endif

