#ifndef _PILATUS_PIXELMASK_H_
#define _PILATUS_PIXELMASK_H_

#include "../../dada.h"

int detector_pixelmask(struct dada_data* data);

#endif

