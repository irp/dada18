/* open.c for pilatus-generic
 *
 * open detector data file
 * this is for generic pilatus detectors;
 * look at more specific versions if required.
 */
#include "open.h"

struct dada_data* detector_open(struct dada_data* data, struct dada_data_path* path, __attribute__((unused))struct dada_data_modi* modi)
{
	int fd = -1;
	struct stat stat;
	const char* CBF   = NULL;
	const char* FIRST = NULL;
	const char* LAST  = NULL;
	int headerlen = 0;
	int binsize   = 0;
	int dims[2];

	void* so_handle = NULL;
	char* (*so_filename)(void*) = NULL;
	//void* so_handle_dumpname = NULL;
	//char* (*so_dumpname)(void*) = NULL;
	char* filename = NULL;
	//char* dumpname = NULL;

	DADA_DL_LOAD_SYMBOL(DADA_DL_PATH_DATA(path, "filename"),  "detector_filename",  so_filename,  so_handle);
	filename = so_filename(path);
	DADA_DL_CLOSE(so_handle);
	if (filename == NULL || strlen(filename) == 0)
	{
		DADA_RING_PRINTF("empty filename from %p", path);
		goto error;
	}


	/* try to open and access file */
	// CHECK_FILE_NAME(filename);
	fd = open(filename, O_RDONLY);
	if (fd < 0)
	{
		DADA_RING_PRINTF("cannot open %s: %s", filename, strerror(errno));
		goto error;
	}
	if (fstat(fd, &stat) != 0)
	{
		DADA_RING_PRINTF("cannot access %s: %s", filename, strerror(errno));
		goto error;
	}

	DADA_RING_PRINTF("file %s size %.3f MB", filename, 1.0*stat.st_size/1048576);

	CBF = (const char*) mmap(NULL, stat.st_size, PROT_READ, MAP_PRIVATE, fd, 0);
	if (CBF == MAP_FAILED)
	{
		DADA_RING_PRINTF("cannot mmap %s: %s", filename, strerror(errno));
		goto error;
	}

	if (pilatus_parse_header(CBF, &headerlen, &binsize, dims) != 0)
	{
		DADA_RING_PRINTF("cannot parse header of %s", filename);
		goto error;
	}


	if (!data || data->d[0] != dims[1] || data->d[1] != dims[0])
	{
		if (data) {dada_data_destroy(data); free(data); data=NULL;}
		data = dada_data_init(DADA_DATA_INT, 2, dims[0], dims[1]);
	}
	if (data == NULL)
	{
		DADA_RING_PRINTF("dada_data_init(INT, 2, %d,%d) failed", dims[0], dims[1]);
		goto error;
	}


	/* load detector data into image */
	FIRST = CBF + headerlen;
	LAST  = CBF + stat.st_size - 1;
	if (pilatus_extract_data(FIRST, data->buffer.p, binsize, LAST) != 0)
	{
		DADA_RING_PRINTF("cannot extract data from %s", filename);
		goto error;
	}


	/* FIXME: dump file / meta data removed; re-do it here. */

cleanup:
	if (CBF)
	{
		if (munmap((void*)CBF, stat.st_size) != 0)
			DADA_RING_PRINTF("cannot munmap %s: %s", filename, strerror(errno));
	}
	if (fd) {close(fd); fd=-1;}

	FREE(filename);

	return data;

error:
	if (data) {dada_data_destroy(data); free(data); data=NULL;}

	goto cleanup;

/*
forbidden:
	DADA_RING_PRINTF("forbidden filename: %s", filename);
	goto error;
*/
}

int pilatus_parse_header(const char* CBF, int* headerlen, int* binsize, int* dims)
{
	int ret = 0;

	const char* end_of_header    = "\x0c\x1a\x04\xd5";
	const char* head_binsize     = "X-Binary-Size:";
	const char* head_dimension_1 = "X-Binary-Size-Fastest-Dimension:";
	const char* head_dimension_2 = "X-Binary-Size-Second-Dimension:";

	const char* pos_eoh         = NULL;
	const char* pos_binsize     = NULL;
	const char* pos_dimension_1 = NULL;
	const char* pos_dimension_2 = NULL;

	pos_eoh         = strstr(CBF, end_of_header   );
	pos_binsize     = strstr(CBF, head_binsize    );
	pos_dimension_1 = strstr(CBF, head_dimension_1);
	pos_dimension_2 = strstr(CBF, head_dimension_2);

	if (pos_eoh == NULL)
	{
		DADA_RING_PRINTF("cannot find end of header (%p)", pos_eoh);
		ret = -1;
		goto cleanup;
	}
	if (pos_binsize == NULL)
	{
		DADA_RING_PRINTF("cannot find binary size (%p)", pos_binsize);
		ret = -1;
		goto cleanup;
	}
	if (pos_dimension_1 == NULL || pos_dimension_2 == NULL)
	{
		DADA_RING_PRINTF("cannot find number of pixels (%p,%p)", pos_dimension_1, pos_dimension_2);
		ret = -1;
		goto cleanup;
	}

	*headerlen = pos_eoh + strlen(end_of_header) - CBF;
	*binsize   = atoi(pos_binsize     + strlen(head_binsize));
	dims[1]    = atoi(pos_dimension_1 + strlen(head_dimension_1));
	dims[0]    = atoi(pos_dimension_2 + strlen(head_dimension_2));

cleanup:
	return ret;
}

int pilatus_extract_data(const char* FIRST, int* image, size_t binsize, const char* LAST)
{
	int ret = 0;

	size_t parsed = 0;
	int cur  = 0;
	int diff = 0;

	union {
		const unsigned char*  uint8;
		const unsigned short* uint16;
		const unsigned int*   uint32;
		const          char*   int8;
		const          short*  int16;
		const          int*    int32;
	} parser;
	parser.uint8 = (const unsigned char*) FIRST;

	if (image == NULL)
	{
		ret = -1;
		goto cleanup;
	}

	while (likely(parsed<binsize))
	{
		if (unlikely(parser.int8 > LAST))
		{
			ret = -1;
			goto cleanup;
		}

		if (likely(*parser.uint8 != 0x80))
		{
			diff = (int) *parser.int8;
			parser.int8++;
			parsed += 1;
		}
		else
		{
			parser.uint8++;
			parsed += 1;

			if (likely(*parser.uint16 != 0x8000))
			{
				diff = (int) *parser.int16;
				parser.int16++;
				parsed += 2;
			}
			else
			{
				parser.uint16++;
				parsed += 2;

				diff = (int) *parser.int32;
				parser.int32++;
				parsed += 4;
			}
		}

		cur += diff;

		*image++ = (int)cur;
	}

cleanup:
	return ret;
}

