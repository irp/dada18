/* filename.c for pilatus-generic
 *
 * parse struct dada_detector_data* and build actual filename
 * for the requested file;
 * this is for generic pilatus detectors;
 * look at more specific versions if required.
 */
#include "filename.h"

char* detector_filename(struct dada_data_path* path)
{
	char* buffer = NULL;
	FILE* mem = NULL;
	size_t memlen;
	mem = open_memstream(&buffer, &memlen);

	if (path->len==5 && strcmp(path->path[0], "GINIX")==0 && path->path[4] != NULL)
	{
		fprintf(mem, "config/%s/%s/data/%s/%s/%s_%05d.cbf",
				path->path[0], path->path[1], path->path[2],
				path->path[3], path->path[3], atoi(path->path[4]));
		goto cleanup;
	}

cleanup:
	fclose(mem);
	// fprintf(stdout, "filename: [%s]\n", buffer);
	return buffer;
}

#if 0
char* detector_dumpname(struct dada_detector_data* data)
{
	char* buffer = NULL;
	FILE* mem = NULL;
	size_t memlen;
	mem = open_memstream(&buffer, &memlen);

        //2011
        //2012
        PILATUS_DUMPNAME(data, "run9",  "2012", "PETRAIII", "2012_05_09_run9");
        PILATUS_DUMPNAME(data, "run10", "2012", "PETRAIII", "2012_05_23_run10");
        PILATUS_DUMPNAME(data, "run12", "2012", "PETRAIII", "2012_06_08_run12_rosenhahn");
        PILATUS_DUMPNAME(data, "run13", "2012", "PETRAIII", "2012_06_13_run13");
        PILATUS_DUMPNAME(data, "run14", "2012", "PETRAIII", "2012_09_17_run14");
        PILATUS_DUMPNAME(data, "run15", "2012", "PETRAIII", "2012_09_22_run15");
        PILATUS_DUMPNAME(data, "run17", "2012", "PETRAIII", "2012_10_01_run17");
        PILATUS_DUMPNAME(data, "run19", "2012", "PETRAIII", "2012_10_11_run19");
        PILATUS_DUMPNAME(data, "run20", "2012", "PETRAIII", "2012_10_15_run20_rosenhahn");
        PILATUS_DUMPNAME(data, "run21", "2012", "PETRAIII", "2012_10_17_run21");
        //2013
        PILATUS_DUMPNAME(data, "prerun22", "2013", "PETRAIII", "prerun22_03_07-03_10");
        PILATUS_DUMPNAME(data, "run22", "2013", "PETRAIII", "run22_03_11-03_16");
        PILATUS_DUMPNAME(data, "run23", "2013", "PETRAIII", "run23_03_16-03_25");
        PILATUS_DUMPNAME(data, "run24", "2013", "PETRAIII", "run24_03_25-03_30");
        PILATUS_DUMPNAME(data, "run26", "2013", "PETRAIII", "run26_Gutt");
        PILATUS_DUMPNAME(data, "run27", "2013", "PETRAIII", "run27_04_18-04_24");
        PILATUS_DUMPNAME(data, "run28", "2013", "PETRAIII", "run28_Vartaniants");
        PILATUS_DUMPNAME(data, "run29", "2013", "PETRAIII", "run29_04_29-05_04");
        PILATUS_DUMPNAME(data, "run30", "2013", "PETRAIII", "run30_Gruebel_Schroer");
        PILATUS_DUMPNAME(data, "run31", "2013", "PETRAIII", "run31_06_21-06_26");
        PILATUS_DUMPNAME(data, "run32", "2013", "PETRAIII", "run32_0626_Lagomarsino");
        PILATUS_DUMPNAME(data, "run33", "2013", "PETRAIII", "run33_09_18-09_23");
        PILATUS_DUMPNAME(data, "run35", "2013", "PETRAIII", "run35_09_25-10_02");
        PILATUS_DUMPNAME(data, "run36", "2013", "PETRAIII", "run36_11_13-11_18");
        PILATUS_DUMPNAME(data, "run37", "2013", "PETRAIII", "run37_12_04-12_11");
        PILATUS_DUMPNAME(data, "run38", "2013", "PETRAIII", "run38_12_11-12_18");
        //2014
        PILATUS_DUMPNAME(data, "run39", "2014", "PETRAIII", "run39_01_03-01_08");
        //2015
        PILATUS_DUMPNAME(data, "run40", "2015", "GINIX",    "run40");
        PILATUS_DUMPNAME(data, "run41", "2015", "GINIX",    "run41");
        PILATUS_DUMPNAME(data, "run42", "2015", "GINIX",    "run42");
	PILATUS_DUMPNAME(data, "run43", "2015", "GINIX",    "run43");
        PILATUS_DUMPNAME(data, "run44", "2015", "GINIX",    "run44");
        PILATUS_DUMPNAME(data, "run45", "2015", "GINIX",    "run45");
        PILATUS_DUMPNAME(data, "run46", "2015", "GINIX",    "run46");
        PILATUS_DUMPNAME(data, "run47", "2015", "GINIX",    "run47");
        PILATUS_DUMPNAME(data, "run48", "2015", "GINIX",    "run48");
        PILATUS_DUMPNAME(data, "run49", "2015", "GINIX",    "run49");

	// 2016
        PILATUS_DUMPNAME(data, "run50", "2016", "GINIX",    "run50/raw/20160518");

cleanup:
	fclose(mem);
	return buffer;
}

char* detector_listdir(struct dada_detector_data* data)
{
	char* buffer = NULL;
	FILE* mem = NULL;
	size_t memlen;
	mem = open_memstream(&buffer, &memlen);

        //2011
        //2012
        PILATUS_LISTDIR(data, "run9",  "2012", "PETRAIII", "2012_05_09_run9");
        PILATUS_LISTDIR(data, "run10", "2012", "PETRAIII", "2012_05_23_run10");
        PILATUS_LISTDIR(data, "run12", "2012", "PETRAIII", "2012_06_08_run12_rosenhahn");
        PILATUS_LISTDIR(data, "run13", "2012", "PETRAIII", "2012_06_13_run13");
        PILATUS_LISTDIR(data, "run14", "2012", "PETRAIII", "2012_09_17_run14");
        PILATUS_LISTDIR(data, "run15", "2012", "PETRAIII", "2012_09_22_run15");
        PILATUS_LISTDIR(data, "run17", "2012", "PETRAIII", "2012_10_01_run17");
        PILATUS_LISTDIR(data, "run19", "2012", "PETRAIII", "2012_10_11_run19");
        PILATUS_LISTDIR(data, "run20", "2012", "PETRAIII", "2012_10_15_run20_rosenhahn");
        PILATUS_LISTDIR(data, "run21", "2012", "PETRAIII", "2012_10_17_run21");
        //2013
        PILATUS_LISTDIR(data, "prerun22", "2013", "PETRAIII", "prerun22_03_07-03_10");
        PILATUS_LISTDIR(data, "run22", "2013", "PETRAIII", "run22_03_11-03_16");
        PILATUS_LISTDIR(data, "run23", "2013", "PETRAIII", "run23_03_16-03_25");
        PILATUS_LISTDIR(data, "run24", "2013", "PETRAIII", "run24_03_25-03_30");
        PILATUS_LISTDIR(data, "run26", "2013", "PETRAIII", "run26_Gutt");
        PILATUS_LISTDIR(data, "run27", "2013", "PETRAIII", "run27_04_18-04_24");
        PILATUS_LISTDIR(data, "run28", "2013", "PETRAIII", "run28_Vartaniants");
        PILATUS_LISTDIR(data, "run29", "2013", "PETRAIII", "run29_04_29-05_04");
        PILATUS_LISTDIR(data, "run30", "2013", "PETRAIII", "run30_Gruebel_Schroer");
        PILATUS_LISTDIR(data, "run31", "2013", "PETRAIII", "run31_06_21-06_26");
        PILATUS_LISTDIR(data, "run32", "2013", "PETRAIII", "run32_0626_Lagomarsino");
        PILATUS_LISTDIR(data, "run33", "2013", "PETRAIII", "run33_09_18-09_23");
        PILATUS_LISTDIR(data, "run35", "2013", "PETRAIII", "run35_09_25-10_02");
        PILATUS_LISTDIR(data, "run36", "2013", "PETRAIII", "run36_11_13-11_18");
        PILATUS_LISTDIR(data, "run37", "2013", "PETRAIII", "run37_12_04-12_11");
        PILATUS_LISTDIR(data, "run38", "2013", "PETRAIII", "run38_12_11-12_18");
        //2014
        PILATUS_LISTDIR(data, "run39", "2014", "PETRAIII", "run39_01_03-01_08");
        //2015
        PILATUS_LISTDIR(data, "run40", "2015", "GINIX",    "run40");
        PILATUS_LISTDIR(data, "run41", "2015", "GINIX",    "run41");
        PILATUS_LISTDIR(data, "run42", "2015", "GINIX",    "run42");
	PILATUS_LISTDIR(data, "run43", "2015", "GINIX",    "run43");
        PILATUS_LISTDIR(data, "run44", "2015", "GINIX",    "run44");
        PILATUS_LISTDIR(data, "run45", "2015", "GINIX",    "run45");
        PILATUS_LISTDIR(data, "run46", "2015", "GINIX",    "run46");
        PILATUS_LISTDIR(data, "run47", "2015", "GINIX",    "run47");
        PILATUS_LISTDIR(data, "run48", "2015", "GINIX",    "run48");
        PILATUS_LISTDIR(data, "run49", "2015", "GINIX",    "run49");

	// 2016
        PILATUS_LISTDIR(data, "run50", "2016", "GINIX",    "run50/raw/20160518");

cleanup:
	fclose(mem);
	return buffer;
}

int detector_listsamples(FILE* mem, const char* dirname, struct dada_detector_data* data, int* status)
{
	int num;
	struct dirent** namelist = NULL;
	int notfirst = 0;

	num = scandir(dirname, &namelist, dirfilter_nohidden, versionsort_r);
	if (num<0) goto listingerror;

	fprintf(mem, "{\n");
	fprintf(mem, "    \"status\": \"okay\",\n");
	fprintf(mem, "    \"listing\": [");
	while (num--)
	{
		char dirname2[256];
		struct stat st;
		const char* d_name = namelist[num]->d_name;
		int empty = 0;
		DIR* dir = NULL;
		struct dirent* dent;

		snprintf(dirname2, sizeof(dirname2)-1, "%s/%s", dirname, d_name);

		/* stat the dir, check if it actually /is/ a directory */
		if (stat(dirname2, &st) != 0) goto skip;
		if (!S_ISDIR(st.st_mode))     goto skip;

		/* IF we are listing sample folders,
		 * check if there is anything nice inside.
		 * NOTE: we are listing samples iff detector is given.
		 */
		if (data->detector==NULL || strlen(data->detector)==0)
			goto nosample;
		dir = opendir(dirname2);
		if (dir == NULL) goto skip;
		empty = 1;
		while (empty>0 && (dent=readdir(dir))!=NULL)
		{
			char dirname3[256];
			struct stat st;
			const char* d_name = NULL;

			d_name = dent->d_name;
			snprintf(dirname3, sizeof(dirname3)-1, "%s/%s", dirname2, d_name);

			if (d_name[0] == '.')         continue;
			if (stat(dirname3, &st) != 0) continue;
			if (!S_ISREG(st.st_mode))     continue;

			empty=0;
		}
		if (dir) closedir(dir); dir=NULL;

nosample:
		if (notfirst++) fprintf(mem, ", ");
		if (empty)
			fprintf(mem, "\".%s\"", d_name); /* leading dot */
		else
			fprintf(mem, "\"%s\"", d_name);

skip:
		FREE(namelist[num]);
	}
	fprintf(mem, "]\n");
	fprintf(mem, "}\n");

cleanup:
	FREE(namelist);
	return num;

listingerror:
	switch (errno)
	{
		case ENOENT:  *status = MHD_HTTP_NOT_FOUND;             break;
		case ENOTDIR: *status = MHD_HTTP_NOT_FOUND;             break;
		default:      *status = MHD_HTTP_INTERNAL_SERVER_ERROR; break;
	}
	fprintf(mem, "{\n");
	fprintf(mem, "    status: \"error\",\n");
	fprintf(mem, "    error: \"%s\"\n", strerror(errno));
	fprintf(mem, "}\n");
	goto cleanup;
}
#endif

