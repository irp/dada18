#include "../../dada.h"

extern int dirfilter_nohidden(const struct dirent* dirent);
extern int versionsort_r(const struct dirent** a, const struct dirent** b);

int list(struct dada_data_path* path, FILE* mem)
{
	int ret = 0;
	char* fol = NULL;

	/* assemble folder name */
	{
		FILE* mem = NULL;
		size_t memlen;
		// int n;
		mem = open_memstream(&fol, &memlen);

		fprintf(mem, "config/%s/%s/data/%s", path->path[0], path->path[1], path->path[2]);
		/*
		for (n=0; path && n<path->len; n++)
			if (path->path[n])
				fprintf(mem, "/%s", path->path[n]);
		*/
		fclose(mem);
	}

	if (0) DADA_RING_PRINTF("fol %s", fol);

	/* list subdirs + master files inside fol */
	{
		int num;
		struct dirent** namelist = NULL;
		int notfirst = 0;

		num = scandir(fol, &namelist, dirfilter_nohidden, versionsort_r);
		if (num<0) goto ignore;
		if (0) goto listingerror;

		fprintf(mem, "{\n");
		fprintf(mem, "    \"status\": \"okay\",\n");
		fprintf(mem, "    \"listing\": [");
		while (num--) /* loop over subfolders */
		{
			char dirname2[256];
			struct stat st;
			const char* d_name = namelist[num]->d_name;
			int empty = 0;
			DIR* dir = NULL;
			struct dirent* dent;

			snprintf(dirname2, sizeof(dirname2)-1, "%s/%s", fol, d_name);

			/* stat the dir, check if it actually /is/ a directory */
			if (stat(dirname2, &st) != 0) goto skip;
			if (!S_ISDIR(st.st_mode))     goto skip;

			/* IF we are listing sample folders,
			 * check if there is anything nice inside.
			 * NOTE: we are listing samples iff detector is given.
			 */
			dir = opendir(dirname2);
			if (dir == NULL) goto skip;
			empty = 1;
			while (empty>0 && (dent=readdir(dir))!=NULL)
			{
				char dirname3[256];
				struct stat st;
				const char* d_name = NULL;

				d_name = dent->d_name;
				snprintf(dirname3, sizeof(dirname3)-1, "%s/%s", dirname2, d_name);

				if (d_name[0] == '.')         continue;
				if (stat(dirname3, &st) != 0) continue;
				if (!S_ISREG(st.st_mode))     continue;

				empty=0;
			}
			if (dir) {closedir(dir); dir=NULL;}

			if (notfirst++) fprintf(mem, ", ");
			if (empty)
				fprintf(mem, "\".%s\"", d_name); /* leading dot */
			else
				fprintf(mem, "\"%s\"", d_name);

skip:
			FREE(namelist[num]);
		}
ignore:
		fprintf(mem, "]\n");
		fprintf(mem, "}\n");
	FREE(namelist);
	}


cleanup:
	return ret;

listingerror:
	fprintf(mem, "{\n");
	fprintf(mem, "    \"status\": \"error\",\n");
	fprintf(mem, "    \"error\": \"%s\"\n", strerror(errno));
	fprintf(mem, "}\n");
	ret = errno;
	goto cleanup;
}

time_t stamp(struct dada_data_path* path)
{
	char* fol = NULL;
	time_t t = 0;
	struct stat st;
	struct timespec tt;

	/* assemble folder name */
	{
		FILE* mem = NULL;
		size_t memlen;
		mem = open_memstream(&fol, &memlen);

		fprintf(mem, "config/%s/%s/data/%s", path->path[0], path->path[1], path->path[2]);
		fclose(mem);
	}

	if (strcmp(path->path[1], "run92")==0) return -1;

	/* obtain time stamp */
	if (stat(fol, &st) != 0) goto cleanup;

	tt = st.st_mtim;
	t  = tt.tv_sec;

cleanup:
	return t;
}

