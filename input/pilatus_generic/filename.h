#ifndef _PILATUS_FILENAME_H_
#define _PILATUS_FILENAME_H_

#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <dirent.h>

#include "../../dada.h"

char* detector_filename(struct dada_data_path* path);
//char* detector_listdir(struct dada_data_path* path);
//int detector_listsamples(FILE* mem, const char* dirname, struct dada_detector_data* data, int* status);

//extern int dirfilter_nohidden(const struct dirent* dirent);
//extern int versionsort_r(const struct dirent** a, const struct dirent** b);

#endif

