#ifndef _PILATUS_OPEN_H_
#define _PILATUS_OPEN_H_

#include "../../dada.h"

struct dada_data* detector_open(struct dada_data* data, struct dada_data_path* path, struct dada_data_modi* modi);

int pilatus_parse_header(const char* CBF, int* headerlen, int* binsize, int* dims);
int pilatus_extract_data(const char* data, int* image, size_t binsize, const char* LAST);

#define likely(x)   __builtin_expect((x),1)
#define unlikely(x) __builtin_expect((x),0)

#define _READLINE(f, buffer) \
do { \
    memset(buffer, 0, sizeof(buffer)); \
    if (fgets(buffer, sizeof(buffer), f) == NULL) goto skipdump; \
} while (0)

#endif

