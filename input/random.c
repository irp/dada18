/* dada18: input/random.c
 * create a random 2D dataset with 16x16 values [0...255]
 * */
#include "../dada.h"

int dada_input_random_generate(struct dada_data* data, int seed);

struct dada_data* dada_input_random(int seed, struct dada_data_modi* modi)
{
	struct dada_data* data = NULL;
	int n, N = modi->len;
	int width  = 16;
	int height = 16;

	for (n=0; n<N; n++)
	{
		if (strcmp(modi->key[n], "width" )==0) width  = atoi(modi->val[n]);
		if (strcmp(modi->key[n], "height")==0) height = atoi(modi->val[n]);
	}

	data = dada_data_init(DADA_DATA_INT, 2, height, width);

	if (data == NULL)
	{
		// RINGBUFFER_PRINTF("dada_data_init failed");
		goto error;
	}

	dada_input_random_generate(data, seed);

cleanup:
	return data;
	
error:
	if (data) {dada_data_destroy(data); data=NULL;}
	goto cleanup;
}

int dada_input_random_generate(struct dada_data* data, int seed)
{
	int Y = data->d[0];
	int X = data->d[1];
	int x, y;

	if (seed>=0) srand48(seed); /* do not seed for negative seeds, this gives changing data */

	fprintf(stdout, "random data for seed %d (%dx%d) ...\n", seed, X, Y);

	for (y=0; y<Y; y++)
		for (x=0; x<X; x++)
			data->buffer.i[y*X+x] = lrand48()%256;

	return 0;
}

