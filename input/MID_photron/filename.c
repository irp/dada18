/* filename.c for pirra @ P10
 */
#include "filename.h"

char* detector_filename(struct dada_data_path* path)
{
	char* buffer = NULL;
	FILE* mem = NULL;
	size_t memlen;
	mem = open_memstream(&buffer, &memlen);

	if (path->len==5 && strcmp(path->path[0], "MID")==0 && path->path[4] != NULL)
	{
		int run;
		if (path->path[3][0] == 'r')
			run = atoi(path->path[3]+1);
		else
			run = atoi(path->path[3]  );
		fprintf(mem, "config/%s/%s/data/IRP/%s/run%d/video%06d.tif",
				path->path[0], path->path[1], path->path[2],
				run, atoi(path->path[4]));
		goto cleanup;
	}

cleanup:
	fclose(mem);
	// DADA_RING_PRINTF("filename: %s", buffer);
	// fprintf(stdout, "filename: [%s]\n", buffer);
	return buffer;
}

