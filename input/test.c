/* dada18: input/test.c
 * parse struct dada_data_path* and obtain data from actual *.so
 * */
#include "../dada.h"

struct dada_data* dada_input_test(struct dada_data* data, struct dada_data_path* path, struct dada_data_modi* modi)
{
	const char* dl_what = NULL;

	if (strcmp(path->path[0], "test")==0 && path->len>1)
	{
		if (strcmp(path->path[1], "random")==0 && path->len==3)
		{
			void* so_handle = NULL;
			struct dada_data* (*so_funcp)(int, struct dada_data_modi*) = NULL;
			dl_what = "dada_input_random";
			DADA_DL_LOAD_SYMBOL(DADA_DL_PATH_CONST("input/random"), dl_what, so_funcp, so_handle);
			data = so_funcp(atoi(path->path[2]), modi);
			DADA_DL_CLOSE(so_handle);
			goto cleanup;
		}
	}

cleanup:
	return data;
	
error:
	if (data) {dada_data_destroy(data); data=NULL;}
	goto error;
}

