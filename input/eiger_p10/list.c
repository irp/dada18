#include "../../dada.h"

extern int dirfilter_nohidden(const struct dirent* dirent);
extern int versionsort_r(const struct dirent** a, const struct dirent** b);

int list(struct dada_data_path* path, FILE* mem)
{
	int ret = 0;
	char* fol = NULL;

	/* assemble folder name */
	{
		FILE* mem = NULL;
		size_t memlen;
		// int n;
		mem = open_memstream(&fol, &memlen);

		fprintf(mem, "config/%s/%s/data/%s", path->path[0], path->path[1], path->path[2]);
		/*
		for (n=0; path && n<path->len; n++)
			if (path->path[n])
				fprintf(mem, "/%s", path->path[n]);
		*/
		fclose(mem);
	}

	/* list subdirs + master files inside fol */
	{
		int num;
		struct dirent** namelist  = NULL;
		struct dirent** namelist2 = NULL;
		int notfirst = 0;

		num = scandir(fol, &namelist, dirfilter_nohidden, versionsort_r);
		if (num<0) goto listingerror;

		fprintf(mem, "{\n");
		fprintf(mem, "    \"status\": \"okay\",\n");
		fprintf(mem, "    \"listing\": [");
		while (num--) /* loop over subfolders */
		{
			int num2;
			char dirname2[256];
			struct stat st;
			const char* d_name = namelist[num]->d_name;

			snprintf(dirname2, sizeof(dirname2)-1, "%s/%s", fol, d_name);

			/* stat the dir, check if it actually /is/ a directory */
			if (stat(dirname2, &st) != 0) goto skip;
			if (!S_ISDIR(st.st_mode))     goto skip;

			num2 = scandir(dirname2, &namelist2, dirfilter_nohidden, versionsort_r);
			// DADA_RING_PRINTF("dirname2: %s -> num %d", dirname2, num2);
			if (num2<0) goto ignore; // goto listingerror;
			while (num2--) /* now: look into subfolder, search for actual _master.h5 */
			{
				const char* d_name = namelist2[num2]->d_name;

				/* check if file name matches pattern "$sample_master.h5" */
				if (strstr(d_name, "_master.h5") == NULL) goto skip2;
				/* truncate file name */
				*strstr(d_name, "_master.h5") = '\0';

				/* convert last underscore to slash,
				 * so that number in file name is treated as data->filename
				 */
				if (strrchr(d_name, '_') == NULL) goto skip2;
				*strrchr(d_name, '_') = '/';

				if (notfirst++) fprintf(mem, ", ");
				fprintf(mem, "\"%s\"", d_name);
skip2:
				FREE(namelist2[num2]);
			}
ignore:
			FREE(namelist2);
skip:
			FREE(namelist[num]);
		}
		fprintf(mem, "]\n");
		fprintf(mem, "}\n");
	FREE(namelist);
	}


cleanup:
	FREE(fol);
	return ret;

listingerror:
	fprintf(mem, "{\n");
	fprintf(mem, "    \"status\": \"error\",\n");
	fprintf(mem, "    \"error\": \"%s\"\n", strerror(errno));
	fprintf(mem, "}\n");
	ret = errno;
	goto cleanup;
}

time_t stamp(struct dada_data_path* path)
{
	char* fol = NULL;
	time_t t = 0;
	struct stat st;
	struct timespec tt;

	/* assemble folder name */
	{
		FILE* mem = NULL;
		size_t memlen;
		mem = open_memstream(&fol, &memlen);

		fprintf(mem, "config/%s/%s/data/%s", path->path[0], path->path[1], path->path[2]);
		fclose(mem);
	}

	/* obtain time stamp */
	if (stat(fol, &st) != 0) goto cleanup;

	if (strcmp(path->path[1], "run92")==0) return -1;

	tt = st.st_mtim;
	t  = tt.tv_sec;

cleanup:
	return t;
}

