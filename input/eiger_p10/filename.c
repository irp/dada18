/* filename.c for eiger @ P10
 *
 * parse struct dada_detector_data* and build actual filename
 * for the requested file;
 */
#include "filename.h"

char* detector_filename(struct dada_data_path* path)
{
	char* buffer = NULL;
	FILE* mem = NULL;
	size_t memlen;
	mem = open_memstream(&buffer, &memlen);

	if (path->len==6 && strcmp(path->path[0], "GINIX")==0 && path->path[4] != NULL && path->path[5] != NULL)
	{
		fprintf(mem, "config/%s/%s/data/%s/%s/%s_%05d_master.h5",
				path->path[0], path->path[1], path->path[2],
				path->path[3], path->path[3], atoi(path->path[4]));
		goto cleanup;
	}

cleanup:
	fclose(mem);
	// fprintf(stdout, "filename: [%s:%s]\n", buffer, path->path[path->len-1]);
	return buffer;
}

