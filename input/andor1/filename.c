/* filename.c for pirra @ P10
 */
#include "filename.h"

char* detector_filename(struct dada_data_path* path)
{
	char* buffer = NULL;
	FILE* mem = NULL;
	size_t memlen;
	mem = open_memstream(&buffer, &memlen);

	if (path->len==5 && strcmp(path->path[0], "GINIX")==0 && path->path[4] != NULL)
	{
		fprintf(mem, "config/%s/%s/data/%s/%s/%s_%04d.tif",
				path->path[0], path->path[1], path->path[2],
				path->path[3], path->path[3], atoi(path->path[4]));
		goto cleanup;
	}

cleanup:
	fclose(mem);
	// DADA_RING_PRINTF("filename: %s", buffer);
	// fprintf(stdout, "filename: [%s]\n", buffer);
	return buffer;
}

