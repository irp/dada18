#include "open.h"

struct dada_data* detector_open(struct dada_data* data, struct dada_data_path* path, __attribute__((unused))struct dada_data_modi* modi)
{
	const int timing = 0;

	int fd = -1;
	struct stat stat;
	int dims[2];
	TIFF* tif = NULL;
	uint32 imagelength;
	tsize_t scanline;
	tdata_t buf = NULL;
	int err = 0;
	int idx, idy;

	struct timespec t0, t1, t2, t3, t4;
	double tt1, tt2, tt3, tt4;
	clock_gettime(CLOCK_REALTIME, &t0);

	void* so_handle = NULL;
	char* (*so_filename)(void*) = NULL;
	char* filename = NULL;

	DADA_DL_LOAD_SYMBOL(DADA_DL_PATH_DATA(path, "filename"),  "detector_filename",  so_filename,  so_handle);
	filename = so_filename(path);
	DADA_DL_CLOSE(so_handle);
	if (filename == NULL || strlen(filename) == 0)
	{
		DADA_RING_PRINTF("empty filename from %p", path);
		goto error;
	}


	// CHECK_FILE_NAME(filename);
	fd = open(filename, O_RDONLY);
	if (fd < 0)
	{
		DADA_RING_PRINTF("cannot open %s: %s", filename, strerror(errno));
		goto error;
	}
	if (fstat(fd, &stat) != 0)
	{
		DADA_RING_PRINTF("cannot access %s: %s", filename, strerror(errno));
		goto error;
	}

	// DADA_RING_PRINTF("file %s size %.3f MB", filename, 1.0*stat.st_size/1048576);

	clock_gettime(CLOCK_REALTIME, &t1);
	tif = TIFFOpen(filename, "r");
	if (tif == NULL)
	{
		DADA_RING_PRINTF("TIFFOpen failed for %s", filename);
		goto cleanup;
	}

	TIFFGetField(tif, TIFFTAG_IMAGELENGTH, &imagelength);
	scanline = TIFFScanlineSize(tif);
	buf = _TIFFmalloc(scanline);

	dims[0] = imagelength;
	dims[1] = scanline/2;

	if (!data || data->d[0] != dims[1] || data->d[1] != dims[0])
	{
		if (data) {dada_data_destroy(data); free(data); data=NULL;}
		data = dada_data_init(DADA_DATA_INT, 2, dims[0], dims[1]);
	}
	if (data == NULL)
	{
		DADA_RING_PRINTF("dada_data_init(INT, 2, %d,%d) failed", dims[0], dims[1]);
		goto error;
	}


	clock_gettime(CLOCK_REALTIME, &t2);
	for (idy=0; idy<dims[0]; idy++)
	{
		err = TIFFReadScanline(tif, buf, (uint32)idy, 0);
		if (err < 0)
			goto cleanup;

		for (idx=0; idx<dims[1]; idx++)
		{
			int value = ((unsigned short*)buf)[idx];
			data->buffer.i[idy*dims[1]+idx] = value;
		}
	}
	clock_gettime(CLOCK_REALTIME, &t3);


cleanup:
	if (fd)  {close(fd);       fd=-1;}
	if (buf) { _TIFFfree(buf); buf=NULL; }
	if (tif) { TIFFClose(tif); tif=NULL; }
	FREE(filename);
	clock_gettime(CLOCK_REALTIME, &t4);

	tt1 = (t1.tv_sec-t0.tv_sec)+(t1.tv_nsec-t0.tv_nsec)*1e-9;
	tt2 = (t2.tv_sec-t1.tv_sec)+(t2.tv_nsec-t1.tv_nsec)*1e-9;
	tt3 = (t3.tv_sec-t2.tv_sec)+(t3.tv_nsec-t2.tv_nsec)*1e-9;
	tt4 = (t4.tv_sec-t3.tv_sec)+(t4.tv_nsec-t3.tv_nsec)*1e-9;
	if (timing) DADA_RING_PRINTF("tiff-timing: %.1f ms+%.1f ms+%.1f ms+%.1f ms", tt1*1e3, tt2*1e3, tt3*1e3, tt4*1e3);

	return data;

error:
	if (data) {dada_data_destroy(data); free(data); data=NULL;}

	goto cleanup;
}

