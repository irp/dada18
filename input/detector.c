/* dada18: input/detector.c
 * try to read actual x-ray detector data
 * */
#include "../dada.h"

struct dada_data* dada_input_detector(struct dada_data* data, struct dada_data_path* path, struct dada_data_modi* modi)
{
	void* so_handle = NULL;
	struct dada_data* (*so_open)(struct dada_data*, struct dada_data_path*, struct dada_data_modi*) = NULL;
	int (*so_pixelmask)(struct dada_data*, struct dada_data_modi*) = NULL;

	DADA_DL_LOAD_SYMBOL(DADA_DL_PATH_DATA(path, "open"), "detector_open", so_open, so_handle);
	data = so_open(data, path, modi);
	DADA_DL_CLOSE(so_handle);

	if (data)
	{
		DADA_DL_LOAD_SYMBOL(DADA_DL_PATH_DATA(path, "pixelmask"), "detector_pixelmask", so_pixelmask, so_handle);
		so_pixelmask(data, modi);
		DADA_DL_CLOSE(so_handle);
	}

	return data;
	
error:
	if (data) {dada_data_destroy(data); free(data); data=NULL;}
	goto error;
}

