/* dada18: input/cache.c
 * check if requested data, defined by canonical key, resides in cache
 * cache checked: aerospike
 * */
#include "../dada.h"
#include "../dada_aero.h"

#include <aerospike/aerospike_key.h>
#include <aerospike/as_record.h>
#include <aerospike/as_arraylist.h>

#define FRAGSIZE (124*1024)

as_record* dada_input_cache_read_record(const char* key_space, const char* key_set, const char* key_name);
int dada_input_cache_write_record(const char* key_space, const char* key_set, const char* key_name, as_record* p_rec);

struct dada_data* dada_input_cache_read_data(const char* key_space, const char* key_set, const char* key_name)
{
	struct dada_data* data = NULL;
	as_record* p_rec = NULL;
	size_t len = 0;
	char fragname[256];
	int fragnum = 0;
	size_t processed = 0;
	size_t fragsize;

	if (key_space == NULL || strlen(key_space) == 0) goto cleanup;
	if (key_set   == NULL                          ) goto cleanup;
	if (key_name  == NULL || strlen(key_name)  == 0) goto cleanup;

	do
	{
		snprintf(fragname, sizeof(fragname)-1, "%s:%d", key_name, fragnum++);
		p_rec = dada_input_cache_read_record(key_space, key_set, fragname);
		if (p_rec)
		{
			as_bytes* bytes = NULL;
			if (data == NULL) /* initialise struct dada_data* */
			{
				as_list* dim;
				int D = 0;
				const char* type = as_record_get_str(p_rec, "data.type");
				enum dada_data_type t = DADA_DATA_VOID;
				if (strcmp(type, "int")   ==0) t = DADA_DATA_INT;
				if (strcmp(type, "double")==0) t = DADA_DATA_DOUBLE;

				dim = as_record_get_list(p_rec, "data.dim");
				D = as_list_size(dim);
				switch (D)
				{
					case 0: data = dada_data_init(t, 0); break;
					case 1: data = dada_data_init(t, 1, as_list_get_int64(dim, 0)); break;
					case 2: data = dada_data_init(t, 2, as_list_get_int64(dim, 0), as_list_get_int64(dim, 1)); break;
					case 3: data = dada_data_init(t, 3, as_list_get_int64(dim, 0), as_list_get_int64(dim, 1), as_list_get_int64(dim, 2)); break;
					default: fprintf(stderr, "list with size %d not supported.\n", D); goto cleanup;
				}
			}

			bytes  = as_record_get_bytes(p_rec, "data.pointer");
			len = as_record_get_int64(p_rec, "data.len", 0);

			fragsize = as_bytes_size(bytes);

			if (processed+fragsize > len)
			{
				fprintf(stderr, "ERROR: processed+fragsize = %ld > %ld = len\n", processed+fragsize, len);
				if (data) {dada_data_destroy(data); data=NULL;}
				goto cleanup;
			}
			memcpy(data->buffer.p+processed, bytes->value, fragsize);
			processed += fragsize;

			as_record_destroy(p_rec);
		}
	} while (p_rec);

cleanup:
	if (p_rec) {as_record_destroy(p_rec); p_rec=NULL;}
	return data;
}

time_t dada_input_cache_ask_web(const char* key_space, const char* key_set, const char* key_name)
{
	time_t t = 0;
	as_record* p_rec = NULL;
	char fragname[256];
	int fragnum = 0;

	if (key_space == NULL || strlen(key_space) == 0) goto cleanup;
	if (key_set   == NULL                          ) goto cleanup;
	if (key_name  == NULL || strlen(key_name)  == 0) goto cleanup;

	snprintf(fragname, sizeof(fragname)-1, "%s:%d", key_name, fragnum);
	p_rec = dada_input_cache_read_record(key_space, key_set, fragname);
	if (p_rec)
		t = as_record_get_int64(p_rec, "web.stamp", -1);
	else
		t = -2;

cleanup:
	if (p_rec) {as_record_destroy(p_rec); p_rec=NULL;}
	return t;
}

struct dada_response* dada_input_cache_read_web(const char* key_space, const char* key_set, const char* key_name)
{
	struct dada_response* response = NULL;
	void* cdata = NULL;
	as_record* p_rec = NULL;
	char fragname[256];
	int fragnum = 0;
	size_t processed = 0;
	size_t fragsize;
	as_bytes* bytes = NULL;
	int64_t clen = 0;

	if (key_space == NULL || strlen(key_space) == 0) goto cleanup;
	if (key_set   == NULL                          ) goto cleanup;
	if (key_name  == NULL || strlen(key_name)  == 0) goto cleanup;

	do
	{
		snprintf(fragname, sizeof(fragname)-1, "%s:%d", key_name, fragnum++);
		if (p_rec) {as_record_destroy(p_rec); p_rec=NULL;}
		p_rec = dada_input_cache_read_record(key_space, key_set, fragname);
		// fprintf(stdout, "fragname: %s, p_rec: %p\n", fragname, p_rec);
		if (p_rec)
		{
			if (response == NULL)
			{
				const char* ctype = NULL;
				const char* xmeta   = NULL;
				const char* xradial = NULL;

				ctype = as_record_get_str  (p_rec, "web.ctype");
				clen  = as_record_get_int64(p_rec, "web.clen", 0);
				if (clen <= 0) break; // goto nix;
				cdata = malloc(clen);
				xmeta   = as_record_get_str  (p_rec, "web.xmeta");
				xradial = as_record_get_str  (p_rec, "web.xradial");

				response = (struct dada_response*) malloc(sizeof(struct dada_response));
				response->type    = strdup(ctype);
				response->len     = clen;
				response->data    = cdata;
				response->xmeta   = NULL;
				response->xradial = NULL;
				if (xmeta)   response->xmeta   = strdup(xmeta);
				if (xradial) response->xradial = strdup(xradial);

			}

			bytes = as_record_get_bytes(p_rec, "web.data");

			fragsize = as_bytes_size(bytes);

			if (processed+fragsize > (size_t)clen)
			{
				fprintf(stderr, "ERROR: processed+fragsize = %ld > %ld = clen\n", processed+fragsize, clen);
				goto cleanup;
			}
			memcpy(cdata+processed, bytes->value, fragsize);
			processed += fragsize;
// nix:
		}
		else
		{
			if (p_rec) {as_record_destroy(p_rec); p_rec=NULL;}
			break;
		}
	} while (p_rec);

cleanup:
	if (p_rec) {as_record_destroy(p_rec); p_rec=NULL;}
	return response;
}

int dada_input_cache_write_data(const char* key_space, const char* key_set, const char* key_name, struct dada_data* data)
{
	int ret = 0;
	int d;
	int fragnum = 0;
	size_t processed = 0;
	size_t len = 0;
	as_bytes* bytes = NULL;
	as_record rec;

	if (key_space == NULL || strlen(key_space) == 0) goto cleanup;
	if (key_set   == NULL                          ) goto cleanup;
	if (key_name  == NULL || strlen(key_name)  == 0) goto cleanup;
	if (data      == NULL                          ) goto cleanup;

	if (data->type == DADA_DATA_DATA)
	{
		fprintf(stderr, "dada_input_cache_write_data: data->type DADA_DATA_DATA not supported.\n");
		DADA_RING_PRINTF("dada_input_cache_write_data: data(%p)->type DADA_DATA_DATA not supported.", data);
		ret = -1;
		goto cleanup;
	}

	/* prepare record */
	as_arraylist dim;
	as_arraylist_inita(&dim, data->D);
	len = 1;
	for (d=0; d<data->D; d++)
	{
		as_arraylist_append_int64(&dim, data->d[d]);
		len *= data->d[d];
	}

	switch (data->type)
	{
		case DADA_DATA_VOID:   len *= 0;              break;
		case DADA_DATA_INT:    len *= sizeof(int);    break;
		case DADA_DATA_DOUBLE: len *= sizeof(double); break;
		case DADA_DATA_DATA:   len *= 0;              break; // not supported!
	}
	/* since aerospike imposes a maximum record size (write-block-size minus overhead),
	 * we split data->buffer into smaller fragments of 124kiB each, see FRAGSIZE
	 * (we assume write-block-size of 128kiB and subtract one page to stay within overhead,
	 * but keep "nice" sizes of the fragments)
	 * */
	while (processed < len)
	{
		char fragname[256];
		size_t remaining = len-processed;
		size_t fragsize = remaining;
		if (fragsize > FRAGSIZE) fragsize = FRAGSIZE;

		/* set record's values */
		as_record_inita(&rec, 5);
		as_record_set_str(  &rec, "data.key",     key_name);
		as_record_set_list (&rec, "data.dim",    (as_list*)&dim);
		as_record_set_int64(&rec, "data.len",     len);

		switch (data->type)
		{
			case DADA_DATA_VOID:   as_record_set_str(  &rec, "data.type",    ""      ); break;
			case DADA_DATA_INT:    as_record_set_str(  &rec, "data.type",    "int"   ); break;
			case DADA_DATA_DOUBLE: as_record_set_str(  &rec, "data.type",    "double"); break;
			case DADA_DATA_DATA:   as_record_set_str(  &rec, "data.type",    "data"  ); break; // not supported!
		}
		/* increment the name of this fragment */
		snprintf(fragname, sizeof(fragname)-1, "%s:%d", key_name, fragnum++);

		/* save respective part of the byte array */
		bytes = as_bytes_new_wrap(data->buffer.p+processed, fragsize, false);
		as_record_set_bytes(&rec, "data.pointer", bytes);

		/* write fragment to aerospike */
		dada_input_cache_write_record(key_space, key_set, fragname, &rec);

		/* prepare for the next fragment */
		processed += fragsize;
		if (bytes) {as_bytes_destroy(bytes); bytes = NULL;}
		// if (rec) {as_record_destroy(rec); rec=NULL;}
	}

cleanup:
	if (bytes) {as_bytes_destroy(bytes); bytes = NULL;}
	as_arraylist_destroy(&dim);
	return ret;
}

int dada_input_cache_write_web(const char* key_space, const char* key_set, const char* key_name, struct dada_response* response)
{
	int ret = 0;
	int fragnum = 0;
	size_t processed = 0;
	as_record rec;
	as_bytes* bytes = NULL;
	time_t t = time(NULL);

	if (key_space == NULL || strlen(key_space) == 0) goto cleanup;
	if (key_set   == NULL                          ) goto cleanup;
	if (key_name  == NULL || strlen(key_name)  == 0) goto cleanup;
	if (response  == NULL                          ) goto cleanup;

	while (processed < (size_t)response->len)
	{
		char fragname[256];
		size_t remaining = response->len-processed;
		size_t fragsize = remaining;
		if (fragsize > 16*FRAGSIZE) fragsize = 16*FRAGSIZE;

		as_record_inita(&rec, 7);
		as_record_set_str(  &rec, "web.key",     key_name);
		as_record_set_int64(&rec, "web.clen",    response->len);
		as_record_set_int64(&rec, "web.stamp",   t);
		as_record_set_str(  &rec, "web.ctype",   response->type);
		if (response->xmeta)
			as_record_set_str(  &rec, "web.xmeta",   response->xmeta);
		else
			as_record_set_str(  &rec, "web.xmeta",   "");
		if (response->xradial)
			as_record_set_str(  &rec, "web.xradial",   response->xradial);
		else
			as_record_set_str(  &rec, "web.xradial", "");

		/* increment the name of this fragment */
		snprintf(fragname, sizeof(fragname)-1, "%s:%d", key_name, fragnum++);

		bytes = as_bytes_new_wrap(response->data+processed, fragsize, false);
		as_record_set_bytes(&rec, "web.data",    bytes);

		/* put record into cache */
		dada_input_cache_write_record(key_space, key_set, fragname, &rec);
		as_bytes_destroy(bytes);

		processed += fragsize;
	}

cleanup:
	return ret;
}

as_record* dada_input_cache_read_record(const char* key_space, const char* key_set, const char* key_name)
{
	as_key _key;
	as_record* p_rec = NULL;

	if (key_space == NULL || strlen(key_space) == 0) goto cleanup;
	if (key_set   == NULL                          ) goto cleanup;
	if (key_name  == NULL || strlen(key_name)  == 0) goto cleanup;

	// DADA_RING_PRINTF("reading cache for [%s:%s:%s] ... ", key_space, key_set, key_name);
	// fprintf(stdout, "\treading cache for [%s:%s:%s] ... ", key_space, key_set, key_name);
	// fflush(stdout);

	as_key_init_str(&_key, key_space, key_set, key_name);
	/*
	if (aerospike_key_exists(&aero.aer, &aero.err, NULL, &_key, &p_rec) == AEROSPIKE_ERR_RECORD_NOT_FOUND)
		goto cleanup;
	*/

	/* retrieve record from cache */
	aerospike_key_get(&aero.aer, &aero.err, NULL, &_key, &p_rec);

cleanup:
	if (0) fprintf(stdout, "%s\n", p_rec?"YES":"NO");
	return p_rec;
}

int dada_input_cache_write_record(const char* key_space, const char* key_set, const char* key_name, as_record* p_rec)
{
	int ret = 0;
	as_key _key;

	if (key_space == NULL || strlen(key_space) == 0) goto cleanup;
	if (key_set   == NULL                          ) goto cleanup;
	if (key_name  == NULL || strlen(key_name)  == 0) goto cleanup;
	if (p_rec     == NULL                          ) goto cleanup;

	fprintf(stdout, "\twriting cache for [%s:%s:%s] ... ", key_space, key_set, key_name);
	fflush(stdout);

	as_key_init_str(&_key, key_space, key_set, key_name);

	if (aerospike_key_put(&aero.aer, &aero.err, NULL, &_key, p_rec) != AEROSPIKE_OK)
	{
		fprintf(stderr, "error: %s:%d aerospike_key_put failed (%d:%s)\n", __FILE__, __LINE__, aero.err.code, aero.err.message);
		ret = -1;
		goto cleanup;
	}

cleanup:
	if (0)
	{
		if (ret == 0)
			fprintf(stdout, "YES\n");
		else
			fprintf(stdout, "no\n");
	}
	else
		fprintf(stdout, "\n");
	return ret;
}

