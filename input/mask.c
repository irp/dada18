#include "../dada.h"

#include <hdf5.h>
#include <hdf5_hl.h>

extern pthread_mutex_t hdf5_mutex;

struct dada_data* dada_mask_load(struct dada_data_path* path, struct dada_data_modi* modi, struct dada_data* data, __attribute__((unused))enum dada_cache_flags flags)
{
	const int timing = 0;

	const char* dset = "/mask/data";

	char* filename = NULL;
	FILE* mem = NULL;
	size_t memlen;
	int n;
	int locked = 0;
	int fd = -1;
	int dims[2] = {0, 0};
	struct stat stat;
	struct dada_process_roi roi;
	int binx, biny;
	void* so_handle = NULL;

	struct timespec t0, t1, t2, t3, t4;
	double tt1, tt2, tt3, tt4;
	double t;
	clock_gettime(CLOCK_REALTIME, &t0);

	if (path==NULL || path->len == 0 || path->path == NULL)
	{
		DADA_RING_PRINTF("empty path: %p", path);
		goto cleanup;
	}

	/* parse modifier options */
	dada_process_parse_roi(modi, &roi);
	dada_process_parse_bin(modi, &binx, &biny);

	DADA_MEMSTREAM(mem, filename, memlen);
	fprintf(mem, "masks");
	for (n=0; path && n<path->len; n++)
		if (path->path[n] && strlen(path->path[n]))
			fprintf(mem, "/%s", path->path[n]);

	fclose(mem);
	// DADA_RING_PRINTF("filename: [%s]", filename);


	pthread_mutex_lock(&hdf5_mutex); locked = 1;
	fd = open(filename, O_RDONLY);
	if (fd < 0)
	{
		DADA_RING_PRINTF("cannot open %s: %s", filename, strerror(errno));
		goto error;
	}
	if (fstat(fd, &stat) != 0)
	{
		DADA_RING_PRINTF("cannot access %s: %s", filename, strerror(errno));
		goto error;
	}

	DADA_RING_PRINTF("file %s size %.3f MB", filename, 1.0*stat.st_size/1048576);

	{
		int i, j;
		hid_t id_file, id_dset;
		hid_t id_data, id_memy;
		hid_t id_type;
		__attribute__((unused)) int rank;
		hsize_t dims_out[2];
		herr_t status = 0;
		float* buffer = NULL;

		clock_gettime(CLOCK_REALTIME, &t1);

		H5Eset_auto2(H5E_DEFAULT, NULL, NULL); /* turn off error messages */
		id_file = H5Fopen(filename, H5F_ACC_RDONLY, H5P_DEFAULT);
		if (id_file < 0) goto error;

		id_dset = H5Dopen2(id_file, dset, H5P_DEFAULT);
		if (id_dset < 0) goto error;

		/* obtain image dimensions; select only first image */
		id_data = H5Dget_space(id_dset);
		rank    = H5Sget_simple_extent_ndims(id_data);
		          H5Sget_simple_extent_dims(id_data, dims_out, NULL);

	        /*
		fprintf(stdout, "%d:%d×%d×%d frame:%d\n",
				(int)rank, (int)dims_out[0], (int)dims_out[1], (int)dims_out[2],
				frame);
		*/
		// DADA_RING_PRINTF("%d:%d×%d", (int)rank, (int)dims_out[0], (int)dims_out[1]);

		dada_process_parse_roi(modi, &roi);

		if (roi.left>=0 && roi.top>=0 && roi.width>=0 && roi.height>=0) /* check if ROI is smaller than actual IMAGE */
		{
			if (roi.top <0 || roi.top  >= (int)dims_out[0]) goto bad_roi;
			if (roi.left<0 || roi.left >= (int)dims_out[1]) goto bad_roi;
			if (roi.top +roi.height    >= (int)dims_out[0]) goto bad_roi;
			if (roi.left+roi.width     >= (int)dims_out[1]) goto bad_roi;
			if (roi.flags && DADA_PROCESS_ROI_MASK)         goto bad_roi; /* roi region is set to -1 by input/retrieve.c */
			goto do_roi;
bad_roi:
			roi.left = roi.top = roi.width = roi.height = -1;
			roi.flags  = DADA_PROCESS_ROI_NORMAL;
		}

do_roi:
		/* if no roi was supplied, or if it was too large: create full FOV roi */
		if (roi.left<0 || roi.top<0 || roi.width<0 || roi.height<0) /* check if ROI is smaller than actual IMAGE */
		{
			roi.left   = 0;
			roi.top    = 0;
			roi.width  = dims_out[1];
			roi.height = dims_out[0];
			roi.flags  = DADA_PROCESS_ROI_NORMAL;
		}

		hsize_t hyper_siz[2]; /* size of the hyperslab in the file */
		hsize_t hyper_off[2]; /* hyperslab offset in the file */
		hyper_siz[0] = roi.height;
		hyper_siz[1] = roi.width;
		hyper_off[0] = roi.top;
		hyper_off[1] = roi.left;

		// fprintf(stdout, "roi: %d,%d,%d,%d\n", roi.left, roi.top, roi.width, roi.height);

		dims[0] = dims_out[1] = roi.width;
		dims[1] = dims_out[0] = roi.height;

		/* select hyperslab, define dataspace */
		H5Sselect_hyperslab(id_data, H5S_SELECT_SET, hyper_off, NULL, hyper_siz, NULL);
		id_memy = H5Screate_simple(2, hyper_siz, NULL);
		hyper_off[0] = 0;
		hyper_off[1] = 0;
		H5Sselect_hyperslab(id_memy, H5S_SELECT_SET, hyper_off, NULL, hyper_siz, NULL);


		// DADA_RING_PRINTF("malloc(%ld×%d×%d = %ld)", sizeof(float), roi.height, roi.width, sizeof(float)*roi.height*roi.width);
		buffer = (float*) malloc(sizeof(float)*roi.height*roi.width);
		IS_NULL_ERRMSG(buffer, error);

		id_type = H5Dget_type(id_dset);
		status = H5Dread(id_dset, id_type, id_memy, id_data, H5P_DEFAULT, buffer);
		if (status != 0)
		{
			fprintf(stderr, "H5 error (%d)\n", __LINE__);
			H5Eprint(H5E_DEFAULT, stderr);
			fprintf(stderr, "\tfilename: %s\n", filename);
			fprintf(stderr, "\thyper_siz[0]: %d\n", (int)hyper_siz[0]);
			fprintf(stderr, "\thyper_siz[1]: %d\n", (int)hyper_siz[1]);
			fprintf(stderr, "\thyper_off[0]: %d\n", (int)hyper_off[0]);
			fprintf(stderr, "\thyper_off[1]: %d\n", (int)hyper_off[1]);
			fprintf(stderr, "\n");
			goto error;
	 	}

		status = H5Dclose(id_dset);
		if (status != 0)
		{
			fprintf(stderr, "H5 error (%d)\n", __LINE__);
			H5Eprint(H5E_DEFAULT, stderr);
	 	}

		status = H5Fclose(id_file);
		if (status != 0)
		{
			fprintf(stderr, "H5 error (%d)\n", __LINE__);
			H5Eprint(H5E_DEFAULT, stderr);
	 	}

		if (locked>0) {pthread_mutex_unlock(&hdf5_mutex); locked=0;}

		clock_gettime(CLOCK_REALTIME, &t2);
		t = (t2.tv_sec-t1.tv_sec)+(t2.tv_nsec-t1.tv_nsec)*1e-9;

		if (!data || data->d[0] != dims[0] || data->d[1] != dims[1])
		{
			if (data) {dada_data_destroy(data); free(data); data=NULL;}
			data = dada_data_init(DADA_DATA_DOUBLE, 2, dims[1], dims[0]);
		}
		if (data == NULL)
		{
			DADA_RING_PRINTF("dada_data_init(INT, 2, %d,%d) failed", dims[1], dims[0]);
			goto error;
		}

		clock_gettime(CLOCK_REALTIME, &t3);
		t = (t3.tv_sec-t2.tv_sec)+(t3.tv_nsec-t2.tv_nsec)*1e-9;

		for (j=0; j<dims[1]; j++)
			for (i=0; i<dims[0]; i++)
				data->buffer.f[j*dims[0]+i] = buffer[j*dims[0]+i];

		clock_gettime(CLOCK_REALTIME, &t4);

		t   = (t4.tv_sec-t0.tv_sec)+(t4.tv_nsec-t0.tv_nsec)*1e-9;
		tt1 = (t1.tv_sec-t0.tv_sec)+(t1.tv_nsec-t0.tv_nsec)*1e-9;
		tt2 = (t2.tv_sec-t1.tv_sec)+(t2.tv_nsec-t1.tv_nsec)*1e-9;
		tt3 = (t3.tv_sec-t2.tv_sec)+(t3.tv_nsec-t2.tv_nsec)*1e-9;
		tt4 = (t4.tv_sec-t3.tv_sec)+(t4.tv_nsec-t3.tv_nsec)*1e-9;

		if (timing) DADA_RING_PRINTF("mask: %s %.1fms", filename, t*1e3);
		if (timing) DADA_RING_PRINTF("hdf5-timing: %.1f ms+%.1f ms+%.1f ms+%.1f ms", tt1*1e3, tt2*1e3, tt3*1e3, tt4*1e3);

		FREE(buffer);
	}


	goto process;

process:

	if (data == NULL) goto error;
	/* ROI */
	if (roi.width>0 && roi.height>0)
		if (roi.left >= 0 && roi.left+roi.width <= data->d[1])
			if (roi.top >= 0 && roi.top+roi.height <= data->d[0])
				dada_process_roi(data, &roi);
	/* BINNING */
	if (binx>1 || biny>1)
		dada_process_bin(data, binx, biny);


cleanup:
	if (locked>0) {pthread_mutex_unlock(&hdf5_mutex); locked=0;}
	if (fd>0) {close(fd); fd=-1;}
	DADA_DL_CLOSE(so_handle);
	path->path[path->len-1] = NULL; /* already free'd, and pointed to local char[] */
	if (data == NULL) DADA_RING_PRINTF("empty data [%s]", filename);
	return data;

error:
	DADA_RING_PRINTF("dada_mask_load failed for %s", filename);
	if (data) {dada_data_destroy(data); free(data); data=NULL;}
	goto cleanup;

err_memstream:
	DADA_RING_PRINTF("memstream failed: %s", strerror(errno));
	if (data) {dada_data_destroy(data); free(data); data=NULL;}
	goto cleanup;
}

