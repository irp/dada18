/* open.c for microphone @ MID -- using array index (counting from 0), not trainId
 *
 * open detector data file
 */
#include "open.h"

/* TODO: think about 1D roi and binning; in general: N-dim processing */

long int _getframefromtrain_MID_fastdac(hid_t id_file, const char* dset, long int trainid);

struct dada_data* detector_open(struct dada_data* data, struct dada_data_path* path, struct dada_data_modi* modi)
{
	int locked = 0;
	pthread_mutex_lock(&hdf5_mutex); locked = 1;
	const char* dset = NULL;

	const int timing = 0;

	struct timespec t0, t1, t2, t3, t4;
	double tt1, tt2, tt3, tt4;
	double t;
	clock_gettime(CLOCK_REALTIME, &t0);
	struct dada_process_roi roi;
	roi.left = roi.top = roi.width = roi.height = -1;
	roi.flags  = DADA_PROCESS_ROI_NORMAL;

	int fd = -1;
	struct stat stat;
	int dims[2] = {0, 0};

	void* so_handle = NULL;
	char* (*so_filename)(void*) = NULL;
	char* filename = NULL;

	int i;
	hid_t id_file = -1;
	hid_t id_dset;
	hid_t id_data, id_memy;
	hid_t id_type;
	__attribute__((unused)) int rank;
	hsize_t dims_out[2];
	herr_t status = 0;
	signed long* buffer = NULL;
	int n = 1;

	DADA_DL_LOAD_SYMBOL(DADA_DL_PATH_DATA(path, "filename"),  "detector_filename",  so_filename,  so_handle);
	filename = so_filename(path);
	DADA_DL_CLOSE(so_handle);
	if (filename == NULL || strlen(filename) == 0)
	{
		DADA_RING_PRINTF("empty filename from %p", path);
		goto error;
	}

	/* try to open and access file */
	// CHECK_FILE_NAME(filename);
	fd = open(filename, O_RDONLY);
	if (fd < 0)
	{
		DADA_RING_PRINTF("cannot open %s: %s", filename, strerror(errno));
		goto error;
	}
	if (fstat(fd, &stat) != 0)
	{
		DADA_RING_PRINTF("cannot access %s: %s", filename, strerror(errno));
		goto error;
	}

	/*
	DADA_RING_PRINTF("file %s size %.3f MB trainid %ld frame %ld", filename, 1.0*stat.st_size/1048576, trainid, frame);
	fprintf(stderr, "file %s size %.3f MB trainid %ld frame %ld\n", filename, 1.0*stat.st_size/1048576, trainid, frame);
	*/

	clock_gettime(CLOCK_REALTIME, &t1);
	t = (t1.tv_sec-t0.tv_sec)+(t1.tv_nsec-t0.tv_nsec)*1e-9;

	H5Eset_auto2(H5E_DEFAULT, NULL, NULL); /* turn off error messages */
	id_file = H5Fopen(filename, H5F_ACC_RDONLY, H5P_DEFAULT);
	if (id_file < 0)
		goto nofile;

	long int trainid = atol(path->path[path->len-1]);
	long int frame   = -1;

	/* get frame (array index) from train id */
	if (strcmp(path->path[1], "p2207") == 0)
	{
		if (strcmp(path->path[2], "gate_tid"    ) == 0) dset = "/INSTRUMENT/MID_EXP_FASTADC/ADC/DESTEST:channel_1.output/data/trainId";
		if (strcmp(path->path[2], "mic_tid"     ) == 0) dset = "/INSTRUMENT/MID_EXP_FASTADC/ADC/DESTEST:channel_3.output/data/trainId";
	}
	if (strcmp(path->path[1], "p2544") == 0)
	{
		if (strcmp(path->path[2], "gate_tid"    ) == 0) dset = "/INSTRUMENT/MID_EXP_FASTADC/ADC/DESTEST:channel_8.output/data/trainId";
		if (strcmp(path->path[2], "mic_tid"     ) == 0) dset = "/INSTRUMENT/MID_EXP_FASTADC/ADC/DESTEST:channel_9.output/data/trainId";
		if (strcmp(path->path[2], "pattern_tid" ) == 0) dset = "/INSTRUMENT/MID_EXP_FASTADC/ADC/DESTEST:channel_9.output/data/trainId";
	}
	if (!dset) goto nodset;
	frame   = _getframefromtrain_MID_fastdac(id_file, dset, trainid);


	/* get data */
	if (strcmp(path->path[1], "p2207") == 0)
	{
		if (strcmp(path->path[2], "gate_tid"    ) == 0) dset = "/INSTRUMENT/MID_EXP_FASTADC/ADC/DESTEST:channel_1.output/data/rawData";
		if (strcmp(path->path[2], "mic_tid"     ) == 0) dset = "/INSTRUMENT/MID_EXP_FASTADC/ADC/DESTEST:channel_3.output/data/rawData";
	}
	if (strcmp(path->path[1], "p2544") == 0)
	{
		if (strcmp(path->path[2], "gate_tid"    ) == 0) dset = "/INSTRUMENT/MID_EXP_FASTADC/ADC/DESTEST:channel_8.output/data/rawData";
		if (strcmp(path->path[2], "mic_tid"     ) == 0) dset = "/INSTRUMENT/MID_EXP_FASTADC/ADC/DESTEST:channel_9.output/data/rawData";
		if (strcmp(path->path[2], "pattern_tid" ) == 0) dset = "/INSTRUMENT/MID_EXP_FASTADC/ADC/DESTEST:channel_9.output/data/rawData";
	}
	if (!dset) goto nodset;
	id_dset = H5Dopen2(id_file, dset, H5P_DEFAULT);
	if (id_dset<0) goto nodset;

	/* obtain image dimensions; select only first image */
	id_data = H5Dget_space(id_dset);
	rank    = H5Sget_simple_extent_ndims(id_data);
                  H5Sget_simple_extent_dims(id_data, dims_out, NULL);

	// DADA_RING_PRINTF("%d:%d×%d frame:%ld", (int)rank, (int)dims_out[0], (int)dims_out[1], frame);
	// fprintf(stdout, "%d:%d×%d frame:%d\n", (int)rank, (int)dims_out[0], (int)dims_out[1], frame);

	dada_process_parse_roi(modi, &roi);

	if (roi.left>=0 && roi.top>=0 && roi.width>=0 && roi.height>=0) /* check if ROI is smaller than actual IMAGE */
	{
		if (roi.left<0 || roi.left >= (int)dims_out[1]) goto bad_roi;
		if (roi.left+roi.width     >= (int)dims_out[1]) goto bad_roi;
		if (roi.flags && DADA_PROCESS_ROI_MASK)         goto bad_roi; /* roi region is set to -1 by input/retrieve.c */
		goto do_roi;
bad_roi:
		roi.left = roi.top = roi.width = roi.height = -1;
		roi.flags  = DADA_PROCESS_ROI_NORMAL;
	}

do_roi:
	/* if no roi was supplied, or if it was too large: create full FOV roi */
	if (roi.left<0 || roi.top<0 || roi.width<0 || roi.height<0) /* check if ROI is smaller than actual IMAGE */
	{
		roi.left   = 0;
		roi.width  = dims_out[1];
		roi.flags  = DADA_PROCESS_ROI_NORMAL;
	}

	if (frame < 0) goto noframe;

	hsize_t hyper_siz[2]; /* size of the hyperslab in the file */
	hsize_t hyper_off[2]; /* hyperslab offset in the file */
	hyper_siz[0] = 1;
	hyper_siz[1] = roi.width;
	hyper_off[0] = frame;
	hyper_off[1] = roi.left;

	// fprintf(stdout, "roi: %d,%d,%d,%d\n", roi.left, roi.top, roi.width, roi.height);
	// DADA_RING_PRINTF("roi: %d,%d,%d,%d", roi.left, roi.top, roi.width, roi.height);

	dims[0] = dims_out[0] = roi.width;

	/* select hyperslab, define dataspace */
	H5Sselect_hyperslab(id_data, H5S_SELECT_SET, hyper_off, NULL, hyper_siz, NULL);
	id_memy = H5Screate_simple(2, hyper_siz, NULL);
	hyper_off[0] = 0;
	hyper_off[1] = 0;
	H5Sselect_hyperslab(id_memy, H5S_SELECT_SET, hyper_off, NULL, hyper_siz, NULL);



	id_type = H5Dget_type(id_dset);
	if (H5Tequal(id_type, H5T_STD_U64LE)) buffer = (signed long*) malloc(sizeof(signed long )*roi.width);
	if (H5Tequal(id_type, H5T_STD_U32LE)) buffer = (signed long*) malloc(sizeof(signed int  )*roi.width);
	if (H5Tequal(id_type, H5T_STD_U16LE)) buffer = (signed long*) malloc(sizeof(signed short)*roi.width);
	IS_NULL_ERRMSG(buffer, error);

	status = H5Dread(id_dset, id_type, id_memy, id_data, H5P_DEFAULT, buffer);
	if (status != 0)
	{
		fprintf(stderr, "H5 error (%d)\n", __LINE__);
		H5Eprint(H5E_DEFAULT, stderr);
		fprintf(stderr, "\tfilename: %s\n", filename);
		fprintf(stderr, "\ttrainid: %ld\n", trainid);
		fprintf(stderr, "\tframenumber: %ld\n", frame);
		fprintf(stderr, "\thyper_siz[0]: %d\n", (int)hyper_siz[0]);
		fprintf(stderr, "\thyper_siz[1]: %d\n", (int)hyper_siz[1]);
		fprintf(stderr, "\thyper_off[0]: %d\n", (int)hyper_off[0]);
		fprintf(stderr, "\thyper_off[1]: %d\n", (int)hyper_off[1]);
		fprintf(stderr, "\n");
		goto error;
	}

	status = H5Dclose(id_dset);
	if (status != 0)
	{
		fprintf(stderr, "H5 error (%d)\n", __LINE__);
		H5Eprint(H5E_DEFAULT, stderr);
	}

	status = H5Fclose(id_file);
	if (status != 0)
	{
		fprintf(stderr, "H5 error (%d)\n", __LINE__);
		H5Eprint(H5E_DEFAULT, stderr);
	}

	clock_gettime(CLOCK_REALTIME, &t2);
	t = (t2.tv_sec-t1.tv_sec)+(t2.tv_nsec-t1.tv_nsec)*1e-9;

	/* TODO:
	 * add size_t allocated to struct dada_data,
	 * and check for this;
	 * if it is large enough, set dimensions / size in else
	 * */
	if (!data || data->d[0] != dims[0] || data->d[1] != dims[1])
	{
		if (data) {dada_data_destroy(data); free(data); data=NULL;}
		// data = dada_data_init(DADA_DATA_INT, 2, dims[1], dims[0]);
		// data = dada_data_init(DADA_DATA_DOUBLE, 2, dims[1], dims[0]);
		data = dada_data_init(DADA_DATA_DOUBLE, 1, dims[0]);
		// DADA_RING_PRINTF("dada_data_init(DOUBLE, 1, %d).", dims[0]);
	}
	if (data == NULL)
	{
		// DADA_RING_PRINTF("dada_data_init(INT, 2, %d,%d) failed", dims[1], dims[0]);
		// DADA_RING_PRINTF("dada_data_init(DOUBLE, 2, %d,%d) failed", dims[1], dims[0]);
		DADA_RING_PRINTF("dada_data_init(DOUBLE, 1, %d) failed", dims[0]);
		goto error;
	}

	clock_gettime(CLOCK_REALTIME, &t3);
	t = (t3.tv_sec-t2.tv_sec)+(t3.tv_nsec-t2.tv_nsec)*1e-9;

	// if (H5Tequal(id_type, H5T_STD_U64LE)) DADA_RING_PRINTF("type: %s", "U64LE");
	// if (H5Tequal(id_type, H5T_STD_U32LE)) DADA_RING_PRINTF("type: %s", "U32LE");
	// if (H5Tequal(id_type, H5T_STD_U16LE)) DADA_RING_PRINTF("type: %s", "U16LE");

	switch (data->type)
	{
		case DADA_DATA_INT:
			if (H5Tequal(id_type, H5T_STD_U64LE))
				for (i=0; i<dims[0]; i++)
					data->buffer.i[i] = ((unsigned long* ) buffer)[i];
			if (H5Tequal(id_type, H5T_STD_U32LE))
				for (i=0; i<dims[0]; i++)
					data->buffer.i[i] = ((unsigned int*  ) buffer)[i];
			if (H5Tequal(id_type, H5T_STD_U16LE))
				for (i=0; i<dims[0]; i++)
					data->buffer.i[i] = ((unsigned short*) buffer)[i];
			break;
		case DADA_DATA_DOUBLE:
			if (H5Tequal(id_type, H5T_STD_U64LE))
				for (i=0; i<dims[0]; i++)
					data->buffer.f[i] = ((unsigned long* ) buffer)[i];
			if (H5Tequal(id_type, H5T_STD_U32LE))
				for (i=0; i<dims[0]; i++)
					data->buffer.f[i] = ((unsigned int*  ) buffer)[i];
			if (H5Tequal(id_type, H5T_STD_U16LE))
				for (i=0; i<dims[0]; i++)
					data->buffer.f[i] = ((unsigned short*) buffer)[i];
			break;
		default: fprintf(stderr, "(unsupported format %s:%d)\n", __FILE__, __LINE__); break;
	}

	clock_gettime(CLOCK_REALTIME, &t4);

	t   = (t4.tv_sec-t0.tv_sec)+(t4.tv_nsec-t0.tv_nsec)*1e-9;
	tt1 = (t1.tv_sec-t0.tv_sec)+(t1.tv_nsec-t0.tv_nsec)*1e-9;
	tt2 = (t2.tv_sec-t1.tv_sec)+(t2.tv_nsec-t1.tv_nsec)*1e-9;
	tt3 = (t3.tv_sec-t2.tv_sec)+(t3.tv_nsec-t2.tv_nsec)*1e-9;
	tt4 = (t4.tv_sec-t3.tv_sec)+(t4.tv_nsec-t3.tv_nsec)*1e-9;

	if (0) DADA_RING_PRINTF("MID_fastdac: %s frame %d:%ld %.1fms", filename, n, frame, t*1e3);
	if (timing) DADA_RING_PRINTF("hdf5-timing: %.1f ms+%.1f ms+%.1f ms+%.1f ms", tt1*1e3, tt2*1e3, tt3*1e3, tt4*1e3);

	FREE(buffer);



cleanup:
	if (timing) fprintf(stdout, "\n");
	if (id_file > 0) {H5Fclose(id_file); id_file=-1;}
	// DL_CLOSE(so_handle_filename);
	FREE(filename);
	if (fd>0) {close(fd); fd=-1;}

	if (locked>0) {pthread_mutex_unlock(&hdf5_mutex); locked=0;}
	return data;

error:
	if (data) {dada_data_destroy(data); free(data); data=NULL;}
	goto cleanup;
// forbidden:
	// snprintf(err, sizeof(err)-1, "forbidden filename: [%s]", filename);
	// goto error;

noframe:
	// DADA_RING_PRINTF("no frame %ld", frame);
	goto cleanup;
nofile:
	H5Eprint(H5E_DEFAULT, stderr);
	goto error;
nodset:
	DADA_RING_PRINTF("cannot find frame %ld in set of inspected datasets", frame);
	goto error;
}

long int _getframefromtrain_MID_fastdac(hid_t id_file, const char* dset, long int trainid)
{
	long int frame = -1;
	int n, N;

	hid_t id_dset, id_data;
	hid_t id_type;
	__attribute__((unused)) int rank;
	hsize_t dims_out[3];
	herr_t status = 0;
	signed long* buffer = NULL;

	H5Eset_auto2(H5E_DEFAULT, NULL, NULL); /* turn off error messages */
	if (id_file < 0) goto nofile;

	id_dset = H5Dopen2(id_file, dset, H5P_DEFAULT);
	if (id_dset<0) goto cleanup;

	/* obtain image dimensions; select only first image */
	id_data = H5Dget_space(id_dset);
	rank    = H5Sget_simple_extent_ndims(id_data);
		  H5Sget_simple_extent_dims(id_data, dims_out, NULL);
	// DADA_RING_PRINTF("%d:%d", (int)rank, (int)dims_out[0]);

	buffer = malloc(sizeof(unsigned long)*dims_out[0]);
	IS_NULL_ERRMSG(buffer, error);

	id_type = H5Dget_type(id_dset);
	status = H5Dread(id_dset, id_type, H5S_ALL, id_data, H5P_DEFAULT, buffer);

	status = H5Dclose(id_dset);
	if (status != 0)
	{
		fprintf(stderr, "H5 error (%d)\n", __LINE__);
		H5Eprint(H5E_DEFAULT, stderr);
	}

	N=dims_out[0];
	for (n=0; n<N; n++)
	{
		if (buffer[n] == trainid)
		{
			frame = n;
			break;
		}
	}


cleanup:
	FREE(buffer);
	return frame;

	goto listingerror;

listingerror:
	frame = -1;
	goto cleanup;
error:
	frame = -1;
	goto cleanup;
nofile:
	H5Eprint(H5E_DEFAULT, stderr);
	frame = -1;
	goto error;
}

