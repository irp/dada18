#ifndef _PIRRA_FILENAME_H_
#define _PIRRA_FILENAME_H_

#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <dirent.h>

#include "../../dada.h"

char* detector_filename(struct dada_data_path* path);

#endif

