/* dada18: input/retrieve.c
 * delegate fetching data to whatever function is competent
 * */
#include "../dada.h"

char* dada_input_cache_canonicalise(struct dada_data_path* path, struct dada_data_modi* modi);

struct dada_data* dada_input_retrieve(struct dada_data_path* path, struct dada_data_modi* modi, struct dada_data* data, enum dada_cache_flags flags)
{
	const char* dl_what = NULL;
	char* canonical = NULL;
	struct dada_process_stack stack;
	struct dada_process_empty empty;
	struct dada_process_subtract subtract;
	struct dada_process_fourier fourier;
	struct dada_process_normalise normalise;
	struct dada_process_roi roi;
	struct dada_process_mask mask;
	int binx, biny;
	void* so_handle = NULL;
	struct dada_data* (*so_funcp)(struct dada_data*, struct dada_data_path*, struct dada_data_modi*) = NULL;

	struct dada_data* dat2 = NULL; /* temp for stack operations */
	int num_cur;
	char num_tmp[16];

	stack.oper = DADA_STACK_OPER_NONE;
	stack.num   = 1;
	stack.first = 0;
	empty.oper = DADA_STACK_EMPTY_NONE;
	empty.num   = 1;
	empty.first = 0;
	subtract.oper = DADA_STACK_SUBTRACT_NONE;
	subtract.num   = 1;
	subtract.first = 0;
	fourier.oper   = DADA_FOURIER_NONE;
	fourier.param1 = 1;
	normalise.norm = DADA_NORMALISE_NONE;

	if (path==NULL || path->len == 0 || path->path == NULL)
	{
		DADA_RING_PRINTF("empty path: %p", path);
		goto cleanup;
	}

	/* canonicalise path for cache retrieval / storage */
	canonical = dada_input_cache_canonicalise(path, modi);
	// if (canonical) fprintf(stdout, "canonical: [%s]\n", canonical);


	/* check if data exists in cache */
	if (flags & DADA_CACHE_READ)
	{
		void* so_handle = NULL;
		struct dada_data* (*so_funcp)(const char*,const char*,const char*) = NULL;
		const char* key_space = "dada_cache";
		const char* key_set   = "";
		struct dada_data* tmp = NULL;
		dl_what = "dada_input_cache_read_data";
		DADA_DL_LOAD_SYMBOL(DADA_DL_PATH_CONST("input/cache"), dl_what, so_funcp, so_handle);
		tmp = so_funcp(key_space, key_set, canonical);
		DADA_DL_CLOSE(so_handle);
		if (tmp)
		{
			if (data) {dada_data_destroy(data); free(data); data=NULL;}
			data = tmp;
			goto cleanup;
		}
	}

	/* parse modifier options */
	dada_process_parse_roi(modi, &roi);
	dada_process_parse_bin(modi, &binx, &biny);
	dada_process_parse_stack(modi, &stack);
	dada_process_parse_empty(modi, &empty);
	dada_process_parse_subtract(modi, &subtract);
	dada_process_parse_fourier(modi, &fourier);
	dada_process_parse_normalise(modi, &normalise);

	dada_process_parse_mask(modi, &mask);


	/* TODO:
	 * division (first implementation done)
	 * stack operations
		* sum (done)
		* min/max (done)
		* median
	* */

	/* let path[N] point to num_tmp, where we sprintf num_cur into */
	stack.first = atoi(path->path[path->len-1]);
	free(path->path[path->len-1]);
	path->path[path->len-1] = num_tmp;

	/* dispatch data retrieval: either input/test.so or input/detector.so for artifical or real data */
	if (strcmp(path->path[0], "test")==0 && path->len>1)
	{
		dl_what = "dada_input_test";
		DADA_DL_LOAD_SYMBOL(DADA_DL_PATH_CONST("input/test"), dl_what, so_funcp, so_handle);
		if (data) {dada_data_destroy(data); free(data); data=NULL;}
		data = so_funcp(NULL, path, modi);
		DADA_DL_CLOSE(so_handle);
		goto process;
	}


	/* otherwise: open detector data, including stack, empty, and subtract operations */
	dl_what = "dada_input_detector";
	DADA_DL_LOAD_SYMBOL(DADA_DL_PATH_CONST("input/detector"), dl_what, so_funcp, so_handle);

	if (stack.oper != DADA_STACK_OPER_NONE)
	{
		dat2 = data;
		data = NULL;
		/* open stacked images and do the operation */
		for (num_cur=stack.first; num_cur<stack.first+stack.num; num_cur++)
		{
			snprintf(num_tmp, sizeof(num_tmp)-1, "%d", num_cur);
			dat2 = so_funcp(dat2, path, modi);

			if (dat2 == NULL) continue;

			if (data == NULL)
				data = dada_process_stack_init(data, dat2);
			else
				data = dada_process_stack(data, &stack, dat2);

		}
		if (dat2) {dada_data_destroy(dat2); free(dat2); dat2 = NULL;}
		/* if we average, divide sum by number of images */
		if (stack.oper == DADA_STACK_OPER_AVG)
		{
			int x, y, X=data->d[1], Y=data->d[0];
			switch (data->type)
			{
				case DADA_DATA_INT:    for (y=0; y<Y; y++) for (x=0; x<X; x++) data->buffer.i[y*X+x] /= stack.num; break;
				case DADA_DATA_DOUBLE: for (y=0; y<Y; y++) for (x=0; x<X; x++) data->buffer.f[y*X+x] /= stack.num; break;
				default: DADA_RING_PRINTF("data->type %d not supported.", data->type); goto error;
			}
		}

	}
	else
	{
		/* just open a single image */
		snprintf(num_tmp,sizeof(num_tmp)-1, "%d", stack.first);
		data = so_funcp(data, path, modi);
	}

	goto process;

process:

	if (data == NULL) goto error;
	/* ROI */
	if (roi.width>0 && roi.height>0)
		if (roi.left >= 0 && roi.left+roi.width <= data->d[1])
			if (roi.top >= 0 && roi.top+roi.height <= data->d[0])
				dada_process_roi(data, &roi);

	/* apply pixel mask */
	/* has to be done without binning (otherwise we spoil the normalisation),
	 * but after ROI (mask will honor the ROI)
	 * */
	if (mask.filename && strlen(mask.filename) != 0)
	{
		/* copy modi to tmp, then delete bin/binning */
		struct dada_data_modi* tmp = NULL;
		if (modi != NULL)
		{
			int n, N = modi->len;

			tmp = (struct dada_data_modi*) malloc(sizeof(struct dada_data_modi));
			tmp->len = N;
			tmp->key = (char**)malloc(N*sizeof(char*));
			tmp->val = (char**)malloc(N*sizeof(char*));

			for (n=0; n<N; n++)
			{
				if (strcmp(modi->key[n], "bin")==0 || strcmp(modi->key[n], "binning")==0)
				{
					tmp->key[n] = strdup("");
					tmp->val[n] = strdup("");
				}
				else
				{
					tmp->key[n] = strdup(modi->key[n]);
					tmp->val[n] = strdup(modi->val[n]);
				}
			}
		}
		dada_process_mask(data, tmp, &mask);
		dada_data_modi_destroy(&tmp);
	}

	/* BINNING */
	if (binx>1 || biny>1)
		dada_process_bin(data, binx, biny);


	/* EMPTY and SUBTRACTION *after* ROI and BINNING */

	/* now do the empty division */
	if (data && empty.oper == DADA_STACK_EMPTY_DIVIDE)
	{
		struct dada_data* divide = NULL;

		if (stack.oper == DADA_STACK_OPER_SUM)
		{
			struct dada_data* tmp    = NULL;
			struct dada_process_stack stack;
			stack.oper  = DADA_STACK_OPER_SUM;
			stack.num   = empty.num;
			stack.first = empty.first;

			/* open stacked images and sum up */
			for (num_cur=stack.first; num_cur<stack.first+stack.num; num_cur++)
			{
				snprintf(num_tmp, sizeof(num_tmp)-1, "%d", num_cur);
				tmp = so_funcp(tmp, path, modi);

				if (tmp == NULL) continue;

				if (divide == NULL)
					divide = dada_process_stack_init(divide, tmp);
				else
					divide = dada_process_stack(divide, &stack, tmp);

			}
			if (tmp) {dada_data_destroy(tmp); free(tmp); tmp = NULL;}
		}
		else
		{
			/* just open a single image */
			snprintf(num_tmp,sizeof(num_tmp)-1, "%d", empty.first);
			divide = so_funcp(NULL, path, modi);
		}

		if (divide != NULL)
		{
			if (roi.width>0 && roi.height>0)
				if (roi.left >= 0 && roi.left+roi.width <= divide->d[1])
					if (roi.top >= 0 && roi.top+roi.height <= divide->d[0])
						dada_process_roi(divide, &roi);
			if (binx>1 || biny>1)
				dada_process_bin(divide, binx, biny);
			data = dada_process_empty(data, divide);
			if (divide) {dada_data_destroy(divide); free(divide); divide=NULL;}
		}
	}

	/* now do the subtraction */
	if (data && subtract.oper == DADA_STACK_SUBTRACT_SUBTRACT)
	{
		struct dada_data* minus = NULL;
		snprintf(num_tmp, sizeof(num_tmp)-1, "%d", subtract.first);
		minus = so_funcp(NULL, path, modi);
		if (minus != NULL)
		{
			if (roi.width>0 && roi.height>0)
				if (roi.left >= 0 && roi.left+roi.width <= minus->d[1])
					if (roi.top >= 0 && roi.top+roi.height <= minus->d[0])
						dada_process_roi(minus, &roi);
			if (binx>1 || biny>1)
				dada_process_bin(minus, binx, biny);
			data = dada_process_subtract(data, minus);
			if (minus) {dada_data_destroy(minus); free(minus); minus=NULL;}
		}
	}

	/* apply Fourier filter */
	/* TODO: allow for a sequence of filters */
	if (data && fourier.oper != DADA_FOURIER_NONE)
	{
		void* so_handle = NULL;
		int (*so_funcp)(struct dada_data*, struct dada_process_fourier*) = NULL;
		dl_what = "dada_algo_fourier";
		DADA_DL_LOAD_SYMBOL(DADA_DL_PATH_CONST("algo/fourier"), dl_what, so_funcp, so_handle);
		so_funcp(data, &fourier);
		DADA_DL_CLOSE(so_handle);
	}

	/* normalise */
	if (data && data->type == DADA_DATA_DOUBLE && normalise.norm != DADA_NORMALISE_NONE)
	{
		if (normalise.norm == DADA_NORMALISE_MEAN)
		{
			int x,y, X,Y;
			double mean = 0.0;
			Y = data->d[0];
			X = data->d[1];
			for (y=0; y<Y; y++) for (x=0; x<X; x++) mean += data->buffer.f[y*X+x];
			mean /= X*Y;
			for (y=0; y<Y; y++) for (x=0; x<X; x++) data->buffer.f[y*X+x] /= mean;
		}
	}

	goto put_cache;
put_cache:
	/* try to cache data */
	if (flags & DADA_CACHE_WRITE && data)
	{
		void* so_handle = NULL;
		int (*so_funcp)(const char*, struct dada_data*) = NULL;
		dl_what = "dada_input_cache_write_data";
		DADA_DL_LOAD_SYMBOL(DADA_DL_PATH_CONST("input/cache"), dl_what, so_funcp, so_handle);
		so_funcp(canonical, data);
		DADA_DL_CLOSE(so_handle);
		if (data) goto cleanup;
	}

cleanup:
	DADA_DL_CLOSE(so_handle);
	path->path[path->len-1] = NULL; /* already free'd, and pointed to local char[] */
	if (data == NULL) DADA_RING_PRINTF("empty data [%s]", canonical);
	if (canonical) {free(canonical); canonical=NULL;}
	return data;

error:
	// DADA_RING_PRINTF("dlopen failed for %s", dl_what);
	if (data) {dada_data_destroy(data); free(data); data=NULL;}
	goto cleanup;
}

struct dada_data* dada_input_multi(struct dada_data_path* path, struct dada_data_modi* modi, struct dada_data* data, enum dada_cache_flags flags)
{
	const char* dl_what = NULL;
	char* canonical = NULL;
	struct dada_process_multi multi;
	int x, y, X, Y;
	int xx, yy, XX, YY;
	struct dada_data* image = NULL;

	if (path==NULL || path->len == 0 || path->path == NULL)
	{
		DADA_RING_PRINTF("empty path: %p", path);
		goto cleanup;
	}

	/* canonicalise path for cache retrieval / storage */
	canonical = dada_input_cache_canonicalise(path, modi);


	/* check if data exists in cache */
	if (0 && flags & DADA_CACHE_READ)
	{
		void* so_handle = NULL;
		struct dada_data* (*so_funcp)(const char*,const char*,const char*) = NULL;
		const char* key_space = "dada_cache";
		const char* key_set   = "";
		struct dada_data* tmp = NULL;
		dl_what = "dada_input_cache_read_data";
		DADA_DL_LOAD_SYMBOL(DADA_DL_PATH_CONST("input/cache"), dl_what, so_funcp, so_handle);
		tmp = so_funcp(key_space, key_set, canonical);
		DADA_DL_CLOSE(so_handle);
		if (tmp)
		{
			if (data) {dada_data_destroy(data); free(data); data=NULL;}
			data = tmp;
			goto cleanup;
		}
	}

	/* parse modifier options */
	dada_process_parse_multi(modi, &multi);
	XX = multi.horz;
	YY = multi.vert;


	/* loop over vert * horz and retrieve images */
	for (yy=0; yy<YY; yy++)
	{
		for (xx=0; xx<XX; xx++)
		{
			char* pold = path->path[path->len-1];
			int first = atoi(pold);
			int current = yy*XX+xx + first;
			char curr[16];
			int idx;
			snprintf(curr, sizeof(curr)-1, "%d", current);
			path->path[path->len-1] = strdup(curr);

			image = dada_input_retrieve(path, modi, image, flags);

			if (image == NULL) goto skip;


			/* if modifier &polar= given: convert cartesian image "data" to polar coordinates */
			if (0) // TODO
			{
				DADA_RING_PRINTF("debug %d", __LINE__);
				struct dada_data* polar = NULL;
				void* so_handle = NULL;
				struct dada_data* (*so_funcp)(struct dada_polar*,struct dada_data_modi*,struct dada_data*,enum dada_cache_flags) = NULL;
				DADA_DL_LOAD_SYMBOL(DADA_DL_PATH_CONST("algo/polar"), "dada_algo_polar", so_funcp, so_handle);
				if (image) polar = so_funcp(NULL, modi, image, DADA_CACHE_NONE);
				if (image) {dada_data_destroy(image); free(image); image=NULL;}
				image = polar;
				DADA_DL_CLOSE(so_handle);
			}




			Y = image->d[0];
			X = image->d[1];

			if (data == NULL)
			{
				switch (multi.layout)
				{
					case DADA_MULTI_LAYOUT_4D:
						data = dada_data_init(image->type, 4, YY, XX, Y, X);
						break;
					case DADA_MULTI_LAYOUT_2D:
						data = dada_data_init(image->type, 2, YY*Y, XX*X);
						break;
					default:
						DADA_RING_PRINTF("multi.layout %d not supported.", multi.layout);
				}
			}

			switch (data->type)
			{
				case DADA_DATA_INT:
					for (y=0; y<Y; y++)
					{
						for (x=0; x<X; x++)
						{
							idx = yy*(Y*XX*X) + y*(XX*X) + xx*X + x;
							data->buffer.i[idx] = image->buffer.i[y*X+x];
						}
					}
					break;
				case DADA_DATA_DOUBLE:
					for (y=0; y<Y; y++)
					{
						for (x=0; x<X; x++)
						{
							idx = yy*(Y*XX*X) + y*(XX*X) + xx*X + x;
							data->buffer.f[idx] = image->buffer.f[y*X+x];
						}
					}
					break;
				default: fprintf(stderr, "(unsupported format %s:%d)\n", __FILE__, __LINE__); break;
			}

skip:
			if (path->path[path->len-1]) {free(path->path[path->len-1]); path->path[path->len-1]=NULL;}
			path->path[path->len-1] = pold;
		}
	}
	if (image) {dada_data_destroy(image); free(image); image=NULL;}

	goto process;

process:

	if (data == NULL) goto error;

	goto put_cache;
put_cache:
	/* try to cache data */
	if (0 && flags & DADA_CACHE_WRITE && data)
	{
		void* so_handle = NULL;
		int (*so_funcp)(const char*, struct dada_data*) = NULL; /* KAPUTT! */
		dl_what = "dada_input_cache_write_data";
		DADA_DL_LOAD_SYMBOL(DADA_DL_PATH_CONST("input/cache"), dl_what, so_funcp, so_handle);
		so_funcp(canonical, data);
		DADA_DL_CLOSE(so_handle);
		if (data) goto cleanup;
	}

cleanup:
	if (data == NULL) DADA_RING_PRINTF("empty data [%s]", canonical);
	if (canonical) {free(canonical); canonical=NULL;}
	if (image) {dada_data_destroy(image); free(image); image=NULL;}
	return data;

error:
	DADA_RING_PRINTF("dlopen failed for %s", dl_what);
	if (data) {dada_data_destroy(data); free(data); data=NULL;}
	goto cleanup;
}

char* dada_input_cache_canonicalise(struct dada_data_path* path, struct dada_data_modi* modi)
{
	char* str = NULL;
	FILE* mem = NULL;
	size_t memlen;
	int n, N;

	DADA_MEMSTREAM(mem, str, memlen);

	N = path->len;
	for (n=0; n<N; n++) fprintf(mem, "/%s", path->path[n]);

	fprintf(mem, "?");

	N = modi->len;
	for (n=0; n<N; n++) fprintf(mem, "&%s=%s", modi->key[n], modi->val[n]);

	fclose(mem);

	return str;

err_memstream:
	return NULL;
}

