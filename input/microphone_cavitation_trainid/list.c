#include "../../dada.h"

#include <hdf5.h>
#include <hdf5_hl.h>

extern int dirfilter_nohidden(const struct dirent* dirent);
extern int versionsort_r(const struct dirent** a, const struct dirent** b);

int list_folder(struct dada_data_path* path, FILE* mem);
int list_trainid(struct dada_data_path* path, FILE* mem);

int list(struct dada_data_path* path, FILE* mem)
{
	if (path->len == 3) return list_folder(path, mem);
	if (path->len == 4) return list_trainid(path, mem);

	fprintf(mem, "{\n");
	fprintf(mem, "    \"status\": \"error\",\n");
	fprintf(mem, "    \"error\": \"path->len neither 2 nor 3.\"\n");
	fprintf(mem, "}\n");

	return 0;
}

int list_folder(struct dada_data_path* path, FILE* mem)
{
	int ret = 0;
	char* fol = NULL;

	/* assemble folder name */
	{
		FILE* mem = NULL;
		size_t memlen;
		// int n;
		mem = open_memstream(&fol, &memlen);

		fprintf(mem, "config/%s/%s/data/raw", path->path[0], path->path[1]);
		/*
		for (n=0; path && n<path->len; n++)
			if (path->path[n])
				fprintf(mem, "/%s", path->path[n]);
		*/
		fclose(mem);
	}

	/* list subdirs */
	{
		int num;
		struct dirent** namelist = NULL;
		int notfirst = 0;

		num = scandir(fol, &namelist, dirfilter_nohidden, versionsort_r);
		if (num<0) goto listingerror;

		fprintf(mem, "{\n");
		fprintf(mem, "    \"status\": \"okay\",\n");
		fprintf(mem, "    \"listing\": [");
		while (num--) /* loop over subfolders */
		{
			char dirname2[256];
			struct stat st;
			const char* d_name = namelist[num]->d_name;
			int empty = 0;
			DIR* dir = NULL;
			struct dirent* dent;

			snprintf(dirname2, sizeof(dirname2)-1, "%s/%s", fol, d_name);

			/* stat the dir, check if it actually /is/ a directory */
			if (stat(dirname2, &st) != 0) goto skip;
			if (!S_ISDIR(st.st_mode))     goto skip;

			/* IF we are listing sample folders,
			 * check if there is anything nice inside.
			 * NOTE: we are listing samples iff detector is given.
			 */
			dir = opendir(dirname2);
			if (dir == NULL) goto skip;
			empty = 1;
			while (empty>0 && (dent=readdir(dir))!=NULL)
			{
				char dirname3[256];
				struct stat st;
				const char* d_name = NULL;

				d_name = dent->d_name;
				snprintf(dirname3, sizeof(dirname3)-1, "%s/%s", dirname2, d_name);

				if (d_name[0] == '.')         continue;
				if (stat(dirname3, &st) != 0) continue;
				if (!S_ISREG(st.st_mode))     continue;

				empty=0;
			}
			if (dir) {closedir(dir); dir=NULL;}

			if (notfirst++) fprintf(mem, ", ");
			if (empty)
				fprintf(mem, "\".%s\"", d_name); /* leading dot */
			else
				fprintf(mem, "\"%s\"", d_name);

skip:
			FREE(namelist[num]);
		}
		fprintf(mem, "]\n");
		fprintf(mem, "}\n");
	FREE(namelist);
	}


cleanup:
	return ret;

listingerror:
	fprintf(mem, "{\n");
	fprintf(mem, "    \"status\": \"error\",\n");
	fprintf(mem, "    \"error\": \"%s\"\n", strerror(errno));
	fprintf(mem, "}\n");
	ret = errno;
	goto cleanup;
}

int list_trainid(struct dada_data_path* path, FILE* mem)
{
	int locked = 0;
	pthread_mutex_lock(&hdf5_mutex); locked = 1;

	int ret = 0;
	int n, N;
	int notfirst = 0;

	void* so_handle = NULL;
	char* (*so_filename)(void*) = NULL;
	char* filename = NULL;

	hid_t id_file, id_dset, id_data;
	hid_t id_type;
	__attribute__((unused)) int rank;
	hsize_t dims_out[3];
	herr_t status = 0;
	signed long* buffer = NULL;

	DADA_DL_LOAD_SYMBOL(DADA_DL_PATH_DATA(path, "filename"),  "detector_filename",  so_filename,  so_handle);
	filename = so_filename(path);
	DADA_DL_CLOSE(so_handle);
	if (filename == NULL || strlen(filename) == 0)
	{
		DADA_RING_PRINTF("empty filename from %p", path);
		goto error;
	}
	DADA_RING_PRINTF("filename: %s", filename);

	H5Eset_auto2(H5E_DEFAULT, NULL, NULL); /* turn off error messages */
	id_file = H5Fopen(filename, H5F_ACC_RDONLY, H5P_DEFAULT);
	if (id_file < 0) goto nofile;

	const char* dset = "/INSTRUMENT/MID_EXP_FASTADC/ADC/DESTEST:channel_3.output/data/trainId";
	id_dset = H5Dopen2(id_file, dset, H5P_DEFAULT);
	if (id_dset<0) goto cleanup;

	/* obtain image dimensions; select only first image */
	id_data = H5Dget_space(id_dset);
	rank    = H5Sget_simple_extent_ndims(id_data);
		  H5Sget_simple_extent_dims(id_data, dims_out, NULL);
	DADA_RING_PRINTF("%d:%d", (int)rank, (int)dims_out[0]);

	buffer = malloc(sizeof(unsigned long)*dims_out[0]);
	IS_NULL_ERRMSG(buffer, error);

	id_type = H5Dget_type(id_dset);
	status = H5Dread(id_dset, id_type, H5S_ALL, id_data, H5P_DEFAULT, buffer);

	status = H5Dclose(id_dset);
	if (status != 0)
	{
		fprintf(stderr, "H5 error (%d)\n", __LINE__);
		H5Eprint(H5E_DEFAULT, stderr);
	}

	status = H5Fclose(id_file);
	if (status != 0)
	{
		fprintf(stderr, "H5 error (%d)\n", __LINE__);
		H5Eprint(H5E_DEFAULT, stderr);
	}

	N=dims_out[0];
	fprintf(mem, "{\n");
	fprintf(mem, "    \"status\": \"okay\",\n");
	fprintf(mem, "    \"listing\": [");
	for (n=0; n<N; n++)
	{
		if (notfirst++) fprintf(mem, ", ");
		fprintf(mem, "%ld", buffer[n]);
	}
	fprintf(mem, "]\n");
	fprintf(mem, "}\n");


cleanup:
	FREE(buffer);
	FREE(filename);
	if (locked>0) {pthread_mutex_unlock(&hdf5_mutex); locked=0;}
	return ret;

	goto listingerror;
listingerror:
	fprintf(mem, "{\n");
	fprintf(mem, "    \"status\": \"error\",\n");
	fprintf(mem, "    \"error\": \"%s\"\n", strerror(errno));
	fprintf(mem, "}\n");
	ret = errno;
	goto cleanup;
error:
	fprintf(mem, "{\n");
	fprintf(mem, "    \"status\": \"error\",\n");
	fprintf(mem, "    \"error\": \"unknown\"\n");
	fprintf(mem, "}\n");
	ret = errno;
	goto cleanup;
nofile:
	H5Eprint(H5E_DEFAULT, stderr);
	goto error;
}

time_t stamp(struct dada_data_path* path)
{
	char* fol = NULL;
	time_t t = 0;
	struct stat st;
	struct timespec tt;

	/* assemble folder name */
	{
		FILE* mem = NULL;
		size_t memlen;
		mem = open_memstream(&fol, &memlen);

		fprintf(mem, "config/%s/%s/data/raw", path->path[0], path->path[1]);
		fclose(mem);
	}

	/* obtain time stamp */
	if (stat(fol, &st) != 0) goto cleanup;

	tt = st.st_mtim;
	t  = tt.tv_sec;

cleanup:
	return t;
}

