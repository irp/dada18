/* pixelmask.c for EigerX 4M @ P10
 *
 * apply trivial pixel mask
 * this is for generic pilatus detectors;
 * look at more specific versions if required.
 */
#include "pixelmask.h"

int detector_pixelmask(struct dada_data* data, struct dada_data_modi* modi)
{
	int ret = 0;
	struct dada_process_roi roi;
	int n;
	int r1, r2, r3, r4;
	int x, y;

	if (data == NULL)
	{
		DADA_RING_PRINTF("invalid data %p", data);
		ret = -1;
		goto cleanup;
	}

	roi.left  = 0;          roi.top    = 0;
	roi.width = data->d[1]; roi.height = data->d[0];
	for (n=0; modi && n<modi->len; n++)
	{
		if (strcmp(modi->key[n], "roi")==0)
		{
			if (sscanf(modi->val[n], "%d,%d,%d,%d", &r1, &r2, &r3, &r4) == 4)
			{
				roi.left   = r1;
				roi.top    = r2;
				roi.width  = r3;
				roi.height = r4;
			}
		}
	}

	/* bad pixel: 215*data->d[1] + 1765, P10, 2017-09-20 */
	if (0)
	{
	x = 1765; y =  215;
	x -= roi.left; y -= roi.top;
	if (x >= 0 && x < roi.width && y >= 0 && y < roi.height)
	{
		switch (data->type)
		{
			case DADA_DATA_INT:    data->buffer.i[y*data->d[1] + x] = -1; break;
			case DADA_DATA_DOUBLE: data->buffer.f[y*data->d[1] + x] = -1; break;
			default: fprintf(stderr, "(unsupported format %s:%d)\n", __FILE__, __LINE__); break;
		}
	}
	}

cleanup:
	return ret;
}

