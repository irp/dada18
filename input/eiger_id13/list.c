#include "../../dada.h"

extern int dirfilter_nohidden(const struct dirent* dirent);
extern int versionsort_r(const struct dirent** a, const struct dirent** b);

int list(struct dada_data_path* path, FILE* mem)
{
	int ret = 0;
	char* fol = NULL;

	/* assemble folder name */
	{
		FILE* mem = NULL;
		size_t memlen;
		// int n;
		mem = open_memstream(&fol, &memlen);

		fprintf(mem, "config/%s/%s/data/%s", path->path[0], path->path[1], path->path[2]);
		/*
		for (n=0; path && n<path->len; n++)
			if (path->path[n])
				fprintf(mem, "/%s", path->path[n]);
		*/
		fclose(mem);
	}

	/* list subdirs + master files inside fol */
	{
		int num;
		struct dirent** namelist  = NULL;
		int notfirst = 0;

		num = scandir(fol, &namelist, dirfilter_nohidden, versionsort_r);
		if (num<0) goto listingerror;

		fprintf(mem, "{\n");
		fprintf(mem, "    \"status\": \"okay\",\n");
		fprintf(mem, "    \"listing\": [");
		while (num--) /* loop over master files */
		{
			const char* d_name = namelist[num]->d_name;

			if (strstr(d_name, "_master.h5") == NULL) goto skip;
			*strstr(d_name, "_master.h5") = '\0';

			if (strrchr(d_name, '_') == NULL) goto skip;
			*strrchr(d_name, '_') = '/';

			if (notfirst++) fprintf(mem, ", ");
			fprintf(mem, "\"%s\"", d_name);
skip:
			FREE(namelist[num]);
		}
		fprintf(mem, "]\n");
		fprintf(mem, "}\n");
	FREE(namelist);
	}


cleanup:
	FREE(fol);
	return ret;

listingerror:
	fprintf(mem, "{\n");
	fprintf(mem, "    \"status\": \"error\",\n");
	fprintf(mem, "    \"error\": \"%s\"\n", strerror(errno));
	fprintf(mem, "}\n");
	ret = errno;
	goto cleanup;
}

time_t stamp(struct dada_data_path* path)
{
	char* fol = NULL;
	time_t t = 0;
	struct stat st;
	struct timespec tt;

	/* assemble folder name */
	{
		FILE* mem = NULL;
		size_t memlen;
		mem = open_memstream(&fol, &memlen);

		fprintf(mem, "config/%s/%s/data/%s", path->path[0], path->path[1], path->path[2]);
		fclose(mem);
	}

	/* obtain time stamp */
	if (stat(fol, &st) != 0) goto cleanup;

	tt = st.st_mtim;
	t  = tt.tv_sec;

cleanup:
	return t;
}

