#ifndef _EIGER_P10_OPEN_H_
#define _EIGER_P10_OPEN_H_

#include "../../dada.h"

#include <hdf5.h>
#include <hdf5_hl.h>

struct dada_data* detector_open(struct dada_data* data, struct dada_data_path* path, struct dada_data_modi* modi);

extern pthread_mutex_t hdf5_mutex;

#endif

