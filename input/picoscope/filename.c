/* filename.c for picoscope */
#include "filename.h"

char* detector_filename(struct dada_data_path* path)
{
	char* buffer = NULL;
	FILE* mem = NULL;
	size_t memlen;
	mem = open_memstream(&buffer, &memlen);

	if (path->len==6 && strcmp(path->path[0], "MID")==0 && path->path[4] != NULL)
	{
		int run;
		if (path->path[4][0] == 'r')
			run = atoi(path->path[4]+1);
		else
			run = atoi(path->path[4]  );
		fprintf(mem, "config/%s/%s/data/IRP/scope/run%04d_master.h5",
				path->path[0], path->path[1], run);
		goto cleanup;
	}

cleanup:
	fclose(mem);
	// DADA_RING_PRINTF("filename: %s", buffer);
	return buffer;
}

