/* pixelmask.c for picoscope */
#include "pixelmask.h"

int detector_pixelmask(struct dada_data* data)
{
	int ret = 0;

	if (data == NULL)
	{
		DADA_RING_PRINTF("invalid data %p", data);
		ret = -1;
		goto cleanup;
	}

cleanup:
	return ret;
}

