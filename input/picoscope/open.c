/* open.c for picoscope.c */

#include "open.h"

/* TODO: think about 1D roi and binning; in general: N-dim processing */

struct dada_data* detector_open(struct dada_data* data, struct dada_data_path* path, struct dada_data_modi* modi)
{
	int locked = 0;
	pthread_mutex_lock(&hdf5_mutex); locked = 1;

	const int timing = 0;

	struct timespec t0, t1, t2, t3, t4;
	double tt1, tt2, tt3, tt4;
	double t;
	clock_gettime(CLOCK_REALTIME, &t0);
	struct dada_process_roi roi;
	roi.left = roi.top = roi.width = roi.height = -1;
	roi.flags  = DADA_PROCESS_ROI_NORMAL;

	int fd = -1;
	struct stat stat;
	int dims[1] = {0};

	void* so_handle = NULL;
	char* (*so_filename)(void*) = NULL;
	char* filename = NULL;

	DADA_DL_LOAD_SYMBOL(DADA_DL_PATH_DATA(path, "filename"),  "detector_filename",  so_filename,  so_handle);
	filename = so_filename(path);
	DADA_DL_CLOSE(so_handle);
	if (filename == NULL || strlen(filename) == 0)
	{
		DADA_RING_PRINTF("empty filename from %p", path);
		goto error;
	}

	int frame = atoi(path->path[path->len-1]);

	/* try to open and access file */
	// CHECK_FILE_NAME(filename);
	fd = open(filename, O_RDONLY);
	if (fd < 0)
	{
		DADA_RING_PRINTF("cannot open %s: %s", filename, strerror(errno));
		goto error;
	}
	if (fstat(fd, &stat) != 0)
	{
		DADA_RING_PRINTF("cannot access %s: %s", filename, strerror(errno));
		goto error;
	}

	// DADA_RING_PRINTF("file %s size %.3f MB frame %d", filename, 1.0*stat.st_size/1048576, frame);
	// fprintf(stdout, "file %s size %.3f MB frame %d\n", filename, 1.0*stat.st_size/1048576, frame);

	clock_gettime(CLOCK_REALTIME, &t1);
	t = (t1.tv_sec-t0.tv_sec)+(t1.tv_nsec-t0.tv_nsec)*1e-9;

	{
		int i;
		hid_t id_file, id_dset;
		hid_t id_data, id_memy;
		hid_t id_type;
		__attribute__((unused)) int rank;
		hsize_t dims_out[1];
		herr_t status = 0;
		signed char* buffer = NULL;
		int n = 1;

		H5Eset_auto2(H5E_DEFAULT, NULL, NULL); /* turn off error messages */
		id_file = H5Fopen(filename, H5F_ACC_RDONLY, H5P_DEFAULT);
		if (id_file < 0)
			goto nofile;


		char dset[64];
		snprintf(dset, sizeof(dset)-1, "/entry/data/data_%06d/%s", atoi(path->path[5]), path->path[3]);
		// DADA_RING_PRINTF("dset: %s", dset);

		id_dset = H5Dopen2(id_file, dset, H5P_DEFAULT);
		if (id_dset<0) goto nodset;

		/* obtain image dimensions; select only first image */
		id_data = H5Dget_space(id_dset);
		rank    = H5Sget_simple_extent_ndims(id_data);
		          H5Sget_simple_extent_dims(id_data, dims_out, NULL);

		/*
		DADA_RING_PRINTF("%d:%d frame:%d", (int)rank, (int)dims_out[0], frame);
		*/

		dada_process_parse_roi(modi, &roi);

		if (roi.left>=0 && roi.top>=0 && roi.width>=0 && roi.height>=0) /* check if ROI is smaller than actual IMAGE */
		{
			if (roi.left<0 || roi.left >= (int)dims_out[0]) goto bad_roi;
			if (roi.left+roi.width     >= (int)dims_out[0]) goto bad_roi;
			if (roi.flags && DADA_PROCESS_ROI_MASK)         goto bad_roi; /* roi region is set to -1 by input/retrieve.c */
			goto do_roi;
bad_roi:
			roi.left = roi.top = roi.width = roi.height = -1;
			roi.flags  = DADA_PROCESS_ROI_NORMAL;
		}

do_roi:
		/* if no roi was supplied, or if it was too large: create full FOV roi */
		if (roi.left<0 || roi.top<0 || roi.width<0 || roi.height<0) /* check if ROI is smaller than actual IMAGE */
		{
			roi.left   = 0;
			roi.width  = dims_out[0];
			roi.flags  = DADA_PROCESS_ROI_NORMAL;
		}

		hsize_t hyper_siz[1]; /* size of the hyperslab in the file */
		hsize_t hyper_off[1]; /* hyperslab offset in the file */
		hyper_siz[0] = roi.width;
		hyper_off[0] = roi.left;

		// fprintf(stdout, "roi: %d,%d,%d,%d\n", roi.left, roi.top, roi.width, roi.height);
		// DADA_RING_PRINTF("roi: %d,%d,%d,%d", roi.left, roi.top, roi.width, roi.height);
		// DADA_RING_PRINTF("dims_out: %lld %lld", dims_out[0], dims_out[1]);

		dims[0] = dims_out[0] = roi.width;

		/* select hyperslab, define dataspace */
		H5Sselect_hyperslab(id_data, H5S_SELECT_SET, hyper_off, NULL, hyper_siz, NULL);
		id_memy = H5Screate_simple(1, hyper_siz, NULL);
		hyper_off[0] = 0;
		H5Sselect_hyperslab(id_memy, H5S_SELECT_SET, hyper_off, NULL, hyper_siz, NULL);



		id_type = H5Dget_type(id_dset);
		if (H5Tequal(id_type, H5T_STD_I8LE)) buffer = (signed char*) malloc(sizeof(signed char)*roi.width);
		IS_NULL_ERRMSG(buffer, error);

		status = H5Dread(id_dset, id_type, id_memy, id_data, H5P_DEFAULT, buffer);
		if (status != 0)
		{
			DADA_RING_PRINTF("H5 error %d", status);
			fprintf(stderr, "H5 error (%d)\n", __LINE__);
			H5Eprint(H5E_DEFAULT, stderr);
			fprintf(stderr, "\tfilename: %s\n", filename);
			fprintf(stderr, "\tframenumber: %d\n", frame);
			fprintf(stderr, "\thyper_siz[0]: %d\n", (int)hyper_siz[0]);
			fprintf(stderr, "\thyper_off[0]: %d\n", (int)hyper_off[0]);
			fprintf(stderr, "\n");
			goto error;
	 	}

		status = H5Dclose(id_dset);
		if (status != 0)
		{
			fprintf(stderr, "H5 error (%d)\n", __LINE__);
			H5Eprint(H5E_DEFAULT, stderr);
	 	}

		status = H5Fclose(id_file);
		if (status != 0)
		{
			fprintf(stderr, "H5 error (%d)\n", __LINE__);
			H5Eprint(H5E_DEFAULT, stderr);
	 	}

		clock_gettime(CLOCK_REALTIME, &t2);
		t = (t2.tv_sec-t1.tv_sec)+(t2.tv_nsec-t1.tv_nsec)*1e-9;

		/* TODO:
		 * add size_t allocated to struct dada_data,
		 * and check for this;
		 * if it is large enough, set dimensions / size in else
		 * */
		if (!data || data->d[0] != dims[0])
		{
			if (data) {dada_data_destroy(data); free(data); data=NULL;}
			data = dada_data_init(DADA_DATA_DOUBLE, 1, dims[0]);
		}
		if (data == NULL)
		{
			DADA_RING_PRINTF("dada_data_init(DOUBLE, 1, %d) failed", dims[0]);
			goto error;
		}

		clock_gettime(CLOCK_REALTIME, &t3);
		t = (t3.tv_sec-t2.tv_sec)+(t3.tv_nsec-t2.tv_nsec)*1e-9;

		switch (data->type)
		{
			case DADA_DATA_INT:
				if (H5Tequal(id_type, H5T_STD_I8LE)) for (i=0; i<dims[0]; i++) data->buffer.i[i] = buffer[i];
				break;
			case DADA_DATA_DOUBLE:
				if (H5Tequal(id_type, H5T_STD_I8LE)) for (i=0; i<dims[0]; i++) data->buffer.f[i] = buffer[i];
				break;
			default: fprintf(stderr, "(unsupported format %s:%d)\n", __FILE__, __LINE__); break;
		}

		clock_gettime(CLOCK_REALTIME, &t4);

		t   = (t4.tv_sec-t0.tv_sec)+(t4.tv_nsec-t0.tv_nsec)*1e-9;
		tt1 = (t1.tv_sec-t0.tv_sec)+(t1.tv_nsec-t0.tv_nsec)*1e-9;
		tt2 = (t2.tv_sec-t1.tv_sec)+(t2.tv_nsec-t1.tv_nsec)*1e-9;
		tt3 = (t3.tv_sec-t2.tv_sec)+(t3.tv_nsec-t2.tv_nsec)*1e-9;
		tt4 = (t4.tv_sec-t3.tv_sec)+(t4.tv_nsec-t3.tv_nsec)*1e-9;

		if (0) DADA_RING_PRINTF("mic: %s frame %d:%d %.1fms", filename, n, frame, t*1e3);
		if (timing) DADA_RING_PRINTF("hdf5-timing: %.1f ms+%.1f ms+%.1f ms+%.1f ms", tt1*1e3, tt2*1e3, tt3*1e3, tt4*1e3);

		FREE(buffer);
	}



cleanup:
	if (timing) fprintf(stdout, "\n");
	// DL_CLOSE(so_handle_filename);
	FREE(filename);
	if (fd>0) {close(fd); fd=-1;}

	if (locked>0) {pthread_mutex_unlock(&hdf5_mutex); locked=0;}
	return data;

error:
	if (data) {dada_data_destroy(data); free(data); data=NULL;}
	goto cleanup;
// forbidden:
	// snprintf(err, sizeof(err)-1, "forbidden filename: [%s]", filename);
	// goto error;

nofile:
	H5Eprint(H5E_DEFAULT, stderr);
	goto error;
nodset:
	// DADA_RING_PRINTF("cannot find frame %d in set of inspected datasets", frame);
	goto error;
}

