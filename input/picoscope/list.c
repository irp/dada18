#include "../../dada.h"

extern int dirfilter_nohidden(const struct dirent* dirent);
extern int versionsort_r(const struct dirent** a, const struct dirent** b);

int list_files(struct dada_data_path* path, FILE* mem);
int list_channels(struct dada_data_path* path, FILE* mem);

int list(struct dada_data_path* path, FILE* mem)
{
	if (path->len == 3 || (path->len>3&&(path->path[3]==NULL||strlen(path->path[3])==0))) return list_channels(path, mem);
	if (path->len == 4 || (path->len>4&&(path->path[4]==NULL||strlen(path->path[4])==0))) return list_files(path, mem);

	return -1;
}

int list_channels(__attribute__((unused))struct dada_data_path* path, FILE* mem)
{
	int ret = 0;

	fprintf(mem, "{\n");
	fprintf(mem, "    \"status\": \"okay\",\n");
	fprintf(mem, "    \"listing\": [");

	fprintf(mem, "\"chanA\",");
	fprintf(mem, "\"chanB\",");
	fprintf(mem, "\"chanC\",");
	fprintf(mem, "\"chanD\"");

	fprintf(mem, "]\n");
	fprintf(mem, "}\n");

	goto cleanup;

cleanup:
	return ret;
}

int list_files(struct dada_data_path* path, FILE* mem)
{
	int ret = 0;
	char* fol = NULL;

	/* assemble folder name */
	{
		FILE* mem = NULL;
		size_t memlen;
		mem = open_memstream(&fol, &memlen);
		fprintf(mem, "config/%s/%s/data/IRP/scope", path->path[0], path->path[1]);
		fclose(mem);
	}

	/* list subdirs */
	{
		int num;
		struct dirent** namelist = NULL;
		int notfirst = 0;

		num = scandir(fol, &namelist, dirfilter_nohidden, versionsort_r);
		if (num<0) goto listingerror;

		fprintf(mem, "{\n");
		fprintf(mem, "    \"status\": \"okay\",\n");
		fprintf(mem, "    \"listing\": [");
		while (num--) /* loop over subfolders */
		{
			char dirname2[256];
			struct stat st;
			const char* d_name = namelist[num]->d_name;
			int empty = 0;

			snprintf(dirname2, sizeof(dirname2)-1, "%s/%s", fol, d_name);

			/* stat the dir, check if it actually /is/ a directory */
			if (stat(dirname2, &st) != 0) goto skip;
			if (!S_ISREG(st.st_mode))     goto skip;

			/* check if file name ends with _master.h5 */
			if (!strchr(d_name, '_')) goto skip;
			if ( strcmp(strchr(d_name, '_'), "_master.h5") != 0) goto skip;

			if (notfirst++) fprintf(mem, ", ");
			if (empty)
				fprintf(mem, "\".%s\"", d_name); /* leading dot */
			else
				fprintf(mem, "\"%s\"", d_name);

skip:
			FREE(namelist[num]);
		}
		fprintf(mem, "]\n");
		fprintf(mem, "}\n");
	FREE(namelist);
	}


cleanup:
	return ret;

listingerror:
	fprintf(mem, "{\n");
	fprintf(mem, "    \"status\": \"error\",\n");
	fprintf(mem, "    \"error\": \"%s\"\n", strerror(errno));
	fprintf(mem, "}\n");
	ret = errno;
	goto cleanup;
}

time_t stamp(struct dada_data_path* path)
{
	char* fol = NULL;
	time_t t = 0;
	struct stat st;
	struct timespec tt;

	/* assemble folder name */
	{
		FILE* mem = NULL;
		size_t memlen;
		mem = open_memstream(&fol, &memlen);

		fprintf(mem, "config/%s/%s/data/IRP/scope", path->path[0], path->path[1]);
		fclose(mem);
	}

	/* obtain time stamp */
	if (stat(fol, &st) != 0) goto cleanup;

	tt = st.st_mtim;
	t  = tt.tv_sec;

cleanup:
	return t;
}

