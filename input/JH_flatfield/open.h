#ifndef _PIRRA_OPEN_H_
#define _PIRRA_OPEN_H_

#include "../../dada.h"
#include <tiffio.h>

struct dada_data* detector_open(struct dada_data* data, struct dada_data_path* path, struct dada_data_modi* modi);

#endif

