#!/bin/sh

F="../../config/MID/p2207/data/JH/flatcorr_jh_20191015";
FILE=p2207_191015.h

echo "#define p2207_20191015() { \\" > $FILE

for FOL in $(ls $F) # | head -1)
do
	echo -e "\tif (run == atoi(\"$FOL\"+6)) switch (tid) { \\"
	for TID in $(ls $F/$FOL)
	do
		echo "${TID}" | awk -F_ '{printf "\t\tcase %ld: fname=\"%s\"; break; \\\n", $3, $0;}'
	done
	echo -e "\t} \\"
done >> $FILE

echo "}" >> $FILE

