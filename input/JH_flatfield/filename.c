/* filename.c for pirra @ P10
 */
#include "filename.h"
#include "p2207_191015.h"

char* detector_filename(struct dada_data_path* path)
{
	char* buffer = NULL;
	FILE* mem = NULL;
	size_t memlen;
	mem = open_memstream(&buffer, &memlen);
	int s, shot = -1;
	int run = -1;

	if (path->path[3][0] == 'f')
		run = atoi(path->path[3]+6);
	else
		run = atoi(path->path[3]  );

	if (path->len==5 && strcmp(path->path[0], "MID")==0 && path->path[4] != NULL && strcmp(path->path[2], "flatcorr_jh_20190617")==0)
	{
		for (s=0; s<400; s++)
		{
			char tmp[256];
			struct stat st;
			snprintf(tmp, sizeof(tmp), "config/%s/%s/data/JH/%s/ff_run%04d/bubble_%04d_shot_%04d.tif",
					path->path[0], path->path[1], path->path[2],
					run, atoi(path->path[4]), s);
			if (stat(tmp, &st) == 0)
			{
				shot = s;
				goto found1;
			}
		}
		goto cleanup;
found1:
		fprintf(mem, "config/%s/%s/data/JH/%s/ff_run%04d/bubble_%04d_shot_%04d.tif",
				path->path[0], path->path[1], path->path[2],
				run, atoi(path->path[4]), shot);
		goto cleanup;
	}

	if (path->len==5 && strcmp(path->path[0], "MID")==0 && path->path[4] != NULL && strcmp(path->path[2], "flatcorr_jh_20191015")==0)
	{
		const char* fname = NULL;
		unsigned long tid = atol(path->path[4]);
		p2207_20191015();
		fprintf(mem, "config/%s/%s/data/JH/%s/ff_run%04d/%s",
				path->path[0], path->path[1], path->path[2],
				run, fname);
		goto cleanup;
	}

cleanup:
	fclose(mem);
	// DADA_RING_PRINTF("filename: %s", buffer);
	// fprintf(stdout, "filename: [%s]\n", buffer);
	return buffer;
}

