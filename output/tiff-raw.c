/* TODO
 * check, if it works so far;
 * check, if Matlab does recognise different kinds of int and float;
 * implement choosing "best kind of int";
 * implement 2Dx2D -> 2D;
 * then proceed with stxm:multiroi
 */

/* dada18: output/tiff-raw.c
 * save 2D dataset as tiff image, using int16 or double
 * save 4D dataset (2D array of images) as 2D layed-out images
 * we write raw data, i.e., without colourpalette
 * */
#include "../dada.h"

enum tiff_type {
	TIFF_TYPE_I8,
	TIFF_TYPE_U8,
	TIFF_TYPE_I16,
	TIFF_TYPE_U16,
	TIFF_TYPE_I32,
	TIFF_TYPE_FLT
};
union tiff_buff {
	void*    p;
	int8_t*  i8;
	int16_t* i16;
	int32_t* i32;
	float*   flt;
};

void* dada_output_tiff_raw(struct dada_data* data, __attribute__((unused))struct dada_data_form* form, size_t* len)
{
	const int timing = 1;

	struct timespec t0, t1, t2, t3, t4;
	double tt1, tt2, tt3, tt4;
	clock_gettime(CLOCK_REALTIME, &t0);

	int x, y;
	int X, Y;

	enum tiff_type tiff_type = TIFF_TYPE_FLT;
	union tiff_buff tiff_buff;
	tiff_buff.p = NULL;

	TIFF* tiff = NULL;

	void* buf = NULL;
	int fd = -1;
	struct stat st;

	if (data==NULL || data->buffer.p==NULL || data->D==0 || data->d==NULL || len==NULL)
	{
		DADA_RING_PRINTF("error: empty data (%p, %p, %d, %p, %p).\n",
				data,
				(data ? data->buffer.p:NULL),
				(data ? data->D:-1),
				(data && data->d ? data->d:NULL),
				(data ? len:NULL)
				);
		goto cleanup;
	}

	*len = 0;

	switch (data->type)
	{
		case DADA_DATA_INT:    goto okay;
		case DADA_DATA_DOUBLE: goto okay;
		default:
				       fprintf(stderr, "data->type %d not yet supported for output/tiff", data->type);
				       goto cleanup;
	}

okay:
	clock_gettime(CLOCK_REALTIME, &t1);
	/* TODO: check which tiff_type is needed */
	switch (data->D)
	{
		case 2:
			Y = data->d[0];
			X = data->d[1];
			switch (data->type)
			{
				case DADA_DATA_INT:
					tiff_type = TIFF_TYPE_FLT;
					tiff_buff.flt = (float*) malloc(X*Y*sizeof(float));

					/* TODO: check for 8, 16, 32; u/i */
					// tiff_type = TIFF_TYPE_I32; /* not working? */
					// tiff_type = TIFF_TYPE_I16;
					// tiff_type = TIFF_TYPE_I8;
					// tiff_buff.i32 = (int32_t*) malloc(X*Y*sizeof(int32_t));
					// tiff_buff.i16 = (int16_t*) malloc(X*Y*sizeof(int16_t));
					// tiff_buff.i8 = (int8_t*) malloc(X*Y*sizeof(int8_t));
					for (y=0; y<Y; y++)
					{
						for (x=0; x<X; x++)
						{
							int idx = y*X+x;
							tiff_buff.flt[idx] = data->buffer.i[idx];

							// tiff_buff.i32[idx] = data->buffer.i[idx];
							// tiff_buff.i16[idx] = data->buffer.i[idx];
							// tiff_buff.i8[idx] = data->buffer.i[idx];
						}
					}
					break;
				case DADA_DATA_DOUBLE:
					tiff_type = TIFF_TYPE_FLT;
					tiff_buff.flt = (float*) malloc(X*Y*sizeof(float));
					for (y=0; y<Y; y++)
					{
						for (x=0; x<X; x++)
						{
							int idx = y*X+x;
							tiff_buff.flt[idx] = data->buffer.f[idx];
						}
					}
					break;
				default: break; /* already catched before */
			}
			break;

		default:
			DADA_RING_PRINTF("error: number of dimenions %d not supported", data->D);
			fprintf(stderr, "[%s:%d] error: number of dimenions %d not supported\n", __FILE__, __LINE__, data->D);
			goto cleanup;
	}
	clock_gettime(CLOCK_REALTIME, &t2);


	/* finally, do the tiff */
	tiff = TIFFOpen("/tmp/blah.tif", "w");
	TIFFSetField(tiff, TIFFTAG_IMAGEWIDTH,  X);
	TIFFSetField(tiff, TIFFTAG_IMAGELENGTH, Y);
	TIFFSetField(tiff, TIFFTAG_SAMPLESPERPIXEL, 1);

	switch (tiff_type)
	{
		case TIFF_TYPE_I8:
			TIFFSetField(tiff, TIFFTAG_SAMPLEFORMAT, SAMPLEFORMAT_INT);
			TIFFSetField(tiff, TIFFTAG_BITSPERSAMPLE, 8);
			break;
		case TIFF_TYPE_U8:
			TIFFSetField(tiff, TIFFTAG_SAMPLEFORMAT, SAMPLEFORMAT_UINT);
			TIFFSetField(tiff, TIFFTAG_BITSPERSAMPLE, 8);
			break;
		case TIFF_TYPE_I16:
			TIFFSetField(tiff, TIFFTAG_SAMPLEFORMAT, SAMPLEFORMAT_INT);
			TIFFSetField(tiff, TIFFTAG_BITSPERSAMPLE, 16);
			break;
		case TIFF_TYPE_U16:
			TIFFSetField(tiff, TIFFTAG_SAMPLEFORMAT, SAMPLEFORMAT_UINT);
			TIFFSetField(tiff, TIFFTAG_BITSPERSAMPLE, 16);
			break;
		case TIFF_TYPE_I32:
			TIFFSetField(tiff, TIFFTAG_SAMPLEFORMAT, SAMPLEFORMAT_INT);
			TIFFSetField(tiff, TIFFTAG_BITSPERSAMPLE, 32);
			break;
		case TIFF_TYPE_FLT:
			TIFFSetField(tiff, TIFFTAG_SAMPLEFORMAT, SAMPLEFORMAT_IEEEFP);
			TIFFSetField(tiff, TIFFTAG_BITSPERSAMPLE, 32);
			break;
	}

	TIFFSetField(tiff, TIFFTAG_ORIENTATION, ORIENTATION_TOPLEFT);
	TIFFSetField(tiff, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_MINISBLACK);

	TIFFSetField(tiff, TIFFTAG_ROWSPERSTRIP, TIFFDefaultStripSize(tiff, X));
	for (y=0; y<Y; y++)
	switch (tiff_type)
	{
		case TIFF_TYPE_I8:  case TIFF_TYPE_U8:  TIFFWriteScanline(tiff, &(tiff_buff.i8 [y*X]), y, 0); break;
		case TIFF_TYPE_I16: case TIFF_TYPE_U16: TIFFWriteScanline(tiff, &(tiff_buff.i16[y*X]), y, 0); break;
		case TIFF_TYPE_I32:                     TIFFWriteScanline(tiff, &(tiff_buff.i32[y*X]), y, 0); break;
		case TIFF_TYPE_FLT:                     TIFFWriteScanline(tiff, &(tiff_buff.flt[y*X]), y, 0); break;
	}
	clock_gettime(CLOCK_REALTIME, &t3);

cleanup:
	if (tiff_buff.p) {free(tiff_buff.p); tiff_buff.p=NULL;}
	if (tiff)        {TIFFClose(tiff);   tiff=NULL;}
	*len = 0;

	/* FIXME: do not use files, but memory! */
	fd = open("/tmp/blah.tif", O_RDONLY);
	if (fd > 0)
	{
		fstat(fd, &st);
		*len = st.st_size;
		buf = (void*) malloc(*len);
		if (read(fd, buf, st.st_size) != st.st_size)
		{
			if (buf) {free(buf); buf=NULL;}
			*len = 0;
		}
		if (fd>0) {close(fd); fd=-1;}
	}

	clock_gettime(CLOCK_REALTIME, &t4);

	tt1 = (t1.tv_sec-t0.tv_sec)+(t1.tv_nsec-t0.tv_nsec)*1e-9;
	tt2 = (t2.tv_sec-t1.tv_sec)+(t2.tv_nsec-t1.tv_nsec)*1e-9;
	tt3 = (t3.tv_sec-t2.tv_sec)+(t3.tv_nsec-t2.tv_nsec)*1e-9;
	tt4 = (t4.tv_sec-t3.tv_sec)+(t4.tv_nsec-t3.tv_nsec)*1e-9;
	if (timing) DADA_RING_PRINTF("tiff-raw-timing: %.1f ms+%.1f ms+%.1f ms+%.1f ms", tt1*1e3, tt2*1e3, tt3*1e3, tt4*1e3);

	return buf;
}

#if 0
/* From: http://stackoverflow.com/questions/1821806/how-to-encode-png-to-buffer-using-libpng */
void png_write_memory(png_structp png_ptr, png_bytep data, png_size_t length)
{
	/* with libpng15 next line causes pointer deference error; use libpng12 */
	struct mem* p = (struct mem*)png_get_io_ptr(png_ptr); /* was png_ptr->io_ptr */
	size_t nsize  = p->size + length;

	/* allocate or grow buffer */
	if (p->buffer)
		p->buffer = realloc(p->buffer, nsize);
	else
		p->buffer = malloc(nsize);

	if (!p->buffer)
		png_error(png_ptr, "Write Error");

	/* copy new bytes to end of buffer */
	memcpy(p->buffer + p->size, data, length);
	p->size += length;
}
void png_flush_memory(__attribute__((unused))png_structp png_ptr)
{
	return;
}
#endif

