/* dada18: output/json.c
 * save 2D dataset in JSON representation
 * */
#include "../dada.h"

char* dada_output_json_1d(struct dada_data* data);
char* dada_output_json_2d(struct dada_data* data);
char* dada_output_json_3d(struct dada_data* data);
char* dada_output_json_4d(struct dada_data* data);

char* dada_output_json(struct dada_data* data)
{
	char* str = NULL;
	FILE* mem = NULL;
	size_t memlen;

	if (data==NULL || data->buffer.p==NULL || data->D==0 || data->d==NULL)
	{
		fprintf(stderr, "[%s:%d] error: empty data.\n", __FILE__, __LINE__);
		fprintf(mem, "[\"empty data\"]\n");
		goto cleanup;
	}

	// DADA_RING_PRINTF("data->D: %d, dims[%d]", data->D, data->d[0]);

	switch (data->D)
	{
		case 1: return dada_output_json_1d(data);
		case 2: return dada_output_json_2d(data);
		case 3: return dada_output_json_3d(data);
		case 4: return dada_output_json_4d(data);
	}

	mem = open_memstream(&str, &memlen);
	fprintf(mem, "[\"unsupported number of dimensions: \", %d]\n", data->D);

cleanup:
	fclose(mem);
	return NULL;
}

char* dada_output_json_1d(struct dada_data* data)
{
	char* str = NULL;
	FILE* mem = NULL;
	size_t memlen;
	int X = data->d[0];
	int x;

	if (data==NULL || data->buffer.p==NULL || data->D==0 || data->d==NULL)
	{
		fprintf(stderr, "[%s:%d] error: empty data.\n", __FILE__, __LINE__);
		fprintf(mem, "[\"empty data\"]\n");
		goto cleanup;
	}

	if (data->D != 1)
	{
		mem = open_memstream(&str, &memlen);
		fprintf(mem, "[\"unsupported number of dimensions: \", %d]\n", data->D);
		goto cleanup;
	}

	mem = open_memstream(&str, &memlen);
	switch (data->type)
	{
		case DADA_DATA_INT:
			fprintf(mem, "[ ");
			for (x=0; x<X; x++)
			{
				fprintf(mem, "%3d", data->buffer.i[x]);
				if (x != X-1) fprintf(mem, ", ");
			}
			fprintf(mem, "]\n");
			break;
		case DADA_DATA_DOUBLE:
			fprintf(mem, "[ ");
			for (x=0; x<X; x++)
			{
				fprintf(mem, "%g", data->buffer.f[x]);
				if (x != X-1) fprintf(mem, ", ");
			}
			fprintf(mem, "]\n");
			break;
		default: fprintf(mem, "[[\"unsupported format\"\n]]"); break;
	}

cleanup:
	fclose(mem);
	return str;
}

char* dada_output_json_2d(struct dada_data* data)
{
	char* str = NULL;
	FILE* mem = NULL;
	size_t memlen;
	int Y = data->d[0];
	int X = data->d[1];
	int x, y;

	if (data==NULL || data->buffer.p==NULL || data->D==0 || data->d==NULL)
	{
		fprintf(stderr, "[%s:%d] error: empty data.\n", __FILE__, __LINE__);
		fprintf(mem, "[\"empty data\"]\n");
		goto cleanup;
	}

	if (data->D != 2)
	{
		mem = open_memstream(&str, &memlen);
		fprintf(mem, "[\"unsupported number of dimensions: \", %d]\n", data->D);
		goto cleanup;
	}

	mem = open_memstream(&str, &memlen);
	switch (data->type)
	{
		case DADA_DATA_INT:
			fprintf(mem, "[\n");
			for (y=0; y<Y; y++)
			{
				fprintf(mem, "    [");
				for (x=0; x<X; x++)
				{
					fprintf(mem, "%3d", data->buffer.i[y*X+x]);
					if (x != X-1) fprintf(mem, ", ");
				}
				if (y != Y-1)
					fprintf(mem, "],\n");
				else
					fprintf(mem, "]\n");
			}
			fprintf(mem, "]\n");
			break;
		case DADA_DATA_DOUBLE:
			fprintf(mem, "[\n");
			for (y=0; y<Y; y++)
			{
				fprintf(mem, "    [");
				for (x=0; x<X; x++)
				{
					fprintf(mem, "%g", data->buffer.f[y*X+x]);
					if (x != X-1) fprintf(mem, ", ");
				}
				if (y != Y-1)
					fprintf(mem, "],\n");
				else
					fprintf(mem, "]\n");
			}
			fprintf(mem, "]\n");
			break;
		default: fprintf(mem, "[[\"unsupported format\"\n]]"); break;
	}

cleanup:
	fclose(mem);
	return str;
}

char* dada_output_json_3d(struct dada_data* data)
{
	char* str = NULL;
	FILE* mem = NULL;
	size_t memlen;
	int X, Y, Z;
	int x, y, z;

	if (data==NULL || data->buffer.p==NULL || data->D==0 || data->d==NULL)
	{
		fprintf(stderr, "[%s:%d] error: empty data.\n", __FILE__, __LINE__);
		fprintf(mem, "[\"empty data\"]\n");
		goto cleanup;
	}

	if (data->D != 3)
	{
		mem = open_memstream(&str, &memlen);
		fprintf(mem, "[\"unsupported number of dimensions: \", %d]\n", data->D);
		goto cleanup;
	}

	Z = data->d[0];
	Y = data->d[1];
	X = data->d[2];

	mem = open_memstream(&str, &memlen);
	fprintf(mem, "[\n");
	for (z=0; z<Z; z++)
	{
		fprintf(mem, "    [\n");
		for (y=0; y<Y; y++)
		{
			fprintf(mem, "        [");
			for (x=0; x<X; x++)
			{
				switch (data->type)
				{
					case DADA_DATA_INT:
						fprintf(mem, "%3d", data->buffer.i[z*X*Y+y*X+x]);
						break;
					case DADA_DATA_DOUBLE:
						fprintf(mem, "%g", data->buffer.f[z*X*Y+y*X+x]);
						break;
					default: fprintf(mem, "[[\"unsupported format\"\n]]"); break;
				}
				if (x != X-1) fprintf(mem, ", ");
			}
			if (y != Y-1)
				fprintf(mem, "],\n");
			else
				fprintf(mem, "]\n");
		}
		if (z != Z-1)
			fprintf(mem, "],\n");
		else
			fprintf(mem, "]\n");
	}
	fprintf(mem, "]\n");

cleanup:
	fclose(mem);
	return str;
}

char* dada_output_json_4d(struct dada_data* data)
{
	char* str = NULL;
	FILE* mem = NULL;
	size_t memlen;
	int YY = data->d[0];
	int XX = data->d[1];
	int Y  = data->d[2];
	int X  = data->d[3];
	int xx, yy, x, y;

	if (data==NULL || data->buffer.p==NULL || data->D==0 || data->d==NULL)
	{
		fprintf(stderr, "[%s:%d] error: empty data.\n", __FILE__, __LINE__);
		fprintf(mem, "[\"empty data\"]\n");
		goto cleanup;
	}

	if (data->D != 4)
	{
		mem = open_memstream(&str, &memlen);
		fprintf(mem, "[\"unsupported number of dimensions: \", %d]\n", data->D);
		goto cleanup;
	}

	mem = open_memstream(&str, &memlen);
	switch (data->type)
	{
		case DADA_DATA_INT:
			fprintf(mem, "[\n");
			for (yy=0; yy<YY; yy++)
			{
				fprintf(mem, "    [\n");
				for (xx=0; xx<XX; xx++)
				{
					fprintf(mem, "        [\n");
					for (y=0; y<Y; y++)
					{
						fprintf(mem, "            [");
						for (x=0; x<X; x++)
						{
							fprintf(mem, "%3d", data->buffer.i[y*X+x]);
							if (x != X-1) fprintf(mem, ", ");
						}
						if (y != Y-1)
							fprintf(mem, "],\n");
						else
							fprintf(mem, "]\n");
					}
					if (xx != XX-1)
						fprintf(mem, "        ],\n");
					else
						fprintf(mem, "        ]\n");
				}
				if (yy != YY-1)
					fprintf(mem, "    ],\n");
				else
					fprintf(mem, "    ]\n");
			}
			fprintf(mem, "]\n");
			break;
		case DADA_DATA_DOUBLE:
			fprintf(mem, "[\n");
			for (yy=0; yy<YY; yy++)
			{
				fprintf(mem, "    [\n");
				for (xx=0; xx<XX; xx++)
				{
					fprintf(mem, "        [\n");
					for (y=0; y<Y; y++)
					{
						fprintf(mem, "            [");
						for (x=0; x<X; x++)
						{
							fprintf(mem, "%g", data->buffer.f[y*X+x]);
							if (x != X-1) fprintf(mem, ", ");
						}
						if (y != Y-1)
							fprintf(mem, "],\n");
						else
							fprintf(mem, "]\n");
					}
					if (xx != XX-1)
						fprintf(mem, "        ],\n");
					else
						fprintf(mem, "        ]\n");
				}
				if (yy != YY-1)
					fprintf(mem, "    ],\n");
				else
					fprintf(mem, "    ]\n");
			}
			fprintf(mem, "]\n");
			break;
		default: fprintf(mem, "[[\"unsupported format\"\n]]"); break;
	}

cleanup:
	fclose(mem);
	return str;
}

