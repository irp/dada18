/* dada18: output/png.c
 * save 2D dataset as PNG image
 * */
#include "../dada.h"

enum dada_colour_palette {
	DADA_COLOUR_PALETTE_TEST1,
	DADA_COLOUR_PALETTE_BW,
	DADA_COLOUR_PALETTE_WB,
	DADA_COLOUR_PALETTE_BWR,
	// DADA_COLOUR_PALETTE_GNUPLOT,
	DADA_COLOUR_PALETTE_HSV,
	DADA_COLOUR_PALETTE_HUSL
};

int dada_colour_rgb(unsigned char* rgb, enum dada_colour_palette pal, double val, char clipmin, char clipmax);
int _dxfHSVtoRGB(float h, float s, float v, unsigned char* red, unsigned char* green, unsigned char* blue);

void png_write_memory(png_structp png_ptr, png_bytep data, png_size_t length);
void png_flush_memory(png_structp png_ptr);

#define LOG10(val) ({ \
    typeof(val) _val = (val); \
    typeof(val) _log = -1; \
    if (val > 0) _log = log10(_val); \
    _log; })

void* dada_output_png(struct dada_data* data, __attribute__((unused))struct dada_data_form* form, size_t* len)
{
	const int timing = 0;

	struct timespec t0, t1, t2, t3, t4;
	double tt1, tt2, tt3, tt4;
	clock_gettime(CLOCK_REALTIME, &t0);

	int x, y;
	int X, Y;
	double min=0, max=1;
	double scale, offset;
	unsigned char* rgb = NULL;
	int n;
	enum dada_colour_palette pal = DADA_COLOUR_PALETTE_TEST1;
	const char* p = NULL;
	char clipmin  = 0;
	char clipmax  = 0;
	char logscale = 0;

	png_structp png_ptr;
	png_infop   info_ptr;
	struct mem state;
	state.buffer = NULL;
	state.size   = 0;
	png_bytep* row_pointers = NULL;

	if (data==NULL || data->buffer.p==NULL || data->D==0 || data->d==NULL || len==NULL)
	{
		fprintf(stderr, "[%s:%d] error: empty data.\n", __FILE__, __LINE__);
		goto cleanup;
	}
	*len = 0;

	for (n=0; n<form->len; n++) if (strcmp(form->key[n], "palette")==0) p = form->val[n];
	if (p && strcmp(p, "test1"  )==0) pal = DADA_COLOUR_PALETTE_TEST1;
	if (p && strcmp(p, "bw"     )==0) pal = DADA_COLOUR_PALETTE_BW;
	if (p && strcmp(p, "wb"     )==0) pal = DADA_COLOUR_PALETTE_WB;
	if (p && strcmp(p, "bwr"    )==0) pal = DADA_COLOUR_PALETTE_BWR;
	// if (p && strcmp(p, "gnuplot")==0) pal = DADA_COLOUR_PALETTE_GNUPLOT;
	if (p && strcmp(p, "hsv"    )==0) pal = DADA_COLOUR_PALETTE_HSV;
	if (p && strcmp(p, "husl"   )==0) pal = DADA_COLOUR_PALETTE_HUSL;

	for (n=0; n<form->len; n++) if (strcmp(form->key[n], "clipping")==0) p = form->val[n];
	if (p && strstr(p, "min")) clipmin = 1;
	if (p && strstr(p, "max")) clipmax = 1;

	for (n=0; n<form->len; n++) if (strcmp(form->key[n], "scale")==0) p = form->val[n];
	if (p && strstr(p, "log")) logscale = 1;

	Y = data->d[0];
	X = data->d[1];
	rgb = (unsigned char*) malloc(sizeof(unsigned char)*3*X*Y);

	switch (data->type)
	{
		case DADA_DATA_INT:    goto okay;
		case DADA_DATA_DOUBLE: goto okay;
		default:
				       fprintf(stderr, "data->type %d not yet supported for output/png", data->type);
				       goto cleanup;
	}

okay:
	clock_gettime(CLOCK_REALTIME, &t1);
	switch (data->type)
	{
		case DADA_DATA_INT:
			min = max = data->buffer.i[0];
			for (y=0; y<Y; y++)
			{
				for (x=0; x<X; x++)
				{
					if (data->buffer.i[y*X+x] > max) max = data->buffer.i[y*X+x];
					if (data->buffer.i[y*X+x] < min) min = data->buffer.i[y*X+x];
				}
			}
			break;
		case DADA_DATA_DOUBLE:
			min = max = data->buffer.f[0];
			for (y=0; y<Y; y++)
			{
				for (x=0; x<X; x++)
				{
					if (data->buffer.f[y*X+x] > max) max = data->buffer.f[y*X+x];
					if (data->buffer.f[y*X+x] < min) min = data->buffer.f[y*X+x];
				}
			}
			break;
		default: break; /* already catched before */
	}
	//DADA_RING_PRINTF("min/max: %g/%g (org)", min, max);
	/* overwrite min/max with supplied struct dada_data_form* */
	for (n=0; n<form->len; n++)
	{
		if (form->key[n] && strcmp(form->key[n], "cmin")==0)
			if (form->val[n] && strlen(form->val[n])>0)
				min = atof(form->val[n]);
		if (form->key[n] && strcmp(form->key[n], "cmax")==0)
			if (form->val[n] && strlen(form->val[n])>0)
				max = atof(form->val[n]);
	}
	//DADA_RING_PRINTF("min/max: %g/%g (user)", min, max);

	/* scale values into [0.0 1.0]; do rgb colour palette */
	if (logscale) max = LOG10(max);
	if (logscale) min = LOG10(min);
	/*
	if (min < 0)
		scale  = 1.0/(max);
	else
		scale  = 1.0/(max-min);
	*/
	scale  = 1.0/(max-min);
	// if (scale < 1) scale = 1;
	if (isinf(scale)) scale = 1;
	offset = -min;
	// printf("min/max: %g/%g -> scale/offset: %g/%g\n", min, max, scale, offset);
	//DADA_RING_PRINTF("min/max: %g/%g -> scale/offset: %g/%g", min, max, scale, offset);
	clock_gettime(CLOCK_REALTIME, &t2);
	switch (data->type)
	{
		case DADA_DATA_INT:
			for (y=0; y<Y; y++)
			{
				for (x=0; x<X; x++)
				{
					int idx = y*X+x;
					double val = data->buffer.i[idx];
					if (logscale) val = LOG10(val);
					val += offset;
					val *= scale;
					dada_colour_rgb(&rgb[3*idx], pal, val, clipmin, clipmax);
				}
			}
			break;
		case DADA_DATA_DOUBLE:
			for (y=0; y<Y; y++)
			{
				for (x=0; x<X; x++)
				{
					int idx = y*X+x;
					double val = data->buffer.f[idx];
					if (logscale) val = LOG10(val);
					val += offset;
					val *= scale;
					dada_colour_rgb(&rgb[3*idx], pal, val, clipmin, clipmax);
				}
			}
			break;
		default: break; /* already catched before */
	}
	clock_gettime(CLOCK_REALTIME, &t3);


	/* finally, do the png */
	row_pointers = (png_bytep*) malloc(sizeof(png_bytep)*Y);

	png_ptr  = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
	info_ptr = png_create_info_struct(png_ptr);
	if (info_ptr == NULL)
	{
		DADA_RING_PRINTF("error: png_create_info_struct failed: %p", info_ptr);
		goto cleanup;
	}
	if (setjmp(png_jmpbuf(png_ptr)))
	{
		DADA_RING_PRINTF("error: png made a long jump for %p", png_ptr);
		goto cleanup;
	}
	png_set_IHDR(png_ptr, info_ptr, (png_uint_32)X, (png_uint_32)Y, 8,
			PNG_COLOR_TYPE_RGB, PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE);
	png_set_write_fn(png_ptr, &state, png_write_memory, png_flush_memory);
	png_write_info(png_ptr, info_ptr);
	for (y=0; y<Y; y++) row_pointers[y] = &rgb[y*X*3];
	png_write_image(png_ptr, row_pointers);
	png_write_end(png_ptr, info_ptr);
	png_destroy_write_struct(&png_ptr, &info_ptr);

cleanup:
	if (row_pointers) {free(row_pointers); row_pointers=NULL;}
	if (rgb) {free(rgb); rgb=NULL;}
	*len = state.size;

	clock_gettime(CLOCK_REALTIME, &t4);

	tt1 = (t1.tv_sec-t0.tv_sec)+(t1.tv_nsec-t0.tv_nsec)*1e-9;
	tt2 = (t2.tv_sec-t1.tv_sec)+(t2.tv_nsec-t1.tv_nsec)*1e-9;
	tt3 = (t3.tv_sec-t2.tv_sec)+(t3.tv_nsec-t2.tv_nsec)*1e-9;
	tt4 = (t4.tv_sec-t3.tv_sec)+(t4.tv_nsec-t3.tv_nsec)*1e-9;
	if (timing) DADA_RING_PRINTF("png-timing: %.1f ms+%.1f ms+%.1f ms+%.1f ms", tt1*1e3, tt2*1e3, tt3*1e3, tt4*1e3);

	return state.buffer;
}

#define COLOURMAP_PALETTE_TEST1(x,h,s,v) \
do { \
    if (1)       h = 0.750*(1.00-x); \
    if (1)       s = 1.000; \
    if (x<1.000) s = 0.718+(1.000-0.718)*(x-0.822)/(1.000-0.822); \
    if (x<0.822) s = 1.000+(0.718-1.000)*(x-0.567)/(0.822-0.567); \
    if (x<0.567) s = 1.000; \
    if (1)       v = 1.000; \
    if (x<1.000) v = 0.668+(1.000-0.668)*(x-0.774)/(1.000-0.774); \
    if (x<0.774) v = 0.628+(0.668-0.628)*(x-0.565)/(0.774-0.565); \
    if (x<0.565) v = 0.368+(0.628-0.368)*(x-0.321)/(0.565-0.321); \
    if (x<0.321) v = 0.481+(0.368-0.481)*(x-0.209)/(0.321-0.209); \
    if (x<0.209) v = 0.481; \
    if (x<0.137) v = 0.000+(0.481-0.000)*(x-0.000)/(0.137-0.000); \
} while (0)

#define COLOURMAP_PALETTE_HSV(x,h,s,v) \
do { \
    if (1)       h = x; \
    if (1)       s = 1.000; \
    if (1)       v = 1.0; \
} while (0)

#define COLOURMAP_PALETTE_HUSL(x, r, g, b) \
do { \
	const float LUT[64][3] = { \
		{0.968,0.340,0.512}, {0.969,0.348,0.450}, {0.970,0.357,0.373}, {0.971,0.365,0.263},  \
		{0.938,0.401,0.172}, {0.883,0.445,0.171}, {0.839,0.474,0.171}, {0.802,0.496,0.170},  \
		{0.770,0.513,0.170}, {0.741,0.527,0.170}, {0.715,0.538,0.170}, {0.690,0.549,0.169},  \
		{0.665,0.558,0.169}, {0.641,0.567,0.169}, {0.617,0.575,0.169}, {0.592,0.583,0.169},  \
		{0.565,0.591,0.169}, {0.536,0.598,0.168}, {0.504,0.606,0.168}, {0.466,0.615,0.168},  \
		{0.421,0.624,0.168}, {0.362,0.633,0.168}, {0.275,0.644,0.167}, {0.168,0.652,0.221},  \
		{0.171,0.648,0.327}, {0.173,0.644,0.391}, {0.175,0.642,0.436}, {0.177,0.639,0.471},  \
		{0.178,0.637,0.500}, {0.180,0.635,0.525}, {0.181,0.633,0.547}, {0.182,0.631,0.568},  \
		{0.183,0.630,0.587}, {0.184,0.628,0.605}, {0.186,0.626,0.623}, {0.187,0.624,0.641},  \
		{0.188,0.622,0.659}, {0.189,0.620,0.678}, {0.191,0.617,0.698}, {0.193,0.615,0.721},  \
		{0.194,0.611,0.746}, {0.197,0.607,0.775}, {0.200,0.602,0.810}, {0.204,0.595,0.853},  \
		{0.209,0.584,0.912}, {0.291,0.567,0.957}, {0.413,0.547,0.957}, {0.500,0.526,0.956},  \
		{0.571,0.503,0.956}, {0.633,0.479,0.956}, {0.691,0.453,0.956}, {0.746,0.422,0.956},  \
		{0.800,0.385,0.956}, {0.855,0.339,0.956}, {0.912,0.274,0.956}, {0.956,0.215,0.937},  \
		{0.958,0.241,0.883}, {0.960,0.262,0.833}, {0.961,0.278,0.788}, {0.963,0.291,0.744},  \
		{0.964,0.303,0.701}, {0.965,0.313,0.657}, {0.966,0.323,0.613}, {0.967,0.332,0.565}  \
	}; \
	int n = round(64*x); \
	if (n <   0) n =  0; \
	if (n >= 64) n = 63; \
	r = 255*(LUT[n][0]); \
	g = 255*(LUT[n][1]); \
	b = 255*(LUT[n][2]); \
} while (0)

int dada_colour_rgb(unsigned char* rgb, enum dada_colour_palette pal, double val, char clipmin, char clipmax)
{
	float h, s, v;

	if (val<0) val = 0;
	if (val>1) val = 1;

	if (clipmin && val <= 0)
	{
		rgb[0] = rgb[1] = rgb[2] = 255;
		return 0;
	}
	if (clipmax && val >= 1)
	{
		rgb[0] = rgb[1] = rgb[2] = 255;
		return 0;
	}

	switch (pal)
	{
		// case DADA_COLOUR_PALETTE_GNUPLOT:
		case DADA_COLOUR_PALETTE_WB:
						rgb[0] = 255*(1-val);
						rgb[1] = 255*(1-val);
						rgb[2] = 255*(1-val);
						break;
		case DADA_COLOUR_PALETTE_BW:
						rgb[0] = 255*val;
						rgb[1] = 255*val;
						rgb[2] = 255*val;
						break;
		case DADA_COLOUR_PALETTE_BWR:
						rgb[0] = 255*( (val<0.5) ? 2*val : 1.0       );
						rgb[1] = 255*( (val<0.5) ? 2*val : 2.0-2*val );
						rgb[2] = 255*( (val<0.5) ? 1.0   : 2.0-2*val );
						break;
		case DADA_COLOUR_PALETTE_HSV:
						COLOURMAP_PALETTE_HSV(val, h, s, v);
						_dxfHSVtoRGB(h, s, v, &rgb[0], &rgb[1], &rgb[2]);
						break;
		case DADA_COLOUR_PALETTE_HUSL:
						COLOURMAP_PALETTE_HUSL(val, rgb[0], rgb[1], rgb[2]);
						break;
		case DADA_COLOUR_PALETTE_TEST1:
						COLOURMAP_PALETTE_TEST1(val, h, s, v);
						_dxfHSVtoRGB(h, s, v, &rgb[0], &rgb[1], &rgb[2]);
						break;
		default: rgb[0] = rgb[1] = rgb[2] = 127;
	}

	return 0;
}

/* From: http://fossies.org/dox/dx-4.4.4/__autocolor_8c_source.html#l01840 */
int _dxfHSVtoRGB(float h, float s, float v, unsigned char* red, unsigned char* green, unsigned char* blue)
/* algorithm is from Fundamentals of Interactive Computer Graphics by Foley and Van Dam, 1984 */
{
	int i;
	float f, p, q, t, valfactor;

	h = h*360.0;

	if (h <   0.0) h = h + ((int)(-h/360)+1) * 360.0;
	if (h > 360.0) h = h - ((int)( h/360)  ) * 360.0;

	if ((s < 0.0)||(s > 1.0)) return -1;
	if (v < 0.0) return -1;

	valfactor = 1.0;

	if (v > 1.0)
	{
		valfactor = v;
		v = 1.0;
	}

	if (s==0.0)
	{
		*red   = 255*(v*valfactor);
		*green = 255*(v*valfactor);
		*blue  = 255*(v*valfactor);
	}
	else
	{
		if (h==360.0) h=0.0;
		h = h/60.0;
		i = (int)h;
		f = h-i;
		p = v*(1.0-s);
		q = v*(1.0-(s*f));
		t = v*(1.0-(s*(1.0-f)));
		switch(i)
		{
			case 0: *red = 255*(v*valfactor); *green=255*(t*valfactor); *blue=255*(p*valfactor); break;
			case 1: *red = 255*(q*valfactor); *green=255*(v*valfactor); *blue=255*(p*valfactor); break;
			case 2: *red = 255*(p*valfactor); *green=255*(v*valfactor); *blue=255*(t*valfactor); break;
			case 3: *red = 255*(p*valfactor); *green=255*(q*valfactor); *blue=255*(v*valfactor); break;
			case 4: *red = 255*(t*valfactor); *green=255*(p*valfactor); *blue=255*(v*valfactor); break;
			case 5: *red = 255*(v*valfactor); *green=255*(p*valfactor); *blue=255*(q*valfactor); break;
		};

	}
	return 0;
}

/* From: http://stackoverflow.com/questions/1821806/how-to-encode-png-to-buffer-using-libpng */
void png_write_memory(png_structp png_ptr, png_bytep data, png_size_t length)
{
	/* with libpng15 next line causes pointer deference error; use libpng12 */
	struct mem* p = (struct mem*)png_get_io_ptr(png_ptr); /* was png_ptr->io_ptr */
	size_t nsize  = p->size + length;

	/* allocate or grow buffer */
	if (p->buffer)
		p->buffer = realloc(p->buffer, nsize);
	else
		p->buffer = malloc(nsize);

	if (!p->buffer)
		png_error(png_ptr, "Write Error");

	/* copy new bytes to end of buffer */
	memcpy(p->buffer + p->size, data, length);
	p->size += length;
}
void png_flush_memory(__attribute__((unused))png_structp png_ptr)
{
	return;
}
