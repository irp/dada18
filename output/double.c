/* dada18: output/double.c
 * returns double array
 * */
#include "../dada.h"

void* dada_output_double(struct dada_data* data, size_t* length)
{
	double* buf = NULL;
	int d, D = 1;

	if (data==NULL || data->buffer.p==NULL || data->D==0 || data->d==NULL)
	{
		fprintf(stderr, "[%s:%d] error: empty data.\n", __FILE__, __LINE__);
		DADA_RING_PRINTF("[%s:%d] error: empty data.", __FILE__, __LINE__);
		goto cleanup;
	}

	switch (data->type)
	{
		case DADA_DATA_INT: break;
		case DADA_DATA_DOUBLE: break;
		default: 
				       DADA_RING_PRINTF("[%s:%d] malloc failed: %s", __FILE__, __LINE__, strerror(errno));
				       goto cleanup;
	}

	/* find total number of pixels */
	for (d=0; d<data->D; d++) D *= data->d[d];

	/* copy data */
	*length = D*sizeof(double);
	buf = (void*) malloc(*length);
	if (buf == NULL)
	{
		DADA_RING_PRINTF("[%s:%d] malloc failed: %s", __FILE__, __LINE__, strerror(errno));
		goto cleanup;
	}

	switch (data->type)
	{
		case DADA_DATA_INT:
			for (d=0; d<D; d++)
				buf[d] = data->buffer.i[d];
			break;
		case DADA_DATA_DOUBLE:
			for (d=0; d<D; d++)
				buf[d] = data->buffer.f[d];
			break;
		default: 
			DADA_RING_PRINTF("[%s:%d] malloc failed: %s", __FILE__, __LINE__, strerror(errno));
			goto cleanup;
	}


cleanup:
	return buf;
}

