/* dada18: output/ascii.c
 * save 2D dataset in ASCII representation
 * */
#include "../dada.h"

char* dada_output_ascii_1d(struct dada_data* data);
char* dada_output_ascii_2d(struct dada_data* data);
char* dada_output_ascii_3d(struct dada_data* data);
char* dada_output_ascii_4d(struct dada_data* data);

char* dada_output_ascii(struct dada_data* data)
{
	char* str = NULL;
	FILE* mem = NULL;
	size_t memlen;

	if (data==NULL || data->buffer.p==NULL || data->D==0 || data->d==NULL)
	{
		fprintf(stderr, "[%s:%d] error: empty data.\n", __FILE__, __LINE__);
		fprintf(mem, "error: empty data\n");
		goto cleanup;
	}

	switch (data->D)
	{
		case 1: return dada_output_ascii_1d(data);
		case 2: return dada_output_ascii_2d(data);
		case 3: return dada_output_ascii_3d(data);
		case 4: return dada_output_ascii_4d(data);
	}

	mem = open_memstream(&str, &memlen);
	fprintf(mem, "unsupported number of dimensions: %d\n", data->D);
	DADA_RING_PRINTF("unsupported number of dimensions: %d", data->D);

cleanup:
	fclose(mem);
	return NULL;
}

char* dada_output_ascii_1d(struct dada_data* data)
{
	char* str = NULL;
	FILE* mem = NULL;
	size_t memlen;
	int X = data->d[0];
	int x;

	if (data==NULL || data->buffer.p==NULL || data->D==0 || data->d==NULL)
	{
		fprintf(stderr, "[%s:%d] error: empty data.\n", __FILE__, __LINE__);
		fprintf(mem, "empty data\n");
		goto cleanup;
	}

	if (data->D != 1)
	{
		mem = open_memstream(&str, &memlen);
		fprintf(mem, "unsupported number of dimensions: %d\n", data->D);
		goto cleanup;
	}

	mem = open_memstream(&str, &memlen);
	switch (data->type)
	{
		case DADA_DATA_INT:
			for (x=0; x<X; x++)
			{
				fprintf(mem, "%3d", data->buffer.i[x]);
				fprintf(mem, "\n");
			}
			break;
		case DADA_DATA_DOUBLE:
			for (x=0; x<X; x++)
			{
				fprintf(mem, "%g", data->buffer.f[x]);
				fprintf(mem, "\n");
			}
			break;
		default: fprintf(mem, "unsupported format\n"); break;
	}

cleanup:
	fclose(mem);
	return str;
}

char* dada_output_ascii_2d(struct dada_data* data)
{
	char* str = NULL;
	FILE* mem = NULL;
	size_t memlen;
	int Y = data->d[0];
	int X = data->d[1];
	int x, y;

	if (data==NULL || data->buffer.p==NULL || data->D==0 || data->d==NULL)
	{
		fprintf(stderr, "[%s:%d] error: empty data.\n", __FILE__, __LINE__);
		fprintf(mem, "empty data\n");
		goto cleanup;
	}

	if (data->D != 2)
	{
		mem = open_memstream(&str, &memlen);
		fprintf(mem, "unsupported number of dimensions: %d\n", data->D);
		goto cleanup;
	}

	mem = open_memstream(&str, &memlen);
	switch (data->type)
	{
		case DADA_DATA_INT:
			for (y=0; y<Y; y++)
			{
				fprintf(mem, "    ");
				for (x=0; x<X; x++)
				{
					fprintf(mem, "%3d", data->buffer.i[y*X+x]);
					if (x != X-1) fprintf(mem, " ");
				}
				if (y != Y-1)
					fprintf(mem, "\n");
				else
					fprintf(mem, "\n");
			}
			fprintf(mem, "\n");
			break;
		case DADA_DATA_DOUBLE:
			for (y=0; y<Y; y++)
			{
				fprintf(mem, "    ");
				for (x=0; x<X; x++)
				{
					fprintf(mem, "%g", data->buffer.f[y*X+x]);
					if (x != X-1) fprintf(mem, " ");
				}
				if (y != Y-1)
					fprintf(mem, "\n");
				else
					fprintf(mem, "\n");
			}
			fprintf(mem, "\n");
			break;
		default: fprintf(mem, "unsupported format\n"); break;
	}

cleanup:
	fclose(mem);
	return str;
}

char* dada_output_ascii_3d(struct dada_data* data)
{
	char* str = NULL;
	FILE* mem = NULL;
	size_t memlen;
	int x,y,z, X,Y,Z;

	if (data==NULL || data->buffer.p==NULL || data->D==0 || data->d==NULL)
	{
		fprintf(stderr, "[%s:%d] error: empty data.\n", __FILE__, __LINE__);
		fprintf(mem, "empty data\n");
		goto cleanup;
	}

	if (data->D != 3)
	{
		mem = open_memstream(&str, &memlen);
		fprintf(mem, "unsupported number of dimensions: %d\n", data->D);
		goto cleanup;
	}

	Z = data->d[0];
	Y = data->d[1];
	X = data->d[2];

	mem = open_memstream(&str, &memlen);
	switch (data->type)
	{
		case DADA_DATA_INT:
			for (z=0; z<Z; z++)
			{
				for (y=0; y<Y; y++)
				{
					for (x=0; x<X; x++)
					{
						fprintf(mem, "%3d", data->buffer.i[z*X*Y+y*X+x]);
						if (x != X-1) fprintf(mem, " ");
					}
					fprintf(mem, "\n");
				}
				fprintf(mem, "\n");
			}
			fprintf(mem, "\n");
			break;
		case DADA_DATA_DOUBLE:
			for (z=0; z<Z; z++)
			{
				for (y=0; y<Y; y++)
				{
					for (x=0; x<X; x++)
					{
						fprintf(mem, "%g", data->buffer.f[z*X*Y+y*X+x]);
						if (x != X-1) fprintf(mem, " ");
					}
					fprintf(mem, "\n");
				}
				fprintf(mem, "\n");
			}
			fprintf(mem, "\n");
			break;
		default: fprintf(mem, "unsupported format\n"); break;
	}

cleanup:
	fclose(mem);
	return str;
}

char* dada_output_ascii_4d(struct dada_data* data)
{
	char* str = NULL;
	FILE* mem = NULL;
	size_t memlen;
	int YY = data->d[0];
	int XX = data->d[1];
	int Y  = data->d[2];
	int X  = data->d[3];
	int xx, yy, x, y;

	if (data==NULL || data->buffer.p==NULL || data->D==0 || data->d==NULL)
	{
		fprintf(stderr, "[%s:%d] error: empty data.\n", __FILE__, __LINE__);
		fprintf(mem, "empty data\n");
		goto cleanup;
	}

	if (data->D != 4)
	{
		mem = open_memstream(&str, &memlen);
		fprintf(mem, "unsupported number of dimensions: %d\n", data->D);
		goto cleanup;
	}

	mem = open_memstream(&str, &memlen);
	switch (data->type)
	{
		case DADA_DATA_INT:
			for (yy=0; yy<YY; yy++)
			{
				for (xx=0; xx<XX; xx++)
				{
					for (y=0; y<Y; y++)
					{
						for (x=0; x<X; x++)
						{
							fprintf(mem, "%3d", data->buffer.i[y*X+x]);
							if (x != X-1) fprintf(mem, " ");
						}
						fprintf(mem, "\n");
					}
					fprintf(mem, "\n");
				}
				fprintf(mem, "\n");
			}
			fprintf(mem, "\n");
			break;
		case DADA_DATA_DOUBLE:
			for (yy=0; yy<YY; yy++)
			{
				for (xx=0; xx<XX; xx++)
				{
					for (y=0; y<Y; y++)
					{
						for (x=0; x<X; x++)
						{
							fprintf(mem, "%g", data->buffer.f[y*X+x]);
							if (x != X-1) fprintf(mem, " ");
						}
						fprintf(mem, "\n");
					}
					fprintf(mem, "\n");
				}
				fprintf(mem, "\n");
			}
			fprintf(mem, "\n");
			break;
		default: fprintf(mem, "unsupported format\n"); break;
	}
	
cleanup:
	fclose(mem);
	return str;
}

