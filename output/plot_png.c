/* dada18: output/plot_png.c
 * render 1D dataset as curve in png
 * */
#include "../dada.h"

void png_write_memory(png_structp png_ptr, png_bytep data, png_size_t length);
void png_flush_memory(png_structp png_ptr);

#define LOG10(val) ({ \
    typeof(val) _val = (val); \
    typeof(val) _log = -1; \
    if (val > 0) _log = log10(_val); \
    _log; })

void* dada_output_plot_png(struct dada_data* data, struct dada_data_form* form, size_t* len)
{
	const int timing = 0;

	struct timespec t0, t1, t2, t3, t4;
	double tt1, tt2, tt3, tt4;
	clock_gettime(CLOCK_REALTIME, &t0);

	int x, y;
	int X, Y; /* plot width / height */
	int xx;
	int XX; /* data width */
	double min=0, max=1;
	double scale, offset;
	unsigned char* rgb = NULL;
	int n;
	char logscale = 0;

	png_structp png_ptr;
	png_infop   info_ptr;
	struct mem state;
	state.buffer = NULL;
	state.size   = 0;
	png_bytep* row_pointers = NULL;

	if (data==NULL || data->buffer.p==NULL || data->D==0 || data->d==NULL || len==NULL)
	{
		fprintf(stderr, "[%s:%d] error: empty data.\n", __FILE__, __LINE__);
		goto cleanup;
	}
	*len = 0;

	if (data->D != 1)
	{
		DADA_RING_PRINTF("data has %d dimensions, only 1 supported.", data->D);
		goto cleanup;
	}

	XX = data->d[0];
	X  = 256; /* width in pixels */
	Y  = 128; /* height in pixels */
	for (n=0; n<form->len; n++)
	{
		if (form->key[n] && strcmp(form->key[n], "plot")==0)
		{
			int parsed;
			int tmp1, tmp2;
			parsed = sscanf(form->val[n], "%d,%d", &tmp1, &tmp2);
			if (parsed != 2) continue;
			X = tmp1;
			Y = tmp2;
		}
	}

	rgb = (unsigned char*) malloc(sizeof(unsigned char)*3*X*Y);
	memset(rgb, 255, 3*X*Y);

	switch (data->type)
	{
		case DADA_DATA_INT:    goto okay;
		case DADA_DATA_DOUBLE: goto okay;
		default:
				       fprintf(stderr, "data->type %d not yet supported for output/png", data->type);
				       goto cleanup;
	}

okay:
	clock_gettime(CLOCK_REALTIME, &t1);
	switch (data->type)
	{
		case DADA_DATA_INT:
			min = max = data->buffer.i[0];
			for (xx=0; xx<XX; xx++)
			{
				if (data->buffer.i[xx] > max) max = data->buffer.i[xx];
				if (data->buffer.i[xx] < min) min = data->buffer.i[xx];
			}
			break;
		case DADA_DATA_DOUBLE:
			min = max = data->buffer.f[0];
			for (xx=0; xx<XX; xx++)
			{
				if (data->buffer.f[xx] > max) max = data->buffer.f[xx];
				if (data->buffer.f[xx] < min) min = data->buffer.f[xx];
			}
			break;
		default: break; /* already catched before */
	}
	// DADA_RING_PRINTF("min/max: %g/%g (org)", min, max);
	/* overwrite min/max with supplied struct dada_data_form* */
	for (n=0; n<form->len; n++)
	{
		if (form->key[n] && strcmp(form->key[n], "cmin")==0)
			if (form->val[n] && strlen(form->val[n])>0)
				min = atof(form->val[n]);
		if (form->key[n] && strcmp(form->key[n], "cmax")==0)
			if (form->val[n] && strlen(form->val[n])>0)
				max = atof(form->val[n]);
	}
	// DADA_RING_PRINTF("min/max: %g/%g (user)", min, max);

	/* scale values into [0.0 1.0]; do rgb colour palette */
	if (logscale) max = LOG10(max);
	if (logscale) min = LOG10(min);
	/*
	if (min < 0)
		scale  = 1.0/(max);
	else
		scale  = 1.0/(max-min);
	*/
	scale  = 1.0/(max-min);
	// if (scale < 1) scale = 1;
	if (isinf(scale)) scale = 1;
	offset = -min;
	// printf("min/max: %g/%g -> scale/offset: %g/%g\n", min, max, scale, offset);
	// DADA_RING_PRINTF("min/max: %g/%g -> scale/offset: %g/%g", min, max, scale, offset);
	clock_gettime(CLOCK_REALTIME, &t2);
	switch (data->type)
	{
		case DADA_DATA_INT:
			xx = 0;
			for (x=0; x<X; x++)
			{
				for (; xx<=(x+1)*XX/X && xx<XX; xx++)
				{
					double val = data->buffer.i[xx];
					if (logscale) val = LOG10(val);
					// DADA_RING_PRINTF("val: %lf", val);
					val += offset;
					val *= scale*Y;
					y = (int)round(val);
					y = Y-(int)round(val);
					// DADA_RING_PRINTF("org: %lf val: %lf y: %d", data->buffer.f[idx], val, y);
					if (y<0  ) y=0  ;
					if (y>Y-1) y=Y-1;
					rgb[3*y*X+3*x+0] = 0;
					rgb[3*y*X+3*x+1] = 0;
					rgb[3*y*X+3*x+2] = 0;
				}
			}
			break;
		case DADA_DATA_DOUBLE:
			xx = 0;
			for (x=0; x<X; x++)
			{
				for (; xx<=(x+1)*XX/X && xx<XX; xx++)
				{
					double val = data->buffer.f[xx];
					if (logscale) val = LOG10(val);
					// DADA_RING_PRINTF("val: %lf", val);
					val += offset;
					val *= scale*Y;
					y = Y-(int)round(val);
					// DADA_RING_PRINTF("org: %lf val: %lf y: %d", data->buffer.f[idx], val, y);
					if (y<0  ) y=0  ;
					if (y>Y-1) y=Y-1;
					rgb[3*y*X+3*x+0] = 0;
					rgb[3*y*X+3*x+1] = 0;
					rgb[3*y*X+3*x+2] = 0;
				}
			}
			break;
		default: break; /* already catched before */
	}
	clock_gettime(CLOCK_REALTIME, &t3);


	/* finally, do the png */
	row_pointers = (png_bytep*) malloc(sizeof(png_bytep)*Y);

	png_ptr  = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
	info_ptr = png_create_info_struct(png_ptr);
	if (info_ptr == NULL)
	{
		DADA_RING_PRINTF("error: png_create_info_struct failed: %p", info_ptr);
		goto cleanup;
	}
	if (setjmp(png_jmpbuf(png_ptr)))
	{
		DADA_RING_PRINTF("error: png made a long jump for %p", png_ptr);
		goto cleanup;
	}
	png_set_IHDR(png_ptr, info_ptr, (png_uint_32)X, (png_uint_32)Y, 8,
			PNG_COLOR_TYPE_RGB, PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE);
	png_set_write_fn(png_ptr, &state, png_write_memory, png_flush_memory);
	png_write_info(png_ptr, info_ptr);
	for (y=0; y<Y; y++) row_pointers[y] = &rgb[y*X*3];
	png_write_image(png_ptr, row_pointers);
	png_write_end(png_ptr, info_ptr);
	png_destroy_write_struct(&png_ptr, &info_ptr);

cleanup:
	if (row_pointers) {free(row_pointers); row_pointers=NULL;}
	if (rgb) {free(rgb); rgb=NULL;}
	*len = state.size;

	clock_gettime(CLOCK_REALTIME, &t4);

	tt1 = (t1.tv_sec-t0.tv_sec)+(t1.tv_nsec-t0.tv_nsec)*1e-9;
	tt2 = (t2.tv_sec-t1.tv_sec)+(t2.tv_nsec-t1.tv_nsec)*1e-9;
	tt3 = (t3.tv_sec-t2.tv_sec)+(t3.tv_nsec-t2.tv_nsec)*1e-9;
	tt4 = (t4.tv_sec-t3.tv_sec)+(t4.tv_nsec-t3.tv_nsec)*1e-9;
	if (timing) DADA_RING_PRINTF("png-timing: %.1f ms+%.1f ms+%.1f ms+%.1f ms", tt1*1e3, tt2*1e3, tt3*1e3, tt4*1e3);

	return state.buffer;
}

/* From: http://stackoverflow.com/questions/1821806/how-to-encode-png-to-buffer-using-libpng */
void png_write_memory(png_structp png_ptr, png_bytep data, png_size_t length)
{
	/* with libpng15 next line causes pointer deference error; use libpng12 */
	struct mem* p = (struct mem*)png_get_io_ptr(png_ptr); /* was png_ptr->io_ptr */
	size_t nsize  = p->size + length;

	/* allocate or grow buffer */
	if (p->buffer)
		p->buffer = realloc(p->buffer, nsize);
	else
		p->buffer = malloc(nsize);

	if (!p->buffer)
		png_error(png_ptr, "Write Error");

	/* copy new bytes to end of buffer */
	memcpy(p->buffer + p->size, data, length);
	p->size += length;
}
void png_flush_memory(__attribute__((unused))png_structp png_ptr)
{
	return;
}

