#ifndef _DADA_WEB_POST_H_
#define _DADA_WEB_POST_H_

#define _GNU_SOURCE

#include "dada.h"

#include "dada_cluster.h"

#define POSTBUFFERSIZE 16384

int dada_web_post(struct connection_meta* meta, const char* url);

int dada_web_post_nodeinfo(struct connection_meta* meta, const char* hostport);

int dada_web_post_snap(struct connection_meta* meta, const char* url);
int dada_web_post_snap_processor(void* coninfo_cls, enum MHD_ValueKind kind, const char* key, const char* filename, const char* content_type, const char* transfer_encoding, const char* data, uint64_t off, size_t size);

int dada_web_post_glue_stxm(struct connection_meta* meta, const char* url);
int dada_web_post_glue_stxm_processor(void* coninfo_cls, enum MHD_ValueKind kind, const char* key, const char* filename, const char* content_type, const char* transfer_encoding, const char* data, uint64_t off, size_t size);

int dada_glue_stxm_parse(struct dada_web_post_glue* glue);
int dada_glue_stxm_process(struct dada_web_post_glue* glue);
int dada_glue_stxm_putcache(struct dada_web_post_glue* glue);
void dada_glue_stxm_free(struct dada_web_post_glue* glue);

int dada_web_error(struct connection_meta* meta, int status);

#define WEB_POST_WHAT(func, what1, what2, meta, url) \
do { \
    if (strncmp(url, what1, strlen(what1)) == 0) { \
	const char* url2 = url + strlen(what2); \
	ret = func(meta, url2); \
	goto cleanup; \
    } \
} while (0)

#define WEB_RESPONSE_STRCPY(response, str) \
do { \
    response = MHD_create_response_from_buffer(strlen(str), (void*)str, MHD_RESPMEM_MUST_COPY); \
} while (0)

#define WEBSERVER_QUEUE_MIME(meta, status, response, mime) \
do { \
    MHD_add_response_header(response, "Content-Type", mime); \
    MHD_queue_response(meta->conn, status, response); \
} while (0)
#endif

