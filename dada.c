/* dada
 * data daemon, show and analyse x-ray data
 * Markus Osterhoff <mosterh1@gwdg.de>
 * Institut für Röntgenphysik Göttingen
 * version 2018, started October 2017
 *
 * */

#include "dada.h"

#include "dada_cluster.h"
#include "dada_aero.h"

int dada_running = 0;
char top_buf[256];
struct dada_cluster_node node;
struct dada_backend_aero aero;
struct dada_backend_aero_host aero_host;

int main(int argc, const char* argv[])
{
	int ret      =    0;
	int port     = 8080;
	int uid      =   -1;
	int gid      =    0;
	int back     =    0;
	pid_t pid    =    0;
	dada_running =    0;
	char pidfile[256];
	int c;

	if (argc > 8
			|| (argc > 1 && strcmp(argv[1], "-h")==0)
			|| (argc > 1 && strcmp(argv[1], "-?")==0)
			|| (argc > 1 && strcmp(argv[1], "--help")==0)
		)
	{
		fprintf(stderr, "usage: %s [-p port] [-u uid] [-g gid] [-d|--daemon]\n", argv[0]);
		ret = -1;
		goto forked;
	}
	for (c=1; c<argc; c++)
	{
		if (strcmp(argv[c], "-p")==0) port = atoi(argv[c+1]);
		if (strcmp(argv[c], "-u")==0) uid  = atoi(argv[c+1]);
		if (strcmp(argv[c], "-g")==0) gid  = atoi(argv[c+1]);
	}
	for (c=1; c<argc; c++)
		if (strcmp(argv[c], "-d")==0 || strcmp(argv[c], "--daemon")==0) back = 1;
	if (back == 1)
	{
		back = 0;
		pid = fork();
		if (pid<0)
		{
			fprintf(stderr, "cannot fork: %s\n", strerror(errno));
			ret = -1;
			goto forked;
		}
		if (pid>0)
		{
			FILE* f = NULL;
			snprintf(pidfile, sizeof(pidfile)-1, "/var/run/%s:%d.pid", "dada", port);
			f = fopen(pidfile, "w");
			if (f)
			{
				fprintf(f, "%d\n", pid);
				fclose(f);
				f = NULL;
			}
			fprintf(stdout, "forking into background\n");
			fprintf(stdout, "\tuid: %d\n", uid);
			fprintf(stdout, "\tgid: %d\n", gid);
			fprintf(stdout, "\tpid: %d\n", pid);
			fprintf(stdout, "\tpidfile: %s\n", pidfile);
			fprintf(stdout, "\n");
			goto forked;
		}
		if (pid==0)
		{
			back=1;
		}
	}

	if (getuid() == 0 && uid != -1) /* drop priviliges */
	{
		gid_t glist[] = {30001};

		if (setgroups(sizeof(glist), glist) != 0)
		{
			fprintf(stderr, "cannot set supplementary groups: %s\n", strerror(errno));
			ret = -1;
			goto cleanup;
		}
		if (setgid(gid) != 0)
		{
			fprintf(stderr, "cannot drop group priviliges: %s\n", strerror(errno));
			ret = -1;
			goto cleanup;
		}
		if (setuid(uid) != 0)
		{
			fprintf(stderr, "cannot drop user priviliges: %s\n", strerror(errno));
			ret = -1;
			goto cleanup;
		}
		if (setuid(0) != -1)
		{
			fprintf(stderr, "could re-gain root priviliges!\n");
			ret = -1;
			goto cleanup;
		}
		if (chdir("/home/dada/g/dada18") != 0)
		{
			fprintf(stderr, "could not chdir to /home/dada/g/dada18: %s\n", strerror(errno));
			ret = -1;
			goto cleanup;
		}
	}


	ret = dada_ringbuffer_init();
	if (ret != 0) goto err_ringbuffer;

	startup(argv[0]);
	curl_global_init(CURL_GLOBAL_NOTHING);
	CRYPTO_cleanup_all_ex_data();

	ret = webserver_init(port);
	if (ret != 0) goto cleanup;
	dada_running = 1;
	signal(SIGINT, dada_signal_handler);
	fprintf(stdout, "dada-%s is now running and listening on port %d.\n", GIT_VERSION, port);
	showlink(port);

	top_loop(port);

cleanup:
	DADA_RING_PRINTF("%s: cleanup", __FUNCTION__);
	fprintf(stdout, "dada will now finish ..."); fflush(stdout);
	aerospike_close(&aero.aer, &aero.err);
	aerospike_destroy(&aero.aer);
	curl_global_cleanup();
	webserver_destroy();
	fprintf(stdout, "\ndada has quit.\n");

	fprintf(stdout, "RING BUFFER\n");
	fprintf(stdout, "-----------\n");
	dada_ringbuffer_dump(stdout);
	dada_ringbuffer_destroy();
	fprintf(stdout, "-----------\n");

forked:
	pthread_exit(NULL);
	return ret;

err_ringbuffer:
	fprintf(stderr, "\nOops, cannot initialise ringbuffer.\n\n");
	exit(-1);
}

void dada_signal_handler(int sig)
{
	if (sig == SIGINT) dada_running = 0;
}

int webserver_init(int port)
{
	mhd_daemon = MHD_start_daemon(
			MHD_USE_THREAD_PER_CONNECTION,
			port, NULL, NULL,
			&webserver_connection, NULL,
			MHD_OPTION_NOTIFY_COMPLETED, webserver_completed, NULL,
			MHD_OPTION_END);

	if (mhd_daemon == NULL)
		goto error;

	DADA_RING_PRINTF("webserver listening on port %d", port);
	return 0;
error:
	DADA_RING_PRINTF("webserver_init: cannot start webserver on port %d", port);
	return -1;
}

int webserver_destroy()
{
	if (mhd_daemon)
	{
		MHD_stop_daemon(mhd_daemon);
		mhd_daemon = NULL;
	}
	return 0;
}

int webserver_connection(__attribute__((unused))void* cls, struct MHD_Connection* conn,
		const char* url, const char* method, __attribute__((unused))const char* version,
		const char* data, size_t* len, void** con_cls)
{
	int ret = MHD_YES;
	int status = MHD_HTTP_INTERNAL_SERVER_ERROR;
	const char* err = NULL;
	const char* mime = "text/html; charset=UTF-8";
	struct MHD_Response* response = NULL;
	void* so_handle = NULL;
	int (*so_funcp)(struct connection_meta* meta, const char* url) = NULL;
	static int connid = 0;
	char s[16];
	struct connection_meta meta;

	clock_gettime(CLOCK_REALTIME, &meta.t0);
	meta.canonical = NULL;
	meta.seconds   = 0.0;
	meta.conn      = conn;
	meta.connid    = connid++;
	meta.xmeta     = NULL;
	meta.xradial   = NULL;
	meta.raeum     = NULL;
	DADA_MEMSTREAM(meta.mem, meta.msg, meta.memlen);

	snprintf(s, sizeof(s)-1, "dada_%08lx", meta.connid);
	prctl(PR_SET_NAME, s);


	if (strcmp(url, "/info/top")!=0 && strstr(url, "/nodeinfo")==NULL)
		fprintf(stdout, "[web%06ld] %s %s\n", meta.connid, method, url);


	if (strcmp(method, "GET" )==0) goto get;
	if (strcmp(method, "POST")==0) goto post;

	/* method is unsupported */
	err = "<!DOCTYPE html>\n<html><head><title>Method Not Allowed</title></head><body><h1>Method Not Allowed</h1><p>Only GET method and POST methods are currently implemented.</p></body></html>\n";
	status = MHD_HTTP_METHOD_NOT_ALLOWED;
	response = MHD_create_response_from_buffer(strlen(err), (void*)err, MHD_RESPMEM_PERSISTENT);
	MHD_queue_response(conn, status, response);
	MHD_add_response_header(response, "Content-Type", mime);
	MHD_add_response_header(response, "Allow", "GET,POST");
	MHD_destroy_response(response);
	goto cleanup;


	/* call GET dispatcher */
get:
	*con_cls = NULL;
	DADA_DL_LOAD_SYMBOL(DADA_DL_PATH_CONST("./dada_web_get"), "dada_web_get", so_funcp, so_handle);
	ret = so_funcp(&meta, url);
	DADA_DL_CLOSE(so_handle);
	if (meta.raeum && meta.raeum->stxm) *con_cls = (void*)meta.raeum;
	goto cleanup;

	/* call POST dispatcher */
post:
	meta.raeum = (struct dada_web_aufraeumer*) *con_cls;
	if (meta.raeum && meta.raeum->type == DADA_WEB_AUFRAEUMER_POST && meta.raeum->post)
	{
		meta.raeum->post->data = data;
		meta.raeum->post->len  = len;
		so_funcp  = meta.raeum->post->so_funcp;
		so_handle = meta.raeum->post->so_handle;
	}
	else
	{
		DADA_DL_LOAD_SYMBOL(DADA_DL_PATH_CONST("./dada_web_post"), "dada_web_post", so_funcp, so_handle);
	}

	ret = so_funcp(&meta, url);
	if (meta.raeum->post) *con_cls = (void*)meta.raeum;
	if (meta.raeum->post)
	{
		if (0) fprintf(stdout, "saving so_handle %p\n", so_handle);
		meta.raeum->post->so_handle = so_handle;
		meta.raeum->post->so_funcp  = so_funcp;
	}
	goto cleanup;

cleanup:
	fclose(meta.mem);
	FREE(meta.canonical);
	FREE(meta.msg);
	FREE(meta.xmeta);
	FREE(meta.xradial);
	if (0 && strcmp(url, "/info/top")!=0 && strstr(url, "/nodeinfo")==NULL)
		fprintf(stdout, "\n");
	// fprintf(stdout, "DEBUG %s:%d %s\n", __FILE__, __LINE__, url);
	// sleep(5);
	// fprintf(stdout, "DEBUG %s:%d %s\n", __FILE__, __LINE__, url);
	return ret;

error:
	*con_cls = NULL;
	err = "<!DOCTYPE html>\n<html><head><title>Internal Server Error</title></head><body><h1>Internal Server Error</h1><p>Sorry, something really bad happened.</p></body></html>\n";
	status = MHD_HTTP_INTERNAL_SERVER_ERROR;
	response = MHD_create_response_from_buffer(strlen(err), (void*)err, MHD_RESPMEM_PERSISTENT);
	MHD_queue_response(conn, status, response);
	MHD_add_response_header(response, "Content-Type", mime);
	MHD_destroy_response(response);
	goto cleanup;

err_memstream:
	goto error;
}

void* webserver_aufraeumer(void* _raeum)
{
	struct dada_web_aufraeumer* raeum = (struct dada_web_aufraeumer*)_raeum;
	struct dada_web_get_stxm*   stxm  = raeum->stxm;
	struct dada_data*           data  = NULL;

	pthread_detach(pthread_self());

	// fprintf(stdout, "*** pthread_join thr is %ld ***\n", stxm->thr);
	if (stxm->thr) pthread_join(stxm->thr, (void**)&data);
	if (data) {dada_data_destroy(data); free(data); data=NULL;}

	dada_data_path_destroy(&stxm->path);
	dada_data_modi_destroy(&stxm->modi);
	dada_data_form_destroy(&stxm->form);
	DADA_DL_CLOSE(stxm->so_handle);

	if (stxm)  {free(stxm);  stxm=NULL;}
	if (raeum) {free(raeum); raeum=NULL;}

	// *con_cls = NULL;

	return NULL;
}

void webserver_completed(__attribute__((unused))void* cls, __attribute__((unused))struct MHD_Connection* conn, void** con_cls, __attribute__((unused))enum MHD_RequestTerminationCode code)
{
	// fprintf(stdout, "webserver_completed: con_cls %p %p\n", con_cls, con_cls?*con_cls:NULL);

	if (con_cls == NULL || *con_cls == NULL) return;

	struct dada_web_aufraeumer* raeum = (struct dada_web_aufraeumer*) *con_cls;

	struct dada_web_get_stxm*  stxm  = NULL;
	struct dada_web_post_post* post  = NULL;

	// fprintf(stdout, "raeum->type: %d\n", raeum->type);

	switch (raeum->type)
	{
		case DADA_WEB_AUFRAEUMER_STXM:
			stxm = raeum->stxm;
			if (stxm)
			{
				pthread_t thr;
				if (pthread_create(&thr, NULL, webserver_aufraeumer, (void*)raeum) != 0)
				{
					struct dada_data* data = NULL;
					// fprintf(stdout, "DEBUG %s:%d\n", __FILE__, __LINE__);
					/* waiting for STXM job to finish ... */
					// fprintf(stdout, "*** pthread_join thr is %ld ***\n", stxm->thr);
					if (stxm->thr) pthread_join(stxm->thr, (void**)&data);
					if (data) {dada_data_destroy(data); free(data); data=NULL;}
					// fprintf(stdout, "DEBUG %s:%d\n", __FILE__, __LINE__);
					dada_data_path_destroy(&stxm->path);
					dada_data_modi_destroy(&stxm->modi);
					dada_data_form_destroy(&stxm->form);
					DADA_DL_CLOSE(stxm->so_handle);
					// fprintf(stdout, "DEBUG %s:%d\n", __FILE__, __LINE__);
					if (stxm)  {free(stxm);  stxm=NULL;}
					if (raeum) {free(raeum); raeum=NULL;}
					*con_cls = NULL;
				}
			}
			break;
		case DADA_WEB_AUFRAEUMER_POST:
			post = raeum->post;
			if (post == NULL) return;

			if (0) fprintf(stdout, "*** webserver_completed ***\n");
			if (0) fprintf(stdout, "\tpost @%p\n", post);

			if (0) fprintf(stdout, "closing so_handle %p\n", post->so_handle);
			if (post->so_handle) {DADA_DL_CLOSE(post->so_handle); post->so_handle=NULL; post->so_funcp=NULL;}

			if (post)
			{
				if (post->glue)
				{
					if (post->glue->poster) {MHD_destroy_post_processor(post->glue->poster); post->glue->poster=NULL;}
					if (post->glue) {free(post->glue); post->glue=NULL;}
				}
				if (post->snap)
				{
					if (post->snap->poster) {MHD_destroy_post_processor(post->snap->poster); post->snap->poster=NULL;}
					if (post->snap) {free(post->snap); post->snap=NULL;}
				}
				if (post) {free(post); post=NULL;}
				*con_cls = NULL;
			}
			break;
	}
}

int startup(const char* argv0)
{
	int ret = 0;
	int n;
	pid_t pid, ppid;
	uid_t uid, euid;
	uid_t gid, egid;
	char cwd[256];
	char host[256];
	mode_t umsk;
	struct utsname utsname;
	struct sysinfo sinfo;

	/* aerospike servers */
	aero.hostc = 1;
	aero_host.host = "aero";
	aero_host.port = 3000;
	aero.hosts = &aero_host;

	/* http://www.fftw.org/fftw3_doc/Thread-safety.html */
	fftw_make_planner_thread_safe();

	pid  = getpid();
	ppid = getppid();
	uid  = getuid();
	euid = geteuid();
	gid  = getgid();
	egid = getegid();
	umsk = umask(0777);
	umask(umsk);
	if (getcwd     (cwd,  sizeof(cwd )-1) == NULL) goto error;
	if (gethostname(host, sizeof(host)-1) != 0   ) goto error;
	uname(&utsname);
	sysinfo(&sinfo);

	DADA_RING_PRINTF("%s-%s is starting ...", argv0, GIT_VERSION);
	DADA_RING_PRINTF("pid: %d, ppid: %d", pid, ppid);
	DADA_RING_PRINTF("uid: %d, euid: %d", uid, euid);
	DADA_RING_PRINTF("gid: %d, egid: %d", gid, egid);
	DADA_RING_PRINTF("umask is 0%03o", umsk);
	DADA_RING_PRINTF("cwd is %s", cwd);
	DADA_RING_PRINTF("host is %s", host);
	DADA_RING_PRINTF("system: %s-%s on %s",
			utsname.sysname, utsname.release, utsname.machine);
	DADA_RING_PRINTF("ram: %.3f GB",
			1.0*sinfo.totalram*sinfo.mem_unit/1073741824);

	prctl(PR_SET_DUMPABLE, 1);
	DADA_RING_PRINTF("core dump enabled: %s.", (prctl(PR_GET_DUMPABLE)>0?"yes":"no"));

	/* config */
	as_config_init(&aero.cfg);
	for (n=0; n<aero.hostc; n++)
	{
		DADA_RING_PRINTF("connecting to aerospike on %s:%d", aero.hosts[n].host, aero.hosts[n].port);
		as_config_add_host(&aero.cfg, aero.hosts[n].host, aero.hosts[n].port);
	}

	/* init, connect, and check for key */
	if (aerospike_init(&aero.aer, &aero.cfg) == NULL)
	{
		fprintf(stderr, "error: aerospike_init failed.\n");
		DADA_RING_PRINTF("%s: aerospike_init failed", __FUNCTION__);
		goto no_aero;
	}
	if (aerospike_connect(&aero.aer, &aero.err) != AEROSPIKE_OK)
	{
		fprintf(stderr, "warning: aerospike_connect failed (%d:%s)\n", aero.err.code, aero.err.message);
		DADA_RING_PRINTF("warning: aerospike_connect: %s", aero.err.message);
		goto no_aero;
	}
	DADA_RING_PRINTF("aerospike_connect: %s", aero.err.message);

no_aero:
	pthread_mutex_init(&hdf5_mutex, NULL);

	return ret;

error:
	fprintf(stderr, "fatal error during startup().\n");
	fprintf(stderr, "[%s]\n", strerror(errno));
	exit(-1);
}

/* showlink:
 * show URLs to webgui for all recognised IP addresses
 */
int showlink(int port)
{
	int ret = 0;
	struct ifaddrs* ifaddr = NULL;
	struct ifaddrs* ifa;
	int family, s, n;
	char host[NI_MAXHOST];
	char domname[256];

	if (getifaddrs(&ifaddr) == -1)
	{
		fprintf(stderr, "cannot get if addrs: %s\n", strerror(errno));
		ret = -1;
		goto cleanup;
	}

	for (ifa=ifaddr, n=0; ifa != NULL; ifa=ifa->ifa_next, n++)
	{
		if (ifa->ifa_addr == NULL) continue;
		family = ifa->ifa_addr->sa_family;
		if (family==AF_INET || family==AF_INET6)
		{
			s = getnameinfo(ifa->ifa_addr,
					(family==AF_INET)?sizeof(struct sockaddr_in):sizeof(struct sockaddr_in6),
					host, NI_MAXHOST, NULL, 0, NI_NUMERICHOST);
			if (s != 0) continue;
			fprintf(stdout, "\taccess the web gui: http://%s:%d/static/index.html\n", host, port);
		}
	}
	if (gethostname(host, sizeof(host)-1) != 0) goto cleanup;
	fprintf(stdout, "\taccess the web gui: http://%s:%d/static/index.html\n", host, port);
	if (strchr(host, '.')) goto cleanup; /* hostname has a dot, so it might include domain name */
	if (getdomainname(domname, sizeof(domname)-1) != 0) goto cleanup;
	if (strcmp(domname, "(none)")==0) goto cleanup; /* no domain name found */
	fprintf(stdout, "\taccess the web gui: http://%s.%s:%d/static/index.html\n", host, domname, port);

cleanup:
	if (ifaddr) {freeifaddrs(ifaddr); ifaddr=NULL;}
	return ret;
}

int top_loop(int port)
{
	FILE* f = NULL;
	const char* fname1 = "/proc/self/stat";
	const char* fname2 = "/proc/self/io";
	long number;
	char text[64];
	unsigned long utime=0, stime=0;
	int prio, nice, Nthrds;
	unsigned long vsize, rss = 0;;
	double now_cpu, now_time;
	double lst_cpu, lst_time;
	double del_cpu, del_time;
	struct timespec t0;
	unsigned long read_bytes=0, write_bytes=0;
	double now_read, now_writ;
	double lst_read, lst_writ;
	double del_read, del_writ;
	char host[63];
	pid_t pid = getpid();
	if (gethostname(host, sizeof(host)-1) != 0   ) goto err;
	if (strchr(host, '.')) *strchr(host, '.') = '\0';

	snprintf(node.host, sizeof(node.host)-1, "%s", host);
	node.port      = port;
	node.pid       = pid;
	node.last.stxm = 0;
	snprintf(node.version, sizeof(node.version)-1, "%s", GIT_VERSION);
	node.cpu       = -1;
	node.mem       = 4096.0*rss/1048576;
	node.next      = NULL; /* only needed for HTTP nodeinfo */

	f = fopen(fname1, "r");
	if (f == NULL) goto err;

	clock_gettime(CLOCK_MONOTONIC, &t0);

	snprintf(top_buf, sizeof(top_buf)-1, "[%-16s:%5d] %8.3f MiB, %5.1f %%, %.2f MiB/s, %.2f MiB/s",
			host, pid, 0.0, 0.0, 0.0, 0.0);

	while (dada_running)
	{
		int ret = 0;
		sleep(2);

		lst_cpu  = 1.0*(utime+stime)/sysconf(_SC_CLK_TCK);
		lst_time = t0.tv_sec + 1e-9*t0.tv_nsec;
		clock_gettime(CLOCK_MONOTONIC, &t0);
		now_time = t0.tv_sec + 1e-9*t0.tv_nsec;
		del_time = now_time - lst_time;

		lst_read = read_bytes;
		lst_writ = write_bytes;

		if (f) { fclose(f); f = NULL; }
		f = fopen(fname1, "r");
		if (f == NULL) goto err;

		ret += fscanf(f, "%ld", &number);
		ret += fscanf(f, "%63s", text);
		ret += fscanf(f, "%63s", text);
		ret += fscanf(f, "%ld", &number);
		ret += fscanf(f, "%ld", &number);
		ret += fscanf(f, "%ld", &number);
		ret += fscanf(f, "%ld", &number);
		ret += fscanf(f, "%ld", &number);
		ret += fscanf(f, "%ld", &number);
		ret += fscanf(f, "%ld", &number);
		ret += fscanf(f, "%ld", &number);
		ret += fscanf(f, "%ld", &number);
		ret += fscanf(f, "%ld", &number);
		ret += fscanf(f, "%ld", &utime);
		ret += fscanf(f, "%ld", &stime);
		ret += fscanf(f, "%lu", &number);
		ret += fscanf(f, "%lu", &number);
		ret += fscanf(f, "%d",  &prio);
		ret += fscanf(f, "%d",  &nice);
		ret += fscanf(f, "%d",  &Nthrds);
		ret += fscanf(f, "%ld", &number);
		ret += fscanf(f, "%ld", &number);
		ret += fscanf(f, "%lu", &vsize);
		ret += fscanf(f, "%lu", &rss);

		if (f) { fclose(f); f = NULL; }
		f = fopen(fname2, "r");
		if (f == NULL) goto err;

		ret += fscanf(f, "rchar: %ld\n", &number);
		ret += fscanf(f, "wchar: %ld\n", &number);
		ret += fscanf(f, "syscr: %ld\n", &number);
		ret += fscanf(f, "syscw: %ld\n", &number);
		ret += fscanf(f, "read_bytes: %ld\n", &read_bytes);
		ret += fscanf(f, "write_bytes: %ld\n", &write_bytes);

		if (ret != 30)
		{
			if (f) { fclose(f); f = NULL; }
			DADA_RING_PRINTF("parsing %s or %s failed.", fname1, fname2);
			continue;
		}

		now_read = read_bytes;
		now_writ = write_bytes;
		del_read = now_read-lst_read;
		del_writ = now_writ-lst_writ;

		now_cpu  = 1.0*(utime+stime)/sysconf(_SC_CLK_TCK);
		del_cpu  = now_cpu  - lst_cpu;
		snprintf(top_buf, sizeof(top_buf)-1, "[%-16s:%5d] %8.3f MiB, %5.1f %%, %.2f MiB/s, %.2f MiB/s", 
				host, pid,
				4096.0*rss/1048576, 100.0*del_cpu/del_time,
				1.0*del_read/1048576/del_time, 1.0*del_writ/1048576/del_time);
		// fprintf(stdout, "\r%s\n", top_buf);
		// fflush(stdout);

		/* now send node info to aerospike */
		node.cpu       = 100.0*del_cpu/del_time;
		node.mem       = 4096.0*rss/1048576;
		dada_cluster_node_update(&node);
	}

cleanup:
	if (f) { fclose(f); f = NULL; }
	return 0;

err:
	fprintf(stderr, "cannot open %s or %s: %s\n", fname1, fname2, strerror(errno));
	fprintf(stderr, "\n");
	fprintf(stdout, "press enter to quit.\n");
	if (fgets(text, sizeof(text)-1, stdin) == NULL) fprintf(stderr, "fgets failed.\n");
	goto cleanup;
}

