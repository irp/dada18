#!/bin/sh

tar --exclude=input/aerospike-client-c/.git --exclude=input/aerospike-client-c/examples --exclude=input/aerospike-client-c/benchmarks --exclude=input/aerospike-client-c/demos --exclude=input/aerospike-client-c/systemtap -cf 3rdparty.tar HDF5Plugin/ input/aerospike-client-c/ input/inc input/lib

ls -lh 3rdparty.tar

cp 3rdparty.tar ~/public_html/dada18/

