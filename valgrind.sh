#!/bin/sh

case "`hostname`" in
	heinzelfrau)
		valgrind -v --leak-check=full --show-leak-kinds=all --track-origins=yes --suppressions=valgrind.heinzel.supp ./dada
		;;
	*)
		valgrind -v --leak-check=full --show-leak-kinds=all --track-origins=yes ./dada
	;;
esac

