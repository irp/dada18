#ifndef _DADA_RING_H_
#define _DADA_RING_H_

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdarg.h>
#include <errno.h>
#include <string.h>

#include <pthread.h>

#include "dada.h"

#define RINGBUFFERS  64 /* number of buffers */
#define RINGBUFLEN  256 /* length of each buffer */

struct dada_ringbuffer {
	time_t t[RINGBUFFERS];
	char* buffer[RINGBUFFERS];
	int current;
	pthread_mutex_t mutex;
};

struct dada_ringbuffer dada_ringbuffer;

#define DADA_RING_PRINTF(format, ...) \
do { \
    dada_ringbuffer_printf("[%s:%d] "format, __FILE__, __LINE__, __VA_ARGS__); \
} while (0)


int dada_ringbuffer_init();
int dada_ringbuffer_printf(const char* format, ...) __attribute__((format (printf, 1, 2)));
int dada_ringbuffer_dump(FILE* f);
int dada_ringbuffer_destroy();

#endif

