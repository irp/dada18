#ifndef _DADA_POINTER_H_
#define _DADA_POINTER_H_

#define FREE(pointer) \
do { \
    if (pointer) free(pointer); pointer = NULL; \
} while (0)


#define IS_NULL_ERRMSG(pointer, where) \
do { \
    if (pointer == NULL) { \
        DADA_RING_PRINTF("malloc failed: %s", strerror(errno)); \
	goto where; \
    } \
} while (0)

#endif

