#include "dada_ring.h"

int dada_ringbuffer_init()
{
	int ret = 0;
	int n, N = RINGBUFFERS;

	size_t buflen = sizeof(char) * RINGBUFLEN;

	pthread_mutex_init(&dada_ringbuffer.mutex, NULL);
	pthread_mutex_lock(&dada_ringbuffer.mutex);

	for (n=0; n<N; n++)
	{
		dada_ringbuffer.t[n] = -1;
		dada_ringbuffer.buffer[n] = (char*) malloc(buflen);
		if (dada_ringbuffer.buffer[n] == NULL)
		{
			fprintf(stderr, "fatal malloc error during %s: %s [%s:%d]\n",
					__FUNCTION__, strerror(errno), __FILE__, __LINE__);
			ret = -1;
			goto cleanup;
		}

		memset(dada_ringbuffer.buffer[n], '\0', buflen);
	}

	dada_ringbuffer.current = 0;
	snprintf(dada_ringbuffer.buffer[0], RINGBUFLEN-1, "[%s:%d] ring buffer initialised.", __FILE__, __LINE__);

	pthread_mutex_unlock(&dada_ringbuffer.mutex);

cleanup:
	return ret;
}

int dada_ringbuffer_destroy()
{
	int ret = 0;
	int n, N = RINGBUFFERS;

	pthread_mutex_lock(&dada_ringbuffer.mutex);
	for (n=0; n<N; n++)
		FREE(dada_ringbuffer.buffer[n]);
	pthread_mutex_unlock(&dada_ringbuffer.mutex);

	return ret;
}

int dada_ringbuffer_printf(const char* format, ...)
{
	int ret = 0;
	va_list ap;
	int m;

	va_start(ap, format);

	pthread_mutex_lock(&dada_ringbuffer.mutex);

	m = (dada_ringbuffer.current+1) % RINGBUFFERS;
	dada_ringbuffer.t[m] = time(NULL);
	ret = vsnprintf(dada_ringbuffer.buffer[m], RINGBUFLEN-1, format, ap);
	dada_ringbuffer.current = m;

	va_end(ap);
	pthread_mutex_unlock(&dada_ringbuffer.mutex);

	return ret;
}

int dada_ringbuffer_dump(FILE* f)
{
	int ret = 0;
	int n, N = RINGBUFFERS;
	int c;

	pthread_mutex_lock(&dada_ringbuffer.mutex);

	c = dada_ringbuffer.current;
	for (n=0; n<N; n++)
	{
		int m;
		m = (c+n+1)%RINGBUFFERS;
		fprintf(f, "[%03d:%ld] %s\n", m, dada_ringbuffer.t[m], dada_ringbuffer.buffer[m]);
	}

	pthread_mutex_unlock(&dada_ringbuffer.mutex);

	return ret;
}

