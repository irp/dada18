#ifndef _IMAGE_H_
#define _IMAGE_H_

#define _GNU_SOURCE
#include <png.h>
#include <complex.h>
#include <fftw3.h>

#include "dada.h"

#define LOG10(val) ({ \
    typeof(val) _val = (val); \
    typeof(val) _log = -1; \
    if (val > 0) _log = log10(_val); \
    _log; })

struct mem {
	unsigned char* buffer;
	ssize_t size;
};

struct radial {
	int show;
	double x;
	double y;
	double r[3];
	double i[4];
	double phi1;
	double lam1, lam2;
	double omeg;
	int radials;
	double* radial;
};

struct image {
	dada_status success;
	char err[512];
	int dimensions;
	int* size;
	double* data;
	unsigned char* rgb;
	struct mem* mem;
	char* meta;
	struct radial radial;
	char hash[18];
};

enum image_palette {
	IMAGE_PALETTE_TEST1,
	IMAGE_PALETTE_BW,
	IMAGE_PALETTE_WB,
	IMAGE_PALETTE_BWR,
	IMAGE_PALETTE_GNUPLOT,
	IMAGE_PALETTE_HSV,
	IMAGE_PALETTE_HUSL
};

struct image* image_create(int dimensions, int* size);
void image_destroy(struct image* image);
int image_roi(struct image* image, int roi_x, int roi_y, int roi_w, int roi_h);
int image_binning(struct image* image, int binx, int biny);
int image_fft2d(struct image* image, int cenx, int ceny);
int image_logscale(struct image* image);
int image_colourpalette(struct image* image, struct dada_colourmap* colourmap);
void image_colour(double value, int* r, int* g, int* b, enum image_palette pal);
void colourmap_test1(float value, float rgb[3]);
void colourmap_bwr(float value, float rgb[3]);
void colourmap_hsv(float value, float rgb[3]);
void colourmap_husl(float value, float rgb[3]);
int _dxfHSVtoRGB(float h, float s, float v, float *red, float *green, float *blue);
dada_status image_save_png(struct image* image);

#define IMAGE_INIT(img, dimensions, size) \
do { \
    int dim, pix, pixels = 1; \
    /* allocate memory for image struct */ \
    img = (struct image*) malloc(sizeof(struct image)); \
    IS_NULL_ERRMSG(img, error); \
    /* set pointers to NULL */ \
    img->data           = NULL; \
    img->mem            = NULL; \
    img->meta           = NULL; \
    img->rgb            = NULL; \
    img->size           = NULL; \
    img->meta           = strdup(""); \
    img->radial.show    =  0; \
    img->radial.x       = -1; \
    img->radial.y       = -1; \
    img->radial.r[0]    = -1; \
    img->radial.r[1]    = -1; \
    img->radial.r[2]    = -1; \
    img->radial.i[0]    = -1; \
    img->radial.i[1]    = -1; \
    img->radial.i[2]    = -1; \
    img->radial.i[3]    = -1; \
    img->radial.phi1    = -1; \
    img->radial.lam1    = -1; \
    img->radial.lam2    = -1; \
    img->radial.omeg    = -1; \
    img->radial.radials =  0; \
    img->radial.radial  = NULL; \
    /* allocate size array */ \
    img->dimensions = dimensions; \
    img->size = (int*) malloc(sizeof(int*)*dimensions); \
    IS_NULL_ERRMSG(img->size, error); \
    /* copy pixel numbers; multiply total number of pixels */ \
    for (dim=0; dim<dimensions; dim++) { \
	img->size[dim]  = size[dim]; \
	pixels         *= size[dim]; \
    } \
    /* allocate memory for actual image data */ \
    img->data = (double*) malloc(sizeof(double)*pixels); \
    IS_NULL_ERRMSG(image->data, error); \
    /* set to zero */ \
    for (pix=0; pix<pixels; pix++) \
	img->data[pix] = 0.0; \
} while (0)

#define IMAGE_FREE(img) \
do { \
    FREE(img->data); \
    FREE(img->rgb); \
    FREE(img->size); \
    FREE(img->meta); \
    FREE(img->radial.radial); img->radial.radials=0; \
    if  (img->mem) FREE(img->mem->buffer); \
    FREE(img->mem); \
    FREE(img); \
} while (0)


/* check for valid ROI */
#define IMAGE_CHECK_ROI(image, roi_x, roi_y, roi_w, roi_h) \
do { \
    if (roi_w <= 0 || roi_h <= 0) \
	return -1; \
    if (roi_x < 0 || roi_y < 0) \
	return -1; \
    if (roi_x+roi_w > image->size[0] || roi_y+roi_h > image->size[1]) \
	return -1; \
} while (0)

/* count number of pixels */
#define PIXELS_GETNUMBER(img, pixels) \
do { \
    int dim; \
    for (dim=0; dim<img->dimensions; dim++) \
	pixels *= img->size[dim]; \
} while (0)


/* set to zero */
#define PIXELS_SETZERO(rgb, pixels) \
do { \
    int pix; \
    for (pix=0; pix<3*pixels; pix++) \
	rgb[pix] = 0; \
} while (0)

/* search minimum / maximum value for normalisation */
#define PIXELS_MINMAX(img, colourmap, pixels, offset, norm) \
do { \
    int pix; \
    double min, max; \
    min = max = image->data[0]; \
    for (pix=0; pix<pixels; pix++) { \
	double value = image->data[pix]; \
	if (value > max) max = value; \
	if (value < min) min = value; \
    } \
    if (!isnan(colourmap->min)) { \
	min = colourmap->min; \
	if (colourmap->scale == COLOURMAP_SCALE_LOG) \
	    min = LOG10(colourmap->min); \
    } \
    if (!isnan(colourmap->max)) { \
	max = colourmap->max; \
	if (colourmap->scale == COLOURMAP_SCALE_LOG) \
	    max = LOG10(colourmap->max); \
    } \
    offset = -min; \
    norm   = 1.0/(max-min); \
} while (0)

#define COLOURMAP_PARSE_PALETTE(colourmap, pal) \
do { \
    if (strcasecmp(colourmap->palette, "test1"  )==0) pal = IMAGE_PALETTE_TEST1;   \
    if (strcasecmp(colourmap->palette, "bwr"    )==0) pal = IMAGE_PALETTE_BWR;     \
    if (strcasecmp(colourmap->palette, "bw"     )==0) pal = IMAGE_PALETTE_BW;      \
    if (strcasecmp(colourmap->palette, "wb"     )==0) pal = IMAGE_PALETTE_WB;      \
    if (strcasecmp(colourmap->palette, "hsv"    )==0) pal = IMAGE_PALETTE_HSV;     \
    if (strcasecmp(colourmap->palette, "husl"   )==0) pal = IMAGE_PALETTE_HUSL;    \
    if (strcasecmp(colourmap->palette, "gnuplot")==0) pal = IMAGE_PALETTE_GNUPLOT; \
} while (0)


/* loop over pixels and calculate RGB values */
#define COLOURMAP_DO_PALETTE(image, colourmap, pixels, offset, norm) \
do { \
    int pix; \
	for (pix=0; pix<pixels; pix++) { \
		int r, g, b; \
		double value = image->data[pix]; \
		value = (value+offset)*norm; \
		if (colourmap->clipmin && value <= 0.0) { \
			r = g = b = 255; \
			goto colour; \
		} \
		if (colourmap->clipmax && value >= 1.0) { \
			r = g = b = 255; \
			goto colour; \
		} \
		image_colour(value, &r, &g, &b, pal); \
colour: \
		image->rgb[3*pix+0] = r; \
		image->rgb[3*pix+1] = g; \
		image->rgb[3*pix+2] = b; \
	} \
} while (0)


#define COLOURMAP_PALETTE_TEST1(x,h,s,v) \
do { \
    if (1)       h = 0.750*(1.00-x); \
    if (1)       s = 1.000; \
    if (x<1.000) s = 0.718+(1.000-0.718)*(x-0.822)/(1.000-0.822); \
    if (x<0.822) s = 1.000+(0.718-1.000)*(x-0.567)/(0.822-0.567); \
    if (x<0.567) s = 1.000; \
    if (1)       v = 1.000; \
    if (x<1.000) v = 0.668+(1.000-0.668)*(x-0.774)/(1.000-0.774); \
    if (x<0.774) v = 0.628+(0.668-0.628)*(x-0.565)/(0.774-0.565); \
    if (x<0.565) v = 0.368+(0.628-0.368)*(x-0.321)/(0.565-0.321); \
    if (x<0.321) v = 0.481+(0.368-0.481)*(x-0.209)/(0.321-0.209); \
    if (x<0.209) v = 0.481; \
    if (x<0.137) v = 0.000+(0.481-0.000)*(x-0.000)/(0.137-0.000); \
} while (0)

#define COLOURMAP_PALETTE_HSV(x,h,s,v) \
do { \
    if (1)       h = x; \
    if (1)       s = 1.000; \
    if (1)       v = 1.0; \
} while (0)

#define COLOURMAP_PALETTE_HUSL(x, r, g, b) \
do { \
	const float LUT[64][3] = { \
		{0.968,0.340,0.512}, {0.969,0.348,0.450}, {0.970,0.357,0.373}, {0.971,0.365,0.263},  \
		{0.938,0.401,0.172}, {0.883,0.445,0.171}, {0.839,0.474,0.171}, {0.802,0.496,0.170},  \
		{0.770,0.513,0.170}, {0.741,0.527,0.170}, {0.715,0.538,0.170}, {0.690,0.549,0.169},  \
		{0.665,0.558,0.169}, {0.641,0.567,0.169}, {0.617,0.575,0.169}, {0.592,0.583,0.169},  \
		{0.565,0.591,0.169}, {0.536,0.598,0.168}, {0.504,0.606,0.168}, {0.466,0.615,0.168},  \
		{0.421,0.624,0.168}, {0.362,0.633,0.168}, {0.275,0.644,0.167}, {0.168,0.652,0.221},  \
		{0.171,0.648,0.327}, {0.173,0.644,0.391}, {0.175,0.642,0.436}, {0.177,0.639,0.471},  \
		{0.178,0.637,0.500}, {0.180,0.635,0.525}, {0.181,0.633,0.547}, {0.182,0.631,0.568},  \
		{0.183,0.630,0.587}, {0.184,0.628,0.605}, {0.186,0.626,0.623}, {0.187,0.624,0.641},  \
		{0.188,0.622,0.659}, {0.189,0.620,0.678}, {0.191,0.617,0.698}, {0.193,0.615,0.721},  \
		{0.194,0.611,0.746}, {0.197,0.607,0.775}, {0.200,0.602,0.810}, {0.204,0.595,0.853},  \
		{0.209,0.584,0.912}, {0.291,0.567,0.957}, {0.413,0.547,0.957}, {0.500,0.526,0.956},  \
		{0.571,0.503,0.956}, {0.633,0.479,0.956}, {0.691,0.453,0.956}, {0.746,0.422,0.956},  \
		{0.800,0.385,0.956}, {0.855,0.339,0.956}, {0.912,0.274,0.956}, {0.956,0.215,0.937},  \
		{0.958,0.241,0.883}, {0.960,0.262,0.833}, {0.961,0.278,0.788}, {0.963,0.291,0.744},  \
		{0.964,0.303,0.701}, {0.965,0.313,0.657}, {0.966,0.323,0.613}, {0.967,0.332,0.565}  \
	}; \
	int n = round(64*x); \
	if (n <   0) n =  0; \
	if (n >= 64) n = 63; \
	r = LUT[n][0]; \
	g = LUT[n][1]; \
	b = LUT[n][2]; \
} while (0)
#endif

