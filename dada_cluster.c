#include "dada_cluster.h"

int dada_cluster_node_getinfo(const char* hostport, struct dada_cluster_node* node)
{
	int ret = 0;
	const char* key_space = "dada_cluster";
	const char* key_set   = "nodes";
	as_key _key;
	as_record* p_rec = NULL;

	/* TODO: get info on all nodes */
	if (hostport==NULL || strlen(hostport)==0) hostport = "*";

	as_key_init_str(&_key, key_space, key_set, hostport);

	if (aerospike_key_exists(&aero.aer, &aero.err, NULL, &_key, &p_rec) != AEROSPIKE_OK)
		goto error;

	/* retrieve node info */
	if (p_rec) {as_record_destroy(p_rec); p_rec=NULL;}
	if (aerospike_key_get(&aero.aer, &aero.err, NULL, &_key, &p_rec) == AEROSPIKE_OK)
	{
		snprintf(node->host, sizeof(node->host)-1, "%s", as_record_get_str  (p_rec, "node.host"));
		node->port      = as_record_get_int64 (p_rec, "node.port", -1);
		node->pid       = as_record_get_int64 (p_rec, "node.pid",  -1);
		snprintf(node->version, sizeof(node->version)-1, "%s", as_record_get_str  (p_rec, "node.version"));
		node->cpu       = as_record_get_double(p_rec, "node.cpu",  -1);
		node->mem       = as_record_get_double(p_rec, "node.mem",  -1);
		node->last.stxm = as_record_get_int64 (p_rec, "node.last.stxm",  -1);

		if (p_rec) {as_record_destroy(p_rec); p_rec=NULL;}
		goto cleanup;
	}

cleanup:
	return ret;

error:
	fprintf(stderr, "\ncannot find [%s].\n", hostport);
	ret = -1;
	goto cleanup;
}

_Bool _cb(const as_val* value, void* udata)
{
	as_record* p_rec = NULL;
	struct dada_cluster_node** nodes = (struct dada_cluster_node**) udata;
	struct dada_cluster_node*  node  = NULL;

	if (value == NULL) return 1;

	p_rec = as_record_fromval(value);

	if (p_rec == NULL) return 0;

	node = (struct dada_cluster_node*) malloc(sizeof(struct dada_cluster_node));
	if (node == NULL) return 0;

	snprintf(node->host, sizeof(node->host)-1, "%s", as_record_get_str  (p_rec, "node.host"));
	node->port      = as_record_get_int64 (p_rec, "node.port", -1);
	node->pid       = as_record_get_int64 (p_rec, "node.pid",  -1);
	snprintf(node->version, sizeof(node->version)-1, "%s", as_record_get_str  (p_rec, "node.version"));
	node->cpu       = as_record_get_double(p_rec, "node.cpu",  -1);
	node->mem       = as_record_get_double(p_rec, "node.mem",  -1);
	node->last.stxm = as_record_get_int64 (p_rec, "node.last.stxm",  -1);

	node->next = NULL;
	if (nodes == NULL)
	{
		*nodes = node;
	}
	else
	{
		node->next = *nodes;
		*nodes = node;
	}

	return 1;
}

int dada_cluster_node_list(struct dada_cluster_node** nodes)
{
	int ret = 0;
	const char* key_space = "dada_cluster";
	const char* key_set   = "nodes";

	as_query query;

	as_query_init(&query, key_space, key_set);

	//as_query_where_inita(&query, 1);
	//as_query_where(&query, "type", as_integer_equals(0));

	if (aerospike_query_foreach(&aero.aer, &aero.err, NULL, &query, _cb, nodes) != AEROSPIKE_OK)
	{
		fprintf(stderr, "error: aerospike_query_foreach failed (%d:%s)\n", aero.err.code, aero.err.message);
		DADA_RING_PRINTF("error: aero failed (%d:%s)", aero.err.code, aero.err.message);
		goto cleanup;
	}

cleanup:
	return ret;
}

int dada_cluster_node_update(struct dada_cluster_node* node)
{
	int ret = 0;
	const char* key_space = "dada_cluster";
	const char* key_set   = "nodes";
	char key_name[80];

	as_key _key;
	as_record rec;

	if (node == NULL)
		return -1;

	snprintf(key_name, sizeof(key_name)-1, "%s:%d", node->host, node->port);

	as_key_init_str(&_key, key_space, key_set, key_name);

	as_record_inita(&rec, 8);
	as_record_set_str(   &rec, "node.key",        key_name);
	as_record_set_str(   &rec, "node.host",       node->host);
	as_record_set_int64( &rec, "node.port",       node->port);
	as_record_set_int64( &rec, "node.pid",        node->pid);
	as_record_set_str(   &rec, "node.version",    node->version);
	as_record_set_double(&rec, "node.cpu",        node->cpu);
	as_record_set_double(&rec, "node.mem",        node->mem);
	as_record_set_int64( &rec, "node.last.stxm",  node->last.stxm);

	if (aerospike_key_put(&aero.aer, &aero.err, NULL, &_key, &rec) != AEROSPIKE_OK)
	{
		fprintf(stderr, "error: aerospike_key_put failed (%d:%s)\n", aero.err.code, aero.err.message);
		ret = -1;
		goto cleanup;
	}

cleanup:
	as_record_destroy(&rec);
	return ret;
}

