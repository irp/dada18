#ifndef _DADA_CLUSTER_H_
#define _DADA_CLUSTER_H_

#include "dada.h"
#include "dada_aero.h"

#include <aerospike/aerospike_key.h>
#include <aerospike/as_query.h>
#include <aerospike/aerospike_query.h>

int dada_cluster_node_update(struct dada_cluster_node* node);
int dada_cluster_node_getinfo(const char* hostport, struct dada_cluster_node* node);
int dada_cluster_node_list(struct dada_cluster_node** nodes);

#endif

