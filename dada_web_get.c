#include "dada_web_get.h"

char* dada_webcache_canonicalise(const char* module, struct dada_data_path* path, struct dada_data_modi* modi, struct dada_data_form* form);

/* dada_web_get
 * is called from webserver in dada.c for all GET requests
 * and this is where we discriminate for the different URLs
 * and call dada_web_get_* with possibly shortened url,
 * or dada_web_error if nothing pleases ourself.
 */
int dada_web_get(struct connection_meta* meta, const char* url)
{
	int ret = 0;
	int status = MHD_HTTP_NOT_IMPLEMENTED;

	/* redirect /, /static and /static/ -> /static/index.html */
	if (strcmp(url, "/") == 0 || strcmp(url, "/static") == 0 || strcmp(url, "/static/") == 0)
	{
		ret = dada_web_get_redirect(meta, "./static/index.html");
		goto cleanup;
	}

	WEB_GET_WHAT(dada_web_get_static,        "/static/",        "/",               meta, url);
	WEB_GET_WHAT(dada_web_get_snap,          "/snap/",          "/snap/",          meta, url);
	WEB_GET_WHAT(dada_web_get_list,          "/list/",          "/list/",          meta, url);
	WEB_GET_WHAT(dada_web_get_show,          "/show/",          "/show/",          meta, url);
	WEB_GET_WHAT(dada_web_get_multi,         "/multi/",         "/multi/",         meta, url);
	WEB_GET_WHAT(dada_web_get_multi,         "/composite/",     "/composite/",     meta, url);
	WEB_GET_WHAT(dada_web_get_stxm,          "/stxm/",          "/stxm/",          meta, url);

	WEB_GET_WHAT(dada_web_get_polar,         "/polar/",         "/polar/",         meta, url);
	WEB_GET_WHAT(dada_web_get_xcca,          "/xcca/",          "/xcca/",          meta, url);

	WEB_GET_WHAT(dada_web_get_glue,          "/glue/",          "/glue/",          meta, url);

	WEB_GET_WHAT(dada_web_get_radial,        "/radial/",        "/radial/",        meta, url);

	WEB_GET_WHAT(dada_web_get_mask,          "/mask/",          "/mask/",          meta, url);

	WEB_GET_WHAT(dada_web_get_colourpalette, "/colourpalette/", "/colourpalette/", meta, url);

	WEB_GET_WHAT(dada_web_get_info,          "/info/",          "/",               meta, url);
	WEB_GET_WHAT(dada_web_get_nodeinfo,      "/nodeinfo/",      "/nodeinfo/",      meta, url);

	/* return error if nothing matches */
	return dada_web_error(meta, status);

cleanup:
	return ret;
}

/* dada_web_get_redirect:
 * send 301 redirect to url
 */
int dada_web_get_redirect(struct connection_meta* meta, const char* url)
{
	int ret = MHD_YES;
	int status = MHD_HTTP_MOVED_PERMANENTLY;
	struct MHD_Response* response = NULL;
	const char* mime = "text/html; charset=UTF-8";

	char* msg = NULL;
	FILE* mem = NULL;
	size_t memlen;

	/* build the redirect HTML body */
	DADA_MEMSTREAM(mem, msg, memlen);
	fprintf(mem, "<!DOCTYPE html>\n");
	fprintf(mem, "<html lang=\"en\">\n");
	fprintf(mem, "    <head>\n");
	fprintf(mem, "        <title>goto index.html</title>\n");
	fprintf(mem, "    </head>\n");
	fprintf(mem, "    <body>\n");
	fprintf(mem, "        <h1>goto index.html</h1>\n");
	fprintf(mem, "        <p><a href=\"%s\">Click here to open dada start page</a>.</p>\n", url);
	fprintf(mem, "    </body>\n");
	fprintf(mem, "</html>\n");
	fclose(mem);

	fprintf(meta->mem, "dada_web_get_redirect: redirecting to [%s] ... ", url);

	/* create and populate the response */
	WEB_RESPONSE_STRCPY(response, msg);
	MHD_queue_response(meta->conn, status, response);
	MHD_add_response_header(response, "Content-Type", mime);
	MHD_add_response_header(response, MHD_HTTP_HEADER_LOCATION, url);

	WEB_FINISH_TIMING_STATUS(meta, status, response);
	FREE(msg);
	return ret;

err_memstream:
	status = MHD_HTTP_INTERNAL_SERVER_ERROR;
	return dada_web_error(meta, status);
}

/* dada_web_get_static:
 * serve static file with given file name
 */
int dada_web_get_static(struct connection_meta* meta, const char* file)
{
	int ret = MHD_YES;
	int status = MHD_HTTP_NO_CONTENT;
	struct MHD_Response* response = NULL;
	const char* mime = "text/html; charset=UTF-8";
	int fd = -1;
	struct stat st;
	char etag[64];
	const char* ETag = NULL;
	char* filegz = NULL;
	const char* refresh = NULL;

	/* check filename: disallow .., allow a-zA-Z0-9 .-_/ */
	CHECK_FILE_NAME(file);

	/* set mime type depending on file name extension */
	WEB_MIMETYPE(filetype, mime);

	/* check if client supports compression, and if compressed file exists */
	WEB_STATIC_GZIP();

	WEBSERVER_OPEN_STAT(file, fd, st);

	/* check if client's cache is still valid:
	 * build server side etag from devide-inode-filesize
	 */
	snprintf(etag, sizeof(etag)-1, "%ld-%ld-%ld", st.st_dev, st.st_ino, st.st_size);

	/* get ETag provided by client */
	ETag = MHD_lookup_connection_value(meta->conn, MHD_HEADER_KIND, "If-None-Match");

	refresh = MHD_lookup_connection_value(meta->conn, MHD_GET_ARGUMENT_KIND, "refresh");

	/* compare server's etag with client's ETag */
	if (ETag && strlen(ETag)>0 && strcmp(ETag, etag)==0)
	{
		/* create and populate the (empty) response */
		status = MHD_HTTP_NOT_MODIFIED;
		if (fd>0) {close(fd); fd=-1;}
		response = MHD_create_response_from_buffer(0, NULL, MHD_RESPMEM_PERSISTENT);
		MHD_add_response_header(response, "Cache-Control", CACHE_1DAY);
		MHD_add_response_header(response, "ETag", etag);
		if (refresh && strlen(refresh) && atoi(refresh)>0)
			MHD_add_response_header(response, "Refresh", refresh);
		fprintf(meta->mem, "get_static: Not Modified [%s] ... ", file);
	}
	else
	{
		/* create and populate the response */
		status = MHD_HTTP_OK;
		response = MHD_create_response_from_fd((size_t)st.st_size, fd);
		MHD_add_response_header(response, "Cache-Control", CACHE_1DAY);
		MHD_add_response_header(response, "ETag", etag);
		if (filegz) MHD_add_response_header(response, "Content-Encoding", "gzip");
		if (refresh && strlen(refresh) && atoi(refresh)>0)
			MHD_add_response_header(response, "Refresh", refresh);
		fprintf(meta->mem, "get_static: serving %.3f kB from [%s] ... ", 1.0*st.st_size/1024, file);
	}

	WEBSERVER_QUEUE_MIME(meta, status, response, mime);

	FREE(filegz);
	return ret;

err_memstream:
	status = MHD_HTTP_INTERNAL_SERVER_ERROR;
	return dada_web_error(meta, status);

forbidden:
	status = MHD_HTTP_FORBIDDEN;
	return dada_web_error(meta, status);
}


/* dada_web_get_info:
 * return some information, e.g. version number
 */
int dada_web_get_info(struct connection_meta* meta, const char* what)
{
	int ret = MHD_YES;
	int status = MHD_HTTP_NOT_FOUND;
	struct MHD_Response* response = NULL;
	const char* mime = "text/html; charset=UTF-8";
	const char* err  = "<!DOCTYPE html>\n<html><head><title>Not Found</title></head><body><h1>Not Found</h1><p>We are sorry, but your requested file cannot be found.</p></body></html>\n";

	char* msg = NULL;
	FILE* mem = NULL;
	size_t memlen;
	const char* refresh = NULL;

	/* open a memstream for the answer,
	   and then check which info was actually requested */
	DADA_MEMSTREAM(mem, msg, memlen);

	/* show a basic HTML with links to all info */
	if (strcmp(what, "info/")==0)
	{
		status = MHD_HTTP_OK;
		mime = "text/html; charset=UTF-8";
		fprintf(mem, "<!DOCTYPE html>\n");
		fprintf(mem, "<html lang=\"en\">\n");
		fprintf(mem, "    <head>\n");
		fprintf(mem, "        <title>Info Pages</title>\n");
		fprintf(mem, "    </head>\n");
		fprintf(mem, "    <body>\n");
		fprintf(mem, "        <h1>Info Pages</h1>\n");
		fprintf(mem, "        <ul>\n");
		// fprintf(mem, "            <li><a href=\"config\">config</a></li>\n");
		fprintf(mem, "            <li><a href=\"ringbuffer\">ringbuffer</a></li>\n");
		fprintf(mem, "            <li><a href=\"system\">system</a></li>\n");
		fprintf(mem, "            <li><a href=\"version\">version</a>: dada-%s</li>\n", GIT_VERSION);
		fprintf(mem, "        </ul>\n");
		fprintf(mem, "    </body>\n");
		fprintf(mem, "</html>\n");
	}

	if (strcmp(what, "info/version")==0)
	{
		/* reply with a fixed version number */
		status = MHD_HTTP_OK;
		mime = "text/plain; charset=UTF-8";
		fprintf(mem, "dada-%s\n", GIT_VERSION);
	}

	if (strcmp(what, "info/ringbuffer")==0)
	{
		/* dump the current ringbuffer's content */
		status = MHD_HTTP_OK;
		mime = "text/plain; charset=UTF-8";
		dada_ringbuffer_dump(mem);
		refresh = MHD_lookup_connection_value(meta->conn, MHD_GET_ARGUMENT_KIND, "refresh");
	}

	if (strcmp(what, "info/system")==0)
	{
		/* give some basic info about the system:
		 * current pid, uid etc.;
		 * host name, host system;
		 * working directory, further sys info
		 */

		/* variables */
		pid_t pid, ppid;
		uid_t uid, euid;
		uid_t gid, egid;
		char cwd[256];
		char host[256];
		mode_t umsk;
		struct utsname utsname;
		struct sysinfo sinfo;

		/* gather information */
		pid  = getpid();
		ppid = getppid();
		uid  = getuid();
		euid = geteuid();
		gid  = getgid();
		egid = getegid();
		umsk = umask(0777);
		umask(umsk);
		if (getcwd     (cwd,  sizeof(cwd )-1) == NULL) goto error;
		if (gethostname(host, sizeof(host)-1) != 0   ) goto error;
		uname(&utsname);
		sysinfo(&sinfo);

		/* build the answer */
		status = MHD_HTTP_OK;
		mime = "text/plain; charset=UTF-8";

		fprintf(mem, "System Specific Statistics\n");
		fprintf(mem, "==========================\n");
		fprintf(mem, "\n");

		fprintf(mem, "pid: %d, ppid: %d\n", pid, ppid);
		fprintf(mem, "uid: %d, euid: %d\n", uid, euid);
		fprintf(mem, "gid: %d, egid: %d\n", gid, egid);
		fprintf(mem, "umask is 0%03o\n", umsk);
		fprintf(mem, "cwd: %s\n", cwd);
		fprintf(mem, "host is %s\n", host);
		fprintf(mem, "system: %s-%s on %s\n",
				utsname.sysname, utsname.release, utsname.machine);
		fprintf(mem, "ram: %.3f GB\n",
				1.0*sinfo.totalram*sinfo.mem_unit/1073741824);
		fprintf(mem, "load: %.2f %.2f %.2f\n",
				sinfo.loads[0]/65536.0, sinfo.loads[1]/65536.0, sinfo.loads[2]/65536.0);

		proc_stat(mem); /* get more process related information */

		refresh = MHD_lookup_connection_value(meta->conn, MHD_GET_ARGUMENT_KIND, "refresh");
	}

	if (strcmp(what, "info/top")==0)
	{
		/* memory + cpu usage, taken from top_loop in dada.c */
		status = MHD_HTTP_OK;
		mime   = "text/plain; charset=UTF-8";
		fprintf(mem, "%s\n", top_buf);
		refresh = MHD_lookup_connection_value(meta->conn, MHD_GET_ARGUMENT_KIND, "refresh");
	}

cleanup:
	fclose(mem);

	/* create and populate the response */
	fprintf(meta->mem, "get_info: answering [%s] ... ", what);
	if (status == MHD_HTTP_OK)
		WEB_RESPONSE_STRCPY(response, msg);
	else
		WEB_RESPONSE_STRCPY(response, err);

	if (refresh && strlen(refresh) && atof(refresh)>0.05)
		MHD_add_response_header(response, "Refresh", refresh);

	MHD_add_response_header(response, "Access-Control-Allow-Origin", "*");

	WEBSERVER_QUEUE_MIME(meta, status, response, mime);
	FREE(msg);
	return ret;

error:
	err  = "<!DOCTYPE html>\n<html><head><title>Internal Server Error</title></head><body><h1>Internal Server Error</h1><p>Your request cannot be processed.</p></body></html>\n";
	status = MHD_HTTP_INTERNAL_SERVER_ERROR;
	goto cleanup;

err_memstream:
	status = MHD_HTTP_INTERNAL_SERVER_ERROR;
	return dada_web_error(meta, status);
}

int dada_web_get_nodeinfo(struct connection_meta* meta, const char* hostport)
{
	int ret = MHD_YES;
	int status = MHD_HTTP_OK;
	struct MHD_Response* response = NULL;
	const char* mime = "application/json; charset=UTF-8";
	const char* refresh = NULL;

	char* msg = NULL;
	FILE* mem = NULL;
	size_t memlen;

	struct dada_cluster_node node;

	refresh = MHD_lookup_connection_value(meta->conn, MHD_GET_ARGUMENT_KIND, "refresh");

	if (strcmp(hostport, "*")==0)
	{
		struct dada_cluster_node* nodes = NULL;
		struct dada_cluster_node* node  = NULL;
		struct dada_cluster_node* next  = NULL;
		dada_cluster_node_list(&nodes);
		status = MHD_HTTP_OK;
		DADA_MEMSTREAM(mem, msg, memlen);
		fprintf(mem, "[\n");
		for (node=nodes; node; node=next)
		{
			fprintf(mem, "    {\"host\": \"%s\", \"port\": %d, \"pid\": %d,\n", node->host, node->port, node->pid);
			fprintf(mem, "        \"version\": \"%s\",\n", node->version);
			fprintf(mem, "        \"cpu\": %.1f,\n", node->cpu);
			fprintf(mem, "        \"mem\": %.1f,\n", node->mem);
			fprintf(mem, "        \"last\": {\n");
			fprintf(mem, "            \"stxm\": %d\n", node->last.stxm);
			fprintf(mem, "            }\n");
			fprintf(mem, "        }");
			next = node->next;
			free(node);
			node = NULL;
			if (next)
				fprintf(mem, ",\n");
			else
				fprintf(mem, "\n");
		}
		fprintf(mem, "]\n");
		nodes = NULL;
		fclose(mem);
		goto senden;
	}

	if (dada_cluster_node_getinfo(hostport, &node) == 0)
	{
		status = MHD_HTTP_OK;
		DADA_MEMSTREAM(mem, msg, memlen);
		fprintf(mem, "{\"host\": \"%s\", \"port\": %d, \"pid\": %d,\n", node.host, node.port, node.pid);
		fprintf(mem, "    \"version\": \"%s\",\n", node.version);
		fprintf(mem, "    \"cpu\": %.1f,\n", node.cpu);
		fprintf(mem, "    \"mem\": %.1f,\n", node.mem);
		fprintf(mem, "    \"last\": {\n");
		fprintf(mem, "        \"stxm\": %d\n", node.last.stxm);
		fprintf(mem, "        }\n");
		fprintf(mem, "    }\n");
		fclose(mem);
		goto senden;
	}
	else
	{
		status = MHD_HTTP_NOT_FOUND;
		DADA_MEMSTREAM(mem, msg, memlen);
		fprintf(mem, "{}\n");
		fclose(mem);
		goto senden;
	}

senden:
	response = MHD_create_response_from_buffer(strlen(msg), (void*)msg, MHD_RESPMEM_MUST_FREE);
	MHD_queue_response(meta->conn, status, response);
	MHD_add_response_header(response, "Content-Type", mime);
	if (refresh && strlen(refresh) && atof(refresh)>0.05)
		MHD_add_response_header(response, "Refresh", refresh);
	MHD_destroy_response(response);

// cleanup:
	return ret;

err_memstream:
	status = MHD_HTTP_INTERNAL_SERVER_ERROR;
	return dada_web_error(meta, status);
//notfound:
//	status = MHD_HTTP_NOT_FOUND;
//	return dada_web_error(meta, status);
}


/* dada_web_error:
 * reply with error message depending on status code
 */
int dada_web_error(struct connection_meta* meta, int status)
{
	const char* err = NULL;
	const char* mime = "text/html; charset=UTF-8";
	struct MHD_Response* response = NULL;

	fprintf(meta->mem, "dada_web_error ... ");
	switch (status)
	{
		case MHD_HTTP_FORBIDDEN:
			err = "<!DOCTYPE html>\n<html><head><title>Forbidden</title></head><body><h1>Forbidden</h1><p>The file you are looking for has restricted access.</p></body></html>\n";
			break;
		case MHD_HTTP_NOT_FOUND:
			err = "<!DOCTYPE html>\n<html><head><title>Not Found</title></head><body><h1>Not Found</h1><p>We are sorry, but your requested file cannot be found.</p></body></html>\n";
			break;
		case MHD_HTTP_NOT_IMPLEMENTED:
			err = "<!DOCTYPE html>\n<html><head><title>Not Implemented</title></head><body><h1>Not Implemented</h1><p>Your request is not implemented.</p></body></html>\n";
			break;
		default:
			// DADA_RING_PRINTF("dada_web_error: unknown status %d", status);
			status = MHD_HTTP_INTERNAL_SERVER_ERROR;
			err = "<!DOCTYPE html>\n<html>\n  <head>\n    <title>Internal Server Error</title>\n  </head>\n  <body>\n    <h1>Internal Server Error</h1>\n    <p>Your request cannot be processed.</p>\n  </body>\n</html>\n";
			break;
	}

	WEB_RESPONSE_STRCPY(response, err);
	WEBSERVER_QUEUE_MIME(meta, status, response, mime);
	return MHD_YES;
}

/* dada_web_accepted:
 * reply with accepted message including progress
 */
int dada_web_accepted(struct connection_meta* meta, double progress)
{
	char msg[256];
	const char* mime = "application/json; charset=UTF-8";
	struct MHD_Response* response = NULL;

	fprintf(meta->mem, "dada_web_accepted ... ");

	snprintf(msg, sizeof(msg)-1, "{\"message\": {\"job\": \"...\", \"running\": true, \"progress\": %g}}\n", progress);
	WEB_RESPONSE_STRCPY(response, msg);
	MHD_add_response_header(response, "Connection", "close");
	WEBSERVER_QUEUE_MIME(meta, MHD_HTTP_ACCEPTED, response, mime);
	return MHD_YES;
}

int _get_snap_list(FILE* mem, const char* fol, int depth)
{
	int ret = 0;

	int d;
	char fol2[256];
	struct stat st;

	int num;
	struct dirent** namelist = NULL;
	num = scandir(fol, &namelist, dirfilter_nohidden, versionsort_r);
	if (num<0)
	{
		ret = -1;
		goto cleanup;
	}

	while (num--)
	{
		const char* d_name = namelist[num]->d_name;
		snprintf(fol2, sizeof(fol2)-1, "%s/%s", fol, d_name);

		if (d_name[0] == '.')          goto skip;
		if (stat(fol2, &st) != 0)      goto skip;
		if (S_ISDIR(st.st_mode))
		{
			for (d=0; d<depth; d++) {fprintf(mem, "    ");} fprintf(mem, "{ \"text\" : \"%s\",\n", d_name);
			for (d=0; d<depth; d++) {fprintf(mem, "    ");} fprintf(mem, "    \"children\" : [\n");
			ret = _get_snap_list(mem, fol2, depth+1);
			for (d=0; d<depth; d++) {fprintf(mem, "    ");} fprintf(mem, "        { \"text\" : \"add\", \"icon\": \"icons/add.svg\" }\n");
			for (d=0; d<depth; d++) {fprintf(mem, "    ");} fprintf(mem, "    ]\n");
			for (d=0; d<depth; d++) {fprintf(mem, "    ");} fprintf(mem, "},\n");
		}
		else
		{
			if (depth == 1) goto skip; /* hide leafs on root */
			for (d=0; d<depth; d++) fprintf(mem, "    ");
			if (strrchr(d_name, '.')) *(strrchr(d_name, '.')) = '\0';
			fprintf(mem, "    { \"text\" : \"%s\", \"icon\": \"icons/file.svg\" }", d_name);
			fprintf(mem, ",\n");
		}

skip:
		FREE(namelist[num]);
	}

cleanup:
	FREE(namelist);
	return ret;
}

/* list snapshots, send snapshot */
int dada_web_get_snap(struct connection_meta* meta, const char* url)
{
	//DADA_RING_PRINTF("get_snap: [%s]", url);

	int ret = MHD_YES;
	int status = MHD_HTTP_OK;
	struct MHD_Response* response = NULL;
	const char* mime = "application/json; charset=UTF-8";

	char* msg = NULL;
	FILE* mem = NULL;
	size_t memlen;

	if (!url || strlen(url)==0) /* list all snapshots */
	{
		const char* fol = "static/snap/";
		DADA_MEMSTREAM(mem, msg, memlen);

		fprintf(mem, "[\n");
		if (_get_snap_list(mem, fol, 1) != 0)
		{
			status = MHD_HTTP_INTERNAL_SERVER_ERROR;
			const char* err = "listing failed.";

			if (mem) {fclose(mem); mem=NULL;}
			FREE(msg);

			WEB_RESPONSE_STRCPY(response, err);
			MHD_queue_response(meta->conn, status, response);
			MHD_add_response_header(response, "Content-Type", mime);

			return ret;
		}
		fprintf(mem, "    { \"text\" : \"add\", \"icon\": \"icons/add.svg\" }\n");
		fprintf(mem, "]\n");

		if (mem) {fclose(mem); mem=NULL;}

		WEB_RESPONSE_STRCPY(response, msg);
		MHD_queue_response(meta->conn, status, response);
		MHD_add_response_header(response, "Content-Type", mime);

		WEB_FINISH_TIMING_STATUS(meta, status, response);

		return ret;
	}
	else /* send specific snapshot */
	{
	}

	if (mem) {fclose(mem); mem=NULL;}
	FREE(msg);
	return ret;

err_memstream:
	status = MHD_HTTP_INTERNAL_SERVER_ERROR;
	return dada_web_error(meta, status);
}

/* list possible paths, format as JSON:
 * instruments
 * experiments
 * detectors
 * newfiles / scans (this is delegated to detector funcp)
 * */
int dada_web_get_list(struct connection_meta* meta, const char* url)
{
	int ret = MHD_YES;
	int fromcache = 1;
	int status = MHD_HTTP_OK;
	struct MHD_Response* response = NULL;
	const char* mime = "application/json; charset=UTF-8";

	struct dada_data_form* form = NULL;

	void* so_handle = NULL;
	int (*so_funcp)(struct dada_data_path*,FILE*) = NULL;
	struct dada_data_path* path = NULL;
	char* fol = NULL;
	char* msg = NULL;
	FILE* mem = NULL;
	size_t memlen;
	int n;
	char fol2[256];
	struct stat st;
	const char* refresh = NULL;

	int num;
	struct dirent** namelist = NULL;
	int notfirst = 0;

	time_t tl, tc = 0;
#if 0
#endif

	form = dada_data_form_extract(meta->conn);
	for (n=0; n<form->len; n++) if (strcmp(form->key[n], "fromcache")==0) fromcache = atoi(form->val[n]);

	path = dada_data_path_extract(url);

	/* before we reply from the cache, check timestamps */
	tl = dada_web_listing_stamp(path);
	tc = dada_web_ask_cache(meta, "list", path, NULL, NULL);

	if (fromcache && tc>0 && tl>0 && tc>=tl) /* reply from cache */
	{
		ret = dada_web_get_cache(meta, "list", path, NULL, NULL);
		/*
		if (path && path->len>=3)
			DADA_RING_PRINTF("path: %s/%s/%s, tl: %ld, tc: %ld, ret: %d", path->path[0], path->path[1], path->path[2], tl, tc, ret);
		else
			DADA_RING_PRINTF("tl: %ld, tc: %ld, ret: %d", tl, tc, ret);
		*/
		if (ret == MHD_YES)
			goto cleanup;
	}
	/*
	if (path && path->len>=3)
		DADA_RING_PRINTF("path: %s/%s/%s, tl: %ld, tc: %ld", path->path[0], path->path[1], path->path[2], tl, tc);
	else
		DADA_RING_PRINTF("tl: %ld, tc: %ld", tl, tc);
	*/
#if 0
#endif

	/* otherwise, prepare the answer */
	DADA_MEMSTREAM(mem, fol, memlen);
	fprintf(mem, "config");
	for (n=0; path && n<path->len; n++)
		if (path->path[n] && strlen(path->path[n]))
			fprintf(mem, "/%s", path->path[n]);
	fclose(mem);
	DADA_MEMSTREAM(mem, msg, memlen);

	/* forbidden path elements: /. and .. */
	if (strstr(fol, "/.") || strstr(fol, ".."))
	{
		errno = EACCES;
		goto forbidden;
	}

	/* either:
	 * call fol/list.so, or
	 * list the subdirs inside fol
	 * */

	while (1)
	{
		if ( strlen(fol) == 0)  goto dolist;
		if (!strrchr(fol, '/')) goto dolist;
		snprintf(fol2, sizeof(fol2)-1, "%s/%s", fol, "list.so");

		/* try to find a suitable list.so */
		// DADA_RING_PRINTF("fol2: %s", fol2);
		if (stat(fol2, &st) != 0)  goto shorterpath;
		if (!S_ISREG(st.st_mode))  goto shorterpath;
		if (!(st.st_mode&S_IRUSR)) goto shorterpath;
		if (!(st.st_mode&S_IXUSR)) goto shorterpath;
		snprintf(fol2, sizeof(fol2)-1, "%s/%s", fol, "list");
		DADA_DL_LOAD_SYMBOL(DADA_DL_PATH_CONST(fol2), "list", so_funcp, so_handle);
		so_funcp(path, mem);
		DADA_DL_CLOSE(so_handle);
		goto cleanup;
shorterpath:
		if (strrchr(fol, '/')) *strrchr(fol, '/') = '\0';
	}
	goto dolist;

dolist:
	/* re-prepare the answer */
	FREE(fol);
	DADA_MEMSTREAM(mem, fol, memlen);
	fprintf(mem, "config");
	for (n=0; path && n<path->len; n++)
		if (path->path[n] && strlen(path->path[n]))
			fprintf(mem, "/%s", path->path[n]);
	fclose(mem);
	DADA_MEMSTREAM(mem, msg, memlen);

	num = scandir(fol, &namelist, dirfilter_nohidden, versionsort_r);
	if (num<0) goto listingerror;

	fprintf(mem, "{\n");
	fprintf(mem, "    \"status\": \"okay\",\n");
	fprintf(mem, "    \"listing\": [");
	while (num--)
	{
		const char* d_name = namelist[num]->d_name;
		snprintf(fol2, sizeof(fol2)-1, "%s/%s", fol, d_name);

		if (d_name[0] == '.')          goto skip;
		if (stat(fol2, &st) != 0)      goto skip;
		if (!S_ISDIR(st.st_mode))      goto skip;
		if (strcmp(d_name, "data")==0) goto skip;

		if (notfirst++) fprintf(mem, ", ");
		fprintf(mem, "\"%s\"", d_name);

skip:
		FREE(namelist[num]);
	}
	fprintf(mem, "]\n");
	fprintf(mem, "}\n");
	FREE(namelist);


cleanup:
	if (mem)
	{
		fclose(mem);

		dada_web_put_cache(meta, mime, strlen(msg), msg);

		WEB_RESPONSE_STRCPY(response, msg);
		MHD_queue_response(meta->conn, status, response);
		MHD_add_response_header(response, "Content-Type", mime);

		refresh = MHD_lookup_connection_value(meta->conn, MHD_GET_ARGUMENT_KIND, "refresh");
		if (refresh && strlen(refresh) && atoi(refresh)>0)
			MHD_add_response_header(response, "Refresh", refresh);

		WEB_FINISH_TIMING_STATUS(meta, status, response);
	}
	dada_data_form_destroy(&form);
	FREE(msg);
	FREE(fol);
	dada_data_path_destroy(&path);
	return ret;

error:
	status = MHD_HTTP_INTERNAL_SERVER_ERROR;
	FREE(msg);
	FREE(fol);
	dada_data_path_destroy(&path);
	return dada_web_error(meta, status);

err_memstream:
	status = MHD_HTTP_INTERNAL_SERVER_ERROR;
	FREE(msg);
	FREE(fol);
	dada_data_path_destroy(&path);
	return dada_web_error(meta, status);

listingerror:
	fprintf(mem, "{\n");
	fprintf(mem, "    \"status\": \"not found\",\n");
	fprintf(mem, "    \"error\": \"%s\"\n", strerror(errno));
	fprintf(mem, "}\n");
	status = MHD_HTTP_NOT_FOUND;
	goto cleanup;

forbidden:
	fprintf(mem, "{\n");
	fprintf(mem, "    \"status\": \"forbidden\",\n");
	fprintf(mem, "    \"error\": \"%s\"\n", strerror(errno));
	fprintf(mem, "}\n");
	status = MHD_HTTP_FORBIDDEN;
	goto cleanup;
}

int dada_web_get_mask_list(struct connection_meta* meta, const char* url)
{
	int ret = MHD_YES;
	int status = MHD_HTTP_OK;
	struct MHD_Response* response = NULL;
	const char* mime = "application/json; charset=UTF-8";

	struct dada_data_path* path = NULL;
	char* fol = NULL;
	char* msg = NULL;
	FILE* mem = NULL;
	size_t memlen;
	int n;
	char fol2[256];
	struct stat st;

	int num;
	struct dirent** namelist = NULL;
	int notfirst = 0;

	path = dada_data_path_extract(url);


	DADA_MEMSTREAM(mem, fol, memlen);
	fprintf(mem, "masks/");
	for (n=0; path && n<path->len; n++)
		if (path->path[n] && strlen(path->path[n]))
			fprintf(mem, "%s/", path->path[n]);
	fclose(mem);

	DADA_MEMSTREAM(mem, msg, memlen);

	/* forbidden path elements: /. and .. */
	if (strstr(fol, "/.") || strstr(fol, ".."))
	{
		errno = EACCES;
		goto forbidden;
	}

	num = scandir(fol, &namelist, dirfilter_nohidden, versionsort_r);
	if (num<0) goto listingerror;

	/* list directories and *.h5 files */
	fprintf(mem, "{\n");
	fprintf(mem, "    \"status\": \"okay\",\n");
	fprintf(mem, "    \"listing\": [");
	while (num--)
	{
		const char* d_name = namelist[num]->d_name;
		snprintf(fol2, sizeof(fol2)-1, "%s/%s", fol, d_name);

		if (d_name[0] == '.')          goto skip;
		if (stat(fol2, &st) != 0)      goto skip;
		if (!S_ISDIR(st.st_mode))
			if (strrchr(d_name, '.') && strcmp(strrchr(d_name, '.'), ".h5") != 0)
				goto skip;

		if (notfirst++) fprintf(mem, ", ");
		fprintf(mem, "\"%s\"", d_name);

skip:
		FREE(namelist[num]);
	}
	fprintf(mem, "]\n");
	fprintf(mem, "}\n");
	FREE(namelist);


cleanup:
	if (mem)
	{
		fclose(mem);

		dada_web_put_cache(meta, mime, strlen(msg), msg);

		WEB_RESPONSE_STRCPY(response, msg);
		MHD_queue_response(meta->conn, status, response);
		MHD_add_response_header(response, "Content-Type", mime);

		WEB_FINISH_TIMING_STATUS(meta, status, response);
	}
	FREE(msg);
	FREE(fol);
	dada_data_path_destroy(&path);
	return ret;

err_memstream:
	status = MHD_HTTP_INTERNAL_SERVER_ERROR;
	FREE(msg);
	FREE(fol);
	dada_data_path_destroy(&path);
	return dada_web_error(meta, status);

listingerror:
	fprintf(mem, "{\n");
	fprintf(mem, "    \"status\": \"not found\",\n");
	fprintf(mem, "    \"error\": \"%s\"\n", strerror(errno));
	fprintf(mem, "}\n");
	status = MHD_HTTP_NOT_FOUND;
	goto cleanup;

forbidden:
	fprintf(mem, "{\n");
	fprintf(mem, "    \"status\": \"forbidden\",\n");
	fprintf(mem, "    \"error\": \"%s\"\n", strerror(errno));
	fprintf(mem, "}\n");
	status = MHD_HTTP_FORBIDDEN;
	goto cleanup;
}

int dada_web_get_show(struct connection_meta* meta, const char* url)
{
	const int timing = 0;
	int fromcache = 1;

	struct timespec t0, t1, t2, t3, t4;
	double tt1, tt2, tt3, tt4;
	clock_gettime(CLOCK_REALTIME, &t0);

	int ret = 0;
	void* so_handle = NULL;
	struct dada_data* (*so_funcp)(struct dada_data_path*,struct dada_data_modi*,struct dada_data*,enum dada_cache_flags) = NULL;
	struct dada_data* data   = NULL;
	struct dada_data* radial = NULL;
	struct dada_data_path* path = NULL;
	struct dada_data_modi* modi = NULL;
	struct dada_data_form* form = NULL;
	int n;

	path = dada_data_path_extract(url);
	modi = dada_data_modi_extract(meta->conn);
	form = dada_data_form_extract(meta->conn);

	for (n=0; n<form->len; n++) if (strcmp(form->key[n], "fromcache")==0) fromcache = atoi(form->val[n]);

	/* if path is too short for full image, delegate to dada_web_get_list */
	if (path == NULL)
		return dada_web_get_list(meta, url);
	if (path->len < 4)
		return dada_web_get_list(meta, url);
	if (path->len == 4 && path->path[3] == NULL)
		return dada_web_get_list(meta, url);
	if (path->path[path->len-1] == NULL)
		return dada_web_get_list(meta, url);

	clock_gettime(CLOCK_REALTIME, &t1);
	if (fromcache) ret = dada_web_get_cache(meta, "show", path, modi, form);
	if (ret == MHD_YES) goto cleanup;

	if (meta->canonical == NULL) meta->canonical = dada_webcache_canonicalise("show", path, modi, form);

	clock_gettime(CLOCK_REALTIME, &t2);
	/* delegate the data preprocessing / retrieval */
	DADA_DL_LOAD_SYMBOL(DADA_DL_PATH_CONST("input/retrieve"), "dada_input_retrieve", so_funcp, so_handle);
	// data = so_funcp(path, modi, data, DADA_CACHE_READ|DADA_CACHE_WRITE);
	data = so_funcp(path, modi, data, DADA_CACHE_NONE);
	DADA_DL_CLOSE(so_handle);
	clock_gettime(CLOCK_REALTIME, &t3);


	/* TODO:
	 * add X-meta header, containing e.g.
	 * {"status":"okay","dump":{"ct": 1.000000, "att": -1 , "dummy": 0.100000, ..., "wgzrot": 4.300000},"min":-2.000000,"max":3.000000}
	 * */

	/* if we got something, prepare response; otherwise error */
	if (data && data->buffer.p)
	{
		double min = 0;
		double max = 0;
		int x, y, X, Y;
		FILE* mem = NULL;
		size_t memlen;

		/* meta data */
		DADA_MEMSTREAM(mem, meta->xmeta, memlen);
		if (data->D == 1)
		{
			X = data->d[0];
			switch (data->type)
			{
				case DADA_DATA_DOUBLE:
					min = max = data->buffer.f[0];
					for (x=0; x<X; x++) if (data->buffer.f[x] < min) min = data->buffer.f[x];
					for (x=0; x<X; x++) if (data->buffer.f[x] > max) max = data->buffer.f[x];
					break;
				case DADA_DATA_INT:
					min = max = data->buffer.f[0];
					for (x=0; x<X; x++) if (data->buffer.i[x] < min) min = data->buffer.i[x];
					for (x=0; x<X; x++) if (data->buffer.i[x] > max) max = data->buffer.i[x];
					break;
				default:
					min = max = 0;
					break;
			}
		}
		if (data->D == 2)
		{
			Y = data->d[0]; X = data->d[1];
			switch (data->type)
			{
				case DADA_DATA_DOUBLE:
					min = max = data->buffer.f[0];
					for (y=0; y<Y; y++) for (x=0; x<X; x++) if (data->buffer.f[y*X+x] < min) min = data->buffer.f[y*X+x];
					for (y=0; y<Y; y++) for (x=0; x<X; x++) if (data->buffer.f[y*X+x] > max) max = data->buffer.f[y*X+x];
					break;
				case DADA_DATA_INT:
					min = max = data->buffer.f[0];
					for (y=0; y<Y; y++) for (x=0; x<X; x++) if (data->buffer.i[y*X+x] < min) min = data->buffer.i[y*X+x];
					for (y=0; y<Y; y++) for (x=0; x<X; x++) if (data->buffer.i[y*X+x] > max) max = data->buffer.i[y*X+x];
					break;
				default:
					min = max = 0;
					break;
			}
		}

		if (isnan(min) || isnan(max) || isinf(min) || isinf(max))
			fprintf(mem, "{\"status\": \"okay\", \"dump\": {}, \"min\": null, \"max\": null}");
		else
			fprintf(mem, "{\"status\": \"okay\", \"dump\": {}, \"min\": %g, \"max\": %g}", min, max);
		fclose(mem);

		/* radial profiles */
		{
			struct dada_data* (*so_funcp)(struct dada_radial*,struct dada_data_modi*,struct dada_data*,enum dada_cache_flags) = NULL;
			DADA_DL_LOAD_SYMBOL(DADA_DL_PATH_CONST("algo/radial"), "dada_algo_radial", so_funcp, so_handle);
			radial = so_funcp(NULL, modi, data, DADA_CACHE_NONE);
			DADA_DL_CLOSE(so_handle);
		}
		if (radial)
		{
			DADA_MEMSTREAM(mem, meta->xradial, memlen);
			int r, R = radial->d[0];
			fprintf(mem, "{\"radial\": [");
			for (r=0; r<R; r++)
			{
				fprintf(mem, "[%d, %g]", r, radial->buffer.f[r]);
				if (r != R-1) fprintf(mem, ", ");
			}
			fprintf(mem, "]}");
			fclose(mem);
		}

err_memstream:
		ret = dada_web_reply(meta, data, form, fromcache);
		goto cleanup;
	}

	goto error;

cleanup:
	if (radial) {dada_data_destroy(radial); free(radial); radial=NULL;}
	dada_data_path_destroy(&path);
	dada_data_modi_destroy(&modi);
	dada_data_form_destroy(&form);
	clock_gettime(CLOCK_REALTIME, &t4);

	tt1 = (t1.tv_sec-t0.tv_sec)+(t1.tv_nsec-t0.tv_nsec)*1e-9;
	tt2 = (t2.tv_sec-t1.tv_sec)+(t2.tv_nsec-t1.tv_nsec)*1e-9;
	tt3 = (t3.tv_sec-t2.tv_sec)+(t3.tv_nsec-t2.tv_nsec)*1e-9;
	tt4 = (t4.tv_sec-t3.tv_sec)+(t4.tv_nsec-t3.tv_nsec)*1e-9;
	if (timing) DADA_RING_PRINTF("show-timing: %.1f ms+%.1f ms+%.1f ms+%.1f ms", tt1*1e3, tt2*1e3, tt3*1e3, tt4*1e3);

	return ret;

error:
	if (data) {dada_data_destroy(data); free(data); data=NULL;}
	ret = dada_web_error(meta, MHD_HTTP_INTERNAL_SERVER_ERROR);
	goto cleanup;

/*
no_url:
	if (data) {dada_data_destroy(data); free(data); data=NULL;}
	ret = dada_web_error(meta, MHD_HTTP_NOT_FOUND);
	goto cleanup;
*/
}

int dada_web_get_multi(struct connection_meta* meta, const char* url)
{
	int ret = 0;
	int fromcache = 1;
	void* so_handle = NULL;
	struct dada_data* (*so_funcp)(struct dada_data_path*,struct dada_data_modi*,struct dada_data*,enum dada_cache_flags) = NULL;
	struct dada_data* data = NULL;
	struct dada_data_path* path = NULL;
	struct dada_data_modi* modi = NULL;
	struct dada_data_form* form = NULL;
	int n;

	path = dada_data_path_extract(url);
	modi = dada_data_modi_extract(meta->conn);
	form = dada_data_form_extract(meta->conn);

				DADA_RING_PRINTF("debug %d", __LINE__);
	for (n=0; n<form->len; n++) if (strcmp(form->key[n], "fromcache")==0) fromcache = atoi(form->val[n]);

	/* if path is too short for full image, delegate to dada_web_get_list */
	if (path == NULL)
		return dada_web_get_list(meta, url);
	if (path->len < 4)
		return dada_web_get_list(meta, url);
	if (path->len == 4 && path->path[3] == NULL)
		return dada_web_get_list(meta, url);
	if (path->path[path->len-1] == NULL)
		return dada_web_get_list(meta, url);

				DADA_RING_PRINTF("debug %d", __LINE__);
	if (fromcache) ret = dada_web_get_cache(meta, "multi", path, modi, form);
	if (ret == MHD_YES) goto cleanup;

				DADA_RING_PRINTF("debug %d", __LINE__);
	/* delegate the data preprocessing / retrieval */
	DADA_DL_LOAD_SYMBOL(DADA_DL_PATH_CONST("input/retrieve"), "dada_input_multi", so_funcp, so_handle);
	// data = so_funcp(path, modi, data, DADA_CACHE_READ|DADA_CACHE_WRITE);
	data = so_funcp(path, modi, data, DADA_CACHE_NONE);
	DADA_DL_CLOSE(so_handle);
				DADA_RING_PRINTF("debug %d", __LINE__);


	/* if we got something, prepare response; otherwise error */
	if (data && data->buffer.p)
	{
		double min, max;
		int x, y, X, Y;
		FILE* mem = NULL;
		size_t memlen;
		DADA_MEMSTREAM(mem, meta->xmeta, memlen);
		Y = data->d[0]; X = data->d[1];
		switch (data->type)
		{
			case DADA_DATA_DOUBLE:
				min = max = data->buffer.f[0];
				for (y=0; y<Y; y++) for (x=0; x<X; x++) if (data->buffer.f[y*X+x] < min) min = data->buffer.f[y*X+x];
				for (y=0; y<Y; y++) for (x=0; x<X; x++) if (data->buffer.f[y*X+x] > max) max = data->buffer.f[y*X+x];
				break;
			case DADA_DATA_INT:
				min = max = data->buffer.f[0];
				for (y=0; y<Y; y++) for (x=0; x<X; x++) if (data->buffer.i[y*X+x] < min) min = data->buffer.i[y*X+x];
				for (y=0; y<Y; y++) for (x=0; x<X; x++) if (data->buffer.i[y*X+x] > max) max = data->buffer.i[y*X+x];
				break;
			default:
				min = max = 0;
				 break;
		}
		if (isnan(min) || isnan(max) || isinf(min) || isinf(max))
			fprintf(mem, "{\"status\": \"okay\", \"dump\": {}, \"min\": null, \"max\": null}");
		else
			fprintf(mem, "{\"status\": \"okay\", \"dump\": {}, \"min\": %g, \"max\": %g}", min, max);
		fclose(mem);

err_memstream:
		ret = dada_web_reply(meta, data, form, fromcache);
		goto cleanup;
	}

	goto error;

cleanup:
	dada_data_path_destroy(&path);
	dada_data_modi_destroy(&modi);
	dada_data_form_destroy(&form);
	return ret;

error:
	if (data) {dada_data_destroy(data); free(data); data=NULL;}
	ret = dada_web_error(meta, MHD_HTTP_INTERNAL_SERVER_ERROR);
	goto cleanup;
}

int dada_web_get_stxm(struct connection_meta* meta, const char* url)
{
	int ret = 0;
	int fromcache = 1;

	void* so_handle = NULL;
	struct dada_data* (*so_funcp)(struct dada_data_path*, struct dada_data_modi*, int fromcache, pthread_t* _thr) = NULL;
	struct dada_data* data = NULL;
	struct dada_data_path* path = NULL;
	struct dada_data_modi* modi = NULL;
	struct dada_data_form* form = NULL;
	pthread_t _thr;
	int n;

	path = dada_data_path_extract(url);
	if (path == NULL)                            goto no_url;
	if (path->len < 4)                           goto no_url;
	if (path->len == 4 && path->path[3] == NULL) goto no_url;
	if (path->path[path->len-1] == NULL)         goto no_url;
	modi = dada_data_modi_extract(meta->conn);
	form = dada_data_form_extract(meta->conn);
	for (n=0; n<form->len; n++) if (strcmp(form->key[n], "fromcache")==0) fromcache = atoi(form->val[n]);
	if (fromcache) ret = dada_web_get_cache(meta, "stxm", path, modi, form);
	if (ret == MHD_YES) goto cleanup;


	/* delegate the data preprocessing / retrieval */
	DADA_DL_LOAD_SYMBOL(DADA_DL_PATH_CONST("algo/stxm"), "dada_algo_stxm", so_funcp, so_handle);
	data = so_funcp(path, modi, fromcache, &_thr);
	// fprintf(stdout, "*** _thr is %ld ***\n", _thr);
	if (!data || data->progress >= 100) DADA_DL_CLOSE(so_handle);

	// DADA_RING_PRINTF("dims: %d (%d,%d,%d)", data->D, data->d[0], data->d[1], data->d[2]);

	/* NOTE:
	 * now the requested signal type is already chosen by algo/stxm,
	 * so whatevere we got here, we can directly send out to the user.
	 * */

	/* if we got something, prepare response; otherwise error */
	if (data && data->buffer.p)
	{
		double min, max;
		int x, y, X, Y;
		FILE* mem = NULL;
		size_t memlen;
		int n;
		int YY   = data->d[0];
		int vert = -1;

		/* first, check for an incomplete stxm, i.e. vert > YY = data->d[1];
		 * if so:
		 * - change modifier,
		 * - re-create canonical,
		 * - send 303 See Other to client with new canonical address */
		for (n=0; n<modi->len; n++) if (strcmp(modi->key[n], "vert")==0) vert = atoi(modi->val[n]);
		if (vert > YY)
		{
			for (n=0; n<modi->len; n++)
			{
				if (strcmp(modi->key[n], "vert")==0)
				{
					char tmp[16];
					snprintf(tmp, sizeof(tmp)-1, "%d", YY);
					if (modi->val[n]) {free(modi->val[n]); modi->val[n]=NULL;}
					modi->val[n] = strdup(tmp);
				}
			}
			if (meta->canonical) {free(meta->canonical); meta->canonical=NULL;}
			meta->canonical = dada_webcache_canonicalise("stxm", path, modi, form);
			if (data) {dada_data_destroy(data); free(data); data=NULL;}
			ret = dada_web_see_other(meta);
			goto cleanup;
		}

		/* find min/max, but only for 2D data */
		DADA_MEMSTREAM(mem, meta->xmeta, memlen);
		if (data->D == 2)
		{
			Y = data->d[0]; X = data->d[1];
			switch (data->type)
			{
				case DADA_DATA_DOUBLE:
					min = max = data->buffer.f[0];
					for (y=0; y<Y; y++) for (x=0; x<X; x++) if (data->buffer.f[y*X+x] < min) min = data->buffer.f[y*X+x];
					for (y=0; y<Y; y++) for (x=0; x<X; x++) if (data->buffer.f[y*X+x] > max) max = data->buffer.f[y*X+x];
					break;
				case DADA_DATA_INT:
					min = max = data->buffer.f[0];
					for (y=0; y<Y; y++) for (x=0; x<X; x++) if (data->buffer.i[y*X+x] < min) min = data->buffer.i[y*X+x];
					for (y=0; y<Y; y++) for (x=0; x<X; x++) if (data->buffer.i[y*X+x] > max) max = data->buffer.i[y*X+x];
					break;
				default:
					min = max = 0;
					break;
			}
			if (isnan(min)!=0 || isnan(max)!=0 || isinf(min)!=0 || isinf(max)!=0)
				fprintf(mem, "{\"status\": \"okay\", \"dump\": {}, \"min\": null, \"max\": null}");
			else
				fprintf(mem, "{\"status\": \"okay\", \"dump\": {}, \"min\": %g, \"max\": %g}", min, max);
		}
		else
		{
			fprintf(mem, "{\"status\": \"okay\", \"dump\": {}, \"min\": null, \"max\": null}");
		}
		fclose(mem);

err_memstream:
		ret = dada_web_reply(meta, data, form, fromcache);
		goto cleanup;
	}

	/* if we got data, but empty buffer: job is already running; reply with 202 ACCEPTED */
	if (data && data->buffer.p == NULL)
	{
		ret = dada_web_accepted(meta, data->progress);
		if (data) {dada_data_destroy(data); free(data); data=NULL;}
		goto cleanup;
	}

	goto error;

cleanup:
	meta->raeum                  = (struct dada_web_aufraeumer*) malloc(sizeof(struct dada_web_aufraeumer));
	meta->raeum->type            = DADA_WEB_AUFRAEUMER_STXM;
	meta->raeum->stxm            = (struct dada_web_get_stxm*)   malloc(sizeof(struct dada_web_get_stxm));
	meta->raeum->stxm->so_handle = so_handle;
	meta->raeum->stxm->path      = path;
	meta->raeum->stxm->modi      = modi;
	meta->raeum->stxm->form      = form;
	meta->raeum->stxm->thr       = _thr;
	meta->raeum->post            = NULL;

	return ret;

error:
	if (data) {dada_data_destroy(data); free(data); data=NULL;}
	ret = dada_web_error(meta, MHD_HTTP_INTERNAL_SERVER_ERROR);
	goto cleanup;

no_url:
	if (data) {dada_data_destroy(data); free(data); data=NULL;}
	ret = dada_web_error(meta, MHD_HTTP_NOT_FOUND);
	goto cleanup;
}

int dada_web_get_polar(struct connection_meta* meta, const char* url)
{
	const int timing = 0;
	int fromcache = 1;

	struct timespec t0, t1, t2, t3, t4;
	double tt1, tt2, tt3, tt4;
	clock_gettime(CLOCK_REALTIME, &t0);

	int ret = 0;
	void* so_handle = NULL;
	struct dada_data* (*so_funcp)(struct dada_data_path*,struct dada_data_modi*,struct dada_data*,enum dada_cache_flags) = NULL;
	struct dada_data* data  = NULL;
	struct dada_data* polar = NULL;
	struct dada_data_path* path = NULL;
	struct dada_data_modi* modi = NULL;
	struct dada_data_form* form = NULL;
	int n;

	path = dada_data_path_extract(url);
	modi = dada_data_modi_extract(meta->conn);
	form = dada_data_form_extract(meta->conn);

	for (n=0; n<form->len; n++) if (strcmp(form->key[n], "fromcache")==0) fromcache = atoi(form->val[n]);

	/* if path is too short for full image, delegate to dada_web_get_list */
	if (path == NULL)
		return dada_web_get_list(meta, url);
	if (path->len < 4)
		return dada_web_get_list(meta, url);
	if (path->len == 4 && path->path[3] == NULL)
		return dada_web_get_list(meta, url);
	if (path->path[path->len-1] == NULL)
		return dada_web_get_list(meta, url);

	clock_gettime(CLOCK_REALTIME, &t1);
	if (fromcache) ret = dada_web_get_cache(meta, "polar", path, modi, form);
	if (ret == MHD_YES) goto cleanup;

	clock_gettime(CLOCK_REALTIME, &t2);
	/* delegate the data preprocessing / retrieval */
	DADA_DL_LOAD_SYMBOL(DADA_DL_PATH_CONST("input/retrieve"), "dada_input_retrieve", so_funcp, so_handle);
	// data = so_funcp(path, modi, data, DADA_CACHE_READ|DADA_CACHE_WRITE);
	data = so_funcp(path, modi, data, DADA_CACHE_NONE);
	DADA_DL_CLOSE(so_handle);
	clock_gettime(CLOCK_REALTIME, &t3);


	/* convert cartesian image "data" to polar coordinates */
	{
		struct dada_data* (*so_funcp)(struct dada_polar*,struct dada_data_modi*,struct dada_data*,enum dada_cache_flags) = NULL;
		DADA_DL_LOAD_SYMBOL(DADA_DL_PATH_CONST("algo/polar"), "dada_algo_polar", so_funcp, so_handle);
		// data = so_funcp(modi, data, DADA_CACHE_READ|DADA_CACHE_WRITE);
		if (data) polar = so_funcp(NULL, modi, data, DADA_CACHE_NONE);
		if (data) {dada_data_destroy(data); free(data); data=NULL;}
		data = polar;
		DADA_DL_CLOSE(so_handle);
	}

	/* if we got something, prepare response; otherwise error */
	if (data && data->buffer.p)
	{
		double min, max;
		int x, y, X, Y;
		FILE* mem = NULL;
		size_t memlen;

		/* meta data */
		DADA_MEMSTREAM(mem, meta->xmeta, memlen);
		Y = data->d[0]; X = data->d[1];
		switch (data->type)
		{
			case DADA_DATA_DOUBLE:
				min = max = data->buffer.f[0];
				for (y=0; y<Y; y++) for (x=0; x<X; x++) if (data->buffer.f[y*X+x] < min) min = data->buffer.f[y*X+x];
				for (y=0; y<Y; y++) for (x=0; x<X; x++) if (data->buffer.f[y*X+x] > max) max = data->buffer.f[y*X+x];
				break;
			case DADA_DATA_INT:
				min = max = data->buffer.f[0];
				for (y=0; y<Y; y++) for (x=0; x<X; x++) if (data->buffer.i[y*X+x] < min) min = data->buffer.i[y*X+x];
				for (y=0; y<Y; y++) for (x=0; x<X; x++) if (data->buffer.i[y*X+x] > max) max = data->buffer.i[y*X+x];
				break;
			default:
				min = max = 0;
				 break;
		}
		if (isnan(min) || isnan(max) || isinf(min) || isinf(max))
			fprintf(mem, "{\"status\": \"okay\", \"dump\": {}, \"min\": null, \"max\": null}");
		else
			fprintf(mem, "{\"status\": \"okay\", \"dump\": {}, \"min\": %g, \"max\": %g}", min, max);
		fclose(mem);

err_memstream:
		ret = dada_web_reply(meta, data, form, fromcache);
		goto cleanup;
	}

	goto error;

cleanup:
	dada_data_path_destroy(&path);
	dada_data_modi_destroy(&modi);
	dada_data_form_destroy(&form);
	clock_gettime(CLOCK_REALTIME, &t4);

	tt1 = (t1.tv_sec-t0.tv_sec)+(t1.tv_nsec-t0.tv_nsec)*1e-9;
	tt2 = (t2.tv_sec-t1.tv_sec)+(t2.tv_nsec-t1.tv_nsec)*1e-9;
	tt3 = (t3.tv_sec-t2.tv_sec)+(t3.tv_nsec-t2.tv_nsec)*1e-9;
	tt4 = (t4.tv_sec-t3.tv_sec)+(t4.tv_nsec-t3.tv_nsec)*1e-9;
	if (timing) DADA_RING_PRINTF("show-timing: %.1f ms+%.1f ms+%.1f ms+%.1f ms", tt1*1e3, tt2*1e3, tt3*1e3, tt4*1e3);

	return ret;

error:
	if (data) {dada_data_destroy(data); free(data); data=NULL;}
	ret = dada_web_error(meta, MHD_HTTP_INTERNAL_SERVER_ERROR);
	goto cleanup;

}

int dada_web_get_xcca(struct connection_meta* meta, const char* url)
{
	const int timing = 0;
	int fromcache = 1;

	struct timespec t0, t1, t2, t3, t4;
	double tt1, tt2, tt3, tt4;
	clock_gettime(CLOCK_REALTIME, &t0);

	int ret = 0;
	void* so_handle = NULL;
	struct dada_data* (*so_funcp)(struct dada_data_path*,struct dada_data_modi*,struct dada_data*,enum dada_cache_flags) = NULL;
	struct dada_data* data  = NULL;
	struct dada_data* polar = NULL;
	struct dada_data* xcca  = NULL;
	struct dada_data_path* path = NULL;
	struct dada_data_modi* modi = NULL;
	struct dada_data_form* form = NULL;
	int n;

	path = dada_data_path_extract(url);
	modi = dada_data_modi_extract(meta->conn);
	form = dada_data_form_extract(meta->conn);

	for (n=0; n<form->len; n++) if (strcmp(form->key[n], "fromcache")==0) fromcache = atoi(form->val[n]);

	/* if path is too short for full image, delegate to dada_web_get_list */
	if (path == NULL)
		return dada_web_get_list(meta, url);
	if (path->len < 4)
		return dada_web_get_list(meta, url);
	if (path->len == 4 && path->path[3] == NULL)
		return dada_web_get_list(meta, url);
	if (path->path[path->len-1] == NULL)
		return dada_web_get_list(meta, url);

	clock_gettime(CLOCK_REALTIME, &t1);
	if (fromcache) ret = dada_web_get_cache(meta, "show", path, modi, form);
	if (ret == MHD_YES) goto cleanup;

	clock_gettime(CLOCK_REALTIME, &t2);
	/* delegate the data preprocessing / retrieval */
	DADA_DL_LOAD_SYMBOL(DADA_DL_PATH_CONST("input/retrieve"), "dada_input_retrieve", so_funcp, so_handle);
	// data = so_funcp(path, modi, data, DADA_CACHE_READ|DADA_CACHE_WRITE);
	data = so_funcp(path, modi, data, DADA_CACHE_NONE);
	DADA_DL_CLOSE(so_handle);
	clock_gettime(CLOCK_REALTIME, &t3);


	/* convert cartesian image "data" to polar coordinates */
	{
		struct dada_data* (*so_funcp)(struct dada_polar*,struct dada_data_modi*,struct dada_data*,enum dada_cache_flags) = NULL;
		DADA_DL_LOAD_SYMBOL(DADA_DL_PATH_CONST("algo/polar"), "dada_algo_polar", so_funcp, so_handle);
		// data = so_funcp(modi, data, DADA_CACHE_READ|DADA_CACHE_WRITE);
		if (data) polar = so_funcp(NULL, modi, data, DADA_CACHE_NONE);
		if (data) {dada_data_destroy(data); free(data); data=NULL;}
		data = polar;
		DADA_DL_CLOSE(so_handle);
	}

	/* next, do XCCA */
	{
		struct dada_data* (*so_funcp)(struct dada_xcca*,struct dada_data_modi*,struct dada_data*,enum dada_cache_flags) = NULL;
		DADA_DL_LOAD_SYMBOL(DADA_DL_PATH_CONST("algo/xcca"), "dada_algo_xcca", so_funcp, so_handle);
		// data = so_funcp(modi, data, DADA_CACHE_READ|DADA_CACHE_WRITE);
		if (data) xcca = so_funcp(NULL, modi, data, DADA_CACHE_NONE);
		if (data) {dada_data_destroy(data); free(data); data=NULL;}
		data = xcca;
		DADA_DL_CLOSE(so_handle);
	}

	/* if we got something, prepare response; otherwise error */
	if (data && data->buffer.p)
	{
		double min, max;
		int x, y, X, Y;
		FILE* mem = NULL;
		size_t memlen;

		/* meta data */
		DADA_MEMSTREAM(mem, meta->xmeta, memlen);
		Y = data->d[0]; X = data->d[1];
		switch (data->type)
		{
			case DADA_DATA_DOUBLE:
				min = max = data->buffer.f[0];
				for (y=0; y<Y; y++) for (x=0; x<X; x++) if (data->buffer.f[y*X+x] < min) min = data->buffer.f[y*X+x];
				for (y=0; y<Y; y++) for (x=0; x<X; x++) if (data->buffer.f[y*X+x] > max) max = data->buffer.f[y*X+x];
				break;
			case DADA_DATA_INT:
				min = max = data->buffer.f[0];
				for (y=0; y<Y; y++) for (x=0; x<X; x++) if (data->buffer.i[y*X+x] < min) min = data->buffer.i[y*X+x];
				for (y=0; y<Y; y++) for (x=0; x<X; x++) if (data->buffer.i[y*X+x] > max) max = data->buffer.i[y*X+x];
				break;
			default:
				min = max = 0;
				 break;
		}
		if (isnan(min) || isnan(max) || isinf(min) || isinf(max))
			fprintf(mem, "{\"status\": \"okay\", \"dump\": {}, \"min\": null, \"max\": null}");
		else
			fprintf(mem, "{\"status\": \"okay\", \"dump\": {}, \"min\": %g, \"max\": %g}", min, max);
		fclose(mem);

err_memstream:
		ret = dada_web_reply(meta, data, form, fromcache);
		goto cleanup;
	}

	goto error;

cleanup:
	// if (xcca) {dada_data_destroy(xcca); free(xcca); xcca=NULL;}
	dada_data_path_destroy(&path);
	dada_data_modi_destroy(&modi);
	dada_data_form_destroy(&form);
	clock_gettime(CLOCK_REALTIME, &t4);

	tt1 = (t1.tv_sec-t0.tv_sec)+(t1.tv_nsec-t0.tv_nsec)*1e-9;
	tt2 = (t2.tv_sec-t1.tv_sec)+(t2.tv_nsec-t1.tv_nsec)*1e-9;
	tt3 = (t3.tv_sec-t2.tv_sec)+(t3.tv_nsec-t2.tv_nsec)*1e-9;
	tt4 = (t4.tv_sec-t3.tv_sec)+(t4.tv_nsec-t3.tv_nsec)*1e-9;
	if (timing) DADA_RING_PRINTF("show-timing: %.1f ms+%.1f ms+%.1f ms+%.1f ms", tt1*1e3, tt2*1e3, tt3*1e3, tt4*1e3);

	return ret;

error:
	if (data) {dada_data_destroy(data); free(data); data=NULL;}
	ret = dada_web_error(meta, MHD_HTTP_INTERNAL_SERVER_ERROR);
	goto cleanup;

/*
no_url:
	if (data) {dada_data_destroy(data); free(data); data=NULL;}
	ret = dada_web_error(meta, MHD_HTTP_NOT_FOUND);
	goto cleanup;
*/
}


int dada_web_get_glue(struct connection_meta* meta, const char* sha)
{
	int ret = 0;
	int fromcache = 1;
	void* so_handle = NULL;
	const char* dl_what = NULL;
	struct dada_data* (*so_funcp)(const char*,const char*,const char*) = NULL;
	struct dada_data* data = NULL;
	struct dada_data_form* form = NULL;
	const char* key_space = "dada_cache";
	const char* key_set   = "";

	DADA_RING_PRINTF("/glue/%s", sha);

	form = dada_data_form_extract(meta->conn);

	/* get data from aerospike */
	// data = so_funcp(path, modi);
	dl_what = "dada_input_cache_read_data";
	DADA_DL_LOAD_SYMBOL(DADA_DL_PATH_CONST("input/cache"), dl_what, so_funcp, so_handle);
	data = so_funcp(key_space, key_set, sha);
	DADA_RING_PRINTF("data: %p", data);
	DADA_DL_CLOSE(so_handle);
	/* fall back: re-create data? */

	/* if we got something, prepare response; otherwise error */
	if (data && data->buffer.p)
	{
		double min, max;
		int x, y, X, Y;
		FILE* mem = NULL;
		size_t memlen;

		/* find min/max, but only for 2D data */
		DADA_MEMSTREAM(mem, meta->xmeta, memlen);
		if (data->D == 2)
		{
			Y = data->d[0]; X = data->d[1];
			switch (data->type)
			{
				case DADA_DATA_DOUBLE:
					min = max = data->buffer.f[0];
					for (y=0; y<Y; y++) for (x=0; x<X; x++) if (data->buffer.f[y*X+x] < min) min = data->buffer.f[y*X+x];
					for (y=0; y<Y; y++) for (x=0; x<X; x++) if (data->buffer.f[y*X+x] > max) max = data->buffer.f[y*X+x];
					break;
				case DADA_DATA_INT:
					min = max = data->buffer.f[0];
					for (y=0; y<Y; y++) for (x=0; x<X; x++) if (data->buffer.i[y*X+x] < min) min = data->buffer.i[y*X+x];
					for (y=0; y<Y; y++) for (x=0; x<X; x++) if (data->buffer.i[y*X+x] > max) max = data->buffer.i[y*X+x];
					break;
				default:
					min = max = 0;
					break;
			}
			if (isnan(min)!=0 || isnan(max)!=0 || isinf(min)!=0 || isinf(max)!=0)
				fprintf(mem, "{\"status\": \"okay\", \"dump\": {}, \"min\": null, \"max\": null}");
			else
				fprintf(mem, "{\"status\": \"okay\", \"dump\": {}, \"min\": %g, \"max\": %g}", min, max);
		}
		else
		{
			fprintf(mem, "{\"status\": \"okay\", \"dump\": {}, \"min\": null, \"max\": null}");
		}
		fclose(mem);

err_memstream:
		ret = dada_web_reply(meta, data, form, fromcache);
		goto cleanup;
	}

	goto error;

cleanup:
	dada_data_form_destroy(&form);
	return ret;

error:
	if (data) {dada_data_destroy(data); free(data); data=NULL;}
	ret = dada_web_error(meta, MHD_HTTP_INTERNAL_SERVER_ERROR);
	goto cleanup;
}

int dada_web_get_radial(struct connection_meta* meta, const char* url)
{
	int ret = 0;
	int fromcache = 1;
	void* so_handle = NULL;
	struct dada_data* (*so_funcp)(struct dada_data_path*,struct dada_data_modi*,struct dada_data*,enum dada_cache_flags) = NULL;
	struct dada_data* data   = NULL;
	struct dada_data* radial = NULL;
	struct dada_data_path* path = NULL;
	struct dada_data_modi* modi = NULL;
	struct dada_data_form* form = NULL;
	int n;

	path = dada_data_path_extract(url);
	modi = dada_data_modi_extract(meta->conn);
	form = dada_data_form_extract(meta->conn);

	for (n=0; n<form->len; n++) if (strcmp(form->key[n], "fromcache")==0) fromcache = atoi(form->val[n]);

	/* if path is too short for full image, delegate to dada_web_get_list */
	if (path == NULL)
		return dada_web_get_list(meta, url);
	if (path->len < 4)
		return dada_web_get_list(meta, url);
	if (path->len == 4 && path->path[3] == NULL)
		return dada_web_get_list(meta, url);

	if (fromcache) ret = dada_web_get_cache(meta, "radial", path, modi, form);
	if (ret == MHD_YES) goto cleanup;

	/* first, retrieve the image */
	DADA_DL_LOAD_SYMBOL(DADA_DL_PATH_CONST("input/retrieve"), "dada_input_retrieve", so_funcp, so_handle);
	// data = so_funcp(path, modi, data, DADA_CACHE_READ|DADA_CACHE_WRITE);
	data = so_funcp(path, modi, data, DADA_CACHE_NONE);
	DADA_DL_CLOSE(so_handle);


	/* next, do azimuthal integration and obtain radial profile */
	{
		struct dada_data* (*so_funcp)(struct dada_radial*,struct dada_data_modi*,struct dada_data*,enum dada_cache_flags) = NULL;
		DADA_DL_LOAD_SYMBOL(DADA_DL_PATH_CONST("algo/radial"), "dada_algo_radial", so_funcp, so_handle);
		// data = so_funcp(modi, data, DADA_CACHE_READ|DADA_CACHE_WRITE);
		if (data) radial = so_funcp(NULL, modi, data, DADA_CACHE_NONE);
		if (data) {dada_data_destroy(data); free(data); data=NULL;}
		data = radial;
		DADA_DL_CLOSE(so_handle);
	}


	/* if we got something, prepare response; otherwise error */
	if (data && data->buffer.p)
	{
		double min, max;
		int x, X;
		FILE* mem = NULL;
		size_t memlen;
		DADA_MEMSTREAM(mem, meta->xmeta, memlen);
		X = data->d[0];
		switch (data->type)
		{
			case DADA_DATA_DOUBLE:
				min = max = data->buffer.f[0];
				for (x=0; x<X; x++) if (data->buffer.f[x] < min) min = data->buffer.f[x];
				for (x=0; x<X; x++) if (data->buffer.f[x] > max) max = data->buffer.f[x];
				break;
			case DADA_DATA_INT:
				min = max = data->buffer.f[0];
				for (x=0; x<X; x++) if (data->buffer.i[x] < min) min = data->buffer.i[x];
				for (x=0; x<X; x++) if (data->buffer.i[x] > max) max = data->buffer.i[x];
				break;
			default:
				min = max = 0;
				 break;
		}
		if (isnan(min) || isnan(max) || isinf(min) || isinf(max))
			fprintf(mem, "{\"status\": \"okay\", \"dump\": {}, \"min\": null, \"max\": null}");
		else
			fprintf(mem, "{\"status\": \"okay\", \"dump\": {}, \"min\": %g, \"max\": %g}", min, max);
		fclose(mem);

err_memstream:
		ret = dada_web_reply(meta, data, form, fromcache);
		goto cleanup;
	}

	goto error;

cleanup:
	dada_data_path_destroy(&path);
	dada_data_modi_destroy(&modi);
	dada_data_form_destroy(&form);
	return ret;

error:
	if (data) {dada_data_destroy(data); free(data); data=NULL;}
	ret = dada_web_error(meta, MHD_HTTP_INTERNAL_SERVER_ERROR);
	goto cleanup;

/*
no_url:
	if (data) {dada_data_destroy(data); free(data); data=NULL;}
	ret = dada_web_error(meta, MHD_HTTP_NOT_FOUND);
	goto cleanup;
*/
}

int dada_web_get_mask(struct connection_meta* meta, const char* url)
{
	const int timing = 0;
	int fromcache = 1;

	struct timespec t0, t1, t2, t3, t4;
	double tt1, tt2, tt3, tt4;
	clock_gettime(CLOCK_REALTIME, &t0);

	int ret = 0;
	void* so_handle = NULL;
	struct dada_data* (*so_funcp)(struct dada_data_path*,struct dada_data_modi*,struct dada_data*,enum dada_cache_flags) = NULL;
	struct dada_data* data   = NULL;
	struct dada_data* radial = NULL;
	struct dada_data_path* path = NULL;
	struct dada_data_modi* modi = NULL;
	struct dada_data_form* form = NULL;
	int n;

	path = dada_data_path_extract(url);
	modi = dada_data_modi_extract(meta->conn);
	form = dada_data_form_extract(meta->conn);

	for (n=0; n<form->len; n++) if (strcmp(form->key[n], "fromcache")==0) fromcache = atoi(form->val[n]);

	/* if path is too short for full image, delegate to dada_web_get_mask_list */
	if (path == NULL)
		return dada_web_get_mask_list(meta, url);
	if (path->len < 4)
		return dada_web_get_mask_list(meta, url);
	if (path->len == 4 && path->path[3] == NULL)
		return dada_web_get_mask_list(meta, url);
	if (path->path[path->len-1] == NULL)
		return dada_web_get_mask_list(meta, url);

	clock_gettime(CLOCK_REALTIME, &t1);
	if (fromcache) ret = dada_web_get_cache(meta, "show", path, modi, form);
	if (fromcache) if (ret == MHD_YES) goto cleanup;

	clock_gettime(CLOCK_REALTIME, &t2);
	/* delegate the data preprocessing / retrieval */
	DADA_DL_LOAD_SYMBOL(DADA_DL_PATH_CONST("input/mask"), "dada_mask_load", so_funcp, so_handle);
	data = so_funcp(path, modi, data, DADA_CACHE_NONE);
	DADA_DL_CLOSE(so_handle);
	clock_gettime(CLOCK_REALTIME, &t3);


	/* if we got something, prepare response; otherwise error */
	if (data && data->buffer.p)
	{
		double min, max;
		int x, y, X, Y;
		FILE* mem = NULL;
		size_t memlen;

		/* meta data */
		DADA_MEMSTREAM(mem, meta->xmeta, memlen);
		Y = data->d[0]; X = data->d[1];
		switch (data->type)
		{
			case DADA_DATA_DOUBLE:
				min = max = data->buffer.f[0];
				for (y=0; y<Y; y++) for (x=0; x<X; x++) if (data->buffer.f[y*X+x] < min) min = data->buffer.f[y*X+x];
				for (y=0; y<Y; y++) for (x=0; x<X; x++) if (data->buffer.f[y*X+x] > max) max = data->buffer.f[y*X+x];
				break;
			case DADA_DATA_INT:
				min = max = data->buffer.f[0];
				for (y=0; y<Y; y++) for (x=0; x<X; x++) if (data->buffer.i[y*X+x] < min) min = data->buffer.i[y*X+x];
				for (y=0; y<Y; y++) for (x=0; x<X; x++) if (data->buffer.i[y*X+x] > max) max = data->buffer.i[y*X+x];
				break;
			default:
				min = max = 0;
				 break;
		}
		if (isnan(min) || isnan(max) || isinf(min) || isinf(max))
			fprintf(mem, "{\"status\": \"okay\", \"dump\": {}, \"min\": null, \"max\": null}");
		else
			fprintf(mem, "{\"status\": \"okay\", \"dump\": {}, \"min\": %g, \"max\": %g}", min, max);
		fclose(mem);

		/* radial profiles */
		{
			struct dada_data* (*so_funcp)(struct dada_radial*,struct dada_data_modi*,struct dada_data*,enum dada_cache_flags) = NULL;
			DADA_DL_LOAD_SYMBOL(DADA_DL_PATH_CONST("algo/radial"), "dada_algo_radial", so_funcp, so_handle);
			radial = so_funcp(NULL, modi, data, DADA_CACHE_NONE);
			DADA_DL_CLOSE(so_handle);
		}
		if (radial)
		{
			DADA_MEMSTREAM(mem, meta->xradial, memlen);
			int r, R = radial->d[0];
			fprintf(mem, "{\"radial\": [");
			for (r=0; r<R; r++)
			{
				fprintf(mem, "[%d, %g]", r, radial->buffer.f[r]);
				if (r != R-1) fprintf(mem, ", ");
			}
			fprintf(mem, "]}");
			fclose(mem);
		}

err_memstream:
		ret = dada_web_reply(meta, data, form, fromcache);
		goto cleanup;
	}

	goto error;

cleanup:
	if (radial) {dada_data_destroy(radial); free(radial); radial=NULL;}
	dada_data_path_destroy(&path);
	dada_data_modi_destroy(&modi);
	dada_data_form_destroy(&form);
	clock_gettime(CLOCK_REALTIME, &t4);

	tt1 = (t1.tv_sec-t0.tv_sec)+(t1.tv_nsec-t0.tv_nsec)*1e-9;
	tt2 = (t2.tv_sec-t1.tv_sec)+(t2.tv_nsec-t1.tv_nsec)*1e-9;
	tt3 = (t3.tv_sec-t2.tv_sec)+(t3.tv_nsec-t2.tv_nsec)*1e-9;
	tt4 = (t4.tv_sec-t3.tv_sec)+(t4.tv_nsec-t3.tv_nsec)*1e-9;
	if (timing) DADA_RING_PRINTF("show-timing: %.1f ms+%.1f ms+%.1f ms+%.1f ms", tt1*1e3, tt2*1e3, tt3*1e3, tt4*1e3);

	return ret;

error:
	if (data) {dada_data_destroy(data); free(data); data=NULL;}
	ret = dada_web_error(meta, MHD_HTTP_INTERNAL_SERVER_ERROR);
	goto cleanup;

/*
no_url:
	if (data) {dada_data_destroy(data); free(data); data=NULL;}
	ret = dada_web_error(meta, MHD_HTTP_NOT_FOUND);
	goto cleanup;
*/
}


int dada_web_get_colourpalette(struct connection_meta* meta, const char* what)
{
	int ret = 0;
	int fromcache = 1;
	struct dada_data* img = NULL;
	struct dada_data_form* form = NULL;
	int x;

	img = dada_data_init(DADA_DATA_INT, 2, 1, 64);
	for (x=0; x<img->d[1]; x++) img->buffer.i[x] = x;

	form = (struct dada_data_form*) malloc(sizeof(struct dada_data_form));
	form->len = 1;
	form->key = (char**) malloc(form->len*sizeof(char*));
	form->val = (char**) malloc(form->len*sizeof(char*));
	form->key[0] = strdup("palette");
	form->val[0] = strdup(what);

	// fprintf(stdout, "what: [%s]\n", what);

	ret = dada_web_reply_png(meta, img, form, fromcache);

	dada_data_form_destroy(&form);

	return ret;
}

int dada_web_reply(struct connection_meta* meta, struct dada_data* data, struct dada_data_form* form, int fromcache)
{
	int n, N=form->len;
	enum dada_data_form_type type = DADA_DATA_FORM_PNG; /*default output format */


	for (n=0; n<N; n++)
	{
		if (strcmp(form->key[n], "format")==0)
		{
			if (strcmp(form->val[n], "double"   )==0) type = DADA_DATA_FORM_DOUBLE;
			if (strcmp(form->val[n], "ascii"    )==0) type = DADA_DATA_FORM_ASCII;
			if (strcmp(form->val[n], "json"     )==0) type = DADA_DATA_FORM_JSON;
			if (strcmp(form->val[n], "png"      )==0) type = DADA_DATA_FORM_PNG;
			if (strcmp(form->val[n], "plotpng"  )==0) type = DADA_DATA_FORM_PLOT;
			if (strcmp(form->val[n], "tiff_raw" )==0) type = DADA_DATA_FORM_TIFF_RAW;
			break;
		}
	}
	switch (data->D)
	{
		case 1:
			switch (type)
			{
				case DADA_DATA_FORM_ASCII:    return dada_web_reply_ascii    (meta, data, fromcache);
				case DADA_DATA_FORM_JSON:     return dada_web_reply_json     (meta, data, fromcache);
				case DADA_DATA_FORM_PLOT:     return dada_web_reply_plot_png (meta, data, form, fromcache);
				default:
							      return dada_web_error(meta, MHD_HTTP_NOT_IMPLEMENTED);
			}
			/* FALLTHROUGH */
		case 2:
			switch (type)
			{
				case DADA_DATA_FORM_ASCII:    return dada_web_reply_ascii    (meta, data, fromcache);
				case DADA_DATA_FORM_JSON:     return dada_web_reply_json     (meta, data, fromcache);
				case DADA_DATA_FORM_PNG:      return dada_web_reply_png      (meta, data, form, fromcache);
				case DADA_DATA_FORM_TIFF_RAW: return dada_web_reply_tiff_raw (meta, data, form, fromcache);
				case DADA_DATA_FORM_DOUBLE:   return dada_web_reply_double   (meta, data, fromcache);
				default:
							      return dada_web_error(meta, MHD_HTTP_NOT_IMPLEMENTED);
			}
			/* FALLTHROUGH */
		default:
			switch (type)
			{
				case DADA_DATA_FORM_ASCII:    return dada_web_reply_ascii (meta, data, fromcache);
				case DADA_DATA_FORM_JSON:     return dada_web_reply_json  (meta, data, fromcache);
				case DADA_DATA_FORM_DOUBLE:   return dada_web_reply_double(meta, data, fromcache);
				default:
							      return dada_web_error(meta, MHD_HTTP_NOT_IMPLEMENTED);
			}
	}

	return dada_web_error(meta, MHD_HTTP_INTERNAL_SERVER_ERROR);
}

int dada_web_reply_png(struct connection_meta* meta, struct dada_data* data, struct dada_data_form* form, int fromcache)
{
	const int timing = 0;

	struct timespec t0, t1, t2, t3;
	double tt1, tt2, tt3;
	clock_gettime(CLOCK_REALTIME, &t0);

	int status = MHD_HTTP_NO_CONTENT;
	struct MHD_Response* response = NULL;
	const char* mime = "image/png";
	char host[NI_MAXHOST];

	void* png = NULL;
	size_t len = 0;

	void* so_handle = NULL;
	void* (*so_funcp)(struct dada_data*, struct dada_data_form*, size_t*) = NULL;

	DADA_DL_LOAD_SYMBOL(DADA_DL_PATH_CONST("output/png"), "dada_output_png", so_funcp, so_handle);
	png = so_funcp(data, form, &len);
	DADA_DL_CLOSE(so_handle);

	clock_gettime(CLOCK_REALTIME, &t1);

	if (png)
	{
		char sec[16];
		char cid[16];
		status = MHD_HTTP_OK;
		mime = "image/png";
		response = MHD_create_response_from_buffer(len, png, MHD_RESPMEM_MUST_FREE);
		MHD_queue_response(meta->conn, status, response);
		MHD_add_response_header(response, "Content-Type", mime);
		if (fromcache) MHD_add_response_header(response, "Cache-Control", CACHE_1DAY);
		else           MHD_add_response_header(response, "Cache-Control", CACHE_NONE);
		if (fromcache) MHD_add_response_header(response, "ETag", meta->canonical);
		clock_gettime(CLOCK_REALTIME, &meta->t1);
		meta->seconds = (meta->t1.tv_sec+1e-9*meta->t1.tv_nsec) - (meta->t0.tv_sec+1e-9*meta->t0.tv_nsec);
		snprintf(sec, sizeof(sec)-1, "%.3fms", 1e3*meta->seconds);
		snprintf(cid, sizeof(cid)-1, "%08lx", meta->connid);
		MHD_add_response_header(response, "X-Timing", sec);
		MHD_add_response_header(response, "X-connid", cid);
		if (meta->xmeta)   MHD_add_response_header(response, "X-meta",   meta->xmeta  );
		if (meta->xradial) MHD_add_response_header(response, "X-radial", meta->xradial);
		gethostname(host, sizeof(host)-1);
		MHD_add_response_header(response, "X-dada-host", host);
		dada_web_put_cache(meta, mime, len, png);
		MHD_destroy_response(response);
		png = NULL; /* will be free'd by MHD */
		clock_gettime(CLOCK_REALTIME, &t2);
		goto cleanup;
	}

	goto error;

cleanup:
	if (data) {dada_data_destroy(data); free(data); data=NULL;}
	if (png)  {free(png); png=NULL;}

	clock_gettime(CLOCK_REALTIME, &t3);
	tt1 = (t1.tv_sec-t0.tv_sec)+(t1.tv_nsec-t0.tv_nsec)*1e-9;
	tt2 = (t2.tv_sec-t1.tv_sec)+(t2.tv_nsec-t1.tv_nsec)*1e-9;
	tt3 = (t3.tv_sec-t2.tv_sec)+(t3.tv_nsec-t2.tv_nsec)*1e-9;

	if (timing) DADA_RING_PRINTF("png-timing: %.1f ms+%.1f ms+%.1f ms", tt1*1e3, tt2*1e3, tt3*1e3);

	return MHD_YES;

error:
	if (data) {dada_data_destroy(data); free(data); data=NULL;}
	if (png)  {free(png); png=NULL;}
	status = MHD_HTTP_INTERNAL_SERVER_ERROR;
	return dada_web_error(meta, status);
}
int dada_web_reply_tiff_raw(struct connection_meta* meta, struct dada_data* data, struct dada_data_form* form, int fromcache)
{
	const int timing = 1;

	struct timespec t0, t1, t2, t3;
	double tt1, tt2, tt3;
	clock_gettime(CLOCK_REALTIME, &t0);

	int status = MHD_HTTP_NO_CONTENT;
	struct MHD_Response* response = NULL;
	const char* mime = "image/tiff";
	char host[NI_MAXHOST];

	void* tiff = NULL;
	size_t len = 0;

	void* so_handle = NULL;
	void* (*so_funcp)(struct dada_data*, struct dada_data_form*, size_t*) = NULL;

	DADA_DL_LOAD_SYMBOL(DADA_DL_PATH_CONST("output/tiff-raw"), "dada_output_tiff_raw", so_funcp, so_handle);
	tiff = so_funcp(data, form, &len);
	DADA_DL_CLOSE(so_handle);

	clock_gettime(CLOCK_REALTIME, &t1);

	if (tiff)
	{
		char sec[16];
		char cid[16];
		status = MHD_HTTP_OK;
		mime = "image/tiff";
		response = MHD_create_response_from_buffer(len, tiff, MHD_RESPMEM_MUST_FREE);
		MHD_queue_response(meta->conn, status, response);
		MHD_add_response_header(response, "Content-Type", mime);
		if (fromcache) MHD_add_response_header(response, "Cache-Control", CACHE_1DAY);
		else           MHD_add_response_header(response, "Cache-Control", CACHE_NONE);
		if (fromcache) MHD_add_response_header(response, "ETag", meta->canonical);
		clock_gettime(CLOCK_REALTIME, &meta->t1);
		meta->seconds = (meta->t1.tv_sec+1e-9*meta->t1.tv_nsec) - (meta->t0.tv_sec+1e-9*meta->t0.tv_nsec);
		snprintf(sec, sizeof(sec)-1, "%.3fms", 1e3*meta->seconds);
		snprintf(cid, sizeof(cid)-1, "%08lx", meta->connid);
		MHD_add_response_header(response, "X-Timing", sec);
		MHD_add_response_header(response, "X-connid", cid);
		if (meta->xmeta)   MHD_add_response_header(response, "X-meta",   meta->xmeta  );
		if (meta->xradial) MHD_add_response_header(response, "X-radial", meta->xradial);
		gethostname(host, sizeof(host)-1);
		MHD_add_response_header(response, "X-dada-host", host);
		dada_web_put_cache(meta, mime, len, tiff);
		MHD_destroy_response(response);
		tiff = NULL; /* will be free'd by MHD */
		clock_gettime(CLOCK_REALTIME, &t2);
		goto cleanup;
	}

	goto error;

cleanup:
	if (data) {dada_data_destroy(data); free(data); data=NULL;}
	if (data) {dada_data_destroy(data); free(data); data=NULL;}
	if (tiff) {free(tiff); tiff=NULL;}

	clock_gettime(CLOCK_REALTIME, &t3);
	tt1 = (t1.tv_sec-t0.tv_sec)+(t1.tv_nsec-t0.tv_nsec)*1e-9;
	tt2 = (t2.tv_sec-t1.tv_sec)+(t2.tv_nsec-t1.tv_nsec)*1e-9;
	tt3 = (t3.tv_sec-t2.tv_sec)+(t3.tv_nsec-t2.tv_nsec)*1e-9;

	if (timing) DADA_RING_PRINTF("tiff-timing: %.1f ms+%.1f ms+%.1f ms", tt1*1e3, tt2*1e3, tt3*1e3);

	return MHD_YES;

error:
	if (data) {dada_data_destroy(data); free(data); data=NULL;}
	if (tiff) {free(tiff); tiff=NULL;}
	status = MHD_HTTP_INTERNAL_SERVER_ERROR;
	return dada_web_error(meta, status);
}
int dada_web_reply_ascii(struct connection_meta* meta, struct dada_data* data, int fromcache)
{
	int status = MHD_HTTP_NO_CONTENT;
	struct MHD_Response* response = NULL;
	const char* mime = "text/plain";

	char* str = NULL;
	void* so_handle = NULL;
	char* (*so_funcp)(struct dada_data* data) = NULL;

	DADA_DL_LOAD_SYMBOL(DADA_DL_PATH_CONST("output/ascii"), "dada_output_ascii", so_funcp, so_handle);
	str = so_funcp(data);
	DADA_DL_CLOSE(so_handle);

	if (str)
	{
		status = MHD_HTTP_OK;
		mime = "text/plain";
		response = MHD_create_response_from_buffer(strlen(str), (void*)str, MHD_RESPMEM_MUST_FREE);
		MHD_queue_response(meta->conn, status, response);
		MHD_add_response_header(response, "Content-Type", mime);
		if (fromcache) MHD_add_response_header(response, "Cache-Control", CACHE_1DAY);
		else           MHD_add_response_header(response, "Cache-Control", CACHE_NONE);
		if (fromcache) MHD_add_response_header(response, "ETag", meta->canonical);
		dada_web_put_cache(meta, mime, strlen(str), str);
		MHD_destroy_response(response);
		str = NULL; /* will be free'd by MHD */
		goto cleanup;
	}

	goto error;

cleanup:
	if (data) {dada_data_destroy(data); free(data); data=NULL;}
	if (str)  {free(str); str=NULL;}
	return MHD_YES;

error:
	if (data) {dada_data_destroy(data); free(data); data=NULL;}
	if (str)  {free(str); str=NULL;}
	status = MHD_HTTP_INTERNAL_SERVER_ERROR;
	return dada_web_error(meta, status);
}
int dada_web_reply_json(struct connection_meta* meta, struct dada_data* data, int fromcache)
{
	int status = MHD_HTTP_NO_CONTENT;
	struct MHD_Response* response = NULL;
	const char* mime = "application/json";

	char* str = NULL;
	void* so_handle = NULL;
	char* (*so_funcp)(struct dada_data* data) = NULL;

	DADA_DL_LOAD_SYMBOL(DADA_DL_PATH_CONST("output/json"), "dada_output_json", so_funcp, so_handle);
	str = so_funcp(data);
	DADA_DL_CLOSE(so_handle);

	if (str)
	{
		status = MHD_HTTP_OK;
		mime = "application/json";
		response = MHD_create_response_from_buffer(strlen(str), (void*)str, MHD_RESPMEM_MUST_FREE);
		MHD_queue_response(meta->conn, status, response);
		MHD_add_response_header(response, "Content-Type", mime);
		if (fromcache) MHD_add_response_header(response, "Cache-Control", CACHE_1DAY);
		else           MHD_add_response_header(response, "Cache-Control", CACHE_NONE);
		if (fromcache) MHD_add_response_header(response, "ETag", meta->canonical);
		dada_web_put_cache(meta, mime, strlen(str), str);
		MHD_destroy_response(response);
		str = NULL; /* will be free'd by MHD */
		goto cleanup;
	}

	goto error;

cleanup:
	if (data) {dada_data_destroy(data); free(data); data=NULL;}
	if (str)  {free(str); str=NULL;}
	return MHD_YES;

error:
	if (data) {dada_data_destroy(data); free(data); data=NULL;}
	if (str)  {free(str); str=NULL;}
	status = MHD_HTTP_INTERNAL_SERVER_ERROR;
	return dada_web_error(meta, status);
}
int dada_web_reply_plot_png(struct connection_meta* meta, struct dada_data* data, struct dada_data_form* form, int fromcache)
{
	int status = MHD_HTTP_NO_CONTENT;
	struct MHD_Response* response = NULL;
	const char* mime = "image/png";
	char host[NI_MAXHOST];

	void* png = NULL;
	size_t len = 0;

	void* so_handle = NULL;
	void* (*so_funcp)(struct dada_data*, struct dada_data_form*, size_t*) = NULL;

	DADA_DL_LOAD_SYMBOL(DADA_DL_PATH_CONST("output/plot_png"), "dada_output_plot_png", so_funcp, so_handle);
	png = so_funcp(data, form, &len);
	DADA_DL_CLOSE(so_handle);

	if (png)
	{
		char sec[16];
		char cid[16];
		status = MHD_HTTP_OK;
		mime = "image/png";
		response = MHD_create_response_from_buffer(len, png, MHD_RESPMEM_MUST_FREE);
		MHD_queue_response(meta->conn, status, response);
		MHD_add_response_header(response, "Content-Type", mime);
		if (fromcache) MHD_add_response_header(response, "Cache-Control", CACHE_1DAY);
		else           MHD_add_response_header(response, "Cache-Control", CACHE_NONE);
		if (fromcache) MHD_add_response_header(response, "ETag", meta->canonical);
		clock_gettime(CLOCK_REALTIME, &meta->t1);
		meta->seconds = (meta->t1.tv_sec+1e-9*meta->t1.tv_nsec) - (meta->t0.tv_sec+1e-9*meta->t0.tv_nsec);
		snprintf(sec, sizeof(sec)-1, "%.3fms", 1e3*meta->seconds);
		snprintf(cid, sizeof(cid)-1, "%08lx", meta->connid);
		MHD_add_response_header(response, "X-Timing", sec);
		MHD_add_response_header(response, "X-connid", cid);
		gethostname(host, sizeof(host)-1);
		MHD_add_response_header(response, "X-dada-host", host);
		dada_web_put_cache(meta, mime, len, png);
		MHD_destroy_response(response);
		png = NULL; /* will be free'd by MHD */
		goto cleanup;
	}

	goto error;

cleanup:
	if (data) {dada_data_destroy(data); free(data); data=NULL;}
	if (png)  {free(png); png=NULL;}

	return MHD_YES;

error:
	if (data) {dada_data_destroy(data); free(data); data=NULL;}
	if (png)  {free(png); png=NULL;}
	status = MHD_HTTP_INTERNAL_SERVER_ERROR;
	return dada_web_error(meta, status);
}
int dada_web_reply_double(struct connection_meta* meta, struct dada_data* data, int fromcache)
{
	int status = MHD_HTTP_NO_CONTENT;
	struct MHD_Response* response = NULL;
	const char* mime = "dada/double";
	char host[NI_MAXHOST];

	void* buf = NULL;
	size_t length = -1;
	void* so_handle = NULL;
	char* (*so_funcp)(struct dada_data* data, size_t* length) = NULL;

	DADA_DL_LOAD_SYMBOL(DADA_DL_PATH_CONST("output/double"), "dada_output_double", so_funcp, so_handle);
	buf = so_funcp(data, &length);
	DADA_DL_CLOSE(so_handle);

	if (buf)
	{
		char sec[16];
		char cid[16];
		char dims[32];
		status = MHD_HTTP_OK;
		response = MHD_create_response_from_buffer(length, buf, MHD_RESPMEM_MUST_FREE);
		MHD_queue_response(meta->conn, status, response);
		MHD_add_response_header(response, "Content-Type", mime);
		if (fromcache) MHD_add_response_header(response, "Cache-Control", CACHE_1DAY);
		else           MHD_add_response_header(response, "Cache-Control", CACHE_NONE);
		if (fromcache) MHD_add_response_header(response, "ETag", meta->canonical);
		clock_gettime(CLOCK_REALTIME, &meta->t1);
		meta->seconds = (meta->t1.tv_sec+1e-9*meta->t1.tv_nsec) - (meta->t0.tv_sec+1e-9*meta->t0.tv_nsec);
		snprintf(sec, sizeof(sec)-1, "%.3fms", 1e3*meta->seconds);
		snprintf(cid, sizeof(cid)-1, "%08lx", meta->connid);
		MHD_add_response_header(response, "X-Timing", sec);
		MHD_add_response_header(response, "X-connid", cid);
		switch (data->D)
		{
			case 1: snprintf(dims, sizeof(dims)-1, "%d: %d",          data->D, data->d[0]);                                     break;
			case 2: snprintf(dims, sizeof(dims)-1, "%d: %d,%d",       data->D, data->d[0], data->d[1]);                         break;
			case 3: snprintf(dims, sizeof(dims)-1, "%d: %d,%d,%d",    data->D, data->d[0], data->d[1], data->d[2]);             break;
			case 4: snprintf(dims, sizeof(dims)-1, "%d: %d,%d,%d,%d", data->D, data->d[0], data->d[1], data->d[2], data->d[3]); break;
		}
		MHD_add_response_header(response, "X-dada-dims", dims);
		if (meta->xmeta)   MHD_add_response_header(response, "X-meta",   meta->xmeta  );
		if (meta->xradial) MHD_add_response_header(response, "X-radial", meta->xradial);
		gethostname(host, sizeof(host)-1);
		MHD_add_response_header(response, "X-dada-host", host);
		MHD_destroy_response(response);
		buf = NULL; /* will be free'd by MHD */
		goto cleanup;
	}

	goto error;

cleanup:
	if (data) {dada_data_destroy(data); free(data); data=NULL;}
	if (buf)  {free(buf); buf=NULL;}
	return MHD_YES;

error:
	if (data) {dada_data_destroy(data); free(data); data=NULL;}
	if (buf)  {free(buf); buf=NULL;}
	status = MHD_HTTP_INTERNAL_SERVER_ERROR;
	return dada_web_error(meta, status);
}

int dada_web_see_other(struct connection_meta* meta)
{
	int status = MHD_HTTP_NO_CONTENT;
	struct MHD_Response* response = NULL;
	const char* mime = "text/html";
	char html[512];

	snprintf(html, sizeof(html)-1, "<html>\n<head><title>See Other</title></head>\n<body><p>The requested resource can be found <a href=\"%s\">here</a>.</p></body>\n</html>\n", meta->canonical);

	status = MHD_HTTP_SEE_OTHER;
	response = MHD_create_response_from_buffer(strlen(html), (void*)html, MHD_RESPMEM_MUST_COPY);
	MHD_queue_response(meta->conn, status, response);
	MHD_add_response_header(response, "Content-Type", mime);
	MHD_add_response_header(response, "Location", meta->canonical);
	MHD_add_response_header(response, "Cache-Control", "no-cache, no-store, must-revalidate");
	MHD_destroy_response(response);
	goto cleanup;

cleanup:
	return MHD_YES;
}

time_t dada_web_listing_stamp(struct dada_data_path* path)
{
	time_t t = 0;

	char fol2[256];
	void* so_handle = NULL;
	time_t (*so_funcp)(struct dada_data_path*) = NULL;
	int n;
	char* fol = NULL;
	FILE* mem = NULL;
	size_t memlen;
	struct stat st;

	DADA_MEMSTREAM(mem, fol, memlen);
	fprintf(mem, "config/");
	for (n=0; path && n<path->len; n++)
		if (path->path[n] && strlen(path->path[n]))
			fprintf(mem, "%s/", path->path[n]);
	fclose(mem);

	/* forbidden path elements: /. and .. */
	if (strstr(fol, "/.") || strstr(fol, ".."))
	{
		errno = EACCES;
		goto cleanup;
	}

	snprintf(fol2, sizeof(fol2)-1, "%s%s", fol, "list.so");
	if (stat(fol2, &st) != 0)  goto cleanup;
	if (!S_ISREG(st.st_mode))  goto cleanup;
	if (!(st.st_mode&S_IRUSR)) goto cleanup;
	if (!(st.st_mode&S_IXUSR)) goto cleanup;
	snprintf(fol2, sizeof(fol2)-1, "%s/%s", fol, "list");
	DADA_DL_LOAD_SYMBOL(DADA_DL_PATH_CONST(fol2), "stamp", so_funcp, so_handle);

	t = so_funcp(path);
	DADA_DL_CLOSE(so_handle);

cleanup:
err_memstream:
	if (fol) {free(fol); fol=NULL;}
	return t;
error:
	// DADA_RING_PRINTF("dlopen failed for %s", fol2);
	t = -1;
	goto cleanup;
}

time_t dada_web_ask_cache(struct connection_meta* meta, const char* module, struct dada_data_path* path, struct dada_data_modi* modi, struct dada_data_form* form)
{
	time_t t = 0;

	char* canonical = NULL;
	const char* dl_what = NULL;


	/* check if data exists in cache */
	if (1)
	{
		void* so_handle = NULL;
		time_t (*so_funcp)(const char*,const char*,const char*) = NULL;
		const char* key_space = "dada_web";
		const char* key_set   = "";

		canonical = dada_webcache_canonicalise(module, path, modi, form);
		meta->canonical = canonical;

		dl_what = "dada_input_cache_ask_web";
		DADA_DL_LOAD_SYMBOL(DADA_DL_PATH_CONST("input/cache"), dl_what, so_funcp, so_handle);
		t = so_funcp(key_space, key_set, canonical);
		DADA_DL_CLOSE(so_handle);
	}

cleanup:
	return t;
error:
	DADA_RING_PRINTF("dlopen failed for %s", dl_what);
	t = -1;
	goto cleanup;
}

int dada_web_get_cache(struct connection_meta* meta, const char* module, struct dada_data_path* path, struct dada_data_modi* modi, struct dada_data_form* form)
{
	int ret = MHD_YES;
	int status = MHD_HTTP_NO_CONTENT;
	struct MHD_Response* response = NULL;
	const char* ETag = NULL;
	char host[NI_MAXHOST];

	char* canonical = NULL;
	struct dada_response* cached = NULL;
	const char* dl_what = NULL;


	/* check if data exists in cache */
	if (1)
	{
		void* so_handle = NULL;
		struct dada_response* (*so_funcp)(const char*,const char*,const char*) = NULL;
		const char* key_space = "dada_web";
		const char* key_set   = "";

		if ( (canonical=meta->canonical) == NULL)
		{
			canonical = dada_webcache_canonicalise(module, path, modi, form);
			meta->canonical = canonical;
		}

		dl_what = "dada_input_cache_read_web";
		DADA_DL_LOAD_SYMBOL(DADA_DL_PATH_CONST("input/cache"), dl_what, so_funcp, so_handle);
		if (1) cached = so_funcp(key_space, key_set, canonical);
		DADA_DL_CLOSE(so_handle);
	}

	if (cached == NULL)
	{
		// DADA_RING_PRINTF("cached == %p", cached);
		ret = -1;
		goto cleanup;
	}

	/* check for If-None-Match request header */
	ETag = MHD_lookup_connection_value(meta->conn, MHD_HEADER_KIND, "If-None-Match");
	if (ETag && strlen(ETag)>0 && strcmp(ETag, meta->canonical)==0)
	{
		status = MHD_HTTP_NOT_MODIFIED;
		response = MHD_create_response_from_buffer(0, NULL, MHD_RESPMEM_PERSISTENT);
		MHD_queue_response(meta->conn, status, response);
	}
	else
	{
		status = MHD_HTTP_OK;
		response = MHD_create_response_from_buffer(cached->len, cached->data, MHD_RESPMEM_MUST_FREE);
		MHD_queue_response(meta->conn, status, response);
		cached->data = NULL; /* will be free'd by MHD */
	}
	MHD_add_response_header(response, "Content-Type", cached->type);
	MHD_add_response_header(response, "Cache-Control", CACHE_1DAY);
	MHD_add_response_header(response, "ETag", meta->canonical);
	if (cached->xmeta)   MHD_add_response_header(response, "X-meta", cached->xmeta);
	if (cached->xradial) MHD_add_response_header(response, "X-radial", cached->xradial);
	gethostname(host, sizeof(host)-1);
	MHD_add_response_header(response, "X-dada-host", host);
	MHD_destroy_response(response);

cleanup:
	if (cached)
	{
		if (cached->xradial) {free(cached->xradial); cached->xradial=NULL;}
		if (cached->xmeta  ) {free(cached->xmeta  ); cached->xmeta  =NULL;}
		if (cached->data   ) {free(cached->data   ); cached->data   =NULL;}
		if (cached->type   ) {free(cached->type   ); cached->type   =NULL;}
		cached->len=0;
		free(cached); cached=NULL;
	}
	return ret;

error:
	DADA_RING_PRINTF("dlopen failed for %s", dl_what);
	goto cleanup;
}

int dada_web_put_cache(struct connection_meta* meta, const char* mime, size_t len, void* data)
{
	int ret = 0;
	struct dada_response cached;
	const char* canonical = meta->canonical;
	const char* dl_what = NULL;

	// DADA_RING_PRINTF("dada_web_put_cache: %s", canonical);

	if (canonical == NULL || strlen(canonical) == 0) return -1;

	cached.type    = strdup(mime);
	cached.len     = len;
	cached.data    = data;
	cached.xmeta   = meta->xmeta;
	cached.xradial = meta->xradial;

	if (1)
	{
		void* so_handle = NULL;
		int (*so_funcp)(const char*,const char*,const char*,struct dada_response*) = NULL;
		const char* key_space = "dada_web";
		const char* key_set   = "";

		dl_what = "dada_input_cache_write_web";
		DADA_DL_LOAD_SYMBOL(DADA_DL_PATH_CONST("input/cache"), dl_what, so_funcp, so_handle);
		ret = so_funcp(key_space, key_set, canonical, &cached);
		DADA_DL_CLOSE(so_handle);
	}

cleanup:
	if (cached.type) {free(cached.type); cached.type=NULL;}
	cached.len = 0;
	cached.data = NULL;

	return ret;
error:
	DADA_RING_PRINTF("dlopen failed for %s", dl_what);
	goto cleanup;
}

char* dada_webcache_canonicalise(const char* module, struct dada_data_path* path, struct dada_data_modi* modi, struct dada_data_form* form)
{
	char* str = NULL;
	FILE* mem = NULL;
	size_t memlen;
	int n, N;

	DADA_MEMSTREAM(mem, str, memlen);
	fprintf(mem, "/%s", module);

	N = (path?path->len:0);
	for (n=0; n<N; n++) fprintf(mem, "/%s", path->path[n]);

	fprintf(mem, "?");

	if (modi) N = modi->len;
	N = (modi?modi->len:0);
	for (n=0; n<N; n++)
	{
		if (modi->key[n] && strlen(modi->key[n]))
		{
			if (strcmp(modi->key[n], "timeout")==0) continue;
			fprintf(mem, "&%s=%s", modi->key[n], modi->val[n]);
		}
	}

	if (form)
	{
		N = form->len;
		for (n=0; n<N; n++)
		{
			if (form->key[n] && strlen(form->key[n]))
			{
				if (strcmp(form->key[n], "fromcache")==0) continue;
				fprintf(mem, "&%s=%s", form->key[n], form->val[n]);
			}
		}
	}

	fclose(mem);

	return str;

err_memstream:
	return NULL;
}

/* dirfilter_nohidden:
 * check if directory entity's name given as struct dirent*
 * has leading . and thus is hidden (return 0), or not (return 1).
 */
int dirfilter_nohidden(const struct dirent* dirent)
{
	const char* name;
	if (dirent == NULL) return 0; /* NULL pointer is "hidden" as well */
	name = dirent->d_name;
	if (name[0] == '.') return 0; /* leading . -> hidden directroy */
	return 1; /* otherwise: not hidden */
}

/* versionsort_r:
 * reversed versionsort
 * compare two struct dirent**s a and b by versionsort,
 * and return the inverse
 * this gives e.g. the ordering
 * aa01, aa2, aa3, aa10, aa19, aa23
 * instead of
 * aa01, aa10, aa19, aa2, aa23, aa3 (alphasort)
 */
int versionsort_r(const struct dirent** a, const struct dirent** b)
{
	return -versionsort(a, b);
}

