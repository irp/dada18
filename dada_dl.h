#ifndef _DADA_DL_H_
#define _DADA_DL_H_

#define DADA_DL_TRY_MAX 20

#define DADA_DL_PATH_DATA(path, filename) \
do { \
    FILE* mem = NULL; \
    size_t memlen; \
    mem = open_memstream(&so_file, &memlen); \
    fprintf(mem, "config/%s/%s/%s/%s.so", \
		    path->path[0], path->path[1], path->path[2], filename); \
    fclose(mem); \
} while (0)

#define DADA_DL_PATH_CONST(filename) \
do { \
    FILE* mem = NULL; \
    size_t memlen; \
    mem = open_memstream(&so_file, &memlen); \
    fprintf(mem, "%s.so", filename); \
    fclose(mem); \
} while (0)


#define DADA_DL_LOAD_SYMBOL(FILENAME, funct, funcp, handle) \
do { \
    char* so_file = NULL; \
    char* error = NULL; \
    int try = 0; \
    FILENAME; \
    while (handle == NULL && (try++)<DADA_DL_TRY_MAX) { \
	handle = dlopen(so_file, RTLD_NOW|RTLD_GLOBAL); \
	if (handle == NULL) { \
	    fflush(stdout); \
	    usleep(100000); \
	} \
    } \
    if (handle == NULL) { \
	DADA_RING_PRINTF("cannot dlopen %s: %s", so_file, dlerror()); \
	FREE(so_file); \
	goto error; \
    } \
    error = dlerror(); \
    funcp = dlsym(handle, funct); \
    error = dlerror(); \
    if (error != NULL) { \
	DADA_RING_PRINTF("cannot dlsym %s from %s: %s", funct, so_file, error); \
	FREE(so_file); \
	goto error; \
    } \
    FREE(so_file); \
} while (0)

#define DADA_DL_CLOSE(handle) \
do { \
    if (1) { if (handle) {dlclose(handle); handle = NULL;} } \
} while (0)

#endif

