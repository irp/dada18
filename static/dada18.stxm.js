var para = 24*3; /* number of Heinzelmännchen cores to be used */
var prog = para; /* divide full image into so many "progressive" vertical tiles */

var t0, t1;

function dada_update_stxm(dada) {
    var url, mod, col;
    var urls  = [];
    var bases = [];
    bases[0] = dada_url_base(dada, 'STXM');
    if (!bases[0]) return;

    var imno = parseInt(dada.find('.dada-navi-stxm-first input').val());
    var horz = parseInt(dada.find('.dada-navi-stxm-horz input').val());
    var vert = parseInt(dada.find('.dada-navi-stxm-vert input').val());
    if (horz < 1) horz = 1;
    if (vert < 1) vert = 1;
    var paravert = vert;
    var sign = dada.find('.dada-navi-stxm-signal').val();

    /* sub-divide large STXM scans into vertical slices for progressive mode */
    if (horz > 100)   prog =  4*para;
    if (horz > 1000)  prog = 16*para;
    if (horz > 10000) prog = 64*para;

    // prog *= 24; /* remove if we have enough Heinzelmännchen */

    prog = Math.ceil(prog);
    if (prog < 1) prog = 1;
    
    // prog = para;

    dada.find('.dada-text').text('starting STXM for '+imno+' '+horz+'×'+vert+' ('+sign+')');

    /* parallel urls */
    var parallel = dada.find('.dada-navi-stxm-parallel').hasClass('dada-aktiv') ? true : false;
    if (parallel) {
	vert = Math.ceil(paravert / prog);
	var i = 0;
	for (f=0; f<paravert*horz; f+=horz*vert) {
	    //console.log([i, f]);
	    bases[i] = dada_url_base(dada, 'STXM', f);
	    i++;
	}
    }
    //console.log([para, prog, vert]);

    var stxm='';
    var mod='', col='';
    stxm = '&horz='+horz+'&vert='+vert+'&signal='+sign;
    mod += dada_url_roi(dada)+dada_url_bin(dada);
    mod += dada_url_empty(dada)+dada_url_subtract(dada)+dada_url_stack(dada);
    col += dada_url_colour(dada);

    if (parallel) {
	var i = 0;
	for (l=0; l<paravert; l+=vert) {
	    if (l+vert > paravert) stxm = '&horz='+horz+'&vert='+(paravert-l)+'&signal='+sign;
	    urls[i] = bases[i] + '?' + stxm + mod;
	    i++;
	}
    } else {
	urls[0] = bases[0] + '?' + stxm + mod;
    }

    dada_show_stxm(dada, '../stxm/', urls, col, imno, horz, vert, paravert);
}

function dada_stxm_view(dada) {
    var width = dada.find('.dada-navi-stxm-view').val();
    dada.find('.img-stxm').css('width', width );
}

function dada_show_stxm(dada, pre, urls, col, imno, horz, vert, paravert) {
    /* first, we have to create and append the images, store their respective url as data */
    var bild = dada.find('.dada-bild');
    bild.empty();

    var parallel = dada.find('.dada-navi-stxm-parallel').hasClass('dada-aktiv') ? true : false;
    var patches  = 0;

    urls.forEach(function(url, i) {
	patches++;
	var img = new Image();
	$(img).css('display', 'block');
	$(img).data('url', pre + url + col); /* full url to PNG */
	$(img).data('glue', '/'+url); /* relevant url for gluing */

	$(img).mousemove(function(event) {
	    var of  = $(this).offset();
	    var x   = parseInt(event.pageX) - parseInt(of.left)+1;
	    var y   = parseInt(event.pageY) - parseInt(of.top);
	    var W   = $(this).width();
	    var H   = $(this).height();
	    var w   = x*horz/W;
	    var h   = y*vert/H;
	    x = parseInt(w+1);
	    y = parseInt(h+1) + i*vert;
	    var n   = y*horz+x;
	    n = parseInt((y-1)*horz+(x-1))+imno;
	    if (x > horz || y > paravert) return; /* signal fired outside effective area */
	    var txt = 'x='+x+' y='+y+' n='+n;
	    dada.find('.dada-info').text(txt);
	    if (1) { /* mouse hover as seperate box */
		var hover = $('#hover');
		var x     = parseInt(event.pageX)+16;
		var y     = parseInt(event.pageY)+16;
		x -= $(document).scrollLeft();
		y -= $(document).scrollTop();
		hover.css({"top": y, "left": x}).text(txt).show();
	    }
	    dada.find('.dada-info').text(txt);
	});
	$(img).mouseleave(function(event) { $('#hover').hide(); });

	bild.append(img);
    });
    dada.find('.dada-bild img').addClass('img-stxm');
    dada_stxm_view(dada);

    var curr = {min: null, max: null};

    var patch = 0;

    function _worker(thread, _queue, toGlue) {
	var img = _queue.pop();
	if (!img) return;

	var url = $(img).data('url');

	if (url == undefined) return;

	// var re   = /.*(vert=[0-9]+).*/;
	// var vert = url.replace(re, "$1");
	// console.log([thread, url, vert]);
	// return;

	var req  = new XMLHttpRequest();
	req.open("GET", url, true);
	req.responseType = 'blob';

	req.onreadystatechange = function() {
	    if (req.readyState != 4) return;
	    switch (req.status) {
		case 202:
		    // console.log(req);
		    // var resp = req.responseText;
		    const reader = new FileReader();
		    reader.addEventListener('loadend', (e) => {
			var resp = e.target.result;
			var json = JSON.parse(resp);
			var prog = json.prog;
			// console.log(prog);
			_queue.push(img);
			window.setTimeout(1000, function() {
			    _worker(thread, _queue, toGlue);
			});
		    });
		    reader.readAsText(req.response);
		    break;
		case 200:
		    var timing = req.getResponseHeader('X-timing');
		    var meta   = req.getResponseHeader('X-meta');
		    var host   = req.getResponseHeader('X-dada-host');
		    var txt = '';
		    if (parallel) {
			patch++;
			if (timing)
			    txt = 'STXM patch '+patch+'/'+patches+' loaded in '+timing;
			else
			    txt = 'STXM patch '+patch+'/'+patches+' loaded';
		    } else {
			if (timing)
			    txt = 'STXM '+imno+' '+horz+'×'+vert+' loaded in '+timing;
			else
			    txt = 'STXM '+imno+' '+horz+'×'+vert+' loaded';
		    }
		    if (host) txt += ' from '+host;
		    if (meta) {
			meta = JSON.parse(meta);
			if (meta.min != null && (curr.min == null || meta.min < curr.min)) curr.min = meta.min;
			if (meta.max != null && (curr.max == null || meta.max > curr.max)) curr.max = meta.max;
			if (curr.min != null) txt += ' min: '+curr.min;
			if (curr.max != null) txt += ' max: '+curr.max;
		    }
		    // dada.find('.dada-text').text(txt);
		    img.src = url;

		    var loading = dada.find('.dada-navi-stxm-first input').data('loading')-1;
		    dada.find('.dada-navi-stxm-first input').data('loading', loading);
		    if (loading < 1) dada.find('.dada-navi-stxm-first input').removeClass('dada-busy');
		    if (loading < 1 && parallel && paravert!=vert && toGlue) {
			//dada.find('.dada-colour-min').val(curr.min);
			//dada.find('.dada-colour-max').val(curr.max);
			t1 = performance.now();
			var tt  = (t1-t0)*1e-3;
			var num = (paravert*horz)/tt;
			dada.find('.dada-text').text('timing: ' + tt + '; '+num+' per s');
			dada_glue_stxm(dada, col);
		    }
		    break;

		default:
		    dada.find('.dada-text').text('error loading STXM '+imno+' '+horz+'×'+vert);
		    // dada.find('.dada-bild').html('<img src="gfx/p300-halter.jpg" width="487" height="619">');
		    req = null;

		    var loading = dada.find('.dada-navi-stxm-first input').data('loading')-1;
		    dada.find('.dada-navi-stxm-first input').data('loading', loading);
		    if (loading < 1) dada.find('.dada-navi-stxm-first input').removeClass('dada-busy');
		    break;
	    }
	    dada.find('.dada-thread[data-tid="'+thread+'"]').removeClass('dada-thread-on');
	    _worker(thread, _queue, toGlue); /* continue with next slice */
	};
	var loading = dada.find('.dada-navi-stxm-first input').data('loading')+1;
	dada.find('.dada-navi-stxm-first input').data('loading', loading);
	dada.find('.dada-navi-stxm-first input').addClass('dada-busy');
	dada.find('.dada-thread[data-tid="'+thread+'"]').addClass('dada-thread-on');
	req.send(null);
    }


    /* guess /glue/ URL and check if the data is already available */
    var arr = [];
    dada.find('.img-stxm').each(function(idx, ele) {
	arr.push([$(ele).data('glue')]);
    });
    var glue = {'glue': arr};
    var req  = new XMLHttpRequest();
    var txt = JSON.stringify(glue);
    var url  = '../glue/'+$.sha256(txt);

    $.ajax({
	url: url,
	type: 'GET',
	success: function() {
	    dada.find('.dada-text').text('loading glued STXM from '+url);
	    var img = new Image();
	    $(img).css('display', 'block');
	    $(img).data('url', url + '?' + col);
	    $(img).mousemove(function(event) {
		var of  = $(this).offset();
		var x   = parseInt(event.pageX) - parseInt(of.left)+1;
		var y   = parseInt(event.pageY) - parseInt(of.top);
		var W   = $(this).width();
		var H   = $(this).height();
		var w   = x*horz/W;
		var h   = y*paravert/H;
		x = parseInt(w+1);
		y = parseInt(h+1);
		var n   = y*horz+x;
		n = parseInt((y-1)*horz+(x-1))+imno;
		if (x > horz || y > paravert) return; /* signal fired outside effective area */
		var txt = 'x='+x+' y='+y+' n='+n;
		dada.find('.dada-info').text(txt);
		if (1) { /* mouse hover as seperate box */
		    var hover = $('#hover');
		    var x     = parseInt(event.pageX)+16;
		    var y     = parseInt(event.pageY)+16;
		    x -= $(document).scrollLeft();
		    y -= $(document).scrollTop();
		    hover.css({"top": y, "left": x}).text(txt).show();
		}
		dada.find('.dada-info').text(txt);
	    });
	    $(img).mouseleave(function(event) { $('#hover').hide(); });
	    bild.empty().append(img);
	    dada.find('.dada-bild img').addClass('img-stxm');
	    dada_stxm_view(dada);
	    var _queue = [];
	    dada.find('.dada-bild img').each(function(i, img) {
		_queue.unshift(img);
	    });
	    dada.find('.dada-pool').empty();
	    for (thread=0; thread<1; thread++) {
		var t = '<span class="dada-thread" data-tid="'+thread+'">'+thread+'</span>';
		dada.find('.dada-pool').append(t);
		_worker(thread, _queue, false);
	    }
	},
	error: function() {
	    /* create queue and spawn workers */
	    var _queue = [];
	    dada.find('.dada-bild img').each(function(i, img) {
		_queue.unshift(img);
	    });
	    dada.find('.dada-pool').empty();
	    t0 = performance.now();
	    for (thread=0; thread<para; thread++) {
		var t = '<span class="dada-thread" data-tid="'+thread+'">'+thread+'</span>';
		dada.find('.dada-pool').append(t);
		_worker(thread, _queue, true);
	    }
	}
    });

    function dada_glue_stxm(dada, col) {
	var arr = [];
	/* make 2D array of gluing URLs */
	dada.find('.img-stxm').each(function(idx, ele) {
	    arr.push([$(ele).data('glue')]);
	});
	var glue = {'glue': arr};
	var frm  = new FormData();
	var req  = new XMLHttpRequest();
	var txt = JSON.stringify(glue);
	frm.append('glue', txt);
	var url  = '../glue:stxm/'+$.sha256(txt);

	/* first, try to GET url */
	/* TODO: implement me */
	/* TODO first: implement server-side */

	/* if GET failed, POST txt to url */
	req.open('POST', url);
	req.onreadystatechange = function() {
	    if (req.readyState != XMLHttpRequest.DONE) return;
	    switch (req.status) {
		case 200:
		    dada.find('.dada-text').text(req.responseText);
		    /* try to find URL */
		    var glue = "";
		    var arr = JSON.parse(req.responseText);
		    var re   = /^URL: (.*)$/;
		    arr.forEach(function(line, i) {
			if (re.test(line)) {
			    var tmp = line.replace(re, "$1");
			    glue = tmp;
			    dada_glue_show(dada, glue, col);
			}
		    });
		    break;
		default:
		    console.log(req.responseText);
		    dada.find('.dada-text').text('ERROR: '+req.responseText);
		    break;
	    }
	}
	req.send(frm);
    }

    function dada_glue_show(dada, glue, col) {
	dada.find('.dada-text').text('STXM glued to url '+glue);
	var url = '..' + glue;
	dada.find('.dada-text').text('loading glued STXM from '+url);
	var img = new Image();
	$(img).css('display', 'block');
	$(img).data('url', url + '?' + col);
	$(img).mousemove(function(event) {
	    var of  = $(this).offset();
	    var x   = parseInt(event.pageX) - parseInt(of.left)+1;
	    var y   = parseInt(event.pageY) - parseInt(of.top);
	    var W   = $(this).width();
	    var H   = $(this).height();
	    var w   = x*horz/W;
	    var h   = y*vert/H;
	    x = parseInt(w+1);
	    y = parseInt(h+1);
	    var n   = y*horz+x;
	    n = parseInt((y-1)*horz+(x-1))+imno;
	    if (x > horz || y > paravert) return; /* signal fired outside effective area */
	    var txt = 'x='+x+' y='+y+' n='+n;
	    dada.find('.dada-info').text(txt);
	    if (1) { /* mouse hover as seperate box */
		var hover = $('#hover');
		var x     = parseInt(event.pageX)+16;
		var y     = parseInt(event.pageY)+16;
		x -= $(document).scrollLeft();
		y -= $(document).scrollTop();
		hover.css({"top": y, "left": x}).text(txt).show();
	    }
	    dada.find('.dada-info').text(txt);
	});
	$(img).mouseleave(function(event) { $('#hover').hide(); });
	bild.empty().append(img);
	dada.find('.dada-bild img').addClass('img-stxm');
	dada_stxm_view(dada);
	var _queue = [];
	dada.find('.dada-bild img').each(function(i, img) {
	    _queue.unshift(img);
	});
	dada.find('.dada-pool').empty();
	for (thread=0; thread<1; thread++) {
	    var t = '<span class="dada-thread" data-tid="'+thread+'">'+thread+'</span>';
	    dada.find('.dada-pool').append(t);
	    _worker(thread, _queue, false);
	}
    }
}

