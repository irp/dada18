function dada_update_single(dada) {
    var url, mod, col;
    var base = dada_url_base(dada, 'single');
    if (!base) return;
    if (dada.find('.dada-navi-imno').val() == "") { /* invalid characters, so clear it */
	dada.find('.dada-navi-imno').val("");
	return;
    }

    var mod = '';
    mod += dada_url_roi(dada);
    mod += dada_url_bin(dada);
    mod += dada_url_empty(dada);
    mod += dada_url_subtract(dada);
    mod += dada_url_stack(dada);
    /* TODO: radial */

    var col = '';
    col += dada_url_colour(dada);

    url = '../show/' + base + '?' + mod + col;

    dada_show_single(dada, url);
}

var loaded = true;

function dada_show_single(dada, url) {
    var bild = dada.find('.dada-bild');
    var imno = parseInt(dada.find('.dada-navi-imno').val());
    var req  = new XMLHttpRequest();
    req.open("GET", url, true);
    req.responseType = 'blob';

    bild.removeClass('img-stxm');

    req.onreadystatechange = function() {
	if (req.readyState != 4) return;
	switch (req.status) {
	    case 200:
		var timing = req.getResponseHeader('X-timing');
		var host   = req.getResponseHeader('X-dada-host');
		var txt    = 'image '+imno+' loaded in '+timing;
		if (host) txt += ' from '+host;
		dada.find('.dada-text').text(txt);

		var img = new Image();
		img.onload = function() {
		    bild.empty().append(this);

		    dada.find('.dada-bild img').mousemove(function(event) {
			var of  = $(this).offset();
			var x   = parseInt(event.pageX) - parseInt(of.left)+1; /* why +1, but not in y?? */
			var y   = parseInt(event.pageY) - parseInt(of.top);
			var txt = 'x='+x+' y='+y;
			var binx = dada.find('.dada-pp-bin-x').val();
			var biny = dada.find('.dada-pp-bin-y').val();
			if (binx*biny != 1) txt += ' (before binning: '+binx*x+','+biny*y+')';
			dada.find('.dada-info').text(txt);
			if (1) { /* mouse hover as seperate box */
			    var hover = $('#hover');
			    var x     = parseInt(event.pageX)+16;
			    var y     = parseInt(event.pageY)+16;
			    hover.css({"top": y, "left": x}).text(txt).show();
			}
			dada.find('.dada-info').text(txt);
		    });
		    $(img).mouseleave(function(event) { $('#hover').hide(); });

		    /* TODO:
		     * zoom
		     * meta
		     * radial; distance from p.b.
		     */

		    var loading = dada.find('.dada-navi-imno').data('loading')-1;
		    dada.find('.dada-navi-imno').data('loading', loading);
		    if (loading < 1) dada.find('.dada-navi-imno').removeClass('dada-busy');
		    req = null;
		    loaded = true;
		}
		img.src = url;
		break;

	    default:
		dada.find('.dada-text').text('error loading image '+imno);
		dada.find('.dada-bild').html('<img src="gfx/p300-halter.jpg" width="487" height="619">');
		req = null;
		var loading = dada.find('.dada-navi-imno').data('loading')-1;
		dada.find('.dada-navi-imno').data('loading', loading);
		if (loading < 1) dada.find('.dada-navi-imno').removeClass('dada-busy');
		break;
	}
    };

    loaded = false;
    var loading = dada.find('.dada-navi-imno').data('loading')+1;
    dada.find('.dada-navi-imno').data('loading', loading);
    dada.find('.dada-navi-imno').addClass('dada-busy');
    req.send(null);
}

function dada_movie_play(dada) {
    var imno = parseInt(dada.find('.dada-movie-first').val());
    dada.find('.dada-navi-imno').val(imno);
    if (dada.find('.dada-movie-play').hasClass('dada-aktiv')) return;
    dada.find('.dada-movie-play').addClass('dada-aktiv');
    dada_movie_step(dada);
}
function dada_movie_stop(dada) {
    dada.find('.dada-movie-play').removeClass('dada-aktiv');
}

function dada_movie_step(dada) {
    dada_update_single(dada);
    var wait = parseInt(dada.find('.dada-movie-wait').val());
    var imno = parseInt(dada.find('.dada-navi-imno' ).val());
    var last = parseInt(dada.find('.dada-movie-last').val());
    var next = parseInt(dada.find('.dada-movie-step').val()) + imno;

    if (dada.find('.dada-movie-play').hasClass('dada-aktiv'))
	window.setTimeout(function() {
	    var imno = parseInt(dada.find('.dada-navi-imno' ).val());
	    var next = parseInt(dada.find('.dada-movie-step').val()) + imno;
	    if (next > last) {
		if (dada.find('.dada-movie-rept').hasClass('dada-aktiv')) {
		    dada_movie_play(dada);
		    imno = parseInt(dada.find('.dada-navi-imno' ).val());
		    next = parseInt(dada.find('.dada-movie-step').val()) + imno;
		} else {
		    dada_movie_stop(dada);
		    return;
		}
	    }
	    dada.find('.dada-navi-imno').val(next);
	    dada_movie_step(dada);
	}, wait);
}

