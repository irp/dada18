var nodes = [];
$(document).ready(function() {
    nodes = $('body').data('nodes');

    nodes.forEach(function(host) {
	var tr = $('<tr></tr>');
	var ping = cpu1 = cpu2 = cpu3 = cpu4 = '&nbsp;'
	tr.append('<td class="status-host">'+host+'</td>');
	tr.append('<td class="status-ping">'+ping+'</td>');
	tr.append('<td class="status-cpu1">'+cpu1+'</td>');
	tr.append('<td class="status-cpu2">'+cpu2+'</td>');
	tr.append('<td class="status-cpu3">'+cpu3+'</td>');
	tr.append('<td class="status-cpu4">'+cpu4+'</td>');
	$('#status').append(tr);
    });
});

