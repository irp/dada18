function nodes_sort(a, b) {
    var aa = a.host + ':' + a.port;
    var bb = b.host + ':' + b.port;
    return ( (aa<bb)?-1 : ((aa>bb)?1:0) );
}

function cluster_nodeinfo() {
    var url = '../../nodeinfo/*';
    $.get(url).done(function(nodes) {
	var tab = $('#top tbody');
	var tmp = $('<tbody>');
	nodes.sort(nodes_sort);
	nodes.forEach(function(node, idx) {
	    var  cls = 'idle';
	    if (node.cpu >   1) cls = 'act0';
	    if (node.cpu >  20) cls = 'act1';
	    if (node.cpu >  60) cls = 'act2';
	    if (node.cpu > 110) cls = 'act3';

	    var td1 = $('<td>'+node.host+':'+node.port+', '+node.pid+'</td>');
	    var td2 = $('<td class='+cls+'>'+node.cpu+'</td>');
	    var td3 = $('<td>'+node.mem+'</td>');
	    var td4 = $('<td>'+node.last.stxm+'</td>');

	    var tr = $('<tr>').append(td1).append(td2).append(td3).append(td4);
	    tmp.append(tr);
	});
	tab.replaceWith(tmp);
    });
    window.setTimeout(function() { cluster_nodeinfo(); }, 2000);
}

$(document).ready(function() {
    cluster_nodeinfo();
});

