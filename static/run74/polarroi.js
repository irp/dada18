var run  = "run74";
//var json = "http://haspp10wg/json/?action=list-scans&run="+run;
//var roi  = "http://haspp10wg/json/?action=list-rois&run="+run;
var json = "stitch2.json";
var roi  = "polar2.json";

var par  = 2;

$(document).ready(function() {
    document.title += ':' + run;
    document.title += ':stitching';

    var rois   = [];
    var params = [];
    load(json, 0);

    $.get(roi, function(data) {
	rois = data;
    });

    function load(url, idx) {
	var jetzt = Date.now();
	$.get(url+'?wann='+jetzt, function(data) {
	    params = data;
	    // rois   = data.rois;
	    // params = data.params;
	    $('#sele').empty();
	    $('#selector>button.show').unbind('click');
	    $('#selector>button.load').unbind('click');
	    $('#selector>button.prev').unbind('click');
	    $('#selector>button.next').unbind('click');
	    params.forEach(function(param) {
		if (param.noshow && param.noshow=='on' || param.noshow==true || param.noshow==1) return true;
		var txt = param.newfile + ', Scan ' + param.scan + ' (' + param.stitch.horz + '×' + param.stitch.vert + ')';
		var opt = $('<option></option>');
		opt.data('param', param);
		opt.text(txt);
		$('#sele').append(opt);
	    });
	    $('#sele').prop('selectedIndex', idx);
	    $('#selector>button.show').click(function() {
		var sel = $(this).siblings('select');
		var opt = sel.find('option:selected');
		var param = opt.data('param');
		stitch(param);
	    });
	    $('#selector>button.load').click(function() {
		var idx = $('#sele').prop('selectedIndex');
		load(url, idx);
	    });
	    $('#selector>button.prev').click(function() {
		var sel = $('#sele');
		var opt = sel.find('option:selected')
		    .prop('selected', false)
		    .prev()
		    .prop('selected', true);
		$('#selector>button.show').click();
	    });
	    $('#selector>button.next').click(function() {
		var sel = $('#sele');
		var opt = sel.find('option:selected')
		    .prop('selected', false)
		    .next()
		    .prop('selected', true);
		$('#selector>button.show').click();
	    });
	});
    }

    // window.setTimeout(function() { ringbuffer(); }, 500);

    function stitch(param) {
	$('#container').empty();
	var div = $('<div class="newfile"></div>');
	div.append($('<h3>'+param.newfile+', scan '+param.scan+'</h3>'));
	var scandiv = $('<div class="scan"></div>');
	//scandiv.append($('<hr></hr>'));
	//scandiv.append($('<h3><span class="scan-num">'+param.scan+'</span></h3>'));
	param.show.forEach(function(show) {
	    if (show.signal == "none") return true;
	    var box = $('<div class="box"></div>');
	    // box.append($('<h3><span class="show-signal">'+show.signal+' (roi: '+show.roi+')</span></h3>'));
	    box.append($('<h3><span class="show-signal">'+rois[show.roi]+'</span></h3>'));

	    var horz = param.stitch.horz;
	    var vert = param.stitch.vert;

	    var table = $('<table></table>');
	    for (v=0; v<vert; v++) {
		var tr = $('<tr></tr>');
		for (h=0; h<horz; h++) {
		    var s = 0;
		    if (param.detector == 'eiger') {
			s = parseInt(param.scan) + v*horz + h;
			s = s + '/1';
		    } else {
			s = parseInt(param.scan) + v*horz*param.stxm.horz*param.stxm.vert + h*param.stxm.horz*param.stxm.vert;
		    }
		    //console.log(s);
		    var roi = '';
		    if (show.roi && rois[show.roi]) roi = rois[show.roi];
		    // console.log(show.roi + ': '+roi);
		    // var mod = '&algo=polarroi&polarroi=1194,1129;706,732;'+roi
		    var mod = '&algo=polarroi&polarroi=800,800;677,758;'+roi
			+'&horz='+param.stxm.horz
			+'&vert='+param.stxm.vert
			+'&roi=394,329,1600,1600'
			//+'&binning=1,1'
			+'&palette=wb'
			//+'&cmin='+2000
			//+'&cmax='+10000
			+'&cmin='+show.cmin
			+'&cmax='+show.cmax;
			+'';
			if (param.scale) mod += '&scale='+param.scale;
		    // mod += '&timeout=0.5';
		    var url = '../../stxm/GINIX/'+run+'/'+param.detector+'/'+param.newfile+'/'+s+'?'+mod;

		    par = $('#para').val();
		    // var url = '../../stxm'+par+'/GINIX/'+run+'/'+param.detector+'/'+param.newfile+'/'+s+'?'+mod;
		    var url = '../../stxm/GINIX/'+run+'/'+param.detector+'/'+param.newfile+'/'+s+'?'+mod;
		    //console.log([par, url]);
		    // console.log(url);
		    
		    var hcen = h-horz/2+0.5;
		    var vcen = v-vert/2+0.5;
		    var td = $('<td><span class="img" data-url="'+url+'" data-newfile="'+param.newfile+'" data-det="'+param.detector+'" data-scan="'+s+'" data-horz="'+param.stxm.horz+'" data-vert="'+param.stxm.vert+'" data-roi="'+rois[show.roi]+'" data-h="'+hcen+'" data-v="'+vcen+'"></span></td>');
		    tr.append(td);
		}
		table.append(tr);
	    }

	    box.append(table);
	    scandiv.append(box);
	});
	// var info = $('<p class="scan-info" data-info="'+scan.info+'">'+scan.info+'</p>');
	var scan_info = param.newfile+' S'+param.scan+' ('+param.stitch.horz+'×'+param.stitch.vert+':'+param.stxm.horz+'×'+param.stxm.vert+')';
	var info = $('<p class="scan-info" data-info="'+scan_info+'">'+scan_info+'</p>');
	scandiv.append(info);
	scandiv.append($('<hr></hr>'));
	div.append(scandiv);
	$('#container').html(div);

        /* put all spans into queue for further serial processing */
        var spans = [];
        $('span.img').each(function() { spans.unshift($(this)); });



	function glue() {
	    var arr = [];
	    $('.box').find('tr').each(function(idx, ele) {
		var lin = [];
		$(ele).find('td').each(function(idy, el2) {
		    var span = $($(el2).find('span'));
		    var url = '/GINIX/'+run+'/'+span.data('det')+'/'+span.data('newfile')+'/'+span.data('scan')+'?';
		    url += '&algo=polarroi';
		    url += '&polarroi=800,800;677,758;'+span.data('roi');
		    url += '&horz='+span.data('horz');
		    url += '&vert='+span.data('vert');
		    url += '&roi=394,329,1600,1600';
		    lin.push(url);
		});
		arr.push(lin);
	    });
	    console.log(arr);
	    var glue = {'glue': arr};
	    var frm  = new FormData();
	    var req  = new XMLHttpRequest();
	    var txt = JSON.stringify(glue);
	    frm.append('glue', txt);
	    var url  = '../../glue:stxm/'+$.sha256(txt);

	    /* first, try to GET url */
	    /* TODO: implement me */
	    /* TODO first: implement server-side */

	    /* if GET failed, POST txt to url */
	    req.open('POST', url);
	    req.onreadystatechange = function() {
		if (req.readyState != XMLHttpRequest.DONE) return;
		switch (req.status) {
		    case 200:
			$('#status').text(req.responseText);
			/* try to find URL */
			var glue = "";
			var arr = JSON.parse(req.responseText);
			var re   = /^URL: (.*)$/;
			arr.forEach(function(line, i) {
			    if (re.test(line)) {
				var tmp = line.replace(re, "$1");
				glue = tmp;
				// dada_glue_show(dada, glue, col);
				console.log(glue);
				$('#glue').html('<a href="'+glue+'?palette=wb&cmin=7000&cmax=15000&scale=log" target="_blank">'+glue+'</a>');
			    }
			});
			break;
		    default:
			console.log(req.responseText);
			$('#status').text('ERROR: '+req.responseText);
			break;
		}
	    }
	    req.send(frm);
	}


        /* process the queue sequentially */
	var runonce = 1;
        function doit(spans) {
            var span = spans.pop();
            if (!span) {
		if (runonce != 1) return;
		var toc = Date.now();
		var diff = (toc-tic)/1000;
		$('#status').text('wall time: '+diff+'s');
		// glue();
		runonce = 0;
		return;
	    }
            var url = span.data('url');

	    var req = new XMLHttpRequest();
	    req.open("GET", url, true);
	    req.responseType = 'blob';

	    req.onreadystatechange = function() {
		if (req.readyState != 4) return;
		switch (req.status) {
		    case 200:
			var timing = req.getResponseHeader('X-timing');
			// console.log(timing);
			var img = new Image();
			img.onload = function() {
			    $('#status').text('loaded: '+url+'; timing: '+timing);
			    var meta = req.getResponseHeader('X-meta');
			    if (meta) {
				meta = JSON.parse(meta);
				//console.log(meta);
				span.data('min', meta.min);
				span.data('max', meta.max);
			    }
			    span.mouseenter(function() {
				var newfile = span.data('newfile');
				var scan    = span.data('scan');
				var txt     = newfile + ', scan ' + scan + '; min: '+span.data('min')+', max: '+span.data('max');
				txt += '; h '+span.data('h')+', v '+span.data('v');
				var info    = span.parents('div.scan').find('p.scan-info');
				info.text(txt);
				$('#status').text(txt);
			    });
			    span.mouseleave(function() {
				var info = span.parents('div.scan').find('p.scan-info');
				var txt  = info.data("info");
				info.text(txt);
				$('#status').text('');
			    });
			    span.click(function() {
				return;
				var comp = $('#composite');
				var newfile = span.data("newfile");
				var det     = span.data("det");
				var scan    = span.data("scan");
				var horz    = span.data("horz");
				var vert    = span.data("vert");
				var roi     = span.data("roi");
				var mod     = "&horz="+horz+"&vert="+vert+"&roi="+roi+"&binning=20,20&palette=test1&scale=log&clipping=min&cmin=20";
				var url     = '../../composite/GINIX/'+run+'/'+det+'/'+newfile+'/'+scan+'/1?'+mod;
				var info    = span.parents('div.scan').find('p.scan-info');
				window.open(url);
			    });
			    // span.append(img);
			    span.html(img);
			    $(img).addClass('stxm');
			    doit(spans);
			}
			img.src = url;
			break;

		    case 202: // accepted, i.e. already running
			var r = new FileReader();
			r.onload = function() {
			    var j = JSON.parse(this.result);
			    var prog = j.progress;
			    // console.log('prog: '+prog);
			    spans.unshift(span);
			    span.html($('<img src="black.png" class="stxm todo"></img>')).css('opacity', prog*0.01);
			    $('#status').text('progress for: '+url+': '+prog);
			    window.setTimeout(function() { doit(spans); }, 200);
			    // doit(spans);
			};
			r.readAsText(this.response);
			break;

		    default:
			// console.log('req.status is '+req.status);
			$('#status').text('FAILED: '+url);
			// span.append($('<img src="missing.png"></img>'));
			span.html($('<img src="missing.png" class="stxm missing"></img>'));
			doit(spans);
			req = null;
			break;
		}
	    };

	    // $('#status').text('loading: '+url);
	    span.html($('<img src="active.png" class="stxm active"></img>'));
	    req.send(null);
        }

	var tic = Date.now();

	par = $('#para').val();
	var per = Math.ceil(spans.length/par);
        $(spans).each(function() { $(this).append('<img src="todo.png" class="stxm todo">'); });

	console.log('par: '+par+'; per: '+per);

	for (var i=0; i<spans.length; i+=per) {
		var splice = spans.slice(i, i+per);
		doit(splice);
	}

    }

    function ringbuffer() {
	$.get('../../../info/ringbuffer', function(data) {
	    var pre = $('<pre class="ringbuffer"></pre>');
	    var lines = data.split('\n');
	    for (i=lines.length-8-1; i<lines.length; i++) {
		pre.append(lines[i]+"\n");
	    }
	    $.get('../../../info/top', function(data) {
		pre.append(data);
		$('#ringbuffer').html(pre);
	    });
	});
	window.setTimeout(function() { ringbuffer(); }, 1000);
    }

});

