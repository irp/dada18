var running = false;
var timing  = 0.0;

$(document).ready(function() {
    $('#browse').prop('disabled', false);
    $('#browse').click(function() {
	if (running) return;
	running = true;
	$(this).prop('disabled', true);
	var stack = [];
	stack.push([null, null, null, $('#list')]);
	timing = Date.now();
	$('#list').html('<thead><tr><th>EXPERIMENT</th></tr></thead><tbody></tbody>');
	update(stack);
    });
    // $('#browse').click();
});

function update(stack) {
    var a = stack.shift();
    if (a) {
	var ins = a[0];
	var exp = a[1];
	var det = a[2];
	var tab = a[3];
	var url = "../list/";
	if (ins) url +=     ins;
	if (exp) url += "/"+exp;
	if (det) url += "/"+det;
	$('#status').text('browsing '+url+' …');

	$.get(url, function(data) {
	    if (data && data.listing) {
		if (ins && exp && det) {
		    var cnt = 0;
		    data.listing.forEach(function(ele, idx) { cnt++; });

		    var tr = tab.find('tbody').find('tr[data-ins="'+ins+'"][data-exp="'+exp+'"]');
		    var td = tr.find('td[data-det="'+det+'"]');
		    if (td.length==0) {
			var th = $('<th data-det="'+det+'">'+det+'</th>');
			tab.find('thead>tr').append(th);
			tab.find('tbody').find('tr').each(function(i, tr) {
			    var td = $('<td data-det="'+det+'"></td>');
			    $(tr).append(td);
			});
			td = tr.find('td[data-det="'+det+'"]');
		    }
		    td.text(cnt);

		} else {
		    var ul = $('<ul></ul>');
		    data.listing.forEach(function(ele, idx) {
			if (!ins && !exp && !det) stack.push([ele, null, null, tab]);
			if ( ins && !exp && !det) stack.push([ins, ele,  null, tab]);
			if ( ins &&  exp && !det) stack.push([ins, exp,  ele,  tab]);
		    });

		    if (ins && exp) {
			var tr = $('<tr data-ins="'+ins+'" data-exp="'+exp+'"></tr>');
			var td = $('<td></td>');
			td.text(ins+': '+exp);
			tr.append(td);
			tab.append(tr);
		    }
		}
		$('#status').text('done.');
	    } else {
		$('#status').text('error.');
	    }
	    update(stack);
	});
    } else {
	timing = (Date.now() - timing) * 1e-3;
	timing = timing.toFixed(1);
	$('#status').text('finished ('+timing+'s)');
	running = false;
	$('#browse').prop('disabled', false);
    }
}

