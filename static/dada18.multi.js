function dada_update_multi(dada) {
    var url, mod, col;
    var base = dada_url_base(dada, 'multi');
    if (!base) return;
    if (dada.find('.dada-navi-multi-first input').val() == "") { /* invalid characters, so clear it */
	dada.find('.dada-navi-multi-first input').val("");
	return;
    }

    var horz = dada.find('.dada-navi-multi-horz input').val();
    var vert = dada.find('.dada-navi-multi-vert input').val();
    var mod = '&horz='+horz+'&vert='+vert;
    mod += dada_url_roi(dada);
    mod += dada_url_bin(dada);
    mod += dada_url_empty(dada);
    mod += dada_url_subtract(dada);
    mod += dada_url_sum(dada); # merge conflict; try without
    // mod += dada_url_sum(dada);

    var col = '';
    col += dada_url_colour(dada);

    url = '../multi/' + base + '?' + mod + col;

    console.log(url);
    dada_show_single(dada, url);
}

var loaded = true;

