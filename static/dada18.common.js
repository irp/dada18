function dada_url_base(dada, module, offset=0) {
    var url = '';
    ['instrument', 'experiment', 'detector', 'sample'].forEach(function(key) {
	if (dada.find('.dada-head-'+key).val() == '') return false;
	url += dada.find('.dada-head-'+key).val()+'/';
    });
    switch (module) {
	case 'single':
	case 'show':
	    if (dada.find('.dada-navi-imno').val() == null) return false;
	    url += parseInt(dada.find('.dada-navi-imno').val());
	    break;
	case 'multi':
	    console.log('multi');
	    if (dada.find('.dada-navi-multi-first input').val() == null) return false;
	    url += parseInt(dada.find('.dada-navi-multi-first input').val());
	    console.log(url);
	    break;
	case 'STXM':
	    if (dada.find('.dada-navi-stxm-first input').val() == null) return false;
	    url += parseInt(dada.find('.dada-navi-stxm-first input').val()) + offset;
	    break;
	default: return false;
    }
    return url;
}

function dada_url_roi(dada) {
    var x = dada.find('.dada-pp-roi-x').val();
    var y = dada.find('.dada-pp-roi-y').val();
    var w = dada.find('.dada-pp-roi-w').val();
    var h = dada.find('.dada-pp-roi-h').val();
    var flags = '';
    flags += (dada.find('.dada-pp-roi-mask' ).hasClass('dada-aktiv')) ? 'mask' : '';
    if (x==null || y==null || w==null || h==null) return '&roi=';
    return '&roi='+x+','+y+','+w+','+h+';'+flags;
}

function dada_url_bin(dada) {
    var x = dada.find('.dada-pp-bin-x').val();
    var y = dada.find('.dada-pp-bin-y').val();
    if (x==null || y==null) return '&binning=';
    return '&binning='+x+','+y;
}

function dada_url_empty(dada) {
    var v = dada.find('.dada-pp-divide').val();
    if (v==null) return '&empty=';
    return '&empty='+v;
}

function dada_url_subtract(dada) {
    var v = dada.find('.dada-pp-subtract').val();
    if (v==null) return '&subtract=';
    return '&subtract='+v;
}

function dada_url_stack(dada) {
    var v = dada.find('.dada-pp-stack').val();
    var w = dada.find('.dada-pp-stack-what').val();
    if (v==null) return '&stack=';
    return '&stack='+v+';'+w;
}

function dada_url_colour(dada) {
    var col = '';

    var pal     = dada.find('.dada-colour-palette-current').data('palette');

    var min     = dada.find('.dada-colour-min').val();
    var max     = dada.find('.dada-colour-max').val();

    var clipmin = (dada.find('.dada-colour-clip-min' ).hasClass('dada-aktiv')) ? true : false;
    var clipmax = (dada.find('.dada-colour-clip-max' ).hasClass('dada-aktiv')) ? true : false;
    var scale   = (dada.find('.dada-colour-scale-log').hasClass('dada-aktiv')) ? 'log' : 'lin';

    col += '&palette=' + pal;
    col += '&cmin=' + ((min==null) ? '' : min);
    col += '&cmax=' + ((max==null) ? '' : max);
    col += '&clipping=' + ((clipmin==false) ? '' : 'cmin') + ',' + ((clipmax==false) ? '' : 'cmax');
    col += '&scale=' + scale;

    return col;
}

