var run = "run66";

$(document).ready(function() {
    document.title += ':' + run;
    document.title += ':stitching';

    var rois   = [];
    var params = [];
    load('stitch1.json', 0);

    function load(url, idx) {
	var jetzt = Date.now();
	$.get(url+'?'+jetzt, function(data) {
	    rois   = data.rois;
	    params = data.params;
	    $('#selector>select').empty();
	    $('#selector>button.show').unbind('click');
	    $('#selector>button.load').unbind('click');
	    $('#selector>button.prev').unbind('click');
	    $('#selector>button.next').unbind('click');
	    params.forEach(function(param) {
		param.scan.forEach(function(scan) {
		    var txt = param.newfile + ', Scan ' + scan.num + ' (' + scan.stitch.horz + '×' + scan.stitch.vert + ')';
		    var opt = $('<option></option>');
		    opt.data('param', param);
		    opt.data('scan', scan);
		    opt.text(txt);
		    $('#selector>select').append(opt);
		});
	    });
	    $('#selector>select').prop('selectedIndex', idx);
	    $('#selector>button.show').click(function() {
		var sel = $(this).siblings('select');
		var opt = sel.find('option:selected');
		var param = opt.data('param');
		var scan  = opt.data('scan');
		stitch(param, scan);
	    });
	    $('#selector>button.load').click(function() {
		var idx = $('#selector>select').prop('selectedIndex');
		load(url, idx);
	    });
	    $('#selector>button.prev').click(function() {
		var sel = $(this).siblings('select');
		var opt = sel.find('option:selected')
		    .prop('selected', false)
		    .prev()
		    .prop('selected', true);
		$('#selector>button.show').click();
	    });
	    $('#selector>button.next').click(function() {
		var sel = $(this).siblings('select');
		var opt = sel.find('option:selected')
		    .prop('selected', false)
		    .next()
		    .prop('selected', true);
		$('#selector>button.show').click();
	    });
	});
    }

    // window.setTimeout(function() { ringbuffer(); }, 500);

    function stitch(param, scan) {
	$('#container').empty();
	var div = $('<div class="newfile"></div>');
	div.append($('<h1>'+param.newfile+'</h1>'));
	var scandiv = $('<div class="scan"></div>');
	scandiv.append($('<hr></hr>'));
	scandiv.append($('<h3><span class="scan-num">'+scan.num+'</span></h3>'));
	scan.show.forEach(function(show) {
	    var box = $('<div class="box"></div>');
	    box.append($('<h3><span class="show-signal">'+show.signal+'</span></h3>'));

	    var horz = scan.stitch.horz;
	    var vert = scan.stitch.vert;

	    var table = $('<table></table>');
	    for (v=0; v<vert; v++) {
		var tr = $('<tr></tr>');
		for (h=0; h<horz; h++) {
		    var s = 0;
		    if (scan.detector == 'eiger') {
			s = scan.num + v*horz + h;
			s = s + '/1';
		    } else {
			s = scan.num + v*horz*scan.stxm.horz*scan.stxm.vert + h*scan.stxm.horz*scan.stxm.vert;
		    }
		    //console.log(s);
		    var mod = '&signal='+show.signal
			+'&horz='+scan.stxm.horz
			+'&vert='+scan.stxm.vert
			+'&roi='+rois[show.roi]
			+'&binning=1,1'
			+'&palette=wb'
			+'&cmin='+show.cmin
			+'&cmax='+show.cmax;
		    // mod += '&timeout=0.5';
		    var url = '../../stxm/GINIX/'+run+'/'+scan.detector+'/'+param.newfile+'/'+s+'?'+mod;
		    //console.log(url);
		    var td = $('<td><span class="img" data-url="'+url+'" data-newfile="'+param.newfile+'" data-det="'+scan.detector+'" data-scan="'+s+'" data-horz="'+scan.stxm.horz+'" data-vert="'+scan.stxm.vert+'" data-roi="'+rois[show.roi]+'"></span></td>');
		    tr.append(td);
		}
		table.append(tr);
	    }

	    box.append(table);
	    scandiv.append(box);
	});
	var info = $('<p class="scan-info" data-info="'+scan.info+'">'+scan.info+'</p>');
	scandiv.append(info);
	div.append(scandiv);
	$('#container').html(div);

        /* put all spans into queue for further serial processing */
        var spans = [];
        $('span.img').each(function() { spans.unshift($(this)); });

        /* process the queue sequentially */
        function doit(spans) {
            var span = spans.pop();
            if (!span) {
		var toc = Date.now();
		var diff = (toc-tic)/1000;
		$('#status').text('wall time: '+diff+'s');
		return;
	    }
            var url = span.data('url');

	    var req = new XMLHttpRequest();
	    req.open("GET", url, true);
	    req.responseType = 'blob';

	    req.onreadystatechange = function() {
		if (req.readyState != 4) return;
		switch (req.status) {
		    case 200:
			var timing = req.getResponseHeader('X-timing');
			// console.log(timing);
			var img = new Image();
			img.onload = function() {
			    $('#status').text('loaded: '+url+'; timing: '+timing);
			    var meta = req.getResponseHeader('X-meta');
			    if (meta) {
				meta = JSON.parse(meta);
				//console.log(meta);
				span.data('min', meta.min);
				span.data('max', meta.max);
			    }
			    span.mouseenter(function() {
				var newfile = span.data('newfile');
				var scan    = span.data('scan');
				var txt     = newfile + ', scan ' + scan + '; min: '+span.data('min')+', max: '+span.data('max');
				var info    = span.parents('div.scan').find('p.scan-info');
				info.text(txt);
			    });
			    span.mouseleave(function() {
				var info = span.parents('div.scan').find('p.scan-info');
				var txt  = info.data("info");
				info.text(txt);
			    });
			    span.click(function() {
				var comp = $('#composite');
				var newfile = span.data("newfile");
				var det     = span.data("det");
				var scan    = span.data("scan");
				var horz    = span.data("horz");
				var vert    = span.data("vert");
				var roi     = span.data("roi");
				var mod     = "&horz="+horz+"&vert="+vert+"&roi="+roi+"&binning=20,20&palette=test1&scale=log&clipping=min&cmin=20";
				var url     = '../../composite/GINIX/'+run+'/'+det+'/'+newfile+'/'+scan+'/1?'+mod;
				var info    = span.parents('div.scan').find('p.scan-info');
				window.open(url);
			    });
			    // span.append(img);
			    span.html(img);
			    $(img).addClass('stxm');
			    doit(spans);
			}
			img.src = url;
			break;

		    case 202: // accepted, i.e. already running
			var r = new FileReader();
			r.onload = function() {
			    var j = JSON.parse(this.result);
			    var prog = j.progress;
			    // console.log('prog: '+prog);
			    spans.unshift(span);
			    span.html($('<img src="black.png" class="stxm todo"></img>')).css('opacity', prog*0.01);
			    $('#status').text('progress for: '+url+': '+prog);
			    window.setTimeout(function() { doit(spans); }, 200);
			    // doit(spans);
			};
			r.readAsText(this.response);
			break;

		    default:
			// console.log('req.status is '+req.status);
			$('#status').text('FAILED: '+url);
			// span.append($('<img src="missing.png"></img>'));
			span.html($('<img src="missing.png" class="stxm missing"></img>'));
			doit(spans);
			req = null;
			break;
		}
	    };

	    // $('#status').text('loading: '+url);
	    span.html($('<img src="active.png" class="stxm active"></img>'));
	    req.send(null);
        }

	var tic = Date.now();
        // doit(spans);

	// console.log(spans);
	// spans = [1,2,3,4,5,6,7,8];
	var par = 1;
	var per = spans.length/par;
        $(spans).each(function() { $(this).append('<img src="todo.png" class="stxm todo">'); });
	for (var i=0; i<spans.length; i+=per) {
		var splice = spans.slice(i, i+per);
		// console.log(splice);
		 doit(splice);
	}

    }

    function ringbuffer() {
	$.get('../../../info/ringbuffer', function(data) {
	    var pre = $('<pre class="ringbuffer"></pre>');
	    var lines = data.split('\n');
	    for (i=lines.length-8-1; i<lines.length; i++) {
		pre.append(lines[i]+"\n");
	    }
	    $.get('../../../info/top', function(data) {
		pre.append(data);
		$('#ringbuffer').html(pre);
	    });
	});
	window.setTimeout(function() { ringbuffer(); }, 1000);
    }

});

