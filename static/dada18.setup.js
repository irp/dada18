/* initialise <div class="dada">:
 * create empty box,
 * load template from dada18.box1.html,
 * load snap from snap/blank.json / #snap=*.json,
 * apply settings,
 * setup event handlers,
 * call initial view.
 * */
var slideshow = false;
$(document).ready(function() {
    var setting = getParameterByName('snap');
    if (setting == null || setting == '') setting = 'blank';

    dada_setup_snap(setting);
});

function dada_setup_snap(setting) {
    var name = $.now();
    var dada = $('<div class="container" id="dada-'+name+'"></div>');
    dada.data('setting', setting);
    dada.load('dada18.box1.html', function() {
	dada.find('.dada-navi-imno').data('loading', 0);
	dada.find('.dada-navi-stxm-first input').data('loading', 0);

	dada_setup_box_open(dada);

	dada.find('.dada-text').text('reading snap '+dada.data('setting'));
	$.ajax({
		url: 'snap/'+dada.data('setting')+'.json',
		type: 'GET',
		success: function(snap) {
		    var txt = $('<p>loading snapshot '+dada.data('setting')+'</p>');
		    $('#loader').prepend(txt);
		    $('#loading').show().animate({'width': '25em'}, 1500);

		    dada_state_box_open(dada,      snap['dada-box-open']);

		    dada_state_module(dada,        snap['dada-module']);

		    dada_state_single(dada,        snap['dada-navi']['single']);
		    dada_state_movie(dada,         snap['dada-navi']['movie']);
		    dada_state_multi(dada,         snap['dada-navi']['multi']); /* check */
		    dada_state_stxm(dada,          snap['dada-navi']['stxm']); /* check */

		    dada_state_colour(dada,        snap['dada-colour']);
		    dada_state_preprocessing(dada, snap['dada-preprocessing']);

		    /* TODO:
		     * radial
		     * (processing)
		     * (meta data)
		     * */

		    /* TODO: events for
		     * colour palette
		     * (processing)
		     * (radial)
		     * (meta data)
		     * */

		    /* toggling buttons */
		    dada.find('button.dada-toggle').click(function() {
			$(this).toggleClass('dada-aktiv');
		    });
		    /* radio buttons */
		    dada.find('button.dada-radio').click(function() {
			var radio = $(this).data('radio');
			dada.find('button.'+radio).removeClass('dada-aktiv');
			$(this).addClass('dada-aktiv');
		    });
		    /* clear buttons */
		    dada.find('button.dada-clear').click(function() {
			var which = $(this).siblings('input');
			var value = $(this).data('value');
			which.each(function(idx,ele) {
			    $(ele).val(value);
			});
		    });

		    /* update-events for input fields */
		    dada.find('input.dada-update').change(function() {
			dada_update(dada);
		    });
		    dada.find('input.dada-update-single').change(function() {
			var module = dada.find('.dada-module').val();
			if (module != 'single') return;
			dada_update(dada);
		    });

		    /* update-events for drop downs */
		    dada.find('select.dada-update-single').change(function() {
			var module = dada.find('.dada-module').val();
			if (module != 'single') return;
			dada_update(dada);
		    });


		    /* update-events for buttons */
		    dada.find('button.dada-update').click(function() {
			dada_update(dada);
		    });
		    dada.find('button.dada-update-single').click(function() {
			var module = dada.find('.dada-module').val();
			if (module != 'single') return;
			dada_update(dada);
		    });

		    dada.find('.dada-colour-palette-dropdown').click(function() {
			dada.find('.dada-colour-palette-select').show();
		    });
		    dada.find('.dada-colour-palette').click(function() {
			dada.find('.dada-colour-palette-select').hide();
			var pal = $(this).data('palette');
			var src = '../colourpalette/'+pal;
			dada.find('.dada-colour-palette-current').attr('src', src).data('palette', pal);
			dada_update(dada);
		    });

		    /* ROI dropdown */
		    dada.find('select.dada-pp-roi-list').change(function() {
			var module = dada.find('.dada-module').val();
			if (module != 'single') return;
			var sel = dada.find('.dada-pp-roi-list option:selected');
			var roi = sel.data('roi');
			dada.find('.dada-pp-roi-x').val(roi[0]);
			dada.find('.dada-pp-roi-y').val(roi[1]);
			dada.find('.dada-pp-roi-w').val(roi[2]);
			dada.find('.dada-pp-roi-h').val(roi[3]);
			dada_update(dada);
		    });


		    /* start a slide show */
		    dada.find('.dada-head-close').click(function() {
			dada.fadeOut(500, function() {
			    $('#fullbg').fadeIn(500, function() {
				slideshow = true;
				window.setTimeout(function() {
				    dada_slideshow(0);
				}, 1000);
			    });
			});
		    });
		    $('#fullbg').click(function() {
			slideshow = false;
			dada.show();
			$('#slideshow').fadeOut(500);
			$('#fullbg').fadeOut(500);
		    });
		    $('#slideshow').click(function() {
			slideshow = false;
			dada.show();
			$('#slideshow').fadeOut(500);
			$('#fullbg').fadeOut(500);
		    });

		    /* and finally, setup dada-head,
		     * which will load list of instruments / experiments / detectors / samples,
		     * and shows image, if appropriate
		     * */
		    dada_state_head(dada, snap['dada-head']);

		    dada.find('.dada-head-reload').click(function() {
			var url = '../list/';
			if (dada.find('.dada-head-detector').prop('selectedIndex') == 0) return;
			url += dada.find('.dada-head-instrument').val()+'/';
			url += dada.find('.dada-head-experiment').val()+'/';
			url += dada.find('.dada-head-detector').val()+'/';
			dada_state_head_load(dada, url, ['sample'], []);
		    });

		    dada.find('.snap').offset({top: '8px', left: $(window).width()-420});

		    dada.find('.snap-rename').val(dada.data('setting'));
		},
		error: function() {
		    var txt = $('<p class="dada-error">cannot load snapshot "'+dada.data('setting')+'"</p>');
		    $('#loader').prepend(txt);
		    $('#loading').hide();
		}
	});

	/* setup list of snapshots */
	$.get('../snap/', function(tree) { dada_snap_setup(dada, tree); });

	/* load new snapshot */
	var snap = dada.find('.snap-list');
	snap.on('activate_node.jstree', function(e, data) {
	    node = data.node;
	    path = snap.jstree(true).get_path(node, "/");
	    if (node.icon == 'icons/add.svg') {
		/* TODO */
		var box = $('<div class="dada-modal"></div>');
		box.load('dada18.snap.create.html', function() {
		    $('body').append(box);
		    box.find('input[name="snap.name"]').keyup(function() {
			var txt = box.find('input[name="snap.name"]').val();
			txt = txt.replace(/[^a-zA-Z0-9_-]+/g, '');
			box.find('input[name="snap.name"]').val(txt);
		    });
		    box.find('button[type="submit"]').click(function() {
			var txt = box.find('input[name="snap.name"]').val();
			txt = txt.replace(/[^a-zA-Z0-9_-]+/g, '');
			box.find('input[name="snap.name"]').val(txt);
			path = snap.jstree(true).get_path(node.parents[0], "/");
			var setting = path +'/' + txt;
			dada.data('setting', setting);
			window.location='#snap='+dada.data('setting');
			snap_save(dada, function() { $.get('../snap/', function(tree) { dada_snap_setup(dada, tree, dada.data('setting')); }); });
			box.remove();
		    });
		    box.find('button[type="cancel"]').click(function() { box.remove(); });
		    box.find('input').focus();
		});
		return;
	    }
	    if (node.children.length) {
		snap.jstree(true).toggle_node(node);
		return;
	    }
	    if (node.icon != 'icons/file.svg') return;
	    if (path == dada.data('setting')) return;
	    window.location='#snap='+path;
	    dada.remove();
	    $('#loader p').remove();
	    $('#loading').css('width', '0em');
	    $('#greeting').show();
	    dada_setup_snap(path);
	});


	/* reload list of snapshots */
	dada.find('.snap-icon-reload').click(function() {
	    $.get('../snap/', function(tree) { dada_snap_setup(dada, tree); });
	});

	/* reload snapshot */
	dada.find('.snap-icon-undo').click(function() {
	    dada.remove();
	    $('#loader p').remove();
	    $('#loading').css('width', '0em');
	    $('#greeting').show();
	    dada_setup_snap(path);
	});

	/* save snapshot */
	dada.find('.snap-icon-save').click(function() { snap_save(dada, null); });

	/* delete snapshot */
	dada.find('.snap-icon-trash').click(function() {
	    /* TODO */
	    var box = $('<div class="dada-modal"></div>');
	    box.load('dada18.snap.delete.html', function() {
		$('body').append(box);
		box.find('button[type="submit"]').click(function() { /* TODO */ });
		box.find('button[type="cancel"]').click(function() { box.remove(); });
		box.find('.snap-del-kind').text('snapshot / folder');
		box.find('.snap-del-name').val(path);
	    });
	});

	/* open snapshot in new tab */
	dada.find('.snap-icon-newtab').click(function() {
	    var url = window.location.href;
	    window.open(url, '_blank');
	});

	/* send snapshot URL via e-mail */
	dada.find('.snap-icon-mail').click(function() {
	    var url = 'mailto:?subject=dada&body='+window.location.href;
	    window.location.href = url;
	});

	var drag = null;

	$('body').mousemove(function(e) {
	    if (drag) drag.ele.offset({top: e.pageY-drag.y, left: e.pageX-drag.x});
	});
	$('body').mouseup(function(e) { drag = null; });
	// $('body').mouseleave(function(e) { drag = null; });
	dada.find('.dada-box-drag').mousedown(function(e) {
	    drag = {ele: $(e.target).parent(), x: e.pageX-$(e.target).offset().left, y: e.pageY-$(e.target).offset().top};
	});
    });

    function snap_save(dada, func) {
	var snap = dada_save_state(dada);
	var frm = new FormData();
	frm.append('snap', JSON.stringify(snap));
	var req = new XMLHttpRequest();
	req.open('POST', 'snap/'+dada.data('setting')+'.json');
	req.onreadystatechange = function() {
	    if (req.readyState != XMLHttpRequest.DONE) return;
	    switch (req.status) {
		case 200:
		    dada.find('.dada-text').text(req.responseText);
		    if (func) func();
		    break;
		default:
		    dada.find('.dada-text').text('ERROR: '+req.responseText);
		    break;
	    }
	}
	req.send(frm);
    }
}

function dada_snap_setup(dada, tree) {
    var snap = dada.find('.snap-list').empty();
    snap.removeClass(function (index, className) { return (className.match (/(^|\s)jstree\S+/g) || []).join(' '); });
    snap.removeClass('jstree');
    var node = null;
    var path = null;

    snap.jstree({'core': { 'data': tree } });

    /* select current node as active */
    window.setTimeout(function() {
	var list = snap.jstree(true).get_json('#', {flat:true})
	$(list).each(function(index, node) {
	    var path = snap.jstree(true).get_path(node, "/");
	    if (path == dada.data('setting')) snap.jstree(true).activate_node(node);
	});
    }, 500); /* we have to wait some time ... */
}

function dada_update(dada) {
    var module = dada.find('.dada-module').val();
    switch (module) {
	case 'single': dada_update_single(dada); break;
	case 'multi':  dada_update_multi(dada);  break;
	case 'STXM':   dada_update_stxm(dada);   break;
    }
}


function dada_setup_box_open(dada) {
    var blah = dada.find('.dada-box-open button');
    blah.click(function() {
	var box = $(this).data('box');
	box = dada.find('.'+box);
	box.toggleClass('dada-box-noshow').toggleClass('dada-box-show');
	$(this).toggleClass('dada-aktiv');
    });
}

function dada_state_box_open(dada, list) {
    if (!dada) return;
    dada.find('.dada-head-open').click(function() {
	$(this).toggleClass('dada-aktiv');
	dada.find('.dada-box-open').toggleClass('dada-box-noshow').toggleClass('dada-box-show');
    });

    if (!list) return;
    list.forEach(function(ele) {
	var blah = dada.find('.dada-box-open button[data-box="'+ele+'"]');
	blah.click();
    });
    dada.find('.dada-head-open').click();
}

function dada_state_module(dada, module) {
    if (!dada || !module) return;
    dada.find('.dada-module').change(function() { dada_module(dada); dada_update(dada); });
    dada.find('.dada-module').val(module);
    dada_module(dada);
}

function dada_module(dada) {
    var module = dada.find('.dada-module').val();
    ['single', 'movie', 'multi', 'stxm'].forEach(function(ele) {
	dada.find('.dada-navi-'+ele).hide();
    });
    switch (module) {
	case 'single':
	case 'show':
	    dada.find('.dada-navi-single').show();
	    dada.find('.dada-navi-movie' ).show();
	    break;
	case 'multi':
	    dada.find('.dada-navi-multi' ).show();
	    break;
	case 'STXM':
	    dada.find('.dada-navi-stxm'  ).show();
	    break;
	default:
	    return;
    }
}

function dada_state_head(dada, head) {
    if (!dada || !head) return;
    var url = '../list/';

    dada_state_head_load(dada, url, ['instrument', 'experiment', 'detector', 'sample'], head);

    dada.find('.dada-head-instrument').change(function() {
	var url = '../list/';
	if (dada.find('.dada-head-instrument').prop('selectedIndex') == 0) return;
	url += dada.find('.dada-head-instrument').val()+'/';
	dada_state_head_load(dada, url, ['experiment'], []);
    });

    dada.find('.dada-head-experiment').change(function() {
	var url = '../list/';
	if (dada.find('.dada-head-experiment').prop('selectedIndex') == 0) return;
	url += dada.find('.dada-head-instrument').val()+'/';
	url += dada.find('.dada-head-experiment').val()+'/';
	dada_state_head_load(dada, url, ['detector'], []);
    });

    dada.find('.dada-head-detector').change(function() {
	var url = '../list/';
	if (dada.find('.dada-head-detector').prop('selectedIndex') == 0) return;
	url += dada.find('.dada-head-instrument').val()+'/';
	url += dada.find('.dada-head-experiment').val()+'/';
	url += dada.find('.dada-head-detector').val()+'/';
	dada_state_head_load(dada, url, ['sample'], []);
    });
}

function dada_state_head_load(dada, url, arr, head) {
    /* obtain list of next elements; in order:
     * instrument,
     * experiment,
     * detector,
     * sample
     * */
    $.get(url+'?_t='+Date.now(), function(data) {
	if (!data.status || data.status != "okay" || data.listing == null) {
	    dada.find('.dada-text').text('error: listing failed.'); return;
	}

	/* add elements to respective <select> */
	var what = arr.shift();
	dada.find('.dada-head-'+what).empty();
	    dada.find('.dada-head-'+what).append('<option name="">'+what+'</option>');
	data.listing.forEach(function(ele) {
	    dada.find('.dada-head-'+what).append('<option name="'+ele+'">'+ele+'</option>');
	});

	/* if snap provided an option, select it; and continue with next <select> */
	var opt = head.shift();
	if (opt) {
	    dada.find('.dada-head-'+what).val(opt);
	    if (arr.length) {
		dada_state_head_load(dada, url+opt+'/', arr, head);
	    } else {
		window.setTimeout(function() {
		    dada.hide();
		    $('body').append(dada);
		    $('#fullbg').fadeOut(300, function() {
			$('#greeting').hide();
			dada.fadeIn(300);
		    });
		}, 500);
		dada.find('.dada-text').text('starting analysis ...');
		dada_update(dada);
	    }

	    if (what == "experiment") { /* try to load list of ROIS from $experiment/roi.json */
		var exp = dada.find('.dada-head-experiment').val();
		var url2 = exp + '/roi.json';
		dada.find('.dada-pp-roi-list').empty();
		dada.find('.dada-pp-roi-list').append('<option data-roi="null">saved ROIs</option>');
		$.get(url2, function(rois) {
		    $.each(rois, function(key,roi) {
			var opt = $('<option>'+key+'</option>');
			opt.data('roi', roi);
			dada.find('.dada-pp-roi-list').append(opt);
		    });
		});
	    }
	}
    });
}

function dada_state_single(dada, single) {
    if (!dada || !single) return;
    ['imno'].forEach(function(key) {
	if (single[key] != null) dada_set_value(dada, '.dada-navi-'+key, single[key]);
    });

    dada.find('.dada-navi-prev').click(function() {
	var imno = parseInt(dada.find('.dada-navi-imno' ).val());
	var next = imno-1;
	dada.find('.dada-navi-imno').val(next);
	dada_update_single(dada);
    });
    dada.find('.dada-navi-next').click(function() {
	var imno = parseInt(dada.find('.dada-navi-imno' ).val());
	var next = imno+1;
	dada.find('.dada-navi-imno').val(next);
	dada_update_single(dada);
    });
}

function dada_state_movie(dada, movie) {
    if (!dada || !movie) return;
    ['first', 'step', 'last', 'wait'].forEach(function(key) {
	if (movie[key] != null) dada_set_value(dada, '.dada-movie-'+key, movie[key]);
    });

    dada.find('.dada-movie-play').click(function() { dada_movie_play(dada); });
    dada.find('.dada-movie-stop').click(function() { dada_movie_stop(dada); });
}

function dada_state_multi(dada, multi) {
    if (!dada || !multi) return;
    ['first', 'horz', 'vert'].forEach(function(key) {
	if (multi[key] != null) dada_set_value(dada, '.dada-navi-multi-'+key, multi[key]);
    });
    dada.find('.dada-navi-multi-show').click(function() { dada_update_multi(dada); });
}

function dada_state_stxm(dada, stxm) {
    if (!dada || !stxm) return;
    ['first', 'horz', 'vert'].forEach(function(key) {
	if (stxm[key] != null) dada_set_value(dada, '.dada-navi-stxm-'+key, stxm[key]);
    });
    if (stxm['parallel']) dada_set_button_aktiv(dada, '.dada-navi-stxm-parallel')
    dada.find('.dada-navi-stxm-signal').val(stxm['signal']);
    dada.find('.dada-navi-stxm-show'  ).click( function() { dada_update_stxm(dada); });
    dada.find('.dada-navi-stxm-signal').change(function() { dada_update_stxm(dada); });
    dada.find('.dada-navi-stxm-view'  ).change(function() { dada_stxm_view(dada);   });

    dada.find('.dada-navi-stxm-add').click(function() {
	var add  = $(this).data('add');
	var ele  = dada.find('.dada-navi-stxm-vert input');
	var vert = parseInt(ele.val());
	vert += add;
	ele.val(vert);
	dada_update_stxm(dada);
    });
}

function dada_state_colour(dada, colour) {
    if (!dada || !colour) return;
    ['min', 'max'].forEach(function(key) {
	if (colour[key] != null) dada_set_value(dada, '.dada-colour-'+key, colour[key]);
    });
    if (colour['clip'] != null) {
	colour['clip'].forEach(function(ele) {
	    dada_set_button_aktiv(dada, '.dada-colour-clip-'+ele);
	});
    }
    if (colour['scale'] == null || colour['scale'] != 'lin') {
	dada_set_button_aktiv(  dada, '.dada-colour-scale-log');
	dada_set_button_inaktiv(dada, '.dada-colour-scale-lin');
    } else {
	dada_set_button_aktiv(  dada, '.dada-colour-scale-lin');
	dada_set_button_inaktiv(dada, '.dada-colour-scale-log');
    }
    if (colour['palette'] != null) {
	dada.find('.dada-colour-palette-select').hide();
	var pal = colour['palette'];
	var src = '../colourpalette/'+pal;
	dada.find('.dada-colour-palette-current').attr('src', src).data('palette', pal);
    }
}

function dada_state_preprocessing(dada, pp) {
    if (!dada || !pp) return;
    if (pp['roi'] != null) {
	dada_set_value(dada, '.dada-pp-roi-x', pp['roi'][0]);
	dada_set_value(dada, '.dada-pp-roi-y', pp['roi'][1]);
	dada_set_value(dada, '.dada-pp-roi-w', pp['roi'][2]);
	dada_set_value(dada, '.dada-pp-roi-h', pp['roi'][3]);
    }
    if (pp['bin'] != null) {
	dada_set_value(dada, '.dada-pp-bin-x', pp['bin'][0]);
	dada_set_value(dada, '.dada-pp-bin-y', pp['bin'][1]);
    }
    ['divide', 'subtract', 'sum'].forEach(function(key) {
	if (pp[key] != null) dada_set_value(dada, '.dada-pp-'+key, pp[key]);
    });
    if (pp['stack'] != null) {
	dada_set_value(dada, '.dada-pp-stack', pp['stack'][0]);
	dada.find('.dada-pp-stack-what').val(pp['stack'][1]);
    }
}

function dada_set_value(dada, key, val) {
    if (!dada || !key) return;
    var obj = dada.find(key+' input');
    if (obj.length==0)
	obj = dada.find(key);
    if (!obj) return;
    if (val == null)
	obj.val();
    else
	obj.val(val);
}

function dada_set_button_aktiv(dada, key) {
    if (!dada || !key) return;
    var obj = dada.find(key);
    if (!obj) return;
    obj.addClass('dada-aktiv');
}
function dada_set_button_inaktiv(dada, key) {
    if (!dada || !key) return;
    var obj = dada.find(key);
    if (!obj) return;
    obj.removeClass('dada-aktiv');
}


/* https://stackoverflow.com/questions/901115/how-can-i-get-query-string-values-in-javascript */
function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    // var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"), /* ? */
    var regex = new RegExp("[#&]" + name + "(=([^&#]*)|&|#|$)"), /* # */
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}


function dada_slideshow(curr) {
    if (!slideshow) return;
    var images = ['p01.jpg','p02.jpg','p03.jpg','p04.jpg','p05.jpg','p06.jpg','p07.jpg','p08.jpg','p09.jpg','p10.jpg','p11.jpg','p12.jpg'];
    var url = 'slides/'+images[curr];

    $('#slideshow').fadeOut(400, function() {
	$('#slideshow').css('background-image', 'url(' + url + ')');
	$('#slideshow').fadeIn(400);
    });

    window.setTimeout(function() {
	if (++curr > images.length) curr = 0;
	dada_slideshow(curr);
    }, 5000);
}


function dada_save_state(dada) {
    var snap = {};
    var box;
    
    snap['dada-module'] = dada.find('.dada-module').val();

    snap['dada-head'] = [];
    ['instrument', 'experiment', 'detector', 'sample'].forEach(function(key) {
	snap['dada-head'].push(dada.find('.dada-head-'+key).val());
    });

    snap['dada-navi'] = {};
    snap['dada-navi']['single'] = {};
    snap['dada-navi']['movie']  = {};
    snap['dada-navi']['multi']  = {};
    snap['dada-navi']['stxm']   = {};

    snap['dada-navi']['single']['imno' ] = parseInt(dada.find('.dada-navi-imno'             ).val());
    snap['dada-navi']['movie' ]['first'] = parseInt(dada.find('.dada-movie-first'           ).val());
    snap['dada-navi']['movie' ]['step' ] = parseInt(dada.find('.dada-movie-step'            ).val());
    snap['dada-navi']['movie' ]['last' ] = parseInt(dada.find('.dada-movie-last'            ).val());
    snap['dada-navi']['movie' ]['wait' ] = parseInt(dada.find('.dada-movie-wait'            ).val());
    snap['dada-navi']['multi' ]['first'] = parseInt(dada.find('.dada-navi-multi-first input').val());
    snap['dada-navi']['multi' ]['horz' ] = parseInt(dada.find('.dada-navi-multi-horz input' ).val());
    snap['dada-navi']['multi' ]['vert' ] = parseInt(dada.find('.dada-navi-multi-vert input' ).val());
    snap['dada-navi']['stxm'  ]['first'] = parseInt(dada.find('.dada-navi-stxm-first input' ).val());
    snap['dada-navi']['stxm'  ]['horz' ] = parseInt(dada.find('.dada-navi-stxm-horz input'  ).val());
    snap['dada-navi']['stxm'  ]['vert' ] = parseInt(dada.find('.dada-navi-stxm-vert input'  ).val());

    box = dada.find('.dada-navi-stxm-signal');
    snap['dada-navi']['stxm'  ]['signal' ] = 'Itot';
    if (box) snap['dada-navi']['stxm'  ]['signal' ] = box.val();

    box = dada.find('.dada-navi-stxm-parallel');
    snap['dada-navi']['stxm'  ]['parallel' ] = false;
    if (box && box.hasClass('dada-aktiv')) snap['dada-navi']['stxm'  ]['parallel' ] = true;

    snap['dada-box-open'] = [];
    ['colour', 'preprocessing', 'processing', 'radial', 'zoom', 'meta'].forEach(function(key) {
	var box = dada.find('.dada-box-'+key);
	if (!box) return;
	if (box.hasClass('dada-box-show')) snap['dada-box-open'].push('dada-box-'+key);
    });

    snap['dada-colour'] = {};
    ['min', 'max'].forEach(function(key) {
	var box = dada.find('.dada-colour-'+key);
	if (!box) return;
	var val = box.val();
	if (val == "") return;
	snap['dada-colour'][key] = val;
    });
    snap['dada-colour']['scale'] = "lin";
    if (dada.find('.dada-colour-scale-log').hasClass('dada-aktiv')) snap['dada-colour']['scale'] = "log";
    snap['dada-colour']['clip'] = [];
    ['min', 'max'].forEach(function(key) {
	var box = dada.find('.dada-colour-clip-'+key);
	if (!box) return;
	if (box.hasClass('dada-aktiv')) snap['dada-colour']['clip'].push(key);
    });

    snap['dada-preprocessing'] = {};
    snap['dada-preprocessing']['roi'] = [];
    ['x', 'y', 'w', 'h'].forEach(function(key) {
	var box = dada.find('.dada-pp-roi-'+key);
	if (!box) return;
	snap['dada-preprocessing']['roi'].push(box.val());
    });
    snap['dada-preprocessing']['bin'] = [];
    ['x', 'y'].forEach(function(key) {
	var box = dada.find('.dada-pp-bin-'+key);
	if (!box) return;
	snap['dada-preprocessing']['bin'].push(box.val());
    });
    ['divide', 'subtract', 'sum'].forEach(function(key) {
	var box = dada.find('.dada-pp-'+key);
	if (!box) return;
	snap['dada-preprocessing'][key] = box.val();
    });

    return snap;
}

