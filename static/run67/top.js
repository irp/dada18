function cluster_top() {
    $('#top tbody tr').each(function(idx, tr) {
	var host = $(tr).data('host');
	var  url = 'http://'+host+'/info/top';

	    $.get(url).done(function(text) {
		var regx = /\[(\w.+) *:\s*(\d+)\]\s*([0-9.]+) MiB,\s*([0-9.]+) %,\s*([0-9.]+) MiB.*/
		var host = text.replace(regx, "$1");
		var  pid = text.replace(regx, "$2");
		var  cpu = text.replace(regx, "$4");
		var  mem = text.replace(regx, "$3");
		var  dt1 = text.replace(regx, "$5");

		var  cls = 'idle';
		if (cpu >   1) cls = 'act0';
		if (cpu >  20) cls = 'act1';
		if (cpu >  60) cls = 'act2';
		if (cpu > 110) cls = 'act3';

		var td1 = $('<td>'+host+':'+pid+'</td>');
		var td2 = $('<td class='+cls+'>'+cpu+'</td>');
		var td3 = $('<td>'+mem+'</td>');
		var td4 = $('<td>'+dt1+'</td>');

		$(tr).html([td1,td2,td3,td4]);
	    }).error(function() {
		var td1 = $('<td class="offline">'+host+'</td>');
		var td2 = $('<td class=>??</td>');
		var td3 = $('<td>??</td>');
		var td4 = $('<td>??</td>');
		$(tr).html([td1,td2,td3,td4]);
	    });
    });
    window.setTimeout(function() { cluster_top(); }, 2000);
}

$(document).ready(function() {
    cluster_top();
});

