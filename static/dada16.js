function dada_setup(dada) {
    var base = null;
    if (what.instrument && what.experiment && what.detector && what.sample)
	base = what.instrument+'/'+what.experiment+'/'+what.detector+'/'+what.sample+'/';

    dada.data('what', what);
    dada.data('base', base);

    var w = '';
    w += '<span class="select-arrow"><select class="dada-what-instrument"></select></span>';
    w += '<span class="select-arrow"><select class="dada-what-experiment"></select></span>';
    w += '<span class="select-arrow"><select class="dada-what-detector"></select></span>';
    w += '<span class="select-arrow select-arrow-wide"><select class="dada-what-sample"></select></span>';
    //w += '<span class="spacer">&nbsp;</span>';
    w += '<span class="absolute-right"><span class="float-right"><span class="kiste-knoepfe"><button class="dada-what-min" tabindex="-1">◣</button><button class="dada-what-open" tabindex="-1">▶</button><button class="dada-what-close" tabindex="-1">×</button></span></span></span>';
    dada.find('.dada-what').empty().html(w);

function dada_what_update(which, url, current) {
    url = '../show/'+url;
    $.get('../show/'+url, function(data) {
	if (data.status && data.status=='okay') {
	    dada.find('.dada-what-'+which).append('<option value="">'+which+'</option>');
	    $.each(data.listing, function(idx, ele) {
		if (ele == current)
		    dada.find('.dada-what-'+which).append('<option selected value="'+ele+'">'+ele+'</option>');
		else
		    dada.find('.dada-what-'+which).append('<option value="'+ele+'">'+ele+'</option>');
	    });
	}

    });

}

function dada_what_update_instrument(what) {
    dada_what_update('instrument', '', what.instrument);
}

function dada_what_update_experiment(what) {
    if (what.instrument) {
	dada_what_update('experiment', what.instrument, what.experiment);
    } else {
	dada.find('.dada-what-experiment').append('<option value="">experiment</option>');
    }
}

function dada_what_update_detector(what) {
    if (what.instrument && what.experiment) {
	dada_what_update('detector', what.instrument+'/'+what.experiment, what.detector);
    } else {
	dada.find('.dada-what-detector').append('<option value="">detector</option>');
    }
}

function dada_what_update_sample(what) {
    if (what.instrument && what.experiment && what.detector) {
	dada_what_update('sample', what.instrument+'/'+what.experiment+'/'+what.detector, what.sample);
    } else {
	dada.find('.dada-what-sample').append('<option value="">sample</option>');
    }
}

    dada.find('.dada-what').prepend('<span class="select-arrow"><select class="dada-module"></select></span>');
function dada_module_add(dada, what) {
    dada.find('.dada-module').append('<option value="'+what+'">'+what+'</option>');
}
    dada_module_add(dada, "single");
    dada_module_add(dada, "multi");
    dada_module_add(dada, "STXM");

    dada_what_update_instrument(what);
    dada_what_update_experiment(what);
    dada_what_update_detector(what);
    dada_what_update_sample(what);

function dada_what_add_sample(sample, current) {
    if (sample == what.sample)
	dada.find('.dada-what-sample').append('<option selected value="'+sample+'">'+sample+'</option>');
    else
	dada.find('.dada-what-sample').append('<option value="'+sample+'">'+sample+'</option>');
}

    dada.find('.dada-what-instrument').change(function() {
	what.instrument = $(this).val();
	what.experiment = null;
	what.detector = null;
	what.sample = null;
	dada.data('what', what);
	dada_setup(dada);
    });
    dada.find('.dada-what-experiment').change(function() {
	what.experiment = $(this).val();
	what.detector = null;
	what.sample = null;
	dada.data('what', what);
	dada_setup(dada);
    });
    dada.find('.dada-what-detector').change(function() {
	what.detector = $(this).val();
	what.sample = null;
	dada.data('what', what);
	dada_setup(dada);
    });
    dada.find('.dada-what-sample').change(function() {
	what.sample = $(this).val();
	dada.data('what', what);
	dada_setup(dada);
    });

    dada.find('.dada-module').off();
    dada.find('.dada-module').change(function() {
	var mod = $(this).val();

	/* first, hide all navigation bars */
	dada.find('.dada-navi-single').hide();
	dada.find('.dada-navi-movie').hide();
	dada.find('.dada-navi-multi').hide();
	dada.find('.dada-navi-stxm').hide();

	/* now, show needed bars again */
	switch (mod) {
	    case 'multi':
		dada.find('.dada-navi-multi').show();
		break;
	    case 'STXM':
		dada.find('.dada-navi-stxm').show();
		break;
	    default:
		dada.find('.dada-navi-single').show();
		dada.find('.dada-navi-movie').show();
		break;
	}
    });

    var text = dada.find('.dada-text');
    var navi = dada.find('.dada-navi');

    var leiste = '';
    leiste += '<span class="dada-navi-single">';
    //leiste += '<button class="dada-navi-frst" tabindex="-1">⇤</button>';
    leiste += '<button class="dada-navi-prev" tabindex="-1">←</button>';
    leiste += '<input class="dada-navi-imno" pattern="[0-9/]*">';
    leiste += '<button class="dada-navi-next" tabindex="-1">→</button>';
    //leiste += '<button class="dada-navi-last" tabindex="-1">⇥</button>';
    leiste += '&nbsp;';
    leiste += '</span>';
    leiste += '<span class="dada-navi-movie">';
    leiste += '<input class="dada-movie-first" pattern="[0-9/]*" value="0">';
    leiste += ':<input class="dada-movie-step" type="number"     value="1">';
    leiste += ':<input class="dada-movie-last" type="number"     value="100">';
    leiste += '<input class="dada-movie-wait"  type="number"     value="50" >ms';
    leiste += '&nbsp;';
    leiste += '<button class="dada-movie-rept" tabindex="-1">⟲</button>';
    leiste += '<button class="dada-movie-play" tabindex="-1">▶</button>';
    leiste += '<button class="dada-movie-stop" tabindex="-1">◼</button>';
    leiste += '<button class="dada-movie-help" tabindex="-1">？</button>';
    leiste += '</span>';
    leiste += '<span class="dada-navi-multi">';
    leiste += '<span class="dada-navi-multi-first"><input type="number" value="0"></span>';
    leiste += '<span class="dada-navi-multi-horz" ><input type="number" value="10"></span>';
    leiste += '<span class="dada-navi-multi-vert" ><input type="number" value="10"></span>';
    leiste += '<button class="dada-navi-multi-progressive">progressive</button>';
    leiste += '<button class="dada-navi-multi-parallel">parallel</button>';
    leiste += '<button class="dada-navi-multi-prev" tabindex="-1">←</button>';
    leiste += '<button class="dada-navi-multi-show" tabindex="-1">✓</button>';
    leiste += '<button class="dada-navi-multi-next" tabindex="-1">→</button>';
    leiste += '<button class="dada-navi-multi-help" tabindex="-1">？</button>';
    leiste += '</span>';
    leiste += '<span class="dada-navi-stxm">';
    leiste += '<span class="dada-navi-stxm-first"><input type="number" value="0"></span>';
    leiste += '<span class="dada-navi-stxm-horz" ><input type="number" value="10"></span>';
    leiste += '<span class="dada-navi-stxm-vert" ><input type="number" value="10"></span>';
    leiste += '<span class="select-arrow">';
    leiste += '<select class="dada-navi-stxm-signal">';
    leiste += '<option value="Itot">Itot</option>';
    leiste += '<option value="Imax">Imax</option>';
    leiste += '<option value="comh">comh</option>';
    leiste += '<option value="comv">comv</option>';
    leiste += '<option value="rad1">rad1</option>';
    leiste += '<option value="rad2">rad2</option>';
    leiste += '<option value="rad3">rad3</option>';
    leiste += '<option value="rad4">rad4</option>';
    leiste += '<option value="phi1">φ₁</option>';
    leiste += '<option value="lam1">λ₁</option>';
    leiste += '<option value="lam2">λ₂</option>';
    leiste += '<option value="omeg">ω=(λ₁-λ₂)/(λ₁+λ₂)</option>';
    leiste += '</select>';
    leiste += '</span>';
    leiste += '<button class="dada-navi-stxm-progressive">progressive</button>';
    leiste += '<button class="dada-navi-stxm-parallel aktiv">parallel</button>';
    leiste += '<button class="dada-navi-stxm-show" tabindex="-1">✓</button>';
    leiste += '<button class="dada-navi-stxm-help" tabindex="-1">？</button>';
    leiste += '</span>';
    navi.html(leiste);
    navi.find('.dada-navi-multi-progressive').click(function() { $(this).toggleClass('aktiv'); });
    navi.find('.dada-navi-stxm-progressive').click(function()  { $(this).toggleClass('aktiv'); });
    navi.find('.dada-navi-multi-parallel').click(function() { $(this).toggleClass('aktiv'); });
    navi.find('.dada-navi-stxm-parallel').click(function()  { $(this).toggleClass('aktiv'); });

    dada.find('.dada-navi-multi').hide();
    dada.find('.dada-navi-stxm').hide();

    navi.find('.dada-navi-imno').off();
    navi.find('.dada-navi-imno').on('keyup', function() {
	var imno = $(this).val();
	dada_show_single_image(dada, imno);
    });
    navi.find('.dada-navi-frst').off();
    navi.find('.dada-navi-prev').off();
    navi.find('.dada-navi-next').off();
    navi.find('.dada-navi-last').off();
    navi.find('.dada-navi-frst').click(function() { dada_show_single_image_first(dada); });
    navi.find('.dada-navi-prev').click(function() { dada_show_single_image_prev(dada);  });
    navi.find('.dada-navi-next').click(function() { dada_show_single_image_next(dada);  });
    navi.find('.dada-navi-last').click(function() { dada_show_single_image_last(dada);  });

    navi.find('.dada-movie-rept').off();
    navi.find('.dada-movie-play').off();
    navi.find('.dada-movie-stop').off();
    navi.find('.dada-movie-rept').click(function() { dada_movie_repeat(dada); });
    navi.find('.dada-movie-play').click(function() { dada_movie_play(dada);   });
    navi.find('.dada-movie-stop').click(function() { dada_movie_stop(dada);   });

    navi.find('.dada-navi-multi-show').off();
    navi.find('.dada-navi-multi-show').click(function() {
	    if ($(this).hasClass('busy')) {
		$(this).addClass('canceled');
		return;
	    }
	    var first = parseInt(dada.find('.dada-navi-multi-first>input').val());
	    var horz  = parseInt(dada.find('.dada-navi-multi-horz>input').val());
	    var vert  = parseInt(dada.find('.dada-navi-multi-vert>input').val());
	    $(this).addClass('busy');
	    dada_show_multi_image(dada, first, horz, vert, true);
    });

    navi.find('.dada-navi-multi-prev').off();
    navi.find('.dada-navi-multi-prev').click(function() {
	    var first = parseInt(dada.find('.dada-navi-multi-first>input').val());
	    var horz  = parseInt(dada.find('.dada-navi-multi-horz>input').val());
	    var vert  = parseInt(dada.find('.dada-navi-multi-vert>input').val());
	    first -= horz*vert;
	    if (first < 0) return;
	    dada.find('.dada-navi-multi-first>input').val(first);
	    dada.find('.dada-navi-multi-show').click()
    });

    navi.find('.dada-navi-multi-next').off();
    navi.find('.dada-navi-multi-next').click(function() {
	    var first = parseInt(dada.find('.dada-navi-multi-first>input').val());
	    var horz  = parseInt(dada.find('.dada-navi-multi-horz>input').val());
	    var vert  = parseInt(dada.find('.dada-navi-multi-vert>input').val());
	    first += horz*vert;
	    dada.find('.dada-navi-multi-first>input').val(first);
	    dada.find('.dada-navi-multi-show').click()
    });

    navi.find('.dada-navi-stxm-show').off();
    navi.find('.dada-navi-stxm-show').click(function() {
	    if ($(this).hasClass('busy')) {
		$(this).addClass('canceled');
		return;
	    }
	    var first = parseInt(dada.find('.dada-navi-stxm-first>input').val());
	    var horz  = parseInt(dada.find('.dada-navi-stxm-horz>input').val());
	    var vert  = parseInt(dada.find('.dada-navi-stxm-vert>input').val());
	    $(this).addClass('busy');
	    dada_show_stxm_image(dada, first, horz, vert, true, vert);
    });

    navi.find('.dada-navi-stxm-signal').off();
    navi.find('.dada-navi-stxm-signal').change(function() {
	    if (navi.find('.dada-navi-stxm-show').hasClass('busy')) {
		return;
	    }
	    navi.find('.dada-navi-stxm-show').click();
    });

    dada.find('.dada-what-open').off();
    if ($(dada.parents('.container').find('.box-open')).data('vis')==true) {
	$(dada.find('.dada-what-open')).css('background', 'lime');
    }
    dada.find('.dada-what-open').click(function() {
	var box = $(dada.parents('.container').find('.box-open'));
	if (box.data('vis')==true) {
	    box.data('vis', false);
	    box.hide();
	    $(this).css('color', 'black');
	} else {
	    box.data('vis', true);
	    box.show();
	    $(this).css('color', 'lime');
	}
    });
    $.each(['open', 'colour', 'meta', 'preprocessing', 'processing', 'radial', 'zoom', 'export'], function(i,e) {
	dada.parents('.container').find('.box-'+e).hide();
    });

    dada.parents('.container').find('.box-open button').off();
    dada.parents('.container').find('div').each(function(i,box) {
	if ($(box).data('vis')==true) {
	    $(box).show();
	}
    });
    dada.parents('.container').find('.box-open button').off();
    dada.parents('.container').find('.box-open button').click(function() {
	var what = '.'+$(this).data('box');
	var box = dada.parents('.container').find(what);
	if (box.data('vis')==true) {
	    box.hide();
	    box.data('vis', false);
	    $(this).css('background', 'white');
	} else {
	    box.show();
	    box.data('vis', true);
	    $(this).css('background', 'lime');
	}
    });

    navi.find('.dada-movie-help').off();
    navi.find('.dada-movie-help').click(function() {
	if ($(this).hasClass('helping')) {
	    $(this).removeClass('helping');
	    $(this).parents('.dada').find('.dada-help').hide();
	} else {
	    $(this).addClass('helping');
	    $(this).parents('.dada').find('.dada-help').show();
	    var hilfe = $('<span></span>');
	    $(hilfe).append('<p><b>show single</b> input image number to show; use arrows to navigate back/forth</p>');
	    $(hilfe).append('<p><b>movie</b> first image : step : last image : wait time (ms); infinite loop; play; stop</p>');
	    $(this).parents('.dada').find('.dada-help').html(hilfe);
	}
    });

var container = null;

function container_setup_colour(container) {
    var span = '';
    span = '<img class="colour-palette colour-palette-current"><button class="colour-palette-dropdown dropdown" tabindex="-1">▽</button>\n';
    container.html(span);
    var src = '../colourpalette/test1';
    container.find('img.colour-palette-current').attr('src', src).data('palette', 'test1');
    container.append('<ul class="colour-palette-select"></ul>');
    $.each(['test1', 'bw', 'wb', 'bwr', 'husl'], function(idx, palette) {
	var li;
	li = $('<li></li>');
	dada.find('.palette ul').append(li);
	var src = '../colourpalette/'+palette;
	var img = $('<img src="'+src+'" class="colour-palette"></img>');
	img.data('palette', palette);
	li.append(img);
	container.find('ul').append(li);
    });
    container.find('.colour-palette-dropdown').click(function() {
	container.find('ul.colour-palette-select').show();
    });
    container.find('.colour-palette').click(function() {
	container.find('ul.colour-palette-select').hide();
	var pal = $(this).data('palette');
	var src = '../colourpalette/'+pal;
	container.find('img.colour-palette-current').attr('src', src).data('palette', pal);
	update(dada, true);
    });
    span = '<table><tr><td>min:</td><td><input type="number" class="colour-min"></input></td><td><button class="colour-clear-min" tabindex="-1">✗</button></td><td><button class="colour-clip colour-clip-min" tabindex="-1">clip</button></td><td><button class="colour-scale colour-scale-lin" tabindex="-1">lin</button></td></tr><tr><td>max:</td><td><input type="number" class="colour-max"></input></td><td><button class="colour-clear-max" tabindex="-1">✗</button></td><td><button class="colour-clip colour-clip-max" tabindex="-1">clip</button></td><td><button class="colour-scale colour-scale-log aktiv" tabindex="-1">log</button></td></tr></table>';
    container.append(span).data('init', true);
    container.find('.colour-scale').click(function() {
	$(this).parents('table').find('.colour-scale').removeClass('aktiv');
	$(this).addClass('aktiv');
	update(dada, true);
    });
    container.find('.colour-clip').click(function() {
	$(this).toggleClass('aktiv');
	update(dada, true);
    });
    container.find('.colour-clear-min').click(function() {
	container.find('.colour-min').val('');
    });
    container.find('.colour-clear-max').click(function() {
	container.find('.colour-max').val('');
    });

    container.find('input').on('keyup', function() {
	update(dada, false);
    });
    container.find('.colour-min').change(function() {
	update(dada, true);
    });
    container.find('.colour-max').change(function() {
	update(dada, true);
    });

}
container = dada.parents('.container').find('.box-colour span');
if (!container.data('init')) container_setup_colour(container);

function container_setup_pp(container) {
    var span = '';
    span = '<table>' +
    '<tr><td>roi</td><td>←<input type="number" class="pp-roi-x">↑<input type="number" class="pp-roi-y">↔<input type="number" class="pp-roi-w">↕<input type="number" class="pp-roi-h"><button class="pp-clear-roi" tabindex="-1">✗</button></td></tr>' +
    '<tr><td>binning</td><td><input type="number" class="pp-bin-x" value="1">&times;<input type="number" class="pp-bin-y" value="1"><button class="pp-clear-bin" tabindex="-1">✗</button></td></tr>' +
    '<tr><td>divide #</td><td><input type="number" class="pp-divide"><button class="pp-clear-divide" tabindex="-1">✗</button></td></tr>' +
    '<tr><td>subtract #</td><td><input type="number" class="pp-subtract"><button class="pp-clear-subtract" tabindex="-1">✗</button></td></tr>' +
    '<tr><td>sum up</td><td><input type="number" class="pp-sum" value="1"><button class="pp-clear-sum" tabindex="-1">✗</button></td></tr>' +
    '</table>';
    container.html(span).data('init', true);
    container.find('input').on('keyup', function() {
	update(dada, false);
    });
    container.find('.pp-clear-roi').click(function() {
	container.find('.pp-roi-x').val('');
	container.find('.pp-roi-y').val('');
	container.find('.pp-roi-w').val('');
	container.find('.pp-roi-h').val('');
	update(dada, false);
    });
    container.find('.pp-clear-bin').click(function() {
	container.find('.pp-bin-x').val(1);
	container.find('.pp-bin-y').val(1);
	update(dada, false);
    });
    container.find('.pp-clear-divide').click(function() {
	container.find('.pp-divide').val('');
	update(dada, false);
    });
    container.find('.pp-clear-subtract').click(function() {
	container.find('.pp-subtract').val('');
	update(dada, false);
    });
    container.find('.pp-clear-sum').click(function() {
	container.find('.pp-sum').val(1);
	update(dada, false);
    });
}
container = dada.parents('.container').find('.box-preprocessing span');
if (!container.data('init')) container_setup_pp(container);

function container_setup_proc(container) {
    var span = '';
    span = '<table>' +
    '<tr><td>fft2d</td><td><input type="checkbox" class="proc-fft2d-enable">←<input type="number" class="proc-fft2d-cenx">↑<input type="number" class="proc-fft2d-cen-y"><button class="proc-fft2d-clear" tabindex="-1">✗</button></td></tr>' +
    '</table>';
    container.html(span).data('init', true);
    container.find('.proc-fft2d-enable').on('change', function() {
	update(dada, false);
    });
    container.find('.proc-fft2d-clear').click(function() {
	container.find('.proc-fft2d-enable').attr('checked', false);
	container.find('.proc-fft2d-cenx').val('');
	container.find('.proc-fft2d-ceny').val('');
	update(dada, false);
    });
}
container = dada.parents('.container').find('.box-processing span');
if (!container.data('init')) container_setup_proc(container);

function container_setup_export(container) {
    var span = '';
    span = '<ul><li>open as PNG</li><li>open as JPEG</li><li>open as PDF</li><li>open with Matlab</li></ul>';
    container.html(span).data('init', true);
}
container = dada.parents('.container').find('.box-export span');
if (!container.data('init')) container_setup_export(container);

function container_setup_radial(container) {
    var span = '';
    span = '<table>' +
    '<tr><td>centre</td><td>←<input type="number" class="radial-pb radial-pb-x" data-pb="x" value="">↑<input type="number" class="radial-pb radial-pb-y" data-pb="y" value=""><button class="radial-click-pb" tabindex="-1">☜</button><button class="radial-clear-pb" tabindex="-1">✗</button></td><td><button class="radial-show" tabindex="-1">show</button></td></tr>' +
    '<tr><td>radius 1</td><td> <input type="number" class="radial-radius radial-radius-1" value=""><button class="radial-click-radius radial-click-radius-1" data-radius="1" tabindex="-1">☜</button><button class="radial-clear-radius radial-clear-radius-1" data-radius="1" tabindex="-1">✗</button></td><td><span class="radial-intensity-1"></span></td></tr>' +
    '<tr><td>radius 2</td><td> <input type="number" class="radial-radius radial-radius-2" value=""><button class="radial-click-radius radial-click-radius-2" data-radius="2" tabindex="-1">☜</button><button class="radial-clear-radius radial-clear-radius-2" data-radius="2" tabindex="-1">✗</button></td><td><span class="radial-intensity-2"></span></td></tr>' +
    '<tr><td>radius 3</td><td> <input type="number" class="radial-radius radial-radius-3" value=""><button class="radial-click-radius radial-click-radius-3" data-radius="3" tabindex="-1">☜</button><button class="radial-clear-radius radial-clear-radius-3" data-radius="3" tabindex="-1">✗</button></td><td><span class="radial-intensity-3"></span></td></tr>' +
    '<tr><td>outside</td><td>&nbsp;</td><td><span class="radial-intensity-4"></span></td></tr>' +
    '</table>';
    span += '<h6>PCA between radius 1 and 2</h6>';
    span += '<table class="radial-pca">' +
    '<tr><td>φ₁</td><td>λ₁</td><td>λ₂</td><td>ω</td></tr>' +
    '<tr><td class="radial-pca-phi1"></td><td class="radial-pca-lam1"></td><td class="radial-pca-lam2"></td><td class="radial-pca-omeg"></td></tr>' +
    '</table>';
    span += '<h6>Azimuthal average between radius 1 and 3</h6>';
    span += '<div id="chart1" class="radial-plot"></div>';
    span += '<span class="radial-info"></span>';
    container.html(span).data('init', true);
    container.find('input').change(function() { update(dada, false); });
    container.find('.radial-clear-pb').click(function() { container.find('.radial-pb').val(''); update(dada, false); });
    container.find('.radial-click-pb').click(function() {
	var btn = $(this);
	btn.addClass('aktiv');
	dada.find('.dada-bild img').click(function(event) {
	    var of  = $(this).offset();
	    var x   = parseInt(event.pageX) - parseInt(of.left)+1; /* why +1, but ont in y?? */
	    var y   = parseInt(event.pageY) - parseInt(of.top);
	    container.find('.radial-pb-x').val(x);
	    container.find('.radial-pb-y').val(y);
	    dada.find('.dada-bild img').off('click');
	    btn.removeClass('aktiv');
	    //console.log(dada);
	    update(dada, false);
	});
    });
    container.find('.radial-show').click(function() { $(this).toggleClass('aktiv'); update(dada, false); });
    container.find('.radial-clear-radius').click(function() { container.find('.radial-radius-'+$(this).data('radius')).val(''); update(dada, false); });
    container.find('.radial-click-radius').click(function() {
	var btn = $(this);
	btn.addClass('aktiv');
	dada.find('.dada-bild img').click(function(event) {
	    var of  = $(this).offset();
	    var x   = parseInt(event.pageX) - parseInt(of.left)+1;
	    var y   = parseInt(event.pageY) - parseInt(of.top);
	    var pbx = container.find('.radial-pb-x').val();
	    var pby = container.find('.radial-pb-y').val();
	    var r2  = (pbx-x)*(pbx-x)+(pby-y)*(pby-y);
	    var rad = Math.round(Math.sqrt(r2), 1);
	    container.find('.radial-radius-'+btn.data('radius')).val(rad);
	    dada.find('.dada-bild img').off('click');
	    btn.removeClass('aktiv');
	    update(dada, false);
	});
    });
}
container = dada.parents('.container').find('.box-radial span');
if (!container.data('init')) container_setup_radial(container);


function update(dada, really) {
    switch (dada.find('.dada-module').val()) {
	case 'single':
	    dada_show_single_image(dada, null);
	    break;
	case 'multi':
	    if (really==false) return;
	    var first = parseInt(dada.find('.dada-navi-multi-first>input').val());
	    var horz  = parseInt(dada.find('.dada-navi-multi-horz>input').val());
	    var vert  = parseInt(dada.find('.dada-navi-multi-vert>input').val());
	    dada.find('.dada-navi-multi-show').addClass('busy');
	    dada_show_multi_image(dada, first, horz, vert, true);
	    break;
	case 'STXM':
	    if (really==false) return;
	    var first = parseInt(dada.find('.dada-navi-stxm-first>input').val());
	    var horz  = parseInt(dada.find('.dada-navi-stxm-horz>input').val());
	    var vert  = parseInt(dada.find('.dada-navi-stxm-vert>input').val());
	    dada.find('.dada-navi-stxm-show').addClass('busy');
	    dada_show_stxm_image(dada, first, horz, vert, true, vert);
	    break;
    }
}

    window.setTimeout(dadatop(dada.parents('.container').find('.dada-top')), 100);

    text.text('initialised.');
    // dada_show_single_image_first(dada);
}

function dadatop(top) {
    return;
    var jqxhr = $.get('../info/top', function(text) {
	top.text(text);
    }).success(function() { window.setTimeout(function() {dadatop(top);},  2000);
    }).fail(   function() { window.setTimeout(function() {dadatop(top);}, 10000);
    });
}

function dada_show_single_image_first(dada) {
    var imno = 0;
    dada_show_single_image(dada, imno);
}

function dada_show_single_image_prev(dada) {
    var imno = dada.data('imno');
    /* check for slash dividing file number / frame number; increase frame number */
    var spl  = imno.split('/');
    spl[spl.length-1] = parseInt(spl[spl.length-1]) + parseInt(-1);
    switch (spl.length) {
	case 1: next = spl[0]; break;
	case 2: next = spl[0]+'/'+spl[1]; break;
    }
    dada_show_single_image(dada, next);
}

function dada_show_single_image_next(dada) {
    var imno = dada.data('imno');
    /* check for slash dividing file number / frame number; increase frame number */
    var spl  = imno.split('/');
    spl[spl.length-1] = parseInt(spl[spl.length-1]) + parseInt(1);
    switch (spl.length) {
	case 1: next = spl[0]+''; break;
	case 2: next = spl[0]+'/'+spl[1]; break;
    }
    dada_show_single_image(dada, next);
}

function dada_show_single_image_last(dada) {
    var imno = -1;
    dada_show_single_image(dada, imno);
}

function dada_movie_repeat(dada) {
    var btn = dada.find('.dada-movie-rept');
    if (btn.data('aktiv')) {
	btn.css('background-color', 'none');
	btn.data('aktiv', false);
    } else {
	btn.css('background-color', 'lime');
	btn.data('aktiv', true);
    }
}

function dada_movie_play(dada) {
    var btn = dada.find('.dada-movie-play');
    if (btn.data('aktiv')) {
	btn.css('background-color', 'none');
	btn.data('aktiv', false);
    } else {
	btn.css('background-color', 'lime');
	var first = dada.find('.dada-movie-first').val();
	var wait  = parseInt(dada.find('.dada-movie-wait' ).val());
	var imno  = first;
	dada_show_single_image(dada, imno);
	btn.data('aktiv', true);
	window.setTimeout(function() { dada_movie_step(dada); }, wait);
    }
}

function dada_movie_step(dada) {
    var btn = dada.find('.dada-movie-play');
    if (btn.data('aktiv')) {
	var imno  = dada.find('.dada-navi-imno'  ).val();
	var first = dada.find('.dada-movie-first').val();
	var step  = parseInt(dada.find('.dada-movie-step' ).val());
	var last  = parseInt(dada.find('.dada-movie-last' ).val());
	var wait  = parseInt(dada.find('.dada-movie-wait' ).val());

	/* check for slash dividing file number / frame number; increase frame number */
	var spl  = imno.split('/');
	spl[spl.length-1] = parseInt(spl[spl.length-1]) + parseInt(step);
	switch (spl.length) {
	    case 1: imno = spl[0]+''; break;
	    case 2: imno = spl[0]+'/'+spl[1]; break;
	}

	if (spl[spl.length-1] > last) {
	    if (dada.find('.dada-movie-rept').data('aktiv')) {
		imno = first;
	    } else {
		btn.css('background-color', 'none');
		btn.data('aktiv', false);
		return;
	    }
	}

	dada_show_single_image(dada, imno);
	window.setTimeout(function() { dada_movie_step(dada); }, wait);
    } else {
	btn.css('background-color', 'none');
	btn.data('aktiv', false);
    }
}

function dada_movie_stop(dada) {
    var btn = dada.find('.dada-movie-play');
    btn.css('background-color', 'none');
    btn.data('aktiv', false);
}

function dada_show_single_image(dada, _imno) {
    var imno = _imno;
    var base = dada.data('base');
    if (base == null) return;

    if (imno == null) {
	imno = dada.data('imno') + '';
    } else {
	dada.data('imno', imno+'');
    }
    dada.find('.dada-navi-imno').val(imno);

    var loading = dada.find('.dada-navi-imno').data('loading')+1;
    if (isNaN(loading)) loading = 1;
    dada.find('.dada-navi-imno').data('loading', loading);
    dada.find('.dada-navi-imno').addClass('busy');

    var url = '../show/' + base + imno;
    var mod = '?';

    var pp  = dada.parents('.container').find('.box-preprocessing');
    var roi = {x: parseInt($(pp).find('.pp-roi-x').val()),
	    y: parseInt($(pp).find('.pp-roi-y').val()),
	    w: parseInt($(pp).find('.pp-roi-w').val()),
	    h: parseInt($(pp).find('.pp-roi-h').val())};
    var bin = {x: parseInt($(pp).find('.pp-bin-x').val()),
	    y: parseInt($(pp).find('.pp-bin-y').val())};
    if (!isNaN(roi.x) && !isNaN(roi.y) && !isNaN(roi.w) && !isNaN(roi.h))
	    mod += '&roi='+roi.x+','+roi.y+','+roi.w+','+roi.h;
    mod += '&binning='+bin.x+','+bin.y;

    var empty = {number: parseInt($(pp).find('.pp-divide').val())};
    mod += '&empty='+empty.number;

    var subtract = {number: parseInt($(pp).find('.pp-subtract').val())};
    mod += '&subtract='+subtract.number;

    var sum = {number: parseInt($(pp).find('.pp-sum').val())};
    mod += '&sum='+sum.number;

    var fft2d = dada.parents('.container').find('.proc-fft2d-enable');
    if (fft2d.is(':checked'))
	mod += '&fft2d=true';
    else
	mod += '&fft2d=false';

    var palette = dada.parents('.container').find('img.colour-palette-current').data('palette');
    mod += '&palette='+palette;
    var scale = dada.parents('.container').find('.colour-scale-log').hasClass('aktiv') ? 'log' : 'lin';
    mod += '&scale='+scale;
    var cmin  = dada.parents('.container').find('.colour-min').val();
    mod += '&cmin='+cmin;
    var cmax  = dada.parents('.container').find('.colour-max').val();
    mod += '&cmax='+cmax;
    cmin = dada.parents('.container').find('.colour-clip-min').hasClass('aktiv') ? 'min' : '';
    cmax = dada.parents('.container').find('.colour-clip-max').hasClass('aktiv') ? 'max' : '';
    mod += '&clipping='+cmin+','+cmax;

    var radial = dada.parents('.container').find('.box-radial');
    var rad = {x: parseInt($(radial).find('.radial-pb-x').val()),
               y: parseInt($(radial).find('.radial-pb-y').val()),
               r: [ parseInt($(radial).find('.radial-radius-1').val()),
		   parseInt($(radial).find('.radial-radius-2').val()),
		   parseInt($(radial).find('.radial-radius-3').val()) ] };
    if (!isNaN(rad.x) && !isNaN(rad.y)) {
	mod += '&radial='+rad.x+','+rad.y;
	if (!isNaN(rad.r[0]) && !isNaN(rad.r[2])) {
	    mod += ';'+rad.r[0]+','+rad.r[2]+';1';
	}

	/*
	if (dada.parents('.container').find('.box-radial').find('.radial-show').hasClass('aktiv'))
	    mod += '&radial.show=true';
	mod += '&radial.pb.x='+rad.x+'&radial.pb.y='+rad.y+'&radial.r1='+rad.r[0]+'&radial.r2='+rad.r[1]+'&radial.r3='+rad.r[2];
	*/
    }

    url = url + mod;

    // dada.find('.dada-text').html('requesting '+url+' ...<br>\n');

    var req = dada.data('req');
    if (req != null)
	return;

    req = new XMLHttpRequest();
    req.open("GET", url, true);
    req.responseType = 'blob';

    req.onreadystatechange = function() {
	if (req.readyState != 4) return;
	switch (req.status) {
	    case 200:
		var timing = req.getResponseHeader('X-timing');
		// dada.find('.dada-text').html('loaded '+url+' ('+timing+')<br>\n');
		var img = new Image();
		img.onload = function() {
		    var img2 = $(this).clone();
		    var zfac = 8;
		    var zdiv = dada.parents('.container').find('.box-zoom div');
		    var zoom = zdiv.find('img');

		    dada.find('.dada-bild').empty().append(this);

		    var mtop = parseInt(zoom.css('margin-top'));
		    var mlft = parseInt(zoom.css('margin-left'));
		    if (isNaN(mtop)) mtop = -0.5*img2.height()+200;
		    if (isNaN(mlft)) mlft = -0.5*img2.width() +200;

		    zdiv.empty().append(img2).append('<canvas class="zoom-can"></canvas>');
		    zoom = zdiv.find('img');
		    zoom.addClass('zoom-img');
		    zoom.attr('width', zfac*img2.width());
		    zoom.css('margin-top',  mtop);
		    zoom.css('margin-left', mlft);
		    var can = zdiv.find('canvas')[0];
		    var ctx = can.getContext('2d');
		    $(can).attr({width: 400, height: 400});
		    ctx.clearRect(0, 0, 400, 400);
		    ctx.strokeStyle = '#000';
		    ctx.beginPath();
		    // ctx.arc(150, 150, 5, 0, 2*Math.PI, true);
		    ctx.rect(200, 200, 8, 8);
		    ctx.closePath();
		    ctx.stroke();

		    dada.find('.dada-bild img').removeClass('img-stxm');
		    dada.find('.dada-bild img').mousemove(function(event) {
			var of  = $(this).offset();
			var x   = parseInt(event.pageX) - parseInt(of.left)+1; /* why +1, but ont in y?? */
			var y   = parseInt(event.pageY) - parseInt(of.top);
			var txt = 'x='+x+' y='+y;
			var pbx = parseInt(dada.parents('.container').find('.radial-pb-x').val());
			var pby = parseInt(dada.parents('.container').find('.radial-pb-y').val());
			if (!isNaN(pbx) && !isNaN(pby)) {
			    var dx = x-pbx;
			    var dy = y-pby;
			    var d2 = dx*dx+dy*dy;
			    var d  = Math.round(Math.sqrt(d2), 0.1);
			    txt += '; Δx='+dx+' Δy='+dy+' √Δ²='+d;

			    /* http://jsfiddle.net/Boro/5QA8r/ */
			    try {
				var can = $('#chart1').find('.jqplot-highlight-canvas')[0];
				var ctx = can.getContext('2d');
				var rad = dada.data('radial-data');
				var idx = Math.round(d, 1);
				if (idx<0 || idx>rad[0].length) idx=0;
				ctx.clearRect(0, 0, can.width, can.height);
				if (rad[0][idx]) {
				    var rx  = dada.data('radial-plot').axes.xaxis.series_u2p(rad[0][idx][0]);
				    var ry  = dada.data('radial-plot').axes.yaxis.series_u2p(rad[0][idx][1]);
				    ctx.fillStyle = '#c00';
				    ctx.beginPath();
				    ctx.arc(rx, ry, 3, 0, 2*Math.PI, true);
				    ctx.closePath();
				    ctx.fill();
				    var txt2 = 'I(Δ='+rad[0][idx][0]+') = '+rad[0][idx][1];
				    var box = dada.parents('.container').find('.box-radial .radial-info');
				    box.text(txt2);
				}
			    } catch(err) {;}
			}
			dada.find('.dada-info').text(txt);
			zoom.css('margin-top',  -zfac*y+200);
			zoom.css('margin-left', -zfac*x+200);
		    });

		    var meta = req.getResponseHeader('X-meta');
		    if (meta) {
			meta = JSON.parse(meta);
			var meta2 = '<span>image '+imno+' loaded ('+timing+'):</span>&nbsp;';
			var meta3 = '<table>';
			meta2 += "<span>min " + meta.min+'</span>&nbsp;';
			meta2 += "<span>max " + meta.max+'</span>&nbsp;';
		        meta3 += '<tr><td>min</td><td>'+meta.min+'</td></tr>';
		        meta3 += '<tr><td>max</td><td>'+meta.max+'</td></tr>';
			if (0 & meta.dump) {
			    meta2 += "<span>ct " + meta.dump.ct+'</span>&nbsp;';
			    meta2 += "<span>att " + meta.dump.att+'</span>&nbsp;';

			    meta3 += '<tr><td>ct</td><td>'+meta.dump.ct+'</td></tr>';
			    meta3 += '<tr><td>att</td><td>'+meta.dump.att+'</td></tr>';

			    $.each(['podx', 'pody', 'podz'], function(i,m) {
				if (meta.dump[m])
				    meta3 += '<tr><td>'+m+'</td><td>'+meta.dump[m]+'</td></tr>';
			    });
			}
			meta3 += '</table>';
			var host = req.getResponseHeader('X-dada-host');
			if (host) meta2 += 'host: '+host;
			dada.find('.dada-text').html(meta2);
			dada.parents('.container').find('.box-meta-data').html(meta3);
		    }
		    var radial = req.getResponseHeader('X-radial');
		    var container = dada.parents('.container').find('.box-radial span');
		    container.find('.radial-intensity-1').empty();
		    container.find('.radial-intensity-2').empty();
		    container.find('.radial-intensity-3').empty();
		    container.find('.radial-intensity-4').empty();
		    if (radial) {
			radial = JSON.parse(radial);

			/*
			if (radial.radial && radial.radial.intensity) {
			    var intensity = radial.radial.intensity;
			    container.find('.radial-intensity-1').text(intensity[0]);
			    container.find('.radial-intensity-2').text(intensity[1]);
			    container.find('.radial-intensity-3').text(intensity[2]);
			    container.find('.radial-intensity-4').text(intensity[3]);
			}
			if (radial.radial && radial.radial.pca) {
			    var pca = radial.radial.pca;
			    container.find('.radial-pca-phi1').text(pca.phi[0]);
			    container.find('.radial-pca-lam1').text(pca.lambda[0]);
			    container.find('.radial-pca-lam2').text(pca.lambda[1]);
			    container.find('.radial-pca-omeg').text(pca.omega);
			}
			*/

			if (radial.radial) {
			    var rad  = [radial.radial];

			    var xmin = container.find('.radial-radius-1').val();
			    var xmax = container.find('.radial-radius-3').val();
			    var ymin = 0.01;
			    var ymax = null;
			    ymin = Math.min.apply(Math, rad[0].map(function(o) { return o[1] <=0 ? +Infinity : o[1]; }));
			    ymax = Math.max.apply(Math, rad[0].map(function(o) { return o[1] <=0 ? -Infinity : o[1]; }));
			    if (ymin <= 0) ymin = 0.001;
			    if (!isFinite(ymin)) ymin = 0.001
			    if (ymax <= 0) ymax = 1;
			    if (!isFinite(ymax)) ymax = 1;

			    myFormat = function (format, val) {
				if (Math.abs(val) < 1e-9) return '0';
				var decade = Math.floor(Math.log10(Math.abs(val)));
				var prefac = val.toExponential(0);
				prefac = (''+prefac).substr(0,1);
				decade = decade.toFixed(0);
				var expo = '×10';
				//console.log(decade);
				decade.split(' ').forEach(function(c,i) {
				    expo += String.fromCharCode(8304+(c-'0'));
				    //console.log([c, 8304+(c-'0'), String.fromCharCode(8304+(c-'0'))]);
				});
				decade = 0x2070 + decade;
				return prefac + expo;
			    }

			    var opts = {
				seriesDefaults: { shadow: false, markerOptions: { shadow: false, size: 1 } },
				axes: {
				    /*xaxis: { min: xmin, max: xmax, tickOptions: {formatString:'%d'} },*/
				    xaxis: {
					min: xmin, max: xmax,
					 tickOptions: { formatString: '%d'},
					 tickRenderer:$.jqplot.CanvasAxisTickRenderer,
					 renderer: $.jqplot.LogAxisRenderer
				     },
				    yaxis: {
					min: ymin, max: ymax,
					 tickOptions: { formatString: '%.0e', angle: 0 /*, formatter: myFormat*/ },
					 tickRenderer:$.jqplot.CanvasAxisTickRenderer,
					 renderer: $.jqplot.LogAxisRenderer
				     }
				}
			    };
			    $('#chart1').empty();
			    var plot1 = $.jqplot('chart1', rad, opts);
			    dada.data('radial-plot', plot1);
			    dada.data('radial-data', rad);
			}
		    }
		    req = null;
		    loaded = true;

		    var loading = dada.find('.dada-navi-imno').data('loading')-1;
		    dada.find('.dada-navi-imno').data('loading', loading);
		    if (loading < 1) dada.find('.dada-navi-imno').removeClass('busy');
		};
		img.src = url;
		break;

	    default:
		dada.find('.dada-text').html('error loading image<br>\n');
		dada.find('.dada-navi-stxm-show').removeClass('busy');
		dada.find('.dada-navi-stxm-show').removeClass('canceled');
		// dada.find('.dada-text').html('error loading '+url+'<br>\n');
		// dada.find('.dada-bild').html('<img src="gfx/p300-halter.jpg" width="487" height="691">');
		req = null;
		var loading = dada.find('.dada-navi-imno').data('loading')-1;
		dada.find('.dada-navi-imno').data('loading', loading);
		if (loading < 1) dada.find('.dada-navi-imno').removeClass('busy');
		break;
	}
    };
    req.send(null);
}

function dada_show_multi_image(dada, first, horz, vert, progFirst) {
    first = parseInt(first);
    horz  = parseInt(horz );
    vert  = parseInt(vert );

    var base = dada.data('base');
    if (base == null) return;

    var progressive = dada.find('.dada-navi-multi-progressive').hasClass('aktiv') ? true : false;
    if (progFirst && progressive) vert = 1;

    dada.data('multi', {'first': first, 'horz': horz, 'vert': vert});
    dada.find('.dada-navi-multi-first').val(first);
    var url = '../composite/' + base + first + '?horz='+horz+'&vert='+vert;
    var mod = '&';

    var pp  = dada.parents('.container').find('.box-preprocessing');
    var roi = {x: parseInt($(pp).find('.pp-roi-x').val()),
	    y: parseInt($(pp).find('.pp-roi-y').val()),
	    w: parseInt($(pp).find('.pp-roi-w').val()),
	    h: parseInt($(pp).find('.pp-roi-h').val())};
    var bin = {x: parseInt($(pp).find('.pp-bin-x').val()),
	    y: parseInt($(pp).find('.pp-bin-y').val())};
    if (!isNaN(roi.x) && !isNaN(roi.y) && !isNaN(roi.w) && !isNaN(roi.h))
	    mod += '&roi='+roi.x+','+roi.y+','+roi.w+','+roi.h;
    mod += '&binning='+bin.x+','+bin.y;

    var fft2d = dada.parents('.container').find('.proc-fft2d-enable');
    if (fft2d.is(':checked'))
	mod += '&fft2d=true';
    else
	mod += '&fft2d=false';

    var palette = dada.parents('.container').find('img.colour-palette-current').data('palette');
    mod += '&palette='+palette;
    var scale = dada.parents('.container').find('.colour-scale-log').hasClass('aktiv') ? 'log' : 'lin';
    mod += '&scale='+scale;
    var cmin  = dada.parents('.container').find('.colour-min').val();
    mod += '&cmin='+cmin;
    var cmax  = dada.parents('.container').find('.colour-max').val();
    mod += '&cmax='+cmax;
    cmin = dada.parents('.container').find('.colour-clip-min').hasClass('aktiv') ? 'min' : '';
    cmax = dada.parents('.container').find('.colour-clip-max').hasClass('aktiv') ? 'max' : '';
    mod += '&clipping='+cmin+','+cmax;

    url = url + mod;

    // dada.find('.dada-text').html('requesting '+url+' ...<br>\n');

    var req = dada.data('req');
    if (req != null)
	return;

    req = new XMLHttpRequest();
    req.open("GET", url, true);
    req.responseType = 'blob';

    req.onreadystatechange = function() {
	if (req.readyState != 4) return;
	switch (req.status) {
	    case 200:
		var timing = req.getResponseHeader('X-timing');
		// dada.find('.dada-text').html('loaded '+url+' ('+timing+')<br>\n');
		var img = new Image();
		img.onload = function() {
		    dada.find('.dada-bild').empty().append(this);
		    dada.find('.dada-bild img').removeClass('img-stxm');
		    var meta = req.getResponseHeader('X-meta');
		    if (meta) {
		    //console.log(meta);
			meta = JSON.parse(meta);
			var meta2 = '<span>image '+first+'('+horz+'×'+vert+') loaded ('+timing+'):</span>&nbsp;';
			meta2 += "<span>min " + meta.min+'</span>&nbsp;';
			meta2 += "<span>max " + meta.max+'</span>&nbsp;';
			if (0 && meta.dump) {
			    meta2 += "<span>ct " + meta.dump.ct+'</span>&nbsp;';
			    meta2 += "<span>att " + meta.dump.att+'</span>&nbsp;';
			}
			dada.find('.dada-text').html(meta2);
		    }
		    req = null;
		    loaded = true;

		    /* increase vertical size progressively */
		    var max = parseInt(dada.find('.dada-navi-multi-vert>input').val());
		    vert++;
		    var canceled = dada.find('.dada-navi-multi-show').hasClass('canceled');
		    if (!canceled && progressive && vert<=max) {
			dada_show_multi_image(dada, first, horz, vert, false);
		    }
		    else {
			dada.find('.dada-navi-multi-show').removeClass('busy');
			dada.find('.dada-navi-multi-show').removeClass('canceled');
		    }
		};
		img.src = url;
		break;

	    default:
		dada.find('.dada-text').html('error loading composite image<br>\n');
		dada.find('.dada-navi-stxm-show').removeClass('busy');
		dada.find('.dada-navi-stxm-show').removeClass('canceled');
		// dada.find('.dada-text').html('error loading '+url+'<br>\n');
		// dada.find('.dada-bild').html('<img src="gfx/p300-halter.jpg" width="487" height="691">');
		req = null;
		break;
	}
    };
    req.send(null);
}

function dada_show_stxm_image(dada, first, horz, vert, progFirst, paravert) {
    first    = parseInt(first);
    horz     = parseInt(horz );
    vert     = parseInt(vert );
    paravert = parseInt(paravert );

    var para = 4*4; /* number of heinzelmännchen times cores */

    var base = dada.data('base');
    if (base == null) return;

    var progressive = dada.find('.dada-navi-stxm-progressive').hasClass('aktiv') ? true : false;
    if (progFirst && progressive) vert = 1;

    var parallel = dada.find('.dada-navi-stxm-parallel').hasClass('aktiv') ? true : false;

    var signal = dada.find('.dada-navi-stxm-signal').val();

    dada.data('stxm', {'first': first, 'horz': horz, 'vert': vert});
    dada.find('.dada-navi-stxm-first').val(first);

    var urls = [];

    if (parallel)
    {
	vert = Math.ceil(paravert / para);
	// console.log('parastxm: ' + [para, vert, paravert]);
	var i = 0;
	for (f=first; f<first+paravert*horz; f += horz*vert)
	{
	    urls[i] = '../stxm/' + base + f + '?horz='+horz+'&vert='+vert+'&signal='+signal;
	    // console.log([i, f]);
	    i++;
	}
    }
    else
    {
	urls[0] = '../stxm/' + base + first + '?horz='+horz+'&vert='+vert+'&signal='+signal;
	//console.log([0, urls[0]]);
    }

    var mod = '&';

    var pp  = dada.parents('.container').find('.box-preprocessing');
    var roi = {x: parseInt($(pp).find('.pp-roi-x').val()),
	    y: parseInt($(pp).find('.pp-roi-y').val()),
	    w: parseInt($(pp).find('.pp-roi-w').val()),
	    h: parseInt($(pp).find('.pp-roi-h').val())};
    var bin = {x: parseInt($(pp).find('.pp-bin-x').val()),
	    y: parseInt($(pp).find('.pp-bin-y').val())};
    if (!isNaN(roi.x) && !isNaN(roi.y) && !isNaN(roi.w) && !isNaN(roi.h))
	    mod += '&roi='+roi.x+','+roi.y+','+roi.w+','+roi.h;
    mod += '&binning='+bin.x+','+bin.y;

    var fft2d = dada.parents('.container').find('.proc-fft2d-enable');
    if (fft2d.is(':checked'))
	mod += '&fft2d=true';
    else
	mod += '&fft2d=false';

    var palette = dada.parents('.container').find('img.colour-palette-current').data('palette');
    mod += '&palette='+palette;
    var scale = dada.parents('.container').find('.colour-scale-log').hasClass('aktiv') ? 'log' : 'lin';
    mod += '&scale='+scale;
    var cmin  = dada.parents('.container').find('.colour-min').val();
    mod += '&cmin='+cmin;
    var cmax  = dada.parents('.container').find('.colour-max').val();
    mod += '&cmax='+cmax;
    cmin = dada.parents('.container').find('.colour-clip-min').hasClass('aktiv') ? 'min' : '';
    cmax = dada.parents('.container').find('.colour-clip-max').hasClass('aktiv') ? 'max' : '';
    mod += '&clipping='+cmin+','+cmax;

    var radial = dada.parents('.container').find('.box-radial');
    var rad = {x: parseInt($(radial).find('.radial-pb-x').val()),
               y: parseInt($(radial).find('.radial-pb-y').val()),
               r: [ parseInt($(radial).find('.radial-radius-1').val()),
		   parseInt($(radial).find('.radial-radius-2').val()),
		   parseInt($(radial).find('.radial-radius-3').val()) ] };
    if (!isNaN(rad.x) && !isNaN(rad.y))
	mod += '&radial.pb.x='+rad.x+'&radial.pb.y='+rad.y+'&radial.r1='+rad.r[0]+'&radial.r2='+rad.r[1]+'&radial.r3='+rad.r[2];

    dada.find('.dada-bild').empty();
    urls.forEach(function(url, i) {
	// console.log([i, url]);
	url = url + mod;
	var img = new Image();
	$(img).css('display', 'block');
	$(img).data('url', url);
	$(img).mousemove(function(event) {
	    var of  = $(this).offset();
	    var x   = parseInt(event.pageX) - parseInt(of.left)+1;
	    var y   = parseInt(event.pageY) - parseInt(of.top);

	    var W   = $(this).width();
	    var H   = $(this).height();
	    var w   = x*horz/W;
	    var h   = y*vert/H;
	    x = parseInt(w+1);
	    y = parseInt(h+1) + i*vert;
	    var n   = y*horz+x;
	    n = parseInt((y-1)*horz+(x-1))+first;

	    var txt = 'x='+x+' y='+y+' n='+n;
	    dada.find('.dada-info').text(txt);
	});
	dada.find('.dada-bild').append(img);
    });
    dada.find('.dada-bild img').addClass('img-stxm');

    dada.find('.dada-bild img').each(function(i, img) {
	var url = $(img).data('url');
	// console.log([i, url]);

	// dada.find('.dada-text').html('requesting '+url+' ...<br>\n');

	var req = dada.data('req');
	if (req != null)
	    return;

	req = new XMLHttpRequest();
	req.open("GET", url, true);
	req.responseType = 'blob';

	req.onreadystatechange = function() {
	    if (req.readyState != 4) return;
	    switch (req.status) {
		case 200:
		    var timing = req.getResponseHeader('X-timing');
		    // dada.find('.dada-text').html('loaded '+url+' ('+timing+')<br>\n');
		    // var img = new Image();
		    img.onload = function() {
			// $(this).css('display', 'block');
			// // dada.find('.dada-bild').empty().append(this);
			// dada.find('.dada-bild').append(this);
			// dada.find('.dada-bild img').addClass('img-stxm');
			/*
			dada.find('.dada-bild img').mousemove(function(event) {
			    var of  = $(this).offset();
			    var x   = parseInt(event.pageX) - parseInt(of.left)+1;
			    var y   = parseInt(event.pageY) - parseInt(of.top);

			    var W   = $(this).width();
			    var H   = $(this).height();
			    var w   = x*horz/W;
			    var h   = y*vert/H;
			    x = parseInt(w+1);
			    y = parseInt(h+1);
			    var n   = y*horz+x;
			    n = parseInt((y-1)*horz+(x-1))+first;

			    var txt = 'x='+x+' y='+y+' n='+n;
			    dada.find('.dada-info').text(txt);
			});
			*/
			var meta = req.getResponseHeader('X-meta');
			if (meta) {
			    //console.log(meta);
			    try {
				meta = JSON.parse(meta);
				var meta2 = '<span>image '+first+'('+horz+'×'+vert+') loaded ('+timing+'):</span>&nbsp;';
				meta2 += "<span>min " + meta.min+'</span>&nbsp;';
				meta2 += "<span>max " + meta.max+'</span>&nbsp;';
				if (meta.dump) {
				    meta2 += "<span>ct " + meta.dump.ct+'</span>&nbsp;';
				    meta2 += "<span>att " + meta.dump.att+'</span>&nbsp;';
				}
				dada.find('.dada-text').html(meta2);
			    } catch(err) {
				dada.find('.dada-text').text('cannot parse X-meta');
			    }
			}
			req = null;
			loaded = true;

			/* increase vertical size progressively */
			var max = parseInt(dada.find('.dada-navi-stxm-vert>input').val());
			var canceled = dada.find('.dada-navi-stxm-show').hasClass('canceled');
			if (!canceled && progressive && (vert+1)<=max) {
			    vert++;
			    dada_show_stxm_image(dada, first, horz, vert, false, paravert);
			}
			else {
			    dada.find('.dada-navi-stxm-show').removeClass('busy');
			    dada.find('.dada-navi-stxm-show').removeClass('canceled');
			}
		    };
		    img.src = url;
		    break;

		default:
		    dada.find('.dada-text').html('error loading stxm image<br>\n');
		    dada.find('.dada-navi-stxm-show').removeClass('busy');
		    dada.find('.dada-navi-stxm-show').removeClass('canceled');
		    // dada.find('.dada-text').html('error loading '+url+'<br>\n');
		    // dada.find('.dada-bild').html('<img src="gfx/p300-halter.jpg" width="487" height="691">');
		    req = null;
		    break;
	    }
	};
	req.send(null);
    });
}

