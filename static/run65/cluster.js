function cluster_nodeinfo() {
    $('#top tbody tr').each(function(idx, tr) {
	var host = $(tr).data('host');
	var  url = '../../nodeinfo/'+host;
	    $.get(url).done(function(node) {
		var  cls = 'idle';
		if (node.cpu >   1) cls = 'act0';
		if (node.cpu >  20) cls = 'act1';
		if (node.cpu >  60) cls = 'act2';
		if (node.cpu > 110) cls = 'act3';

		var td1 = $('<td>'+node.host+':'+node.port+', '+node.pid+'</td>');
		var td2 = $('<td class='+cls+'>'+node.cpu+'</td>');
		var td3 = $('<td>'+node.mem+'</td>');
		var td4 = $('<td>'+node.last.stxm+'</td>');

		$(tr).html([td1,td2,td3,td4]);
	    });
    });
    window.setTimeout(function() { cluster_nodeinfo(); }, 2000);
}

$(document).ready(function() {
    cluster_nodeinfo();
});

