#ifndef _DADA_H_
#define _DADA_H_

#define _GNU_SOURCE

#ifndef GIT_VERSION
/* see http://stackoverflow.com/questions/1704907/ */
#define GIT_VERSION "unknown"
#endif

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <string.h>
//#include <complex.h>
#include <math.h>
#include <fftw3.h>
#include <ctype.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <dirent.h>
#include <sys/mman.h>
#include <stdarg.h>
#include <dlfcn.h>
#include <microhttpd.h>
#include <signal.h>
#include <sys/prctl.h>
#include <sys/utsname.h>
#include <sys/sysinfo.h>
#include <grp.h>
#include <curl/curl.h>
#include <openssl/ssl.h>
#include <openssl/sha.h>
//#include <arpa/inet.h>
//#include <sys/socket.h>
#include <netdb.h>
#include <ifaddrs.h>
//#include <linux/if_link.h>
#include <png.h>
#include <tiffio.h>

#include "dada_ring.h"
#include "dada_helper.h" // contains _pointer.h _dl.h _memstream.h

#include "dada_data.h"
#include "output/ascii.h"

void dada_signal_handler(int sig);
extern int dada_running;
extern char top_buf[256];
extern struct dada_cluster_node node;

struct MHD_Daemon* mhd_daemon;
int webserver_init(int port);
int webserver_destroy();
int webserver_connection(void* cls, struct MHD_Connection* conn, const char* url, const char* method, const char* version, const char* data, size_t* len, void** con_cls);
void webserver_completed(void* cls, struct MHD_Connection* conn, void** con_cls, enum MHD_RequestTerminationCode code);

struct dada_cluster_last {
	int stxm;
};
struct dada_cluster_node {
	char host[64];
	int port;
	int pid;
	char version[16];
	float cpu;
	float mem;
	struct dada_cluster_last last;
	struct dada_cluster_node* next;
};

struct dada_web_post_snap {
	struct MHD_PostProcessor* poster;
	int code;
	char* message;
	char* fname;
	int fd;
};

struct dada_web_post_glue {
	struct MHD_PostProcessor* poster;
	int code;
	FILE* mem;
	char* msg;
	size_t memlen;
	char* fname;
	int fd;
	int XX;
	int YY;
	char** keys;
	struct dada_data* data;
};


struct dada_web_get_stxm {
	void* so_handle;
	struct dada_data_path* path;
	struct dada_data_modi* modi;
	struct dada_data_form* form;
	pthread_t thr;
};
struct dada_web_post_post {
	const char* data;
	size_t* len;
	struct dada_web_post_snap* snap;
	struct dada_web_post_glue* glue;
	void* so_handle;
	void* so_funcp;
};

struct connection_meta {
	struct MHD_Connection* conn;
	long unsigned int connid;
	struct timespec t0;
	struct timespec t1;
	double seconds;
	char* msg;
	char* canonical;
	FILE* mem;
	size_t memlen;
	struct dada_web_aufraeumer* raeum;
	char* xmeta;
	char* xradial;
};

enum dada_web_aufraeumer_type {
	DADA_WEB_AUFRAEUMER_STXM,
	DADA_WEB_AUFRAEUMER_POST
};
struct dada_web_aufraeumer {
	enum dada_web_aufraeumer_type type;
	struct dada_web_get_stxm*     stxm;
	struct dada_web_post_post*    post;
};

struct mem {
	unsigned char* buffer;
	ssize_t size;
};

int startup(const char* argv0);
int showlink(int port);
int top_loop(int port);

pthread_mutex_t hdf5_mutex;

/* check file name against blacklist: ..
 * check file name against whitelist:
 * a-zA-Z0-9 (+ locale specific)
 * .,-, _,
 */
#define CHECK_FILE_NAME(str) \
do { \
    int i, len; \
    if (strstr(str, "..") != NULL) \
	goto forbidden; \
    len = strlen(str); \
    for (i=0; i<len; i++) { \
	unsigned char c = str[i]; \
	int  ok = 0; \
	if (isalnum(c)) ok++; \
	if (c == '.')   ok++; \
	if (c == '_')   ok++; \
	if (c == '-')   ok++; \
	if (c == '/')   ok++; \
	if (ok == 0) goto forbidden; \
    } \
} while (0)

#endif

