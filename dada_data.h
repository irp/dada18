#ifndef _DADA_DATA_H_
#define _DADA_DATA_H_

enum dada_data_type {
	DADA_DATA_VOID,
	DADA_DATA_INT,
	DADA_DATA_DOUBLE,
	DADA_DATA_DATA
};
union dada_data_buffer {
	void* p;
	int* i;
	double* f;
	struct dada_data* d;
};

struct dada_data {
	enum dada_data_type type;
	union dada_data_buffer buffer;
	int D;  // number of dimensions
	int* d; // actual values of dimensions
	double progress; // progress report for already running job
};

struct dada_data_path {
	int len;
	char** path;
};
struct dada_data_modi {
	int len;
	char** key;
	char** val;
};
struct dada_data_form {
	int len;
	char** key;
	char** val;
};

enum dada_process_roi_flag {
	DADA_PROCESS_ROI_NORMAL,
	DADA_PROCESS_ROI_MASK
};
struct dada_process_roi {
	int left;
	int top;
	int width;
	int height;
	enum dada_process_roi_flag flags;
};

enum dada_stack_oper {
	DADA_STACK_OPER_NONE,
	DADA_STACK_OPER_SUM,
	DADA_STACK_OPER_AVG,
	DADA_STACK_OPER_MIN,
	DADA_STACK_OPER_MAX
};
struct dada_process_stack {
	enum dada_stack_oper oper;
	int first;
	int num;
};

enum dada_empty_oper {
	DADA_STACK_EMPTY_NONE,
	DADA_STACK_EMPTY_DIVIDE
};
struct dada_process_empty {
	enum dada_empty_oper oper;
	int first;
	int num;
};

enum dada_fourier_oper {
	DADA_FOURIER_NONE,
	DADA_FOURIER_LOWPASS,
	DADA_FOURIER_LOWPASS2, /* lowpass + mean */
	DADA_FOURIER_HIGHPASS
};
struct dada_process_fourier {
	enum dada_fourier_oper oper;
	double param1;
};

enum dada_normalise {
	DADA_NORMALISE_NONE,
	DADA_NORMALISE_MEAN
};
struct dada_process_normalise {
	enum dada_normalise norm;
};

enum dada_subtract_oper {
	DADA_STACK_SUBTRACT_NONE,
	DADA_STACK_SUBTRACT_SUBTRACT
};
struct dada_process_subtract {
	enum dada_subtract_oper oper;
	int first;
	int num;
};

struct dada_process_mask {
	char* filename;
};

enum dada_multi_layout {
	DADA_MULTI_LAYOUT_2D,
	DADA_MULTI_LAYOUT_4D
};
struct dada_process_multi {
	int horz;
	int vert;
	enum dada_multi_layout layout;
};

enum dada_data_form_type {
	DADA_DATA_FORM_ASCII,
	DADA_DATA_FORM_JSON,
	DADA_DATA_FORM_PNG,
	DADA_DATA_FORM_TIFF_RAW,
	DADA_DATA_FORM_DOUBLE,
	DADA_DATA_FORM_PLOT,
};

enum dada_cache_flags {
	DADA_CACHE_NONE  = 0x00,
	DADA_CACHE_READ  = 0x01,
	DADA_CACHE_WRITE = 0x02
};

/* for caching web responses */
struct dada_response {
	char* type;
	int64_t len;
	void* data;
	char* xmeta;
	char* xradial;
};

struct dada_radial {
	float cen_x;
	float cen_y;
	int   min_r;
	int   max_r;
	int   bin_r;
};

struct dada_polar {
	float cen_x;
	float cen_y;
	int   min_q;
	int   max_q;
	int   min_phi;
	int   max_phi;
	int   num_phi;
};

struct dada_xcca {
	int   min_del;
	int   max_del;
};

struct dada_polarroi {
	float cen_x;
	float cen_y;
	float min_r;
	float max_r;
	float min_phi;
	float max_phi;
};

struct dada_data* dada_data_init(enum dada_data_type type, int dimensions, ...);
int dada_data_destroy(struct dada_data* data);

int dada_data_path_destroy(struct dada_data_path** path);
int dada_data_modi_destroy(struct dada_data_modi** modi);
int dada_data_form_destroy(struct dada_data_form** form);

struct dada_data_path* dada_data_path_extract(const char* url);
struct dada_data_modi* dada_data_modi_extract(struct MHD_Connection* conn);
int dada_data_modi_extract_callback(void* cls, enum MHD_ValueKind kind, const char* key, const char* value);
struct dada_data_form* dada_data_form_extract(struct MHD_Connection* conn);
int dada_data_form_extract_callback(void* cls, enum MHD_ValueKind kind, const char* key, const char* value);

int dada_process_parse_roi(struct dada_data_modi* modi, struct dada_process_roi* roi);
int dada_process_roi(struct dada_data* data, struct dada_process_roi* roi);

int dada_process_parse_bin(struct dada_data_modi* modi, int* binx, int* biny);
int dada_process_bin(struct dada_data* data, int binx, int biny);

int dada_process_parse_stack(struct dada_data_modi* modi, struct dada_process_stack* stack);
struct dada_data* dada_process_stack_init(struct dada_data* data, struct dada_data* dat2);
struct dada_data* dada_process_stack(struct dada_data* data, struct dada_process_stack* stack, struct dada_data* dat2);

int dada_process_parse_empty(struct dada_data_modi* modi, struct dada_process_empty* empty);
struct dada_data* dada_process_empty(struct dada_data* data, struct dada_data* divide);

int dada_process_parse_fourier(struct dada_data_modi* modi, struct dada_process_fourier* fourier);

int dada_process_parse_normalise(struct dada_data_modi* modi, struct dada_process_normalise* normalise);

int dada_process_parse_subtract(struct dada_data_modi* modi, struct dada_process_subtract* subtract);
struct dada_data* dada_process_subtract(struct dada_data* data, struct dada_data* divide);

int dada_process_parse_mask(struct dada_data_modi* modi, struct dada_process_mask* mask);
int dada_process_mask(struct dada_data* data, struct dada_data_modi* modi, struct dada_process_mask* mask);

int dada_process_parse_multi(struct dada_data_modi* modi, struct dada_process_multi* multi);

#endif

