all: dada libs input output algo

GIT := -DGIT_VERSION=\"$(shell git describe --abbrev=6 --dirty --always --tags)\"

#dada: dada.c dada.h common.h dada_status.h ringbuffer.o image.h so_detector.h
dada: dada.c dada.h dada_aero.h dada_helper.h dada_data.o dada_ring.o dada_cluster.o
	@#gcc -o $@ $< $(GIT) -Wall -Wextra -g -ggdb -O2 -rdynamic ringbuffer.o -lm -ldl -lpthread -lmicrohttpd
	gcc -o $@ $< $(GIT) dada_data.o -Wall -Wextra -g -ggdb -O2 -rdynamic dada_ring.o -ldl -lpthread -lmicrohttpd -DAS_USE_LIBEVENT -laerospike -levent_core -levent_pthreads -lssl -lcrypto -lpthread -lrt -ldl -lm -lfftw3 -lfftw3_threads -lz -lcurl -Iinput/inc -Linput/lib dada_cluster.o

dada_helper.h: dada_pointer.h dada_dl.h dada_memstream.h
	cat $^ > $@

dada_ring.o: dada_ring.c dada_ring.h dada.h dada_helper.h
	gcc -o $@ $< -c $(GIT) -Wall -Wextra -g -ggdb -O2
dada_data.o: dada_data.c dada_data.h dada.h dada_helper.h
	gcc -o $@ $< -c $(GIT) -Wall -Wextra -g -ggdb -O2


libs: dada_web_get.so dada_web_post.so #chk_open.so
dada_web_get.so: dada_web_get.c dada_web_get.h dada.h dada_helper.h dada_cluster.o #image.h common.h dada_status.h
	@#gcc -o $@ $< $(GIT) -Wall -Wextra -g -ggdb -O2 -shared -fPIC
	gcc -o $@ $< $(GIT) -Wall -Wextra -g -ggdb -O2 -shared -fPIC -DAS_USE_LIBEVENT -laerospike -levent_core -levent_pthreads -lssl -lcrypto -lpthread -lrt -ldl -lm -lz -Iinput/inc -Linput/lib dada_cluster.o
dada_web_post.so: dada_web_post.c dada_web_post.h dada.h dada_helper.h dada_cluster.o #image.h common.h dada_status.h
	gcc -o $@ $< $(GIT) -Wall -Wextra -g -ggdb -O2 -shared -fPIC -DAS_USE_LIBEVENT -laerospike -levent_core -levent_pthreads -lssl -lcrypto -lpthread -lrt -ldl -lm -lz -Iinput/inc -Linput/lib dada_cluster.o

dada_cluster.o: dada_cluster.c dada_cluster.h dada.h dada_helper.h
	gcc -o $@ $< -c $(GIT) -Wall -Wextra -g -ggdb -O0 -shared -fPIC -DAS_USE_LIBEVENT -Iinput/inc

chk_open.so: chk_open.c
	gcc -o $@ $< $(GIT) -Wall -Wextra -g -ggdb -O2 -shared -fPIC -ldl


.PHONY: input algo output
input:
	cd input; $(MAKE) $(MFLAGS)
algo:
	cd algo; $(MAKE) $(MFLAGS)
output:
	cd output; $(MAKE) $(MFLAGS)

.PHONY: tags
tags:
	ctags -R . --c-kinds=+defglmpstuv --fields=+nSt --languages=C

