#include "dada.h"
#include "dada_data.h"

struct dada_data* dada_data_init(enum dada_data_type type, int dimensions, ...)
{
	struct dada_data* data = NULL;
	size_t len = 0;
	int d;
	va_list ap;

	data = (struct dada_data*) malloc(sizeof(struct dada_data));
	data->type   = type;
	data->D      = dimensions;
	data->d      = (int*) malloc(data->D*sizeof(int));

	data->progress = -1.0; /* default */

	va_start(ap, dimensions);
	len = 1;
	for (d=0; d<data->D; d++)
	{
		data->d[d] = va_arg(ap, int);
		len *= data->d[d];
	}
	va_end(ap);

	switch (type)
	{
		case DADA_DATA_INT:
			data->buffer.i = (int*)              malloc(len*sizeof(int));
			memset(data->buffer.i, 0, len*sizeof(int));
			break;
		case DADA_DATA_DOUBLE:
			data->buffer.f = (double*)           malloc(len*sizeof(double));
			memset(data->buffer.f, 0, len*sizeof(double));
			break;
		case DADA_DATA_DATA:
			data->buffer.d = (struct dada_data*) malloc(len*sizeof(struct dada_data));
			memset(data->buffer.d, 0, len*sizeof(struct dada_data));
			break;
		case DADA_DATA_VOID:
			data->buffer.p = NULL;
	}
	
	return data;
}

int dada_data_destroy(struct dada_data* data)
{
	if (data == NULL) goto cleanup;

	if (data->buffer.p != NULL) 
	{
		free(data->buffer.p);
		data->buffer.p = NULL;
	}
	if (data->d != NULL) 
	{
		free(data->d);
		data->d = NULL;
	}
	data->D = 0;

cleanup:
	return 0;
}

int dada_data_path_destroy(struct dada_data_path** path)
{
	if (!path || !*path) return -1;
	int n, N;
	struct dada_data_path* p = *path;
	N = p->len;

	for (n=0; n<N; n++)
		if (p->path[n]) {free(p->path[n]); p->path[n] = NULL;}
	if (p->path) {free(p->path); p->path=NULL;}
	if (p) {free(p); p=NULL;}

	return 0;
}
int dada_data_modi_destroy(struct dada_data_modi** modi)
{
	if (!modi || !*modi) return -1;
	int n, N;
	struct dada_data_modi* m = *modi;
	N = m->len;

	for (n=0; n<N; n++)
		if (m->key[n]) {free(m->key[n]); m->key[n] = NULL;}
	for (n=0; n<N; n++)
		if (m->val[n]) {free(m->val[n]); m->val[n] = NULL;}
	if (m->key) {free(m->key); m->key=NULL;}
	if (m->val) {free(m->val); m->val=NULL;}
	if (m) {free(m); m=NULL;}

	return 0;
}
int dada_data_form_destroy(struct dada_data_form** form)
{
	if (!form || !*form) return -1;
	int n, N;
	struct dada_data_form* m = *form;
	N = m->len;

	for (n=0; n<N; n++)
		if (m->key[n]) {free(m->key[n]); m->key[n] = NULL;}
	for (n=0; n<N; n++)
		if (m->val[n]) {free(m->val[n]); m->val[n] = NULL;}
	if (m->key) {free(m->key); m->key=NULL;}
	if (m->val) {free(m->val); m->val=NULL;}
	if (m) {free(m); m=NULL;}

	return 0;
}

struct dada_data_path* dada_data_path_extract(const char* url)
{
	struct dada_data_path* path = NULL;
	const char* q = NULL;
	const char* tok_delim = "/";
	char* tok_save = NULL;
	char temp[256];
	int n, N = 0;
	int len;

	/* extract data path; copy url to temp */
	snprintf(temp, sizeof(temp)-1, "%s", url);
	len = strlen(temp);

	if (len == 0) return NULL; /* skip if empty url */

	/* count number of entries, i.e. count(/) */
	for (n=0; n<len; n++)
		if (temp[n] == '/') N++;
	if (len>0) N++;

	path = (struct dada_data_path*) malloc(sizeof(struct dada_data_path));

	/* prepate path struct */
	path->len = N;
	path->path = (char**) malloc(N*sizeof(char*));
	for (n=0; n<N; n++) path->path[n] = NULL;

	/* now extract the N entries */
	q = strtok_r(temp, tok_delim, &tok_save);
	for (n=0; n<N && q!=NULL; n++)
	{
		path->path[n] = strdup(q);
		q = strtok_r(NULL, tok_delim, &tok_save);
	}
	
	return path;
}

struct dada_data_modi* dada_data_modi_extract(struct MHD_Connection* conn)
{
	int len = 0;
	int n;
	struct dada_data_modi* modi = NULL;

	/* count number of query_string arguments, allocate memory */
	len = MHD_get_connection_values(conn, MHD_GET_ARGUMENT_KIND, NULL, NULL);
	// fprintf(stdout, "\tlen: %d\n", len);

	modi = (struct dada_data_modi*) malloc(sizeof(struct dada_data_modi));
	modi->key = (char**) malloc(len*sizeof(char*));
	modi->val = (char**) malloc(len*sizeof(char*));
	modi->len = 0; /* will be incremented by callback */
	for (n=0; n<len; n++)
		modi->key[n] = modi->val[n] = NULL;

	/* iterate through all arguments, and let callback function strdup them */
	MHD_get_connection_values(conn, MHD_GET_ARGUMENT_KIND, dada_data_modi_extract_callback, (void*)modi);

	return modi;
}

int dada_data_modi_extract_callback(void* cls, __attribute__((unused))enum MHD_ValueKind kind, const char* key, const char* value)
{
	struct dada_data_modi* modi = (struct dada_data_modi*) cls;
	int n = modi->len;

	/* skip these keys, which are reserved for format / colour map */
	if (strcmp(key, "fromcache") == 0) goto skip;
	if (strcmp(key, "format")    == 0) goto skip;
	if (strcmp(key, "plot")      == 0) goto skip;
	if (strcmp(key, "palette")   == 0) goto skip;
	if (strcmp(key, "scale")     == 0) goto skip;
	if (strcmp(key, "cmin")      == 0) goto skip;
	if (strcmp(key, "cmax")      == 0) goto skip;
	if (strcmp(key, "clipping")  == 0) goto skip;
	if (strlen(key) == 0) goto skip;

	modi->key[n] = strdup(key);
	if (value) modi->val[n] = strdup(value);
	
	modi->len++;
	return 1;
skip:
	modi->key[n] = NULL;
	modi->val[n] = NULL;
	return 1;
}

struct dada_data_form* dada_data_form_extract(struct MHD_Connection* conn)
{
	int len = 0;
	struct dada_data_form* form = NULL;
	int n;

	/* count number of query_string arguments, allocate memory */
	len = MHD_get_connection_values(conn, MHD_GET_ARGUMENT_KIND, NULL, NULL);
	// fprintf(stdout, "\tlen: %d\n", len);

	form = (struct dada_data_form*) malloc(sizeof(struct dada_data_form));
	form->key = (char**) malloc(len*sizeof(char*));
	form->val = (char**) malloc(len*sizeof(char*));
	form->len = 0; /* will be incremented by callback */
	for (n=0; n<len; n++)
		form->key[n] = form->val[n] = NULL;

	/* iterate through all arguments, and let callback function strdup them */
	MHD_get_connection_values(conn, MHD_GET_ARGUMENT_KIND, dada_data_form_extract_callback, (void*)form);

	return form;
}

int dada_data_form_extract_callback(void* cls, __attribute__((unused))enum MHD_ValueKind kind, const char* key, const char* value)
{
	struct dada_data_form* form = (struct dada_data_form*) cls;
	int n = form->len;
	int use = 0;

	/* use only these keys, which are reserved for format / colour map */
	if (strcmp(key, "fromcache") == 0) use++;
	if (strcmp(key, "format")    == 0) use++;
	if (strcmp(key, "plot")      == 0) use++;
	if (strcmp(key, "palette")   == 0) use++;
	if (strcmp(key, "scale")     == 0) use++;
	if (strcmp(key, "cmin")      == 0) use++;
	if (strcmp(key, "cmax")      == 0) use++;
	if (strcmp(key, "clipping")  == 0) use++;

	if (use == 0) goto skip;

	form->key[n] = strdup(key);
	if (value) form->val[n] = strdup(value);
	
	form->len++;
	return 1;
skip:
	form->key[n] = NULL;
	form->val[n] = NULL;
	return 1;
}

int dada_process_parse_roi(struct dada_data_modi* modi, struct dada_process_roi* roi)
{
	int n, N = modi->len;
	int r1, r2, r3, r4;

	if (modi == NULL || roi == NULL) return -1;

	roi->left   = -1;
	roi->top    = -1;
	roi->width  = -1;
	roi->height = -1;
	roi->flags  = DADA_PROCESS_ROI_NORMAL;

	for (n=0; n<N; n++)
	{
		// DADA_RING_PRINTF("key: %s val: %s", modi->key[n], modi->val[n]);
		if (strcmp(modi->key[n], "roi")==0)
		{
			int parsed = -1;
			char tmp[16];
			memset(tmp, 0, sizeof(tmp));
			parsed = sscanf(modi->val[n], "%d,%d,%d,%d;%16s", &r1, &r2, &r3, &r4, tmp);
			// DADA_RING_PRINTF("parsed: %d, tmp: [%s]", parsed, tmp);
			switch (parsed)
			{
				case 5:
					if (strcmp(tmp, "mask")==0) roi->flags = DADA_PROCESS_ROI_MASK;
					/* FALLTHROUGH */
				case 4:
					roi->left   = r1;
					roi->top    = r2;
					roi->width  = r3;
					roi->height = r4;
				default: continue;
			}
		}
	}

	return 0;
}

int dada_process_roi(struct dada_data* data, struct dada_process_roi* roi)
{
	int x, y, X, Y;
	int l, t, w, h;

	if (data == NULL || roi == NULL) return -1;

	if (data->D == 1)
	{
		X = data->d[0];
		l = roi->left;
		w = roi->width;

		/* only set non-ROI pixels to -1 */
		if (roi->flags && DADA_PROCESS_ROI_MASK)
		{
			switch (data->type)
			{
				case DADA_DATA_INT:
					for (x=0;   x<l; x++) data->buffer.i[x] = -1;
					for (x=l+w; x<X; x++) data->buffer.i[x] = -1;
					break;
				case DADA_DATA_DOUBLE:
					for (x=0;   x<l; x++) data->buffer.f[x] = -1;
					for (x=l+w; x<X; x++) data->buffer.f[x] = -1;
					break;
				default:
					DADA_RING_PRINTF("%s not supported for type !(int|double)", __FUNCTION__);
					return -1;
			}
			return 0;
		}

		/* otherweise, really cut out ROI region */
		switch (data->type)
		{
			case DADA_DATA_INT:
				for (x=0; x<w; x++)
					data->buffer.i[x] = data->buffer.i[l+x];
				break;
			case DADA_DATA_DOUBLE:
				for (x=0; x<w; x++)
					data->buffer.f[x] = data->buffer.f[l+x];
				break;
			default:
				DADA_RING_PRINTF("%s not supported for type !(int|double)", __FUNCTION__);
				return -1;
		}

		data->d[0] = w;
	}
	if (data->D == 2)
	{
		X = data->d[1];
		Y = data->d[0];
		l = roi->left;  t = roi->top;
		w = roi->width; h = roi->height;

		/* only set non-ROI pixels to -1 */
		if (roi->flags && DADA_PROCESS_ROI_MASK)
		{
			switch (data->type)
			{
				case DADA_DATA_INT:
					for (y=0;   y<t; y++) for (x=0;   x<X; x++) data->buffer.i[y*X+x] = -1;
					for (y=t+h; y<Y; y++) for (x=0;   x<X; x++) data->buffer.i[y*X+x] = -1;
					for (y=0;   y<Y; y++) for (x=0;   x<l; x++) data->buffer.i[y*X+x] = -1;
					for (y=0;   y<Y; y++) for (x=l+w; x<X; x++) data->buffer.i[y*X+x] = -1;
					break;
				case DADA_DATA_DOUBLE:
					for (y=0;   y<t; y++) for (x=0;   x<X; x++) data->buffer.f[y*X+x] = -1;
					for (y=t+h; y<Y; y++) for (x=0;   x<X; x++) data->buffer.f[y*X+x] = -1;
					for (y=0;   y<Y; y++) for (x=0;   x<l; x++) data->buffer.f[y*X+x] = -1;
					for (y=0;   y<Y; y++) for (x=l+w; x<X; x++) data->buffer.f[y*X+x] = -1;
					break;
				default:
					DADA_RING_PRINTF("%s not supported for type !(int|double)", __FUNCTION__);
					return -1;
			}
			return 0;
		}

		/* otherweise, really cut out ROI region */
		switch (data->type)
		{
			case DADA_DATA_INT:
				for (y=0; y<h; y++)
					for (x=0; x<w; x++)
						data->buffer.i[y*w+x] = data->buffer.i[(t+y)*X+(l+x)];
				break;
			case DADA_DATA_DOUBLE:
				for (y=0; y<h; y++)
					for (x=0; x<w; x++)
						data->buffer.f[y*w+x] = data->buffer.f[(t+y)*X+(l+x)];
				break;
			default:
				DADA_RING_PRINTF("%s not supported for type !(int|double)", __FUNCTION__);
				return -1;
		}

		data->d[0] = h;
		data->d[1] = w;
	}

	return 0;
}

int dada_process_parse_bin(struct dada_data_modi* modi, int* binx, int* biny)
{
	int n, N = modi->len;
	int b1, b2;

	if (modi == NULL || binx == NULL || biny == 0) return -1;

	*binx = 1;
	*biny = 1;

	for (n=0; n<N; n++)
	{
		if (strcmp(modi->key[n], "bin")==0 || strcmp(modi->key[n], "binning")==0)
		{
			if (sscanf(modi->val[n], "%d,%d", &b1, &b2) == 2)
			{
				*binx = b1;
				*biny = b2;
			}
		}
	}

	return 0;
}

int dada_process_bin(struct dada_data* data, int binx, int biny)
{
	int x, y, xx, yy;

	if (data == NULL) return -1;

	if (data->D == 1)
	{
		switch (data->type)
		{
			case DADA_DATA_INT:
				for (x=0; x<=data->d[0]-binx; x+=binx)
				{
					int value = 0;
					for (xx=0; xx<binx; xx++)
						value += data->buffer.i[x+xx];
					data->buffer.i[x/binx] = value;
				}
				break;
			case DADA_DATA_DOUBLE:
					for (x=0; x<=data->d[1]-binx; x+=binx)
					{
						double value = 0;
						for (xx=0; xx<binx; xx++)
							value += data->buffer.f[x+xx];
						data->buffer.f[x/binx] = value;
					}
				break;
			default:
				DADA_RING_PRINTF("%s not supported for type !(int|double)", __FUNCTION__);
				return -1;
		}
		data->d[0] /= binx;
	}

	if (data->D == 2)
	{
		switch (data->type)
		{
			case DADA_DATA_INT:
				for (y=0; y<=data->d[0]-biny; y+=biny)
				{
					for (x=0; x<=data->d[1]-binx; x+=binx)
					{
						int value = 0;
						for (yy=0; yy<biny; yy++)
							for (xx=0; xx<binx; xx++)
								value += data->buffer.i[(y+yy)*data->d[1] + (x+xx)];
						data->buffer.i[(y/biny)*(data->d[1]/binx) + (x/binx)] = value;
					}
				}
				break;
			case DADA_DATA_DOUBLE:
				for (y=0; y<=data->d[0]-biny; y+=biny)
				{
					for (x=0; x<=data->d[1]-binx; x+=binx)
					{
						double value = 0;
						for (yy=0; yy<biny; yy++)
							for (xx=0; xx<binx; xx++)
								value += data->buffer.f[(y+yy)*data->d[1] + (x+xx)];
						data->buffer.f[(y/biny)*(data->d[1]/binx) + (x/binx)] = value;
					}
				}
				break;
			default:
				DADA_RING_PRINTF("%s not supported for type !(int|double)", __FUNCTION__);
				return -1;
		}
		data->d[0] /= biny;
		data->d[1] /= binx;
	}

	return 0;
}

int dada_process_parse_stack(struct dada_data_modi* modi, struct dada_process_stack* stack)
{
	int n;

	if (stack==NULL) return -1;

	stack->oper = DADA_STACK_OPER_NONE;
	stack->num  = 1;

	if (modi==NULL) return 0;

	/* parse stack operation; currently, only sum= is implemetned */
	for (n=0; n<modi->len; n++)
	{
		if (strcmp(modi->key[n], "sum")==0) /* syntax: sum=N, short for: stack=N,sum */
		{
			stack->oper = DADA_STACK_OPER_SUM;
			stack->num  = atoi(modi->val[n]);
		}
		if (strcmp(modi->key[n], "stack")==0) /* syntax: stack=N,oper */
		{
			stack->num  = atoi(modi->val[n]);
			stack->oper = DADA_STACK_OPER_SUM; /* default operation */
			if (strstr(modi->val[n], "sum")) stack->oper = DADA_STACK_OPER_SUM;
			if (strstr(modi->val[n], "avg")) stack->oper = DADA_STACK_OPER_AVG;
			if (strstr(modi->val[n], "min")) stack->oper = DADA_STACK_OPER_MIN;
			if (strstr(modi->val[n], "max")) stack->oper = DADA_STACK_OPER_MAX;
		}
	}
	if (stack->num <= 1)
	{
		stack->oper = DADA_STACK_OPER_NONE;
		stack->num  = 0;
	}

	return 0;
}

struct dada_data* dada_process_stack_init(struct dada_data* data, struct dada_data* dat2)
{
	int x, y, X, Y;
	if (dat2->D != 2) goto error;

	Y = dat2->d[0]; X = dat2->d[1];
	{
		data = dada_data_init(dat2->type, dat2->D, Y, X);
		switch (data->type)
		{
			case DADA_DATA_INT:
				for (y=0; y<Y; y++)
					for (x=0; x<X; x++)
						data->buffer.i[y*X+x] = dat2->buffer.i[y*X+x];
				break;
			case DADA_DATA_DOUBLE:
				for (y=0; y<Y; y++)
					for (x=0; x<X; x++)
						data->buffer.f[y*X+x] = dat2->buffer.f[y*X+x];
				break;
			default:
				DADA_RING_PRINTF("%s not supported for type !(int|double)", __FUNCTION__);
				goto error;
		}
	}
cleanup:
	return data;
error:
	if (data) {dada_data_destroy(data); free(data); data=NULL;}
	goto cleanup;
}

struct dada_data* dada_process_stack(struct dada_data* data, struct dada_process_stack* stack, struct dada_data* dat2)
{
	int x, y, X, Y;

	Y = data->d[0];
	X = data->d[1];

	switch (stack->oper)
	{
		case DADA_STACK_OPER_SUM:
		case DADA_STACK_OPER_AVG:
			switch (dat2->type)
			{
				case DADA_DATA_INT:    for (y=0; y<Y; y++) for (x=0; x<X; x++) data->buffer.i[y*X+x] += dat2->buffer.i[y*X+x]; break;
				case DADA_DATA_DOUBLE: for (y=0; y<Y; y++) for (x=0; x<X; x++) data->buffer.f[y*X+x] += dat2->buffer.f[y*X+x]; break;
				default: DADA_RING_PRINTF("dat2->type %d not supported.", dat2->type); goto error;
			}
			break;
		case DADA_STACK_OPER_MIN:
			switch (dat2->type)
			{
				case DADA_DATA_INT:
					for (y=0; y<Y; y++)
						for (x=0; x<X; x++)
							if (dat2->buffer.i[y*X+x] < data->buffer.i[y*X+x])
								data->buffer.i[y*X+x] = dat2->buffer.i[y*X+x];
					break;
				case DADA_DATA_DOUBLE:
					for (y=0; y<Y; y++)
						for (x=0; x<X; x++)
							if (dat2->buffer.f[y*X+x] < data->buffer.f[y*X+x])
								data->buffer.f[y*X+x] = dat2->buffer.f[y*X+x];
					break;
				default: DADA_RING_PRINTF("dat2->type %d not supported.", dat2->type); goto error;
			}
			break;
		case DADA_STACK_OPER_MAX:
			switch (dat2->type)
			{
				case DADA_DATA_INT:
					for (y=0; y<Y; y++)
						for (x=0; x<X; x++)
							if (dat2->buffer.i[y*X+x] > data->buffer.i[y*X+x])
								data->buffer.i[y*X+x] = dat2->buffer.i[y*X+x];
					break;
				case DADA_DATA_DOUBLE:
					for (y=0; y<Y; y++)
						for (x=0; x<X; x++)
							if (dat2->buffer.f[y*X+x] > data->buffer.f[y*X+x])
								data->buffer.f[y*X+x] = dat2->buffer.f[y*X+x];
					break;
				default: DADA_RING_PRINTF("dat2->type %d not supported.", dat2->type); goto error;
			}
			break;
		default: DADA_RING_PRINTF("stack->oper %d not yet implemented.", stack->oper); goto error;
	}

cleanup:
	return data;

error:
	if (data) {dada_data_destroy(data); free(data); data=NULL;}
	goto cleanup;
}

int dada_process_parse_empty(struct dada_data_modi* modi, struct dada_process_empty* empty)
{
	int n;

	if (empty==NULL) return -1;

	empty->oper  = DADA_STACK_EMPTY_NONE;
	empty->num   = 1;
	empty->first = 1;

	if (modi==NULL) return 0;

	for (n=0; n<modi->len; n++)
	{
		/* syntax: empty=N,M describing a stack of M empty images starting at N
		 * currenlty, M!=1 is being implemented, but maybe not working everywhere
		 * negative N means relative (in negative direction only), while
		 * positive N (or 0) is absolute image number
		 * */
		if (strcmp(modi->key[n], "empty")==0 && modi->val[n] && strlen(modi->val[n]) && strcmp(modi->val[n], "NaN")!=0)
		{
			int parsed;
			int v1, v2;

			parsed = sscanf(modi->val[n], "%d,%d", &v1, &v2);

			switch (parsed)
			{
				case 2: empty->num   = v2;
					/* FALLTHROUGH */
				case 1: empty->first = v1;
					empty->oper  = DADA_STACK_EMPTY_DIVIDE;
			}
		}
	}

	return 0;
}

struct dada_data* dada_process_empty(struct dada_data* data, struct dada_data* divide)
{
	struct dada_data* tmp = NULL;
	int x, y, X, Y;

	Y = data->d[0];
	X = data->d[1];

	/* convert old data.int to tmp.double, or copy data.double to tmp.double */
	tmp = dada_data_init(DADA_DATA_DOUBLE, 2, Y, X);
	switch (data->type)
	{
		case DADA_DATA_DOUBLE:
			for (y=0; y<Y; y++) for (x=0; x<X; x++) tmp->buffer.f[y*X+x] = data->buffer.f[y*X+x];
			break;
		case DADA_DATA_INT:
			for (y=0; y<Y; y++) for (x=0; x<X; x++) tmp->buffer.f[y*X+x] = data->buffer.i[y*X+x];
			break;
		default: DADA_RING_PRINTF("data->type %d not supported.", data->type); goto error;
	}

	/* make new data.double */
	if (data) {dada_data_destroy(data); free(data); data=NULL;}
	data = dada_data_init(DADA_DATA_DOUBLE, 2, Y, X);

	/* actual division: data = tmp / divide */
	switch (divide->type)
	{
		case DADA_DATA_DOUBLE:
			for (y=0; y<Y; y++)
				for (x=0; x<X; x++)
					if (divide->buffer.f[y*X+x] == tmp->buffer.f[y*X+x])
					{
							data->buffer.f[y*X+x] = 1;
					}
					else
					{
						if (divide->buffer.f[y*X+x] > 0)
							data->buffer.f[y*X+x] = tmp->buffer.f[y*X+x] / divide->buffer.f[y*X+x];
						else
							data->buffer.f[y*X+x] = -1;
					}
			break;
		case DADA_DATA_INT:
			for (y=0; y<Y; y++)
				for (x=0; x<X; x++)
					if (divide->buffer.i[y*X+x] == tmp->buffer.f[y*X+x])
					{
						data->buffer.f[y*X+x] = 1;
					}
					else
					{
						if (divide->buffer.i[y*X+x] > 0)
							data->buffer.f[y*X+x] = tmp->buffer.f[y*X+x] / divide->buffer.i[y*X+x];
						else
							data->buffer.f[y*X+x] = -1;
					}
			break;
		default: DADA_RING_PRINTF("divide->type %d not supported.", divide->type); goto error;
	}

cleanup:
	if (tmp) {dada_data_destroy(tmp); free(tmp); tmp=NULL;}
	return data;

error:
	if (data) {dada_data_destroy(data); free(data); data=NULL;}
	goto cleanup;
}

int dada_process_parse_fourier(struct dada_data_modi* modi, struct dada_process_fourier* fourier)
{
	int n;

	if (fourier==NULL) return -1;

	fourier->oper   = DADA_FOURIER_NONE;
	fourier->param1 = 1;

	if (modi==NULL) return 0;

	for (n=0; n<modi->len; n++)
	{
		/* syntax examples:
		 * fourier=lowpass:3  -> lowpass filtering with Gaussian sigma of width/3
		 * fourier=lowpass2:3 -> lowpass filter + mean
		 * fourier=highpass:3 -> highapss filtering with Gaussian sigma of width/3
		 * TODO: chain several comma separated filters
		 */
		if (strcmp(modi->key[n], "fourier")==0 && modi->val[n] && strlen(modi->val[n]))
		{
			int parsed = 0;
			char what[64];
			double val;
			parsed = sscanf(modi->val[n], "%64[^:]:%lf", what, &val);
			// DADA_RING_PRINTF("dada_process_fourier: %d %s %g", parsed, what, val);
			if (parsed == 2 && strcmp(what, "lowpass")==0)
			{
				fourier->oper   = DADA_FOURIER_LOWPASS;
				fourier->param1 = val;
			}
			if (parsed == 2 && strcmp(what, "lowpass2")==0)
			{
				fourier->oper   = DADA_FOURIER_LOWPASS2;
				fourier->param1 = val;
			}
			if (parsed == 2 && strcmp(what, "highpass")==0)
			{
				fourier->oper   = DADA_FOURIER_HIGHPASS;
				fourier->param1 = val;
			}
		}
	}

	return 0;
}

int dada_process_parse_normalise(struct dada_data_modi* modi, struct dada_process_normalise* normalise)
{
	int n;

	if (normalise==NULL) return -1;

	normalise->norm = DADA_NORMALISE_NONE;

	if (modi==NULL) return 0;

	for (n=0; n<modi->len; n++)
	{
		if (strcmp(modi->key[n], "normalise")==0 && modi->val[n] && strlen(modi->val[n]))
		{
			if (strcmp(modi->val[n], "mean")==0)
			{
				normalise->norm = DADA_NORMALISE_MEAN;
			}
		}
	}

	return 0;
}

int dada_process_parse_subtract(struct dada_data_modi* modi, struct dada_process_subtract* subtract)
{
	int n;

	if (subtract==NULL) return -1;

	subtract->oper  = DADA_STACK_SUBTRACT_NONE;
	subtract->num   = 1;
	subtract->first = 1;

	if (modi==NULL) return 0;

	for (n=0; n<modi->len; n++)
	{
		/* syntax: subtract=N,M describing a stack of M subtract images starting at N
		 * currenlty, only M=1 is implemented (since M is not supported at all)
		 * negative N means relative (in negative direction only), while
		 * positive N (or 0) is absolute image number
		 * */
		if (strcmp(modi->key[n], "subtract")==0 && modi->val[n] && strlen(modi->val[n]) && strcmp(modi->val[n], "NaN")!=0)
		{
			subtract->oper  = DADA_STACK_SUBTRACT_SUBTRACT;
			subtract->first = atoi(modi->val[n]);
		}
	}

	return 0;
}

struct dada_data* dada_process_subtract(struct dada_data* data, struct dada_data* minus)
{
	struct dada_data* tmp = NULL;
	int x, y, X, Y;

	Y = data->d[0];
	X = data->d[1];

	/* convert old data.int to tmp.double */
	tmp = dada_data_init(DADA_DATA_DOUBLE, 2, Y, X);
	for (y=0; y<Y; y++) for (x=0; x<X; x++) tmp->buffer.f[y*X+x] = data->buffer.i[y*X+x];

	/* make new data.double */
	if (data) {dada_data_destroy(data); free(data); data=NULL;}
	data = dada_data_init(DADA_DATA_DOUBLE, 2, Y, X);

	/* actual division: data = tmp - minus */
	switch (minus->type)
	{
		case DADA_DATA_DOUBLE:
			for (y=0; y<Y; y++)
				for (x=0; x<X; x++)
					if (minus->buffer.f[y*X+x] > 0)
						data->buffer.f[y*X+x] = tmp->buffer.f[y*X+x] - minus->buffer.f[y*X+x];
					else
						data->buffer.f[y*X+x] = -1;
			break;
		case DADA_DATA_INT:
			for (y=0; y<Y; y++)
				for (x=0; x<X; x++)
					if (minus->buffer.i[y*X+x] > 0)
						data->buffer.f[y*X+x] = tmp->buffer.f[y*X+x] - minus->buffer.i[y*X+x];
					else
						data->buffer.f[y*X+x] = -1;
			break;
		default: DADA_RING_PRINTF("minus->type %d not supported.", minus->type); goto error;
	}

cleanup:
	if (tmp) {dada_data_destroy(tmp); free(tmp); tmp=NULL;}
	return data;

error:
	if (data) {dada_data_destroy(data); free(data); data=NULL;}
	goto cleanup;
}

int dada_process_parse_mask(struct dada_data_modi* modi, struct dada_process_mask* mask)
{
	int n, N = modi->len;

	if (modi == NULL || mask == NULL) return -1;

	mask->filename = NULL;

	for (n=0; n<N; n++)
	{
		if (strcmp(modi->key[n], "mask")==0)
		{
			int parsed = -1;
			char tmp[64];
			memset(tmp, 0, sizeof(tmp));
			parsed = sscanf(modi->val[n], "%64s", tmp);
			DADA_RING_PRINTF("parsed: %d, tmp: [%s]", parsed, tmp);
			if (parsed == 1) mask->filename = strdup(tmp);
			else continue;
		}
	}

	return 0;
}

int dada_process_mask(struct dada_data* data, struct dada_data_modi* modi, struct dada_process_mask* mask)
{
	int ret = 0;
	int x, y, X, Y;
	struct dada_data* tmp = NULL;
	struct dada_data_path* path = NULL;

	void* so_handle = NULL;
	struct dada_data* (*so_funcp)(struct dada_data_path*,struct dada_data_modi*,struct dada_data*,enum dada_cache_flags) = NULL;

	if (data == NULL || mask == NULL)
	{
		ret = -1;
		goto cleanup;
	}

	X = data->d[1];
	Y = data->d[0];

	path = dada_data_path_extract(mask->filename);

	DADA_DL_LOAD_SYMBOL(DADA_DL_PATH_CONST("input/mask"), "dada_mask_load", so_funcp, so_handle);
	tmp = so_funcp(path, modi, tmp, DADA_CACHE_NONE);
	DADA_DL_CLOSE(so_handle);

	if (tmp == NULL)
	{
		DADA_RING_PRINTF("cannot open pixel mask %s", (mask?mask->filename:""));
		ret = -1;
		goto cleanup;
	}

	/* apply pixel mask */
	switch (data->type)
	{
		case DADA_DATA_INT:
			for (y=0; y<Y; y++)
				for (x=0; x<X; x++)
					if (tmp->buffer.f[y*X+x] <= 0)
						data->buffer.i[y*X+x]  = -1;
					else
						data->buffer.i[y*X+x] *= tmp->buffer.f[y*X+x];
			break;
		case DADA_DATA_DOUBLE:
			for (y=0; y<Y; y++)
				for (x=0; x<X; x++)
					if (tmp->buffer.f[y*X+x] <= 0)
						data->buffer.f[y*X+x]  = -1;
					else
						data->buffer.f[y*X+x] *= tmp->buffer.f[y*X+x];
			break;
		default:
			DADA_RING_PRINTF("%s not supported for type !(int|double)", __FUNCTION__);
			ret = -1;
			goto cleanup;
	}

cleanup:
	if (tmp) {dada_data_destroy(tmp); free(tmp); tmp=NULL;}
	return ret;
error:
	goto cleanup;
}

int dada_process_parse_multi(struct dada_data_modi* modi, struct dada_process_multi* multi)
{
	int n, N = modi->len;

	if (modi == NULL || multi == NULL) return -1;

	multi->horz   = 1;
	multi->vert   = 1;
	multi->layout = DADA_MULTI_LAYOUT_2D;

	for (n=0; n<N; n++)
	{
		if (strcmp(modi->key[n], "horz")==0) multi->horz = atoi(modi->val[n]);

		if (strcmp(modi->key[n], "vert")==0) multi->vert = atoi(modi->val[n]);

		if (strcmp(modi->key[n], "layout")==0)
			if (strstr(modi->val[n], "4d") || strstr(modi->val[n], "4D") || strstr(modi->val[n], "grid"))
				multi->layout = DADA_MULTI_LAYOUT_4D;
	}

	return 0;
}

