#ifndef _DADA_MEMSTREAM_H_
#define _DADA_MEMSTREAM_H_

#include "dada.h"

#define DADA_MEMSTREAM(mem, msg, memlen) \
do { \
    mem = open_memstream(&msg, &memlen); \
    DADA_CHECK_MEMSTREAM(mem); \
} while (0)

#define DADA_CHECK_MEMSTREAM(mem) \
do { \
    if (mem == NULL) { \
        DADA_RING_PRINTF("open_memstream failed: %s", strerror(errno)); \
	goto err_memstream; \
    } \
} while (0)

#endif

