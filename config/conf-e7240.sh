#!/bin/sh

set -e

rm -rf GINIX/
mkdir GINIX/

mkdir GINIX/run64
ln -s ../../../input/pilatus_generic GINIX/run64/p300
ln -s /data/GINIX/run64 GINIX/run64/data

mkdir GINIX/run66
ln -s ../../../input/pilatus_generic GINIX/run66/p300
ln -s ../../../input/eiger_p10       GINIX/run66/eiger
ln -s /data/GINIX/run66 GINIX/run66/data

