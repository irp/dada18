#!/bin/sh

set -e

rm -rf GINIX/
mkdir GINIX/

mkdir GINIX/run66
ln -s ../../../input/pilatus_generic GINIX/run66/p300
ln -s ../../../input/eiger_p10       GINIX/run66/eiger
ln -s /data/GINIX/run66 GINIX/run66/data

mkdir GINIX/run67
ln -s ../../../input/pilatus_generic GINIX/run67/p300
ln -s ../../../input/eiger_p10       GINIX/run67/eiger
ln -s /gpfs/current/raw/20171110/detectors GINIX/run67/data

