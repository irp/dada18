#ifndef _DADA_WEB_GET_H_
#define _DADA_WEB_GET_H_

#define _GNU_SOURCE

#include "dada.h"

#include "dada_cluster.h"

int dada_web_get(struct connection_meta* meta, const char* url);

int dada_web_get_redirect(struct connection_meta* meta, const char* url);
int dada_web_get_static(struct connection_meta* meta, const char* file);
int dada_web_get_snap(struct connection_meta* meta, const char* what);

int dada_web_get_list(struct connection_meta* meta, const char* what);
int dada_web_get_show(struct connection_meta* meta, const char* what);
int dada_web_get_multi(struct connection_meta* meta, const char* what);
int dada_web_get_stxm(struct connection_meta* meta, const char* what);

int dada_web_get_polar(struct connection_meta* meta, const char* what);
int dada_web_get_xcca(struct connection_meta* meta, const char* what);

int dada_web_get_glue(struct connection_meta* meta, const char* what);

int dada_web_get_radial(struct connection_meta* meta, const char* what);

int dada_web_get_mask(struct connection_meta* meta, const char* what);
int dada_web_get_mask_list(struct connection_meta* meta, const char* what);

int dada_web_error(struct connection_meta* meta, int status);

int dada_web_get_colourpalette(struct connection_meta* meta, const char* what);

int dada_web_reply(struct connection_meta* meta, struct dada_data* data, struct dada_data_form* form, int fromcache);
int dada_web_reply_png(struct connection_meta* meta, struct dada_data* data, struct dada_data_form* form, int fromcache);
int dada_web_reply_tiff_raw(struct connection_meta* meta, struct dada_data* data, struct dada_data_form* form, int fromcache);
int dada_web_reply_ascii(struct connection_meta* meta, struct dada_data* data, int fromcache);
int dada_web_reply_json (struct connection_meta* meta, struct dada_data* data, int fromcache);
int dada_web_reply_double(struct connection_meta* meta, struct dada_data* data, int fromcache);

int dada_web_reply_plot_png(struct connection_meta* meta, struct dada_data* data, struct dada_data_form* form, int fromcache);

int dada_web_see_other(struct connection_meta* meta);

time_t dada_web_listing_stamp(struct dada_data_path* path);
time_t dada_web_ask_cache(struct connection_meta* meta, const char* module, struct dada_data_path* path, struct dada_data_modi* modi, struct dada_data_form* form);
int dada_web_get_cache(struct connection_meta* meta, const char* module, struct dada_data_path* path, struct dada_data_modi* modi, struct dada_data_form* form);
int dada_web_put_cache(struct connection_meta* meta, const char* mime, size_t len, void* data);

int dada_web_get_info(struct connection_meta* meta, const char* what);
int dada_web_get_nodeinfo(struct connection_meta* meta, const char* hostport);

int dirfilter_nohidden(const struct dirent* dirent);
int versionsort_r(const struct dirent** a, const struct dirent** b);


#define CACHE_1DAY "max-age=86400,public"
#define CACHE_NONE "no-cache, no-store, must-revalidate"


#define WEB_GET_WHAT(func, what1, what2, meta, url) \
do { \
    if (strncmp(url, what1, strlen(what1)) == 0) { \
	const char* url2 = url + strlen(what2); \
	ret = func(meta, url2); \
	goto cleanup; \
    } \
} while (0)

#define WEB_MIMETYPE(filetype, mime) \
do { \
    const char* filetype = NULL; \
    filetype = strrchr(file, '.'); \
    if (filetype != NULL) { \
	if (strcmp(filetype, ".txt" )==0) mime = "text/plain; charset=UTF-8"; \
	if (strcmp(filetype, ".html")==0) mime = "text/html; charset=UTF-8"; \
	if (strcmp(filetype, ".css" )==0) mime = "text/css; charset=UTF-8"; \
	if (strcmp(filetype, ".js"  )==0) mime = "text/javascript; charset=UTF-8"; \
	if (strcmp(filetype, ".json")==0) mime = "application/json; charset=UTF-8"; \
	if (strcmp(filetype, ".jpg" )==0) mime = "image/jpeg"; \
	if (strcmp(filetype, ".jpeg")==0) mime = "image/jpeg"; \
	if (strcmp(filetype, ".png" )==0) mime = "image/png"; \
	if (strcmp(filetype, ".svg" )==0) mime = "image/svg+xml"; \
	if (strcmp(filetype, ".pdf" )==0) mime = "application/pdf"; \
    } \
} while (0)

#define WEB_STATIC_GZIP() \
do { \
    const char* AccEnc = NULL; \
    /* check for request header: accept-encoding: gzip? */ \
    AccEnc = MHD_lookup_connection_value(meta->conn, MHD_HEADER_KIND, "Accept-Encoding"); \
    if (AccEnc && strlen(AccEnc) && strstr(AccEnc, "gzip")) { \
	/* build filename.gz and test if it exists */ \
	FILE* mem = NULL; \
	size_t memlen; \
	struct stat st; \
	DADA_MEMSTREAM(mem, filegz, memlen); \
	fprintf(mem, "%s.gz", file); \
	fclose(mem); \
	/* check if we can stat, i.e. access, the gzipped file */ \
	if (stat(filegz, &st) == 0) file = filegz; \
	else FREE(filegz); \
    } \
} while (0)

#define WEBSERVER_OPEN_STAT(filename, fd, stat) \
do { \
    fd = open(filename, O_RDONLY); if (fd < 0) \
    { \
	    fprintf(meta->mem, "get static: Not Found [%s] ", filename); \
	    status = MHD_HTTP_NOT_FOUND; \
	    return dada_web_error(meta, status); \
    } \
    if (fstat(fd, &stat) != 0) { \
	    fprintf(meta->mem, "get static: Forbidden [%s] ", filename); \
	    status = MHD_HTTP_FORBIDDEN; \
	    return dada_web_error(meta, status); \
    } \
} while (0)

#define WEB_RESPONSE_STRCPY(response, str) \
do { \
    response = MHD_create_response_from_buffer(strlen(str), (void*)str, MHD_RESPMEM_MUST_COPY); \
} while (0)

#define WEBSERVER_QUEUE_MIME(meta, status, response, mime) \
do { \
    MHD_queue_response(meta->conn, status, response); \
    MHD_add_response_header(response, "Content-Type", mime); \
    WEB_FINISH_TIMING_STATUS(meta, status, response); \
} while (0)
#define WEB_FINISH_TIMING_STATUS(meta, status, response) \
do { \
    char sec[16]; \
    char cid[16]; \
    clock_gettime(CLOCK_REALTIME, &meta->t1); \
    meta->seconds = (meta->t1.tv_sec+1e-9*meta->t1.tv_nsec) - (meta->t0.tv_sec+1e-9*meta->t0.tv_nsec); \
    snprintf(sec, sizeof(sec)-1, "%.3fms", 1e3*meta->seconds); \
    snprintf(cid, sizeof(cid)-1, "%08lx", meta->connid); \
    MHD_add_response_header(response, "X-Timing", sec); \
    MHD_add_response_header(response, "X-connid", cid); \
    /*MHD_add_response_header(response, "Connection", "close"); */ \
    fprintf(meta->mem, "(%d)", status); \
    fprintf(meta->mem, " (%.3f ms)\n", 1e3*meta->seconds); \
    MHD_destroy_response(response); \
} while (0)

/* fprintf process-specific statistics into mem */
int proc_stat(FILE* mem)
{
	FILE* f = NULL;
	const char* fname = "/proc/self/stat";
	long number;
	char text[64];
	unsigned long utime, stime;
	int prio, nice, Nthrds;
	unsigned long vsize, rss;
	int ret = 0;

	f = fopen(fname, "r");
	if (f == NULL) goto err;

	/* http://stackoverflow.com/questions/669438/how-to-get-memory-usage-at-run-time-in-c */
	ret += fscanf(f, "%ld", &number);
	ret += fscanf(f, "%63s", text);
	ret += fscanf(f, "%63s", text);
	ret += fscanf(f, "%ld", &number);
	ret += fscanf(f, "%ld", &number);
	ret += fscanf(f, "%ld", &number);
	ret += fscanf(f, "%ld", &number);
	ret += fscanf(f, "%ld", &number);
	ret += fscanf(f, "%ld", &number);
	ret += fscanf(f, "%ld", &number);
	ret += fscanf(f, "%ld", &number);
	ret += fscanf(f, "%ld", &number);
	ret += fscanf(f, "%ld", &number);
	ret += fscanf(f, "%ld", &utime);
	ret += fscanf(f, "%ld", &stime);
	ret += fscanf(f, "%lu", &number);
	ret += fscanf(f, "%lu", &number);
	ret += fscanf(f, "%d",  &prio);
	ret += fscanf(f, "%d",  &nice);
	ret += fscanf(f, "%d",  &Nthrds);
	ret += fscanf(f, "%ld", &number);
	ret += fscanf(f, "%ld", &number);
	ret += fscanf(f, "%lu", &vsize);
	ret += fscanf(f, "%lu", &rss);

	if (ret != 24)
		goto noparse;

	fprintf(mem, "\n");
	fprintf(mem, "Process Specific Statistics\n");
	fprintf(mem, "===========================\n");
	fprintf(mem, "\n");

	fprintf(mem, "utime: %.2f, stime: %.2f\n", 1.0*utime/sysconf(_SC_CLK_TCK), 1.0*stime/sysconf(_SC_CLK_TCK));
	fprintf(mem, "priority: %d, nice: %d\n", prio, nice);
	fprintf(mem, "number of threads: %d\n", Nthrds);
	fprintf(mem, "virtual memory: %12lu B, %9.3f MiB, %6.3f GiB\n", 
			vsize, 1.0*vsize/1048576, 1.0*vsize/1073741824);
	fprintf(mem, "resident set:   %12lu B, %9.3f MiB, %6.3f GiB\n", 
			rss*4096, 4096.0*rss/1048576, 4096.0*rss/1073741824);
	fprintf(mem, "\n");

	fprintf(mem, "top: %s\n", top_buf);
	fprintf(mem, "\n");

cleanup:
	if (f) {fclose(f); f = NULL;}
	return 0;

err:
	fprintf(mem, "cannot open %s: %s\n", fname, strerror(errno));
	goto cleanup;
noparse:
	fprintf(mem, "cannot parse %s\n", fname);
	goto cleanup;
}

#endif

