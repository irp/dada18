/* dada18: algo/polarroi.c
 * polarroi: Itot between min_r...max_r (in pixels) and min_phi...max_phi (in degree)
 * */
#include "../dada.h"

struct dada_data* dada_algo_polarroi(struct dada_polarroi* pol, struct dada_data_modi* modi, struct dada_data* data, __attribute__((unused))enum dada_cache_flags flags)
{
	struct dada_data* polarroi  = NULL;
	struct dada_data* norm  = NULL;
	int n;
	float r, phi;
	int x,y, X,Y;
	struct dada_polarroi mypol;

	if (pol==NULL && modi==NULL)
	{
		DADA_RING_PRINTF("empty pol %p && modi %p", pol, modi);
		goto cleanup;
	}
	if (data==NULL || data->buffer.p == 0)
	{
		DADA_RING_PRINTF("empty data: %p", data);
		goto cleanup;
	}

	if (data->D != 2)
	{
		DADA_RING_PRINTF("data has %d dimensions, only 2 supported.", data->D);
		goto cleanup;
	}

	/* if not given, parse polarroi definition from modifiers */
	if (pol == NULL)
	{
		pol = &mypol;
		pol->cen_x   =   -1;
		pol->cen_y   =   -1;
		pol->min_r   =    0;
		pol->max_r   = 1000;
		pol->min_phi = -180;
		pol->max_phi =  180;
		for (n=0; n<modi->len; n++)
		{
			int m;
			float t1,t2, t3,t4, t5,t6;
			if (modi->key[n] && strcmp(modi->key[n], "polarroi")!=0) continue;
			m = sscanf(modi->val[n], "%f,%f;%f,%f;%f,%f", &t1, &t2, &t3, &t4, &t5, &t6);
			if (m != 6) break;

			pol->cen_x   = t1;
			pol->cen_y   = t2;
			pol->min_r   = t3;
			pol->max_r   = t4;
			pol->min_phi = t5;
			pol->max_phi = t6;
		}
		if (pol->cen_x < 0 || pol->cen_y < 0) goto cleanup;
		if (pol->min_r < 0) pol->min_r = 0;
		if (pol->max_r < pol->min_r) pol->max_r = pol->min_r+1;

		while (pol->max_phi >  180)
		{
			pol->min_phi -= 360.0;
			pol->max_phi -= 360.0;
		}
		while (pol->min_phi < -180)
		{
			pol->min_phi += 360.0;
			pol->max_phi += 360.0;
		}
		while (pol->min_phi >  180)
		{
			pol->min_phi -= 360.0;
			pol->max_phi -= 360.0;
		}
		if (pol->max_phi <= pol->min_phi)
			pol->max_phi = pol->min_phi + 1;

		DADA_RING_PRINTF("pol: %g,%g ; %g,%g ; %g,%g", pol->cen_x, pol->cen_y, pol->min_r, pol->max_r, pol->min_phi, pol->max_phi);
	}

	polarroi = dada_data_init(DADA_DATA_DOUBLE, 1, 1);
	norm     = dada_data_init(DADA_DATA_DOUBLE, 1, 1);

	polarroi->buffer.f[0] = 0.0;
	norm->buffer.f[0]     = 0.0;

	Y = data->d[0]; X = data->d[1];

	const int subpix = 3; /* number of sub pixels per dimensions: 2*subpix+1 */
	int sx, sy;

	for (y=0; y<Y; y++)
	{
		for (x=0; x<X; x++)
		{
			float intensity = -1.0;
			int   fraction  =  0;

			switch (data->type)
			{
				case DADA_DATA_INT:    intensity = data->buffer.i[y*X+x]; break;
				case DADA_DATA_DOUBLE: intensity = data->buffer.f[y*X+x]; break;
				default: fprintf(stderr, "(unsupported format %s:%d)\n", __FILE__, __LINE__); break;
			}

			if (intensity <= 0) continue;

			float dely = (y-pol->cen_y);
			float delx = (x-pol->cen_x);
			float del2 = delx*delx + dely*dely;

			r   = sqrt(del2);
			phi = 360/2/M_PI * atan2(dely,delx);

			/* if "far" away, discard even without sub pixel: */
			if (pol->min_r  -1 > r   || pol->max_r  +1 < r  ) continue;
			if (pol->min_phi-1 > phi || pol->max_phi+1 < phi) continue;

			/* count how many subpixels are inside region */
			for (sy=-subpix; sy<=subpix; sy++)
			{
				for (sx=-subpix; sx<=subpix; sx++)
				{
					float suby = 1.0*sy/(2*subpix+1);
					float subx = 1.0*sx/(2*subpix+1);

					float dely = (y+suby-pol->cen_y);
					float delx = (x+subx-pol->cen_x);
					float del2 = delx*delx + dely*dely;

					r   = sqrt(del2);
					phi = 360/2/M_PI * atan2(dely,delx);

					if (pol->min_r   > r   || pol->max_r   < r  ) continue;
					if (pol->min_phi > phi || pol->max_phi < phi) continue;

					fraction++;
				}
			}

			polarroi->buffer.f[0] += intensity * fraction / ( (2*subpix+1)*(2*subpix+1) );
		}
	}

cleanup:
	if (norm) {dada_data_destroy(norm); free(norm); norm=NULL;}
	return polarroi;
}

