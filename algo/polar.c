/* dada18: algo/polar.c
 * convert cartesian image to polar coordinates
 * */
#include "../dada.h"

struct dada_data* dada_algo_polar(struct dada_polar* roi, struct dada_data_modi* modi, struct dada_data* data, __attribute__((unused))enum dada_cache_flags flags)
{
	struct dada_data* polar = NULL;
	struct dada_data* ipol  = NULL;
	struct dada_data* norm  = NULL;
	int n;
	int x,y, X,Y;
	int xx, yy;
	int q, phi;
	struct dada_polar _roi;
	int Q, PHI;
	double scaling;

	if (roi==NULL && modi==NULL)
	{
		DADA_RING_PRINTF("empty roi %p && modi %p", roi, modi);
		goto cleanup;
	}
	if (data==NULL || data->buffer.p == 0)
	{
		DADA_RING_PRINTF("empty data: %p", data);
		goto cleanup;
	}

	if (data->D != 2)
	{
		DADA_RING_PRINTF("data has %d dimensions, only 2 supported.", data->D);
		goto cleanup;
	}

	/* if not given, parse polar definition from modifiers */
	if (roi == NULL)
	{
		int m = 0;

		roi = &_roi;

		/* default values:
		 * centred;
		 * 1024 radial pixels;
		 * 360 azimuthal pixels
		 */
		roi->cen_y   = data->d[0]/2;
		roi->cen_x   = data->d[1]/2;
		roi->min_q   =     1;
		roi->max_q   =  1024;
		roi->min_phi =     0; /* phi is in degree, obviously... */
		roi->max_phi =   359;
		roi->num_phi =   360;

		/* parse user's values */
		for (n=0; n<modi->len; n++)
		{
			float t1, t2;
			int t3, t4, t5, t6, t7;
			if (modi->key[n] && strcmp(modi->key[n], "polar")!=0) continue;

			m = sscanf(modi->val[n], "%f,%f;%d,%d;%d,%d;%d", &t1, &t2, &t3, &t4, &t5, &t6, &t7);

			switch (m)
			{
				case 7:
					roi->num_phi = t7;
					/* FALLTHROUGH */
				case 6:
					roi->min_phi = t5;
					roi->max_phi = t6;
					/* FALLTHROUGH */
				case 4:
					roi->min_q   = t3;
					roi->max_q   = t4;
					/* FALLTHROUGH */
				case 2:
					roi->cen_x   = t1;
					roi->cen_y   = t2;
					break;
			}
		}

		if (roi->min_q   <  0)            roi->min_q   = 0;
		if (roi->max_q   <= roi->min_q)   roi->max_q   = roi->min_q;
		if (roi->max_phi <= roi->min_phi) roi->max_phi = roi->min_phi;

		if (m < 7) /* if not given, set number of azimuthal pixels to |max-min|+1, i.e. steps of 1 degree */
			roi->num_phi = (roi->max_phi-roi->min_phi)+1;

		DADA_RING_PRINTF("polar roi: %g,%g ; %d,%d ; %d,%d ; %d (m=%d)", roi->cen_x, roi->cen_y, roi->min_q, roi->max_q, roi->min_phi, roi->max_phi, roi->num_phi, m);
	}

	/* image dimensions before / after transformation */
	Y       = data->d[0];
	X       = data->d[1];
	Q       = roi->max_q-roi->min_q+1;
	PHI     = roi->num_phi;
	scaling = roi->num_phi * 1.0 / (roi->max_phi-roi->min_phi+1);

	DADA_RING_PRINTF("min %d max %d num %d PHI %d scaling %g", roi->min_phi, roi->max_phi, roi->num_phi, PHI, scaling);

	/* temporary arrays during pixel-wise transformation */
	ipol  = dada_data_init(DADA_DATA_DOUBLE, 2, Q, PHI);
	norm  = dada_data_init(DADA_DATA_DOUBLE, 2, Q, PHI);

	/* final image */
	polar = dada_data_init(DADA_DATA_DOUBLE, 2, Q, PHI);

	/* simple transformation to polar coordinates */
	for (y=0; y<Y; y++)
	{
		for (x=0; x<X; x++)
		{
			double angle, dist;

			/* centred cartesian coordinates */
			xx  = x-roi->cen_x;
			yy  = y-roi->cen_y;

			/* polar coordinates */
			angle = atan2(yy, xx);
			dist  = xx*xx + yy*yy;

			if (angle < 0) angle += 2*M_PI;

			phi = round((angle*180/M_PI - roi->min_phi) * scaling);
			q   = round(sqrt(dist)-roi->min_q);

			/* ROI */
			if (q   <  0  ) continue;
			if (q   >= Q  ) continue;
			if (phi <  0  ) continue;
			if (phi >= PHI) continue;

			switch (data->type)
			{
				case DADA_DATA_INT:
					if (data->buffer.i[y*X+x] < 0) continue;
					ipol->buffer.f[q*PHI+phi] += data->buffer.i[y*X+x];
					break;
				case DADA_DATA_DOUBLE:
					if (data->buffer.f[y*X+x] < 0) continue;
					ipol->buffer.f[q*PHI+phi] += data->buffer.f[y*X+x];
					break;
				default: fprintf(stderr, "(unsupported format %s:%d)\n", __FILE__, __LINE__); break;
			}
			norm->buffer.f[q*PHI+phi] += 1;
		}
	}

	for (q=0; q<Q; q++)
	{
		for (phi=0; phi<PHI; phi++)
		{
			if (norm->buffer.f[q*PHI+phi] > 0)
			{
				ipol ->buffer.f[q*PHI+phi] /= norm->buffer.f[q*PHI+phi];
				polar->buffer.f[q*PHI+phi]  = ipol->buffer.f[q*PHI+phi];
			}
			else
			{
				ipol ->buffer.f[q*PHI+phi] = -1;
				polar->buffer.f[q*PHI+phi] = -1;
			}
		}
	}


cleanup:
	if (norm) {dada_data_destroy(norm); free(norm); norm=NULL;}
	if (ipol) {dada_data_destroy(ipol); free(ipol); ipol=NULL;}
	return polar;
}

