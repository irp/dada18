/* dada18: dada_stxmjob.c
 * check if requested stxm job is already running, and return its progress
 * */

#include "dada_stxmjob.h"

double dada_stxmjob_check(const char* key_name)
{
	double progress = -1.0; /* no such job */
	const char* key_space = "dada_cluster";
	const char* key_set   = "jobs";
	as_key _key;
	as_record* p_rec = NULL;
	as_policy_write wpol;
	as_record rec;

	pid_t pid;
	char host[256];
	pid  = getpid();
	if (gethostname(host, sizeof(host)-1) != 0   ) { snprintf(host, sizeof(host)-1, "."); }

	if (key_name==NULL || strlen(key_name)==0) goto cleanup;

	// fprintf(stdout, "\tchecking jobcache for [%s] ... ", key_name);
	// fflush(stdout);

	as_key_init_str(&_key, key_space, key_set, key_name);

	/* try to create the job, if it does not exist */
        as_policy_write_init(&wpol);
        wpol.exists = AS_POLICY_EXISTS_CREATE;

	as_record_inita(&rec, 4);
	as_record_set_str(   &rec, "job.key",      key_name);
	as_record_set_double(&rec, "job.progress", 0.0); /* try to create new job */
	as_record_set_str(   &rec, "job.host",     host);
	as_record_set_int64( &rec, "job.pid",      pid);

	if (aerospike_key_put(&aero.aer, &aero.err, &wpol, &_key, &rec) != AEROSPIKE_OK)
	{
		// fprintf(stderr, "error: %s:%d aerospike_key_put failed (%d:%s)\n", __FILE__, __LINE__, aero.err.code, aero.err.message);

		if (aerospike_key_exists(&aero.aer, &aero.err, NULL, &_key, &p_rec) == AEROSPIKE_ERR_RECORD_NOT_FOUND)
			goto error;

		/* retrieve cached response */
		if (p_rec) {as_record_destroy(p_rec); p_rec=NULL;}
		if (aerospike_key_get(&aero.aer, &aero.err, NULL, &_key, &p_rec) == AEROSPIKE_OK)
		{
			progress = as_record_get_double(p_rec, "job.progress", -1.0);
			if (p_rec) {as_record_destroy(p_rec); p_rec=NULL;}
			goto cleanup;
		}

	}
	progress = -1.0;

cleanup:
	as_record_destroy(&rec);
	fprintf(stdout, " %.1f%%\n", progress);
	return progress;

error:
	fprintf(stdout, " ERROR: does exist, but cannot read ??\n");
	goto cleanup;
}

int dada_stxmjob_update(const char* key_name, double progress)
{
	int ret = 0;
	const char* key_space = "dada_cluster";
	const char* key_set   = "jobs";
	as_key _key;
	as_record rec;

	if (key_name==NULL || strlen(key_name)==0)
	{
		ret = -1;
		goto cleanup;
	}

	fprintf(stdout, "\tupdating jobcache for [%s] to %.1f ... ", key_name, progress);
	fflush(stdout);


	as_key_init_str(&_key, key_space, key_set, key_name);

	/* write cached data */
	as_record_inita(&rec, 2);
	as_record_set_str(  &rec, "job.key",       key_name);
	as_record_set_double(&rec, "job.progress", progress);

	if (aerospike_key_put(&aero.aer, &aero.err, NULL, &_key, &rec) != AEROSPIKE_OK)
	{
		fprintf(stderr, "error: %s:%d aerospike_key_put failed (%d:%s)\n", __FILE__, __LINE__, aero.err.code, aero.err.message);
		ret = -1;
		goto cleanup;
	}
	as_record_destroy(&rec);

cleanup:
	if (0)
	{
		if (ret == 0)
			fprintf(stdout, "YES\n");
		else
			fprintf(stdout, "no\n");
	}
	else
		fprintf(stdout, "\n");
	return ret;
}

int dada_stxmjob_remove(const char* key_name)
{
	int ret = 0;
	const char* key_space = "dada_cluster";
	const char* key_set   = "jobs";
	as_key _key;

	if (key_name==NULL || strlen(key_name)==0)
	{
		ret = -1;
		goto cleanup;
	}

	fprintf(stdout, "\tremoving jobcache for [%s] ... ", key_name);
	fflush(stdout);


	as_key_init_str(&_key, key_space, key_set, key_name);


	if (aerospike_key_remove(&aero.aer, &aero.err, NULL, &_key) != AEROSPIKE_OK)
	{
		fprintf(stderr, "error: aerospike_key_remove failed (%d:%s)\n", aero.err.code, aero.err.message);
		ret = -1;
		goto cleanup;
	}

cleanup:
	if (0)
	{
		if (ret == 0)
			fprintf(stdout, "YES\n");
		else
			fprintf(stdout, "no\n");
	}
	else
		fprintf(stdout, "\n");
	return ret;
}

