/* dada18: algo/retrieve.c
 * delegate fetching data to whatever function is competent
 * */
#include "../dada.h"
#include "dada_stxmjob.h"

extern int dada_cluster_node_update(struct dada_cluster_node* node);

char* dada_stxmjob_canonicalise(struct dada_data_path* path, struct dada_data_modi* modi);
char* dada_stxmcache_canonicalise(struct dada_data_path* path, struct dada_data_modi* modi);
void* _stxm(void* _stxm_arg);

int _stxm_darkfield_1(struct dada_data* stxm, int index, struct dada_data* image);
int _stxm_radial_1   (struct dada_data* stxm, int index, struct dada_data* image, struct dada_radial* rad);
int _stxm_polarroi_1 (struct dada_data* stxm, int index, struct dada_data* image, struct dada_polarroi* polarroi);
int _stxm_polar_1    (struct dada_data* stxm, int index, struct dada_data* image, struct dada_polar* polar);
int _stxm_xcca_1     (struct dada_data* stxm, int index, struct dada_data* image, struct dada_polar* polar, struct dada_xcca* xccaroi);

enum _stxm_algo {
	_STXM_ALGO_DARKFIELD_1,
	_STXM_ALGO_RADIAL_1,
	_STXM_ALGO_POLARROI_1,
	_STXM_ALGO_POLAR_1,
	_STXM_ALGO_XCCA_1
};

struct _stxm_arg {
	enum _stxm_algo algo;
	int X;
	int Y;
	int first;
	char* job_key;
	char* canonical;
	struct dada_data_path p;
	struct dada_data_modi m;
	struct dada_data* tmp;
};

struct dada_data* dada_algo_stxm(struct dada_data_path* path, struct dada_data_modi* modi, int fromcache, pthread_t* _thr)
{
	struct dada_data* stxm  = NULL;
	int n;
	int X, Y;
	char* job_key = NULL;
	double job_run = -1;
	const char* dl_what = NULL;
	char* canonical = NULL;

	struct _stxm_arg* _arg = NULL;
	enum _stxm_algo algo;
	pthread_t thr;
	struct timespec timeout;
	double t = -1.0;

	*_thr = 0;

	if (path==NULL || path->len == 0 || path->path == NULL)
	{
		DADA_RING_PRINTF("empty path: %p", path);
		goto cleanup;
	}

	/* TODO: create struct dada_data_modi* m with relevant modificators for input/retrieve */

	/* extract
	 * horz / vert from modifiers, default to 1;
	 * algo, default to _STXM_ALGO_DARKFIELD_1
	 * */
	X=Y=1;
	algo = _STXM_ALGO_DARKFIELD_1;
	for (n=0; n<modi->len; n++)
	{
		if (modi->key[n] && strcmp(modi->key[n], "horz")==0)
			if (modi->val[n] && strlen(modi->val[n])>0)
				X = atoi(modi->val[n]);
		if (modi->key[n] && strcmp(modi->key[n], "vert")==0)
			if (modi->val[n] && strlen(modi->val[n])>0)
				Y = atoi(modi->val[n]);
		if (modi->key[n] && strcmp(modi->key[n], "algo")==0)
		{
			const char* val = modi->val[n];
			if (strstr(val, "df") || strstr(val, "darkfield"))
				algo = _STXM_ALGO_DARKFIELD_1;
			if (strstr(val, "rad") || strstr(val, "radial"))
				algo = _STXM_ALGO_RADIAL_1;
			if (strstr(val, "pol") || strstr(val, "polar"))
				algo = _STXM_ALGO_POLAR_1;
			if (strstr(val, "xcca"))
				algo = _STXM_ALGO_XCCA_1;
			if (strstr(val, "polarroi"))
				algo = _STXM_ALGO_POLARROI_1;
		}
	}


	job_key = dada_stxmjob_canonicalise(path, modi);
	job_run = dada_stxmjob_check(job_key);
	if (job_run >= 0) goto job_running;

	canonical = dada_stxmcache_canonicalise(path, modi);

	/* check if data exists in cache */
	if (fromcache)
	{
		void* so_handle = NULL;
		struct dada_data* (*so_funcp)(const char*,const char*,const char*) = NULL;
		dl_what = "dada_input_cache_read_data";
		DADA_DL_LOAD_SYMBOL(DADA_DL_PATH_CONST("input/cache"), dl_what, so_funcp, so_handle);
		stxm = so_funcp("dada_cache", "", canonical);
		DADA_DL_CLOSE(so_handle);
		if (stxm)
		{
			int n;
			int d0 = stxm->d[0];
			int vert = -1;
			struct dada_data* tmp = NULL;

			for (n=0; n<modi->len; n++)
				if (strcmp(modi->key[n], "vert"  )==0) vert = atoi(modi->val[n]);

			DADA_RING_PRINTF("stxm:cache: vert: %d; stxm.d[0]: %d", vert, d0);

			/* if vert == stxm->d[1], we are done */
			if (vert == d0) goto job_done;

			/* if vert < stxm->d[1], we have to cut out the first vert rows */
			if (vert < d0)
			{
				tmp = dada_data_init(stxm->type, 3, vert, stxm->d[1], stxm->d[2]);
				size_t len = tmp->d[0] * tmp->d[1] * tmp->d[2];
				switch (tmp->type)
				{
					case DADA_DATA_INT:    len *= sizeof(int);    break;
					case DADA_DATA_DOUBLE: len *= sizeof(double); break;
					default:               len  = 0;
							       DADA_RING_PRINTF("%s not supported for type !(int|double)", __FUNCTION__);
							       goto cleanup;
				}
				memcpy(tmp->buffer.p, stxm->buffer.p, len);
				if (stxm) {dada_data_destroy(stxm); free(stxm); stxm=NULL;}
				stxm = tmp;
				goto job_done;
			}

			/* if vert > stxm->d[1], we have to continue what was already started ... */
			/* this is donw by the worker thread in _stxm */
		}
	}


	/* now spawn a background job */

	_arg = (struct _stxm_arg*) malloc(sizeof(struct _stxm_arg));
	_arg->algo      = algo;
	_arg->X         = X;
	_arg->Y         = Y;
	_arg->job_key   = strdup(job_key);
	_arg->canonical = strdup(canonical);
	_arg->tmp       = NULL;
	if (stxm) _arg->tmp = stxm; /* to be continued */

	/* copy path structure to local p,
	 * which is passed on with incrementing image/frame number
	 * to input/retrieve
	 * */
	_arg->p.len = path->len;
	_arg->p.path = (char**) malloc(_arg->p.len*sizeof(char*));
	for (n=0; n<_arg->p.len; n++)
		_arg->p.path[n] = strdup(path->path[n]);
	_arg->first = atoi(_arg->p.path[_arg->p.len-1]);
	free(_arg->p.path[_arg->p.len-1]);
	_arg->p.path[_arg->p.len-1]=NULL;

	_arg->m.len = modi->len;
	_arg->m.key = (char**) malloc(_arg->m.len*sizeof(char*));
	_arg->m.val = (char**) malloc(_arg->m.len*sizeof(char*));
	for (n=0; n<_arg->m.len; n++)
	{
		_arg->m.key[n] = strdup(modi->key[n]);
		_arg->m.val[n] = strdup(modi->val[n]);
	}

	pthread_create(&thr, NULL, _stxm, _arg);

	/* check if user requested a background job,
	 * i.e. if modi[timeout] > 0;
	 * */
	for (n=0; n<modi->len; n++)
	{
		if (modi->key[n] && strcmp(modi->key[n], "timeout")==0 && modi->val[n] && atof(modi->val[n])>0)
		{
			t = atof(modi->val[n]);
			clock_gettime(CLOCK_REALTIME, &timeout);
			timeout.tv_sec  += (int)t;
			timeout.tv_nsec += 1e3*(t-(int)t);

		}
	}
	if (t > 0)
	{
		if (pthread_timedjoin_np(thr, (void**)&stxm, &timeout) == ETIMEDOUT)
		{
			*_thr = thr;
			// fprintf(stdout, "*** pthread_timedjoin_np, thr is %ld (%ld) ***\n", thr, *_thr);
			goto job_running;
		}
	}
	else
	{
		if (pthread_join(thr, (void**)&stxm) != 0)
		{
			fprintf(stderr, "*** pthread_join failed: %s ***\n", strerror(errno));
			*_thr = thr;
		}
		else
		{
			// fprintf(stderr, "*** pthread_join OK ***\n");
			*_thr = 0;
		}
	}


	/* put STXM data into cache */
	dada_stxmjob_update(job_key, 100.0);
	if (0 && stxm) /* already taken care of inside _stxm thread */
	{
		void* so_handle = NULL;
		int (*so_funcp)(const char*,const char*,const char*, struct dada_data*) = NULL;
		dl_what = "dada_input_cache_write_data";
		DADA_DL_LOAD_SYMBOL(DADA_DL_PATH_CONST("input/cache"), dl_what, so_funcp, so_handle);
		so_funcp("dada_cache", "", canonical, stxm);
		DADA_DL_CLOSE(so_handle);
	}

job_done:
	dada_stxmjob_remove(job_key);

	/* now cut out the selected signal, and reduce dimensions */
	if (algo == _STXM_ALGO_DARKFIELD_1)
	{
		int Nsig = 0; /* default is Itot */
		int n, x,y, X,Y, N;
		Y = stxm->d[0];
		X = stxm->d[1];
		N = stxm->d[2];

		for (n=0; n<modi->len; n++)
		{
			if (strcmp(modi->key[n], "signal")==0)
			{
				if (modi->val[n])
				{
					if (strcmp(modi->val[n], "Itot")==0) Nsig = 0;
					if (strcmp(modi->val[n], "Imax")==0) Nsig = 1;
					if (strcmp(modi->val[n], "comh")==0) Nsig = 2;
					if (strcmp(modi->val[n], "comv")==0) Nsig = 3;
				}
			}
		}

		for (y=0; y<Y; y++) for (x=0; x<X; x++) stxm->buffer.f[y*X+x] = stxm->buffer.f[y*X*N+x*N+Nsig];

		stxm->D = 2;

		goto cleanup;
	}

	if (algo == _STXM_ALGO_POLARROI_1)
	{
		stxm->D = 2;
		goto cleanup;
	}


cleanup:
	if (0 && _arg)
	{
		if (_arg->job_key)   {free(_arg->job_key);   _arg->job_key=  NULL;}
		if (_arg->canonical) {free(_arg->canonical); _arg->canonical=NULL;}
		for (n=0; n<_arg->p.len; n++) if (_arg->p.path[n]) {free(_arg->p.path[n]); _arg->p.path[n]=NULL;}
		for (n=0; n<_arg->m.len; n++) if (_arg->m.key[n] ) {free(_arg->m.key[n] ); _arg->m.key[n] =NULL;}
		for (n=0; n<_arg->m.len; n++) if (_arg->m.val[n] ) {free(_arg->m.val[n] ); _arg->m.val[n] =NULL;}
		if (_arg->p.path) {free(_arg->p.path); _arg->p.path=NULL;}
		if (_arg->m.key ) {free(_arg->m.key ); _arg->m.key =NULL;}
		if (_arg->m.val ) {free(_arg->m.val ); _arg->m.val =NULL;}
		if (_arg) {free(_arg); _arg=NULL;}
	}
cleanup2:
	if (job_key  ) {free(job_key  ); job_key  =NULL;}
	if (canonical) {free(canonical); canonical=NULL;}

	// fprintf(stdout, "%s: return %p\n", __FUNCTION__, stxm);
	return stxm;
error:
	// dada_stxmjob_update(job_key, -1.0);
	dada_stxmjob_remove(job_key);
	fprintf(stderr, "DADA_DL_LOAD_SYMBOL failed.\n");
	goto cleanup;

job_running:
	stxm = dada_data_init(DADA_DATA_VOID, 0);
	stxm->buffer.p = NULL;
	job_run = dada_stxmjob_check(job_key);
	stxm->progress = job_run;
	fprintf(stderr, "\tjob [%s] is running\n", job_key);
	goto cleanup2;
}

void* _stxm(void* _stxm_arg)
{
	struct _stxm_arg* arg = (struct _stxm_arg*) _stxm_arg;
	struct dada_data* stxm = NULL;
	struct dada_data* tmp  = NULL;
	const char* job_key = arg->job_key;
	const char* dl_what = NULL;
	const char* canonical = arg->canonical;

	void* so_handle = NULL;
	struct dada_data* (*so_funcp)(struct dada_data_path*,struct dada_data_modi*,struct dada_data*,enum dada_cache_flags) = NULL;
	struct dada_data* image = NULL;

	int x,y,n, X,Y,N;
	int y0 = 0;
	int first;
	int current;
	struct dada_data_modi* modi = NULL;
	int m = 0;

	struct dada_radial rad;
	rad.cen_x =   -1;
	rad.cen_y =   -1;
	rad.min_r =    0;
	rad.max_r = 1000;
	rad.bin_r =    1;

	struct dada_polarroi pol;
	pol.cen_x   =   -1;
	pol.cen_y   =   -1;
	pol.min_r   =    0;
	pol.max_r   = 1000;
	pol.min_phi = -180;
	pol.max_phi =  180;

	struct dada_polar polar;
	polar.cen_y   =     0;
	polar.cen_x   =     0;
	polar.min_q   =     1;
	polar.max_q   =  1024;
	polar.min_phi =     0;
	polar.max_phi =   359;
	polar.num_phi =   360;

	struct dada_xcca xccaroi;
	xccaroi.min_del =     0;
	xccaroi.max_del =   359;

	if (arg == NULL)
	{
		DADA_RING_PRINTF("empty arg: %p", arg);
		goto cleanup;
	}
	node.last.stxm++;
	dada_cluster_node_update(&node);

	modi = &arg->m;
	switch (arg->algo)
	{
		case _STXM_ALGO_DARKFIELD_1: N = 4; /* Itot, Imax, comh, comv */
					     break;
		case _STXM_ALGO_RADIAL_1:
					     {
						     for (n=0; n<modi->len; n++)
						     {
							     float t1, t2;
							     int t3, t4, t5;
							     if (modi->key[n] && strcmp(modi->key[n], "radial")!=0) continue;
							     m = sscanf(modi->val[n], "%f,%f;%d,%d;%d", &t1, &t2, &t3, &t4, &t5);
							     switch (m)
							     {
								     case 5:
									     rad.bin_r = t5;
									     /* FALLTHROUGH */
								     case 4:
									     rad.min_r = t3;
									     rad.max_r = t4;
									     /* FALLTHROUGH */
								     case 2:
									     rad.cen_x = t1;
									     rad.cen_y = t2;
									     break;
							     }
						     }
						     if (rad.cen_x < 0 || rad.cen_y < 0) goto cleanup;
						     rad.bin_r = 1; /* currently unsupported */
						     if (rad.min_r < 0) rad.min_r = 0;
						     if (rad.max_r <= rad.min_r) rad.max_r = rad.min_r+1;
						     DADA_RING_PRINTF("rad: %g,%g ; %d;%d ; %d", rad.cen_x, rad.cen_y, rad.min_r, rad.max_r, rad.bin_r);
						     N = rad.max_r + 1;
					     } break;
		case _STXM_ALGO_POLARROI_1:
					     {
						     for (n=0; n<modi->len; n++)
						     {
							     float t1,t2, t3,t4, t5,t6;
							     if (modi->key[n] && strcmp(modi->key[n], "polarroi")!=0) continue;
							     m = sscanf(modi->val[n], "%f,%f;%f,%f;%f,%f", &t1, &t2, &t3, &t4, &t5, &t6);
							     switch (m)
							     {
								     case 6:
									     pol.max_phi = t6;
									     pol.min_phi = t5;
									     /* FALLTHROUGH */
								     case 4:
									     pol.max_r   = t4;
									     pol.min_r   = t3;
									     /* FALLTHROUGH */
								     case 2:
									     pol.cen_y   = t2;
									     pol.cen_x   = t1;
							     }
						     }
						     if (pol.cen_x < 0 || pol.cen_y < 0) goto cleanup;
						     if (pol.min_r < 0) pol.min_r = 0;
						     if (pol.max_r < pol.min_r) pol.max_r = pol.min_r+1;

						     while (pol.min_phi < -180)
						     {
							     pol.min_phi += 360.0;
							     pol.max_phi += 360.0;
						     }
						     while (pol.min_phi >  180)
						     {
							     pol.min_phi -= 360.0;
							     pol.max_phi -= 360.0;
						     }
						     if (pol.max_phi <= pol.min_phi)
							     pol.max_phi = pol.min_phi + 1;

						     DADA_RING_PRINTF("polarroi: %g,%g ; %g,%g ; %g,%g", pol.cen_x, pol.cen_y, pol.min_r, pol.max_r, pol.min_phi, pol.max_phi);
						     N = 1;
					     } break;
		case _STXM_ALGO_POLAR_1:
					     {
						     /* parse user's values */
						     for (n=0; n<modi->len; n++)
						     {
							     float t1, t2;
							     int t3, t4, t5, t6, t7;
							     if (modi->key[n] && strcmp(modi->key[n], "polar")!=0) continue;

							     m = sscanf(modi->val[n], "%f,%f;%d,%d;%d,%d;%d", &t1, &t2, &t3, &t4, &t5, &t6, &t7);

							     switch (m)
							     {
								     case 7:
									     polar.num_phi = t7;
									     /* FALLTHROUGH */
								     case 6:
									     polar.min_phi = t5;
									     polar.max_phi = t6;
									     /* FALLTHROUGH */
								     case 4:
									     polar.min_q   = t3;
									     polar.max_q   = t4;
									     /* FALLTHROUGH */
								     case 2:
									     polar.cen_x   = t1;
									     polar.cen_y   = t2;
									     break;
							     }
						     }

						     if (polar.min_q   <  0)             polar.min_q   = 0;
						     if (polar.max_q   <= polar.min_q)   polar.max_q   = polar.min_q;
						     if (polar.max_phi <= polar.min_phi) polar.max_phi = polar.min_phi;

						     if (m < 7) /* if not given, set number of azimuthal pixels to |max-min|+1, i.e. steps of 1 degree */
							     polar.num_phi = (polar.max_phi-polar.min_phi)+1;

						     DADA_RING_PRINTF("polar roi: %g,%g ; %d,%d ; %d,%d ; %d (m=%d)", polar.cen_x, polar.cen_y, polar.min_q, polar.max_q, polar.min_phi, polar.max_phi, polar.num_phi, m);
						     N = polar.num_phi;
					     } break;
		case _STXM_ALGO_XCCA_1:
					     {
						     /* parse user's values: polar */
						     for (n=0; n<modi->len; n++)
						     {
							     float t1, t2;
							     int t3, t4, t5, t6, t7;
							     if (modi->key[n] && strcmp(modi->key[n], "polar")!=0) continue;

							     m = sscanf(modi->val[n], "%f,%f;%d,%d;%d,%d;%d", &t1, &t2, &t3, &t4, &t5, &t6, &t7);

							     switch (m)
							     {
								     case 7:
									     polar.num_phi = t7;
									     /* FALLTHROUGH */
								     case 6:
									     polar.min_phi = t5;
									     polar.max_phi = t6;
									     /* FALLTHROUGH */
								     case 4:
									     polar.min_q   = t3;
									     polar.max_q   = t4;
									     /* FALLTHROUGH */
								     case 2:
									     polar.cen_x   = t1;
									     polar.cen_y   = t2;
									     break;
							     }
						     }

						     if (polar.min_q   <  0)             polar.min_q   = 0;
						     if (polar.max_q   <= polar.min_q)   polar.max_q   = polar.min_q;
						     if (polar.max_phi <= polar.min_phi) polar.max_phi = polar.min_phi;

						     if (m < 7) /* if not given, set number of azimuthal pixels to |max-min|+1, i.e. steps of 1 degree */
							     polar.num_phi = (polar.max_phi-polar.min_phi)+1;

						     DADA_RING_PRINTF("polar roi: %g,%g ; %d,%d ; %d,%d ; %d (m=%d)", polar.cen_x, polar.cen_y, polar.min_q, polar.max_q, polar.min_phi, polar.max_phi, polar.num_phi, m);

						     xccaroi.min_del = 0;
						     xccaroi.max_del = polar.num_phi;

						     /* parse user's values: XCCA */
						     for (n=0; n<modi->len; n++)
						     {
							     int t1, t2;
							     if (modi->key[n] && strcmp(modi->key[n], "xcca")!=0) continue;

							     m = sscanf(modi->val[n], "%d,%d", &t1, &t2);

							     switch (m)
							     {
								     case 2:
									     xccaroi.min_del = t1;
									     xccaroi.max_del = t2;
									     break;
							     }
						     }

						     if (xccaroi.max_del <= xccaroi.min_del) xccaroi.max_del = xccaroi.min_del;

						     DADA_RING_PRINTF("xcca roi: %d,%d (%d)", xccaroi.min_del, xccaroi.max_del, (xccaroi.max_del-xccaroi.min_del+1));
						     N = xccaroi.max_del-xccaroi.min_del+1;
					     } break;
		default: DADA_RING_PRINTF("unsupported algorithm %d\n", arg->algo);
			 goto cleanup;
	}

	X     = arg->X;
	Y     = arg->Y;
	first = arg->first;

	stxm = dada_data_init(DADA_DATA_DOUBLE, 3, Y, X, N);
	stxm->progress = 0.0;

	/* if arg->tmp != NULL, we have to continue by (i) copying data and (ii) reading the remaining lines;
	 * otherwise, we start from 0
	 * */
	if (arg->tmp)
	{
		/* copy cached data */
		size_t len = arg->tmp->d[0] * arg->tmp->d[1] * arg->tmp->d[2];
		switch (arg->tmp->type)
		{
			case DADA_DATA_INT:    len *= sizeof(int);    break;
			case DADA_DATA_DOUBLE: len *= sizeof(double); break;
			default:               len  = 0;
					       DADA_RING_PRINTF("%s not supported for type !(int|double)", __FUNCTION__);
					       goto cleanup;
		}
		memcpy(stxm->buffer.p, arg->tmp->buffer.p, len);

		/* first of the remaining lines */
		y0 = arg->tmp->d[0];
	}

	/* do the remaining lines */
	for (y=y0; y<Y; y++)
	{
		for (x=0; x<X; x++)
		{
			// double progress = 100.0*y/Y;
			double progress = 100.0 * (y*X+x) / (Y*X);
			stxm->progress = progress;
			dada_stxmjob_update(job_key, progress);
			current = y*X+x + first;
			char curr[16];
			int index;
			snprintf(curr, sizeof(curr)-1, "%d", current);
			arg->p.path[arg->p.len-1] = strdup(curr);

			DADA_DL_LOAD_SYMBOL(DADA_DL_PATH_CONST("input/retrieve"), "dada_input_retrieve", so_funcp, so_handle);
			image = so_funcp(&arg->p, &arg->m, image, DADA_CACHE_NONE);
			// image = so_funcp(NULL, NULL, DADA_CACHE_NONE);
			DADA_DL_CLOSE(so_handle);

			if (image == NULL) goto incomplete;

			index = y*X*N + x*N;

			switch (arg->algo)
			{
				case _STXM_ALGO_DARKFIELD_1: _stxm_darkfield_1(stxm, index, image);                   break;
				case _STXM_ALGO_RADIAL_1:    _stxm_radial_1   (stxm, index, image, &rad  );           break;
				case _STXM_ALGO_POLARROI_1:  _stxm_polarroi_1 (stxm, index, image, &pol  );           break;
				case _STXM_ALGO_POLAR_1:     _stxm_polar_1    (stxm, index, image, &polar);           break;
				case _STXM_ALGO_XCCA_1:      _stxm_xcca_1     (stxm, index, image, &polar, &xccaroi); break;
				default: DADA_RING_PRINTF("unsupported algorithm %d\n", arg->algo);
					 goto cleanup;
			}
		}
	}

	/* put STXM data into cache */
finished:
	dada_stxmjob_update(job_key, 100.0);
	if (1 && stxm)
	{
		void* so_handle = NULL;
		int (*so_funcp)(const char*,const char*,const char*, struct dada_data*) = NULL;
		dl_what = "dada_input_cache_write_data";
		DADA_DL_LOAD_SYMBOL(DADA_DL_PATH_CONST("input/cache"), dl_what, so_funcp, so_handle);
		so_funcp("dada_cache", "", canonical, stxm);
		DADA_DL_CLOSE(so_handle);
	}


cleanup:

	arg->p.path[arg->p.len-1] = NULL;
	dada_stxmjob_remove(job_key);
	if (arg->tmp) {dada_data_destroy(arg->tmp); free(arg->tmp); arg->tmp=NULL;}
	if (image) {dada_data_destroy(image); free(image); image=NULL;}
	node.last.stxm--;
	dada_cluster_node_update(&node);

	if (1 && arg)
	{
		if (arg->job_key)   {free(arg->job_key);   arg->job_key=  NULL;}
		if (arg->canonical) {free(arg->canonical); arg->canonical=NULL;}
		for (n=0; n<arg->p.len; n++) if (arg->p.path[n]) {free(arg->p.path[n]); arg->p.path[n]=NULL;}
		for (n=0; n<arg->m.len; n++) if (arg->m.key[n] ) {free(arg->m.key[n] ); arg->m.key[n] =NULL;}
		for (n=0; n<arg->m.len; n++) if (arg->m.val[n] ) {free(arg->m.val[n] ); arg->m.val[n] =NULL;}
		if (arg->p.path) {free(arg->p.path); arg->p.path=NULL;}
		if (arg->m.key ) {free(arg->m.key ); arg->m.key =NULL;}
		if (arg->m.val ) {free(arg->m.val ); arg->m.val =NULL;}
		if (arg)         {free(arg);         arg        =NULL;}
	}


	return stxm;

error:
	// dada_stxmjob_update(job_key, -1.0);
	dada_stxmjob_remove(job_key);
	fprintf(stderr, "DADA_DL_LOAD_SYMBOL failed.\n");
	goto cleanup;

incomplete:
	/* we only got (y-1) full lines,
	 * so create a new struct dada_data*
	 * and copy ROI into it;
	 * then clear old stxm and
	 * goto finished;
	 * */
	Y=y;
	tmp = dada_data_init(stxm->type, 3, Y, X, N);
	size_t len = tmp->d[0] * tmp->d[1] * tmp->d[2];
	switch (tmp->type)
	{
		case DADA_DATA_INT:    len *= sizeof(int);    break;
		case DADA_DATA_DOUBLE: len *= sizeof(double); break;
		default:               len  = 0;
				       DADA_RING_PRINTF("%s not supported for type !(int|double)", __FUNCTION__);
				       goto cleanup;
	}
	memcpy(tmp->buffer.p, stxm->buffer.p, len);
	if (stxm) {dada_data_destroy(stxm); free(stxm); stxm=NULL;}
	stxm = tmp;
	goto finished;
}


int _stxm_darkfield_1(struct dada_data* stxm, int index, struct dada_data* image)
{
	int xx, yy, XX, YY;
	double Itot = 0.0, Imax = 0.0;
	double comh = 0.0, comv = 0.0;

	YY = image->d[0];
	XX = image->d[1];
	switch (image->type)
	{
		case DADA_DATA_INT:
			for (yy=0; yy<YY; yy++)
			{
				for (xx=0; xx<XX; xx++)
				{
					int value = image->buffer.i[yy*XX+xx];
					if (value < 0) continue;
					Itot += value;
					if (value > Imax) Imax = value;
					comh += value*xx;
					comv += value*yy;
				}
			}
			break;
		case DADA_DATA_DOUBLE:
			for (yy=0; yy<YY; yy++)
			{
				for (xx=0; xx<XX; xx++)
				{
					double value = image->buffer.f[yy*XX+xx];
					if (value < 0) continue;
					Itot += value;
					if (value > Imax) Imax = value;
					comh += value*xx;
					comv += value*yy;
				}
			}
			break;
		default: fprintf(stderr, "(unsupported format %s:%d)\n", __FILE__, __LINE__); break;
	}

	switch (stxm->type)
	{
		case DADA_DATA_INT:
			stxm->buffer.i[index+0] = Itot;
			stxm->buffer.i[index+1] = Imax;
			stxm->buffer.i[index+2] = comh/Itot;
			stxm->buffer.i[index+3] = comv/Itot;
			break;
		case DADA_DATA_DOUBLE:
			stxm->buffer.f[index+0] = Itot;
			stxm->buffer.f[index+1] = Imax;
			stxm->buffer.f[index+2] = comh/Itot;
			stxm->buffer.f[index+3] = comv/Itot;
			break;
		default: fprintf(stderr, "(unsupported format %s:%d)\n", __FILE__, __LINE__); break;
	}

	return 0;
}

int _stxm_radial_1(struct dada_data* stxm, int index, struct dada_data* image, struct dada_radial* rad)
{
	struct dada_data* radial = NULL;
	int r, R;

	void* so_handle = NULL;
	struct dada_data* (*so_funcp)(struct dada_radial*,struct dada_data_modi*,struct dada_data*,enum dada_cache_flags) = NULL;

	DADA_DL_LOAD_SYMBOL(DADA_DL_PATH_CONST("algo/radial"), "dada_algo_radial", so_funcp, so_handle);
	radial = so_funcp(rad, NULL, image, DADA_CACHE_NONE);
	DADA_DL_CLOSE(so_handle);

	if (radial == NULL)
	{
		DADA_RING_PRINTF("%s dada_algo_radial failed.", __FUNCTION__);
		goto error;
	}

	R = rad->max_r + 1;

	if (radial->type != DADA_DATA_DOUBLE)
	{
		DADA_RING_PRINTF("%s not supported for type !(double)", __FUNCTION__);
		goto error;
	}
	switch (stxm->type)
	{
		case DADA_DATA_INT:
			for (r=0; r<R; r++) stxm->buffer.i[index+r] = radial->buffer.f[r];
			break;
		case DADA_DATA_DOUBLE:
			for (r=0; r<R; r++) stxm->buffer.f[index+r] = radial->buffer.f[r];
			break;
		default: DADA_RING_PRINTF("stxm->type %d not supported.", stxm->type); goto error;
	}

	if (radial) {dada_data_destroy(radial); free(radial); radial=NULL;}
	return 0;

error:
	if (radial) {dada_data_destroy(radial); free(radial); radial=NULL;}
	return -1;
}

int _stxm_polarroi_1(struct dada_data* stxm, int index, struct dada_data* image, struct dada_polarroi* pol)
{
	struct dada_data* polarroi = NULL;

	void* so_handle = NULL;
	struct dada_data* (*so_funcp)(struct dada_polarroi*,struct dada_data_modi*,struct dada_data*,enum dada_cache_flags) = NULL;

	DADA_DL_LOAD_SYMBOL(DADA_DL_PATH_CONST("algo/polarroi"), "dada_algo_polarroi", so_funcp, so_handle);
	polarroi = so_funcp(pol, NULL, image, DADA_CACHE_NONE);
	DADA_DL_CLOSE(so_handle);

	if (polarroi == NULL)
	{
		DADA_RING_PRINTF("%s dada_algo_polarroi failed.", __FUNCTION__);
		goto error;
	}

	if (polarroi->type != DADA_DATA_DOUBLE)
	{
		DADA_RING_PRINTF("%s not supported for type !(double)", __FUNCTION__);
		goto error;
	}
	switch (stxm->type)
	{
		case DADA_DATA_INT:
			stxm->buffer.i[index+0] = polarroi->buffer.f[0];
			break;
		case DADA_DATA_DOUBLE:
			stxm->buffer.f[index+0] = polarroi->buffer.f[0];
			break;
		default: DADA_RING_PRINTF("stxm->type %d not supported.", stxm->type); goto error;
	}

	if (polarroi) {dada_data_destroy(polarroi); free(polarroi); polarroi=NULL;}
	return 0;

error:
	if (polarroi) {dada_data_destroy(polarroi); free(polarroi); polarroi=NULL;}
	return -1;
}

int _stxm_polar_1(struct dada_data* stxm, int index, struct dada_data* image, struct dada_polar* polar)
{
	struct dada_data* data = NULL;

	int phi;

	void* so_handle = NULL;
	struct dada_data* (*so_funcp)(struct dada_polar*,struct dada_data_modi*,struct dada_data*,enum dada_cache_flags) = NULL;

	DADA_DL_LOAD_SYMBOL(DADA_DL_PATH_CONST("algo/polar"), "dada_algo_polar", so_funcp, so_handle);
	data = so_funcp(polar, NULL, image, DADA_CACHE_NONE);
	DADA_DL_CLOSE(so_handle);

	if (data == NULL)
	{
		DADA_RING_PRINTF("%s dada_algo_polar failed.", __FUNCTION__);
		goto error;
	}

	if (data->type != DADA_DATA_DOUBLE)
	{
		DADA_RING_PRINTF("%s not supported for type !(double)", __FUNCTION__);
		goto error;
	}
	for (phi=0; phi<polar->num_phi; phi++)
	{
		switch (stxm->type)
		{
			case DADA_DATA_INT:
				stxm->buffer.i[index+phi] = data->buffer.f[phi];
				break;
			case DADA_DATA_DOUBLE:
				stxm->buffer.f[index+phi] = data->buffer.f[phi];
				break;
			default: DADA_RING_PRINTF("stxm->type %d not supported.", stxm->type); goto error;
		}
	}

	if (data) {dada_data_destroy(data); free(data); data=NULL;}
	return 0;

error:
	if (data) {dada_data_destroy(data); free(data); data=NULL;}
	return -1;
}

int _stxm_xcca_1(struct dada_data* stxm, int index, struct dada_data* image, struct dada_polar* polar, struct dada_xcca* xccaroi)
{
	struct dada_data* data = NULL;
	struct dada_data* xcca = NULL;

	int del;

	void* so_handle = NULL;
	struct dada_data* (*so_funcp1)(struct dada_polar*,struct dada_data_modi*,struct dada_data*,enum dada_cache_flags) = NULL;
	struct dada_data* (*so_funcp2)(struct dada_xcca* ,struct dada_data_modi*,struct dada_data*,enum dada_cache_flags) = NULL;

	DADA_DL_LOAD_SYMBOL(DADA_DL_PATH_CONST("algo/polar"), "dada_algo_polar", so_funcp1, so_handle);
	data = so_funcp1(polar, NULL, image, DADA_CACHE_NONE);
	DADA_DL_CLOSE(so_handle);

	if (data == NULL)
	{
		DADA_RING_PRINTF("%s dada_algo_polar failed.", __FUNCTION__);
		goto error;
	}

	if (data->type != DADA_DATA_DOUBLE)
	{
		DADA_RING_PRINTF("%s not supported for type !(double)", __FUNCTION__);
		goto error;
	}

	DADA_DL_LOAD_SYMBOL(DADA_DL_PATH_CONST("algo/xcca"), "dada_algo_xcca", so_funcp2, so_handle);
	xcca = so_funcp2(xccaroi, NULL, data, DADA_CACHE_NONE);
	DADA_DL_CLOSE(so_handle);

	if (xcca == NULL)
	{
		DADA_RING_PRINTF("%s dada_algo_xcca failed.", __FUNCTION__);
		goto error;
	}

	if (xcca->type != DADA_DATA_DOUBLE)
	{
		DADA_RING_PRINTF("%s not supported for type !(double)", __FUNCTION__);
		goto error;
	}

	if (xcca->D == 1) DADA_RING_PRINTF("xcca->d: %d", xcca->d[0]);
	if (xcca->D == 2) DADA_RING_PRINTF("xcca->d: %d,%d", xcca->d[0], xcca->d[1]);
	if (xcca->D == 1) fprintf(stdout, "xcca->d: %d\n", xcca->d[0]);
	if (xcca->D == 2) fprintf(stdout, "xcca->d: %d,%d\n", xcca->d[0], xcca->d[1]);
	for (del=0; del<xcca->d[1]; del++)
	{
		switch (stxm->type)
		{
			case DADA_DATA_INT:
				stxm->buffer.i[index+del] = xcca->buffer.f[del];
				break;
			case DADA_DATA_DOUBLE:
				stxm->buffer.f[index+del] = xcca->buffer.f[del];
				break;
			default: DADA_RING_PRINTF("stxm->type %d not supported.", stxm->type); goto error;
		}
	}

	if (xcca) {dada_data_destroy(xcca); free(xcca); xcca=NULL;}
	if (data) {dada_data_destroy(data); free(data); data=NULL;}
	return 0;

error:
	if (xcca) {dada_data_destroy(xcca); free(xcca); xcca=NULL;}
	if (data) {dada_data_destroy(data); free(data); data=NULL;}
	return -1;
}

char* dada_stxmjob_canonicalise(struct dada_data_path* path, struct dada_data_modi* modi)
{
	char* str = NULL;
	FILE* mem = NULL;
	size_t memlen;
	int n, N;

	DADA_MEMSTREAM(mem, str, memlen);

	N = path->len;
	for (n=0; n<N; n++) fprintf(mem, "/%s", path->path[n]);

	fprintf(mem, "?");

	N = modi->len;
	for (n=0; n<N; n++)
	{
		if (modi->key[n] && strlen(modi->key[n]))
		{
			if (strcmp(modi->key[n], "timeout")==0) continue;
			if (strcmp(modi->key[n], "signal" )==0) continue;
			fprintf(mem, "&%s=%s", modi->key[n], modi->val[n]);
		}
	}
	/* TODO:
	 * differentiate signals of different analysis kinds
	 * */

	fclose(mem);

	return str;

err_memstream:
	return NULL;
}

char* dada_stxmcache_canonicalise(struct dada_data_path* path, struct dada_data_modi* modi)
{
	char* str = NULL;
	FILE* mem = NULL;
	size_t memlen;
	int n, N;

	DADA_MEMSTREAM(mem, str, memlen);

	N = path->len;
	for (n=0; n<N; n++) fprintf(mem, "/%s", path->path[n]);

	fprintf(mem, "?");

	N = modi->len;
	for (n=0; n<N; n++)
	{
		if (modi->key[n] && strlen(modi->key[n]))
		{
			if (strcmp(modi->key[n], "timeout") == 0) continue;
			if (strcmp(modi->key[n], "signal")  == 0) continue;
			if (strcmp(modi->key[n], "vert"  )  == 0) continue;
			fprintf(mem, "&%s=%s", modi->key[n], modi->val[n]);
		}
	}

	fclose(mem);

	return str;

err_memstream:
	return NULL;
}

