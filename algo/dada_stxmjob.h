#ifndef _DADA_STXMJOB_H_
#define _DADA_STXMJOB_H_

#include "../dada.h"

#include "../dada_aero.h"

#include <aerospike/aerospike_key.h>

double dada_stxmjob_check(const char* key_name);
int dada_stxmjob_update(const char* key_name, double progress);
int dada_stxmjob_remove(const char* key_name);

#endif

