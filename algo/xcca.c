/* dada18: algo/xcca.c
 * simple XCCA algorithm
 * */
#include "../dada.h"

struct dada_data* dada_algo_xcca(struct dada_xcca* roi, struct dada_data_modi* modi, struct dada_data* data, __attribute__((unused))enum dada_cache_flags flags)
{
	struct dada_data* xcca  = NULL;
	int n;
	int q, phi, del;
	struct dada_xcca _roi;
	int Q, PHI;

	if (roi==NULL && modi==NULL)
	{
		DADA_RING_PRINTF("empty roi %p && modi %p", roi, modi);
		goto cleanup;
	}
	if (data==NULL || data->buffer.p == 0)
	{
		DADA_RING_PRINTF("empty data: %p", data);
		goto cleanup;
	}

	if (data->D != 2)
	{
		DADA_RING_PRINTF("data has %d dimensions, only 2 supported.", data->D);
		goto cleanup;
	}

	Q   = data->d[0];
	PHI = data->d[1];

	/* if not given, parse xcca definition from modifiers */
	if (roi == NULL)
	{
		int m = 0;

		roi = &_roi;

		/* default values:
		 * everything.
		 */
		roi->min_del =     0;
		roi->max_del = PHI-1;

		/* parse user's values */
		for (n=0; n<modi->len; n++)
		{
			int t1, t2;
			if (modi->key[n] && strcmp(modi->key[n], "xcca")!=0) continue;

			m = sscanf(modi->val[n], "%d,%d", &t1, &t2);

			switch (m)
			{
				case 2:
					roi->min_del = t1;
					roi->max_del = t2;
					break;
			}
		}

		if (roi->max_del <= roi->min_del) roi->max_del = roi->min_del;

		DADA_RING_PRINTF("xcca roi: %d,%d", roi->min_del, roi->max_del);
	}

	PHI = roi->max_del-roi->min_del+1;

	/* final image */
	xcca  = dada_data_init(DADA_DATA_DOUBLE, 2, Q, PHI);

	for (q=0; q<Q; q++)
	{
		for (del=0; del<PHI; del++)
		{
			double Ic = 0;
			double I0 = 0;
			int    Nc = 0;
			int    N0 = 0;

			for (phi=0; phi<PHI; phi++)
			{
				int    idx1 = q*PHI + (phi+roi->min_del    );
				int    idx2 = q*PHI + (phi+roi->min_del+del)%PHI;
				double Iphi = data->buffer.f[idx1];
				double Idel = data->buffer.f[idx2];

				if (Iphi >= 0 && Idel >= 0) Ic += Iphi * Idel;
				if (Iphi >= 0)              I0 += Iphi;
				if (Iphi >= 0 && Idel >= 0) Nc++;
				if (Iphi >= 0)              N0++;
			}

			if (I0>0 && N0>0 && Nc>0)
				xcca->buffer.f[q*PHI+del] = (Ic/Nc-I0*I0/N0/N0) / (I0*I0/N0/N0);
			else
				xcca->buffer.f[q*PHI+del] = 0.0;
		}
	}


cleanup:
	return xcca;
}

