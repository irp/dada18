/* dada18: algo/radial.c
 * radial profile vom azimuthal average
 * */
#include "../dada.h"

struct dada_data* dada_algo_radial(struct dada_radial* rad, struct dada_data_modi* modi, struct dada_data* data, __attribute__((unused))enum dada_cache_flags flags)
{
	struct dada_data* radial  = NULL;
	struct dada_data* norm  = NULL;
	int n;
	int r, R;
	int x,y, X,Y;
	struct dada_radial myrad;

	if (rad==NULL && modi==NULL)
	{
		DADA_RING_PRINTF("empty rad %p && modi %p", rad, modi);
		goto cleanup;
	}
	if (data==NULL || data->buffer.p == 0)
	{
		DADA_RING_PRINTF("empty data: %p", data);
		goto cleanup;
	}

	if (data->D != 2)
	{
		// DADA_RING_PRINTF("data has %d dimensions, only 2 supported.", data->D);
		goto cleanup;
	}

	/* if not given, parse radial definition from modifiers */
	if (rad == NULL)
	{
		rad = &myrad;
		rad->cen_x =   -1;
		rad->cen_y =   -1;
		rad->min_r =    0;
		rad->max_r = 1000;
		rad->bin_r =    1;
		for (n=0; n<modi->len; n++)
		{
			int m;
			float t1, t2;
			int t3, t4, t5;
			if (modi->key[n] && strcmp(modi->key[n], "radial")!=0) continue;
			m = sscanf(modi->val[n], "%f,%f;%d,%d;%d", &t1, &t2, &t3, &t4, &t5);
			switch (m)
			{
				case 5:
					rad->bin_r = t5;
					/* FALLTHROUGH */
				case 4:
					rad->min_r = t3;
					rad->max_r = t4;
					/* FALLTHROUGH */
				case 2:
					rad->cen_x = t1;
					rad->cen_y = t2;
					break;
			}
		}
		if (rad->cen_x < 0 || rad->cen_y < 0) goto cleanup;

		rad->bin_r = 1; /* currently unsupported */
		if (rad->min_r < 0) rad->min_r = 0;
		if (rad->max_r <= rad->min_r) rad->max_r = rad->min_r+1;

		DADA_RING_PRINTF("rad: %g,%g ; %d;%d ; %d", rad->cen_x, rad->cen_y, rad->min_r, rad->max_r, rad->bin_r);
	}

	R = rad->max_r + 1;

	radial = dada_data_init(DADA_DATA_DOUBLE, 1, R);
	norm   = dada_data_init(DADA_DATA_DOUBLE, 1, R);

	Y = data->d[0]; X = data->d[1];

	switch (data->type)
	{
		case DADA_DATA_INT:
			for (y=0; y<Y; y++)
			{
				float dely = (y-rad->cen_y);
				for (x=0; x<X; x++)
				{
					double intensity = data->buffer.i[y*X+x];
					float delx = (x-rad->cen_x);
					float del2 = delx*delx + dely*dely;
					float del  = sqrt(del2);
					int   r1   = (int)floor(del);
					int   r2   = (int)ceil (del);
					float delr = del-r1;

					if (r1 < rad->min_r || r1 >= rad->max_r) continue;
					if (r2 < rad->min_r || r2 >= rad->max_r) continue;
					if (intensity < 0) continue;

					radial->buffer.f[r1] += intensity*(1-delr);
					radial->buffer.f[r2] += intensity*   delr ;
					norm->buffer.f[r1] += (1-delr);
					norm->buffer.f[r2] +=    delr ;
				}
			}
			break;
		case DADA_DATA_DOUBLE:
			for (y=0; y<Y; y++)
			{
				float dely = (y-rad->cen_y);
				for (x=0; x<X; x++)
				{
					double intensity = data->buffer.f[y*X+x];
					float delx = (x-rad->cen_x);
					float del2 = delx*delx + dely*dely;
					float del  = sqrt(del2);
					int   r1   = (int)floor(del);
					int   r2   = (int)ceil (del);
					float delr = del-r1;

					if (r1 < rad->min_r || r1 >= rad->max_r) continue;
					if (r2 < rad->min_r || r2 >= rad->max_r) continue;
					if (intensity < 0) continue;

					radial->buffer.f[r1] += intensity*(1-delr);
					radial->buffer.f[r2] += intensity*   delr ;
					norm->buffer.f[r1] += (1-delr);
					norm->buffer.f[r2] +=    delr ;
				}
			}
			break;
		default: fprintf(stderr, "(unsupported format %s:%d)\n", __FILE__, __LINE__); break;
	}

	for (r=0; r<R; r++) if (norm->buffer.f[r] > 0) radial->buffer.f[r] /= norm->buffer.f[r];

cleanup:
	if (norm) {dada_data_destroy(norm); free(norm); norm=NULL;}
	return radial;
}

