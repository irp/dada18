/* dada18: algo/fourier.c
 * apply certain Fourier filters to (image) data
 * */
#include "../dada.h"
//#include <complex.h>
//#include <fftw3.h>

int dada_algo_fourier_lowpass(struct dada_data* data, double sigma);
int dada_algo_fourier_lowpass2(struct dada_data* data, double sigma);

int dada_algo_fourier(struct dada_data* data, struct dada_process_fourier* fourier)
{
	int ret = 0;

	if (data == NULL || fourier == NULL)
	{
		fprintf(stderr, "NULL data (%p) or fourier (%p)\n", data, fourier);
		ret = -1;
		goto cleanup;
	}

	switch (fourier->oper)
	{
		case DADA_FOURIER_LOWPASS:
			ret = dada_algo_fourier_lowpass(data, fourier->param1);
			break;
		case DADA_FOURIER_LOWPASS2:
			ret = dada_algo_fourier_lowpass2(data, fourier->param1);
			ret = -1;
			break;
		case DADA_FOURIER_HIGHPASS:
			DADA_RING_PRINTF("%s not yet implemented.", "highpass");
			ret = -1;
			break;
		case DADA_FOURIER_NONE:
			ret = -1;
			break;
	}

cleanup:
	return ret;
}

int dada_algo_fourier_lowpass(struct dada_data* data, double sigma)
{
	int ret = 0;
	int x,y, X,Y, xx, yy;
	double norm   = 1.0/(2*M_PI*sigma);
	norm = 1.0;

	double mean0 = 0.0;
	double mean1 = 0.0;

	fftw_plan     plan = 0;
	fftw_complex* fft0 = NULL;
	fftw_complex* fft1 = NULL;

	if (data == NULL)
	{
		fprintf(stderr, "NULL data (%p)\n", data);
		ret = -1;
		goto cleanup;
	}

	if (data->D != 2)
	{
		DADA_RING_PRINTF("dada_algo_fourier_lowpass only accepts images of dimensions 2 at the moment; data->D=%d refused.", data->D);
		ret = -1;
		goto cleanup;
	}
	if (data->type != DADA_DATA_INT && data->type != DADA_DATA_DOUBLE)
	{
		DADA_RING_PRINTF("dada_algo_fourier_lowpass only accepts images of data->type=INT|DOUBLE; data->type=%d refused.", data->type);
		ret = -1;
		goto cleanup;
	}

	X = data->d[1];
	Y = data->d[0];

	fft0 = fftw_malloc(sizeof(fftw_complex)*Y*X);
	fft1 = fftw_malloc(sizeof(fftw_complex)*Y*X);

	/* copy image data into fft0 */
	for (y=0; y<Y; y++)
	{
		for (x=0; x<X; x++)
		{
			double value = 0;
			xx = (X+x-X/2)%X;
			yy = (Y+y-Y/2)%Y;
			switch (data->type)
			{
				case DADA_DATA_INT:    value = data->buffer.i[y*X+x]; break;
				case DADA_DATA_DOUBLE: value = data->buffer.f[y*X+x]; break;
				default:               value = 0;
			}
			fft0[yy*X+xx][0] = value;
			fft0[yy*X+xx][1] = 0.0;
		}
	}

	for (y=0; y<Y; y++) for (x=0; x<X; x++) mean0 += sqrt(fft0[y*X+x][0]*fft0[y*X+x][0]+fft0[y*X+x][1]*fft0[y*X+x][1]);
	mean0 /= X*Y;
	/* Fourier transform image */
	plan = fftw_plan_dft_2d(Y, X, fft0, fft1, FFTW_FORWARD, FFTW_ESTIMATE);
	fftw_execute(plan);
	if (plan) {fftw_destroy_plan(plan); plan=0;}

	/* point-wise multiplication with (Fourier transformed) Gaussian */
	for (y=0; y<Y; y++)
	{
		for (x=0; x<X; x++)
		{
			xx = (x-X/2)%X;
			yy = (y-Y/2)%Y;
			// double g  = norm * (1-exp(-(xx*xx+yy*yy)/(2*sigma*sigma)));
			double g  = norm *    exp(-(xx*xx+yy*yy)/(2*sigma*sigma));
			xx = (X+x-X/2)%X;
			yy = (Y+y-Y/2)%Y;
			fft1[yy*X+xx][0] *= (0+1*g);
			fft1[yy*X+xx][1] *= (0+1*g);
		}
	}

	/* Fourier back-transform */
	plan = fftw_plan_dft_2d(Y, X, fft1, fft0, FFTW_BACKWARD, FFTW_ESTIMATE);
	fftw_execute(plan);
	if (plan) {fftw_destroy_plan(plan); plan=0;}

	/* copy data back to image, take real part */
	for (y=0; y<Y; y++) for (x=0; x<X; x++) mean1 += sqrt(fft0[y*X+x][0]*fft0[y*X+x][0]+fft0[y*X+x][1]*fft0[y*X+x][1]);
	mean1 /= X*Y;

	for (y=0; y<Y; y++)
	{
		for (x=0; x<X; x++)
		{
			xx = (X+x-X/2)%X;
			yy = (Y+y-Y/2)%Y;
			double neu   = sqrt(fft0[y*X+x][0]*fft0[y*X+x][0]+fft0[y*X+x][1]*fft0[y*X+x][1]);
			double value = neu/mean1*mean0;
			xx = (X+x-X/2)%X;
			yy = (Y+y-Y/2)%Y;
			switch (data->type)
			{
				case DADA_DATA_INT:    data->buffer.i[yy*X+xx] = value; break;
				case DADA_DATA_DOUBLE: data->buffer.f[yy*X+xx] = value; break;
				default:               break;
			}
		}
	}


cleanup:
	if (fft0) {fftw_free(fft0); fft0=NULL;}
	if (fft1) {fftw_free(fft1); fft1=NULL;}
	if (plan) {fftw_destroy_plan(plan); plan=0;}
	return ret;
}

int dada_algo_fourier_lowpass2(struct dada_data* data, double sigma)
{
	int ret = 0;
	int x,y, X,Y, xx, yy;
	double norm   = 1.0/(2*M_PI*sigma);
	norm = 1.0;

	double mean0 = 0.0;
	double mean1 = 0.0;

	fftw_plan     plan = 0;
	fftw_complex* fft0 = NULL;
	fftw_complex* fft1 = NULL;

	if (data == NULL)
	{
		fprintf(stderr, "NULL data (%p)\n", data);
		ret = -1;
		goto cleanup;
	}

	if (data->D != 2)
	{
		DADA_RING_PRINTF("dada_algo_fourier_lowpass only accepts images of dimensions 2 at the moment; data->D=%d refused.", data->D);
		ret = -1;
		goto cleanup;
	}
	if (data->type != DADA_DATA_INT && data->type != DADA_DATA_DOUBLE)
	{
		DADA_RING_PRINTF("dada_algo_fourier_lowpass only accepts images of data->type=INT|DOUBLE; data->type=%d refused.", data->type);
		ret = -1;
		goto cleanup;
	}

	X = data->d[1];
	Y = data->d[0];

	fft0 = fftw_malloc(sizeof(fftw_complex)*Y*X);
	fft1 = fftw_malloc(sizeof(fftw_complex)*Y*X);

	/* copy image data into fft0 */
	for (y=0; y<Y; y++)
	{
		for (x=0; x<X; x++)
		{
			double value = 0;
			xx = (X+x-X/2)%X;
			yy = (Y+y-Y/2)%Y;
			switch (data->type)
			{
				case DADA_DATA_INT:    value = data->buffer.i[y*X+x]; break;
				case DADA_DATA_DOUBLE: value = data->buffer.f[y*X+x]; break;
				default:               value = 0;
			}
			fft0[yy*X+xx][0] = value;
			fft0[yy*X+xx][1] = 0.0;
		}
	}

	for (y=0; y<Y; y++) for (x=0; x<X; x++) mean0 += sqrt(fft0[y*X+x][0]*fft0[y*X+x][0]+fft0[y*X+x][1]*fft0[y*X+x][1]);
	mean0 /= X*Y;
	/* Fourier transform image */
	plan = fftw_plan_dft_2d(Y, X, fft0, fft1, FFTW_FORWARD, FFTW_ESTIMATE);
	fftw_execute(plan);
	if (plan) {fftw_destroy_plan(plan); plan=0;}

	/* point-wise multiplication with (Fourier transformed) Gaussian */
	for (y=0; y<Y; y++)
	{
		for (x=0; x<X; x++)
		{
			xx = (x-X/2)%X;
			yy = (y-Y/2)%Y;
			// double g  = norm * (1-exp(-(xx*xx+yy*yy)/(2*sigma*sigma)));
			double g  = norm *    exp(-(xx*xx+yy*yy)/(2*sigma*sigma));
			xx = (X+x-X/2)%X;
			yy = (Y+y-Y/2)%Y;
			fft1[yy*X+xx][0] *= (0+1*g);
			fft1[yy*X+xx][1] *= (0+1*g);
		}
	}

	/* Fourier back-transform */
	plan = fftw_plan_dft_2d(Y, X, fft1, fft0, FFTW_BACKWARD, FFTW_ESTIMATE);
	fftw_execute(plan);
	if (plan) {fftw_destroy_plan(plan); plan=0;}

	/* copy data back to image, take real part */
	for (y=0; y<Y; y++) for (x=0; x<X; x++) mean1 += sqrt(fft0[y*X+x][0]*fft0[y*X+x][0]+fft0[y*X+x][1]*fft0[y*X+x][1]);
	mean1 /= X*Y;

	for (y=0; y<Y; y++)
	{
		for (x=0; x<X; x++)
		{
			xx = (X+x-X/2)%X;
			yy = (Y+y-Y/2)%Y;
			double alt = 0.0;
			switch (data->type)
			{
				case DADA_DATA_INT:    alt = data->buffer.i[yy*X+xx]; break;
				case DADA_DATA_DOUBLE: alt = data->buffer.f[yy*X+xx]; break;
				default:               break;
			}

			double neu   = sqrt(fft0[y*X+x][0]*fft0[y*X+x][0]+fft0[y*X+x][1]*fft0[y*X+x][1]);
			double value = alt /(neu/mean1);
			xx = (X+x-X/2)%X;
			yy = (Y+y-Y/2)%Y;
			switch (data->type)
			{
				case DADA_DATA_INT:    data->buffer.i[yy*X+xx] = value; break;
				case DADA_DATA_DOUBLE: data->buffer.f[yy*X+xx] = value; break;
				default:               break;
			}
		}
	}


cleanup:
	if (fft0) {fftw_free(fft0); fft0=NULL;}
	if (fft1) {fftw_free(fft1); fft1=NULL;}
	if (plan) {fftw_destroy_plan(plan); plan=0;}
	return ret;
}

