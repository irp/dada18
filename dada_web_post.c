#include "dada_web_post.h"

int dada_web_post(struct connection_meta* meta, const char* url)
{
	int ret = 0;
	int status = MHD_HTTP_NOT_FOUND;

	WEB_POST_WHAT(dada_web_post_nodeinfo,    "/nodeinfo/",      "/nodeinfo/",      meta, url);
	WEB_POST_WHAT(dada_web_post_snap,        "/static/snap/",   "/static/snap/",   meta, url);
	WEB_POST_WHAT(dada_web_post_glue_stxm,   "/glue:stxm/",     "/glue/:stxm",     meta, url);

	return dada_web_error(meta, status);

cleanup:
	return ret;
}

/* example:
 * curl -vs -X POST 'http://127.0.0.1:8080/nodeinfo/e7240:8080?pid=42&version=git-eabbe5-dirty&cpu=130&running={"stxm": 3}'
 */
int dada_web_post_nodeinfo(struct connection_meta* meta, const char* hostport)
{
	int ret = MHD_YES;
	int status = MHD_HTTP_OK;
	struct MHD_Response* response = NULL;
	const char* mime = "text/plain; charset=UTF-8";

	char* msg = NULL;
	FILE* mem = NULL;
	size_t memlen;

	struct dada_cluster_node node;

	const char* q = NULL;

	fprintf(stdout, "hostport: [%s]\n", hostport);
	if (sscanf(hostport, "%64[a-z0-9.-]:%d", node.host, &node.port) != 2) goto notfound;

	if ( (q=MHD_lookup_connection_value(meta->conn, MHD_GET_ARGUMENT_KIND, "version")) != NULL)
		snprintf(node.version, sizeof(node.version)-1, "%s", q);
	if ( (q=MHD_lookup_connection_value(meta->conn, MHD_GET_ARGUMENT_KIND, "pid")) != NULL)
		node.pid = atoi(q);
	if ( (q=MHD_lookup_connection_value(meta->conn, MHD_GET_ARGUMENT_KIND, "cpu")) != NULL)
		node.cpu = atof(q);
	if ( (q=MHD_lookup_connection_value(meta->conn, MHD_GET_ARGUMENT_KIND, "mem")) != NULL)
		node.mem = atof(q);
	if ( (q=MHD_lookup_connection_value(meta->conn, MHD_GET_ARGUMENT_KIND, "running")) != NULL)
	{
		if (strstr(q, "\"stxm\":") != NULL)
			node.last.stxm = atoi(strstr(q, "\"stxm\":")+strlen("\"stxm:\""));
	}

	DADA_MEMSTREAM(mem, msg, memlen);
	fprintf(mem, "okay\n");
	fprintf(mem, "\thost:port is %s:%d (pid %d)\n", node.host, node.port, node.pid);
	fprintf(mem, "\tversion: %s\n", node.version);
	fprintf(mem, "\tcpu: %.1f%%\n", node.cpu);
	fprintf(mem, "\tmem: %.1f%%\n", node.mem);
	fprintf(mem, "\t\tstxm: %d\n", node.last.stxm);
	fclose(mem);

	dada_cluster_node_update(&node);

	status = MHD_HTTP_OK;
	response = MHD_create_response_from_buffer(strlen(msg), (void*)msg, MHD_RESPMEM_MUST_FREE);
	MHD_queue_response(meta->conn, status, response);
	MHD_add_response_header(response, "Content-Type", mime);
	MHD_destroy_response(response);

// cleanup:
	return ret;

err_memstream:
	status = MHD_HTTP_INTERNAL_SERVER_ERROR;
	return dada_web_error(meta, status);
notfound:
	status = MHD_HTTP_NOT_FOUND;
	return dada_web_error(meta, status);
}

int dada_web_error(struct connection_meta* meta, int status)
{
	const char* err = NULL;
	const char* mime = "text/html; charset=UTF-8";
	struct MHD_Response* response = NULL;

	fprintf(meta->mem, "dada_web_error ... ");
	switch (status)
	{
		case MHD_HTTP_FORBIDDEN:
			err = "<!DOCTYPE html>\n<html><head><title>Forbidden</title></head><body><h1>Forbidden</h1><p>The file you are looking for has restricted access.</p></body></html>\n";
			break;
		case MHD_HTTP_NOT_FOUND:
			err = "<!DOCTYPE html>\n<html><head><title>Not Found</title></head><body><h1>Not Found</h1><p>We are sorry, but your requested file cannot be found.</p></body></html>\n";
			break;
		case MHD_HTTP_NOT_IMPLEMENTED:
			err = "<!DOCTYPE html>\n<html><head><title>Not Implemented</title></head><body><h1>Not Implemented</h1><p>Your request is not implemented.</p></body></html>\n";
			break;
		default:
			DADA_RING_PRINTF("dada_web_error: unknown status %d", status);
			status = MHD_HTTP_INTERNAL_SERVER_ERROR;
			err = "<!DOCTYPE html>\n<html>\n  <head>\n    <title>Internal Server Error</title>\n  </head>\n  <body>\n    <h1>Internal Server Error</h1>\n    <p>Your request cannot be processed.</p>\n  </body>\n</html>\n";
			break;
	}

	WEB_RESPONSE_STRCPY(response, err);
	WEBSERVER_QUEUE_MIME(meta, status, response, mime);
	return MHD_YES;
}

int dada_web_post_snap(struct connection_meta* meta, const char* url)
{
	struct dada_web_post_snap* snap = NULL;
	if (0) fprintf(stdout, "dada_web_post_snap(%p, %s)\n", meta, url);
	if (meta->raeum == NULL) /* new request */
	{
		char fname[256];

		meta->raeum             = (struct dada_web_aufraeumer*) malloc(sizeof(struct dada_web_aufraeumer));
		meta->raeum->type       = DADA_WEB_AUFRAEUMER_POST;
		meta->raeum->post       = (struct dada_web_post_post*)  malloc(sizeof(struct dada_web_post_post));
		meta->raeum->post->glue = NULL;
		meta->raeum->post->snap = (struct dada_web_post_snap*)  malloc(sizeof(struct dada_web_post_snap));

		snap = meta->raeum->post->snap;

		snap->poster = NULL;
		snap->code = MHD_HTTP_OK;
		snap->message = "upload not yet started";
		snap->fname = NULL;
		snap->fd = -1;
		meta->raeum->post->data = NULL;
		meta->raeum->post->len = 0;

		/* check filename (once again, to be sure) */
		if (strstr(url, "..") != NULL)
		{
			snap->code    = MHD_HTTP_FORBIDDEN;
			snap->message = "forbidden filename.\n";
			return MHD_YES;
		}

		/* try to open the file */
		snprintf(fname, sizeof(fname)-1, "static/snap/%s", url);
		snap->fname = strdup(fname);
		fprintf(stdout, "\topening %s for writing...\n", snap->fname);
		snap->fd = open(snap->fname, O_WRONLY|O_CREAT|O_TRUNC, 00644);
		if (snap->fd < 0)
		{
			snap->code    = MHD_HTTP_FORBIDDEN;
			snap->message = strerror(errno);
			snap->fd      = -1;
			if (snap->fname) {free(snap->fname); snap->fname=NULL;}
			return MHD_YES;
		}

		snap->poster = MHD_create_post_processor(meta->conn, POSTBUFFERSIZE, &dada_web_post_snap_processor, (void*)snap);

		if (snap->poster == NULL)
		{
			if (snap->fd>0) {close(snap->fd); snap->fd=-1;}
			if (snap->fname) {free(snap->fname); snap->fname=NULL;}
			if (snap) {free(snap); snap=NULL;}
			fprintf(stdout, "MHD_create_post_processor FAILED.\n");
			return MHD_NO;
		}
		if (0) fprintf(stdout, "\tMHD_create_post_processor succeded %p.\n", snap->poster);
		return MHD_YES;
	}
	else /* continuing old request */
	{
		snap = meta->raeum->post->snap;

		if (*meta->raeum->post->len != 0) /* got new data */
		{
			snap->message = "upload started";
			if (0) fprintf(stdout, "continuing... %p\n", snap->poster);
			if (0) fprintf(stdout, "\tlen: %ld\n", *meta->raeum->post->len);
			MHD_post_process(snap->poster, meta->raeum->post->data, *meta->raeum->post->len);
			*meta->raeum->post->len = 0;
			return MHD_YES;
		}
		else /* upload is finished */
		{
			struct MHD_Response* response = NULL;

			if (0) fprintf(stdout, "upload finished.\n");
			snap->message = "upload finished.\n";
			if (snap->fd>0) {close(snap->fd); snap->fd=-1;}
			if (snap->fname) {free(snap->fname); snap->fname=NULL;}

			if (0) fprintf(stdout, "response: (%ld) %s\n", strlen(snap->message), snap->message);

			response = MHD_create_response_from_buffer(strlen(snap->message), (void*)snap->message, MHD_RESPMEM_PERSISTENT);
			MHD_queue_response(meta->conn, snap->code, response);
			MHD_add_response_header(response, "Content-Type", "text/plain");
			MHD_destroy_response(response);
			if (0) fprintf(stdout, "done.\n");

			return MHD_YES;
		}
	}

	if (0) fprintf(stdout, "\n");
	return MHD_YES;
}

int dada_web_post_snap_processor(void* coninfo_cls, __attribute__((unused))enum MHD_ValueKind kind, const char* key, __attribute__((unused))const char* filename, __attribute__((unused))const char* content_type, __attribute__((unused))const char* transfer_encoding, const char* data, uint64_t off, size_t size)
{
	struct dada_web_post_snap* snap = (struct dada_web_post_snap*) coninfo_cls;
	if (0) fprintf(stdout, "\tPROCESSOR %p (%s:%p:%ld:%ld)\n", coninfo_cls, key, data, off, size);
	if (0) fprintf(stdout, "\tDATA: [%s]\n", data);

	if (snap->fd < 0)
	{
		snap->code    = MHD_HTTP_INTERNAL_SERVER_ERROR;
		snap->message = "no file opened.\n";
		return MHD_YES;
	}

	if (write(snap->fd, data, size) != (ssize_t)size)
	{
		fprintf(stderr, "cannot write: %s\n", strerror(errno));
		snap->code    = MHD_HTTP_BAD_REQUEST;
		snap->message = strerror(errno);
	}
	if (0) fprintf(stdout, "\twrote %ld to %s.\n", size, snap->fname);

	return MHD_YES;
}


int dada_web_post_glue_stxm(struct connection_meta* meta, const char* url)
{
	struct dada_web_post_glue* glue = NULL;
	if (0) fprintf(stdout, "dada_web_post_glue(%p, %s)\n", meta, url);
	if (meta->raeum == NULL) /* new request */
	{
		char fname[256];

		meta->raeum             = (struct dada_web_aufraeumer*) malloc(sizeof(struct dada_web_aufraeumer));
		meta->raeum->type       = DADA_WEB_AUFRAEUMER_POST;
		meta->raeum->post       = (struct dada_web_post_post*)  malloc(sizeof(struct dada_web_post_post));
		meta->raeum->post->glue = (struct dada_web_post_glue*)  malloc(sizeof(struct dada_web_post_glue));
		meta->raeum->post->snap = NULL;

		glue = meta->raeum->post->glue;

		glue->poster     = NULL;
		glue->code       = MHD_HTTP_OK;
		glue->mem        = NULL;
		glue->msg        = NULL;
		glue->memlen     = 0;
		glue->fname      = NULL;
		glue->fd         = -1;
		glue->XX         = 0;
		glue->YY         = 0;
		glue->keys       = NULL;
		glue->data       = NULL;
		meta->raeum->post->data = NULL;
		meta->raeum->post->len  = 0;

		DADA_MEMSTREAM(glue->mem, glue->msg, glue->memlen);
		fprintf(glue->mem, "[\n");

		/* try to open the file */
		snprintf(fname, sizeof(fname)-1, "/tmp/glue_%03d.json.tmp", (int)(random()%1000));
		glue->fname = strdup(fname);
		fprintf(stdout, "\topening %s for writing...\n", glue->fname);
		fprintf(glue->mem, "\t\"temp file: %s\",", glue->fname);
		glue->fd = open(glue->fname, O_WRONLY|O_CREAT|O_TRUNC, 00644);
		if (glue->fd < 0)
		{
			fprintf(glue->mem, "\t\"error: cannot open %s: %s.\",\n", glue->fname, strerror(errno));
			glue->code    = MHD_HTTP_FORBIDDEN;
			glue->fd      = -1;
			if (glue->fname) {free(glue->fname); glue->fname=NULL;}
			return MHD_YES;
		}

		glue->poster = MHD_create_post_processor(meta->conn, POSTBUFFERSIZE, &dada_web_post_glue_stxm_processor, (void*)glue);

		if (glue->poster == NULL)
		{
			if (glue->mem)   {fclose(glue->mem); glue->mem=NULL;}
			if (glue->msg)   {free(glue->msg);   glue->msg=NULL;}
			if (glue->fd>0)  {close(glue->fd);   glue->fd=-1;}
			if (glue->fname) {free(glue->fname); glue->fname=NULL;}
			if (glue)        {free(glue);        glue=NULL;}
			fprintf(stdout, "MHD_create_post_processor FAILED.\n");
			return MHD_NO;
		}
		if (0) fprintf(stdout, "\tMHD_create_post_processor succeded %p.\n", glue->poster);
		return MHD_YES;
	}
	else /* continuing old request */
	{
		glue = meta->raeum->post->glue;

		if (*meta->raeum->post->len != 0) /* got new data */
		{
			fprintf(glue->mem, "\t\"upload started\",\n");
			if (0) fprintf(stdout, "continuing... %p\n", glue->poster);
			if (0) fprintf(stdout, "\tlen: %ld\n", *meta->raeum->post->len);
			MHD_post_process(glue->poster, meta->raeum->post->data, *meta->raeum->post->len);
			*meta->raeum->post->len = 0;
			return MHD_YES;
		}
		else /* upload is finished */
		{
			struct MHD_Response* response = NULL;

			if (0) fprintf(stdout, "upload finished.\n");
			fprintf(glue->mem, "\t\"upload finished.\",\n");
			if (glue->fd>0)  {close(glue->fd);   glue->fd=-1;}

			dada_glue_stxm_parse(glue); /* TODO: check return code */

			dada_glue_stxm_process(glue); /* TODO: check return code */

			dada_glue_stxm_putcache(glue);

			dada_glue_stxm_free(glue);

			fprintf(glue->mem, "\t\"end.\"\n");
			fprintf(glue->mem, "]\n");
			if (glue->mem) {fclose(glue->mem); glue->mem=NULL;}

			if (0) fprintf(stdout, "response: (%ld) %s\n", strlen(glue->msg), glue->msg);

			response = MHD_create_response_from_buffer(strlen(glue->msg), (void*)glue->msg, MHD_RESPMEM_MUST_FREE);
			MHD_queue_response(meta->conn, glue->code, response);
			MHD_add_response_header(response, "Content-Type", "application/json; charset=UTF-8");
			MHD_destroy_response(response);
			if (0) fprintf(stdout, "done.\n");

			// if (glue->msg)   {free(glue->msg);   glue->msg=NULL;} /* freed by MHD */
			if (glue->fname) {free(glue->fname); glue->fname=NULL;}
			if (glue->data) {dada_data_destroy(glue->data); free(glue->data); glue->data=NULL;}

			return MHD_YES;
		}
	}

	if (0) fprintf(stdout, "\n");
	return MHD_YES;

err_memstream:
		glue->code    = MHD_HTTP_FORBIDDEN;
		fprintf(glue->mem, "\t\"error: %s\",\n", strerror(errno));
		if (glue->mem) {fclose(glue->mem); glue->mem=NULL;}
		if (glue->msg) {free(glue->msg);   glue->msg=NULL;}
		return MHD_YES;

}

int dada_web_post_glue_stxm_processor(void* coninfo_cls, __attribute__((unused))enum MHD_ValueKind kind, const char* key, __attribute__((unused))const char* filename, __attribute__((unused))const char* content_type, __attribute__((unused))const char* transfer_encoding, const char* data, uint64_t off, size_t size)
{
	struct dada_web_post_glue* glue = (struct dada_web_post_glue*) coninfo_cls;
	if (0) fprintf(stdout, "\tPROCESSOR %p (%s:%p:%ld:%ld)\n", coninfo_cls, key, data, off, size);
	if (0) fprintf(stdout, "\tDATA: [%s]\n", data);

	if (glue->mem == NULL)
	{
		glue->code    = MHD_HTTP_INTERNAL_SERVER_ERROR;
		fprintf(glue->mem, "\"no memstream opened.\",\n");
		return MHD_YES;
	}

	if (write(glue->fd, data, size) != (ssize_t)size)
	{
		fprintf(stderr, "cannot fprintf: %s\n", strerror(errno));
		glue->code    = MHD_HTTP_BAD_REQUEST;
		fprintf(glue->mem, "\t\"error: %s\",\n", strerror(errno));
	}
	if (0) fprintf(stdout, "\twritten: %ld.\n", size);

	return MHD_YES;
}

int dada_glue_stxm_parse(struct dada_web_post_glue* glue)
{
	int ret = 0;

	int XX = 0;
	int YY = 0;
	int num_brackets   = 0;
	int num_quotations = 0;
	int k = 0;
	size_t n, m, len;
	char c;
	char* buf = NULL;
	struct stat st;

	glue->fd = open(glue->fname, O_RDONLY);
	if (glue->fd < 0)
	{
		fprintf(glue->mem, "\t\"error: cannot open %s: %s.\",\n", glue->fname, strerror(errno));
		glue->code = MHD_HTTP_FORBIDDEN;
		glue->fd   = -1;
		ret = -1;
		goto cleanup;
	}
	fstat(glue->fd, &st);
	buf = (char*) malloc(st.st_size+1);
	buf[st.st_size] = '\0';
	if (read(glue->fd, buf, st.st_size) != (ssize_t)st.st_size)
	{
		fprintf(glue->mem, "\t\"error: cannot read from %s: %s.\",\n", glue->fname, strerror(errno));
		glue->code = MHD_HTTP_FORBIDDEN;
		ret = -1;
		goto cleanup;
	}

	/* {"glue":[["line1:1","line1:2"],["line2:1","line2:2"],["line3:1","line3:2"],["line4:1","line4:2"]]} */

	/* estimate dimensions */
	for (n=0; n<strlen(buf); n++)
	{
		c = buf[n];
		switch (c)
		{
			case '[': num_brackets++;   break;
			case '"': num_quotations++; break;
		}
	}

	YY = glue->YY = num_brackets-1;
	if (YY <= 0)
	{
		ret = -1;
		goto cleanup;
	}
	XX = glue->XX = (num_quotations-2)/2 / YY;
	if (XX <= 0)
	{
		ret = -1;
		goto cleanup;
	}

	if (0) fprintf(stdout, "num_brackets: %d, num_quotations: %d\n", num_brackets, num_quotations);
	if (0) fprintf(stdout, "XX: %d, YY: %d\n", XX, YY);

	glue->keys = (char**) malloc(YY*XX*sizeof(char*));

	/* copy keys */
	for (n=0; n<strlen(buf); n++)
	{
		c = buf[n];
		if (c == '[') break;
	}
	for (; n<strlen(buf); n++)
	{
		/* find next " */
		c = buf[n];

		if (c != '"') continue;
		n++;

		/* find length of this key */
		for (m=n+1; m<strlen(buf); m++)
		{
			c = buf[m];
			if (c == '"') break;
		}

		len = m-n;
		if (0)  fprintf(stdout, "k:n,m,len: %d:%ld,%ld,%ld\n", k, n,m,len);
		glue->keys[k++] = strndup(buf+n, len);

		n = m;
	}

	// for (k=0; k<YY*XX; k++) fprintf(stdout, "%2d: %s\n", k, glue->keys[k]);

cleanup:
	FREE(buf);
	if (glue->fd>0) {close(glue->fd); glue->fd=-1;}
	return ret;
}

/* internal function for curl: save server's reply to plain memory */
size_t _glue_curl_mem(void* contents, size_t size, size_t nmemb, void* userp)
{
        size_t realsize = size * nmemb;
        struct mem* mem = (struct mem*)userp;

        mem->buffer = realloc(mem->buffer, mem->size + realsize + 1);
        if(mem->buffer == NULL)
        {
		DADA_RING_PRINTF("[%s:%d] realloc failed: %s", __FILE__, __LINE__, strerror(errno));
                return 0;
        }

        memcpy(&(mem->buffer[mem->size]), contents, realsize);
        mem->size += realsize;
        mem->buffer[mem->size] = 0;

        return realsize;
}

struct _glue_head {
	int D;
	int* d;
	double* data;
};
size_t _glue_curl_head(char* b, size_t size, size_t nmemb, __attribute__((unused))void* userp)
{
	size_t n = size*nmemb;
	struct _glue_head* head = (struct _glue_head*) userp;
	int m;
	int t0, t1, t2, t3, t4;

	if (strncmp(b, "X-dada-dims:", strlen("X-dada-dims:")) != 0) goto cleanup;
	m = sscanf(b, "X-dada-dims: %d: %d,%d,%d,%d\n", &t0, &t1, &t2, &t3, &t4);
	if (m == 0) goto cleanup;
	if (m  > 4) goto cleanup;

	head->D = t0;

	if (head->d) {free(head->d); head->d=NULL;}
	head->d = (int*) malloc(t0*sizeof(int));
	switch (m)
	{
		case 5: head->d[3] = t4; /* FALLTHROUGH */
		case 4: head->d[2] = t3; /* FALLTHROUGH */
		case 3: head->d[1] = t2; /* FALLTHROUGH */
		case 2: head->d[0] = t1;
	}

cleanup:
	return n;
}

int dada_glue_stxm_process(struct dada_web_post_glue* glue)
{
	int ret = 0;
	int xx,yy, XX, YY;
	CURL* curl = NULL;
	CURLcode res = CURLE_OK;
	char url[256];
	struct _glue_head* head = NULL;
	const char* dada = "http://heinzel";
	long int http_code;
	struct mem chunk;
	chunk.buffer = NULL;
	const char* _err="";
	int tot_XX = 0;
	int tot_YY = 0;

	if (glue == NULL || glue->keys == NULL || glue->XX == 0 || glue->YY == 0)
	{
		ret = -1;
		goto cleanup;
	}

	curl = curl_easy_init();
	if (!curl)
	{
		DADA_RING_PRINTF("curl_easy_init failed %p", curl);
		ret = -1;
		goto cleanup;
	}
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, _glue_curl_mem);
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void*)&chunk);
	curl_easy_setopt(curl, CURLOPT_HEADERFUNCTION, _glue_curl_head);
	curl_easy_setopt(curl, CURLOPT_USERAGENT, "dada18/0.1");
	XX = glue->XX;
	YY = glue->YY;
	head = (struct _glue_head*) malloc(YY*XX*sizeof(struct _glue_head));

	fprintf(stdout, "glueing %d×%d patches ...\n", YY, XX);
	fprintf(glue->mem, "\t\"downloading data ...\",\n");

	/* TODO: parallelise these loops */
	for (yy=0; yy<YY; yy++)
	{
		for (xx=0; xx<XX; xx++)
		{
			int idx = yy*XX+xx;
			head[idx].D = 0;
			head[idx].d = NULL;
			head[idx].data = NULL;

			if (chunk.buffer) free(chunk.buffer);
			chunk.buffer = NULL;
			chunk.size = 0;
			snprintf(url, sizeof(url)-1, "%s/stxm%s&format=double", dada, glue->keys[idx]);
			// fprintf(stdout, "\tpatch %2d,%2d: %s ...", yy, xx, url);
			fprintf(glue->mem, "\t\t\"patch[%d,%d]: %s\",\n", yy, xx, url);
			fprintf(stdout, "\tpatch %2d,%2d ...", yy, xx);
			curl_easy_setopt(curl, CURLOPT_HEADERDATA, (void*)&head[idx]);
			curl_easy_setopt(curl, CURLOPT_URL, url);

			res = curl_easy_perform(curl);

			if (res != CURLE_OK)
			{
				DADA_RING_PRINTF("curl_easy_perform failed: %s", curl_easy_strerror(res));
				continue;
			}

			curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &http_code);
			fprintf(stdout, " HTTP %ld\n", http_code);

			if (http_code != 200) goto nix;

			head[idx].data = (void*)chunk.buffer;
			chunk.buffer = NULL;

			continue;
nix:
			head[idx].D = 0;
			if (head[idx].d)    {free(head[idx].d);    head[idx].d=NULL;}
			if (head[idx].data) {free(head[idx].data); head[idx].data=NULL;}
		}
	}

	/* check if dimensions agree
	 * all rows have same height,
	 * all cols have same width
	 */
	for (yy=0; yy<YY; yy++)
	{
		if (head[yy*XX].D != 2   ) {_err="mismatch1"; goto mismatch;}
		if (head[yy*XX].d == NULL) {_err="mismatch2"; goto mismatch;}
		for (xx=1; xx<XX; xx++)
		{
			int idx = yy*XX+xx;
			if (head[idx].D    != 2               ) {_err="mismatch3"; goto mismatch;}
			if (head[idx].D    != head[yy*XX].D   ) {_err="mismatch4"; goto mismatch;}
			if (head[idx].d    == NULL            ) {_err="mismatch5"; goto mismatch;}
			if (head[idx].d[0] != head[yy*XX].d[0]) {_err="mismatch6"; goto mismatch;}
			if (head[idx].d[1] != head[yy*XX].d[1]) {_err="mismatch7"; goto mismatch;}
		}
	}
	fprintf(stdout, "dimensions are consistent.");
	fprintf(glue->mem, "\t\"dimensions are consistent\",\n");

	/* find total height and width */
	for (yy=0; yy<YY; yy++) if (head[yy*XX+0].d) tot_YY += head[yy*XX+0].d[0];
	for (xx=0; xx<XX; xx++) if (head[0*XX+xx].d) tot_XX += head[0*XX+xx].d[1];
	fprintf(stdout, " total size: %d×%d\n", tot_YY, tot_XX);
	fprintf(glue->mem, "\t\"total size: %d×%d\",\n", tot_YY, tot_XX);

	/* make new data array */
	if (head[0].D != 2) {_err="mismatch8"; goto mismatch;}
	glue->data = dada_data_init(DADA_DATA_DOUBLE, 2, YY*head[0].d[0], XX*head[0].d[1]);
	if (glue->data == NULL)
	{
		DADA_RING_PRINTF("[%s:%d] dada_data_init failed.", __FILE__, __LINE__);
		ret = -1;
		goto cleanup;
	}

	/* copy data over */
	fprintf(glue->mem, "\t\"starting to glue ...\",\n");
	for (yy=0; yy<YY; yy++)
	{
		for (xx=0; xx<XX; xx++)
		{
			int x,y, X,Y, idx;
			Y = head[yy*XX+xx].d[0];
			X = head[yy*XX+xx].d[1];
			for (y=0; y<Y; y++)
			{
				for (x=0; x<X; x++)
				{
					idx = yy*(Y*XX*X) + y*(XX*X) + xx*X + x;
					glue->data->buffer.f[idx] = head[yy*XX+xx].data[y*X+x];
				}
			}
		}
	}

	fprintf(glue->mem, "\t\"gluing complete.\",\n");

cleanup:
	if (curl) {curl_easy_cleanup(curl); curl=NULL;}
	if (head)
	{
		int idx;
		for (idx=0; idx<YY*XX; idx++)
		{
			head[idx].D = 0;
			if (head[idx].d)    {free(head[idx].d);    head[idx].d=NULL;}
			if (head[idx].data) {free(head[idx].data); head[idx].data=NULL;}
		}
		if (head) {free(head); head=NULL;}
	}

	return ret;
mismatch:
	fprintf(stdout, "dimension mismatch (%s).\n", _err);
	fprintf(glue->mem, "\t\"error: dimensions mismatch (%s).\",\n", _err);
	DADA_RING_PRINTF("[%s:%d] dimensions mismatch (%s)", __FILE__, __LINE__, _err);
	ret = -1;
	goto cleanup;
}

int dada_glue_stxm_putcache(struct dada_web_post_glue* glue)
{
	int ret = 0;
	char* buf = NULL;
	struct stat st;
	SHA256_CTX ctx;
	SHA256_Init(&ctx);
	unsigned char tmp[SHA256_DIGEST_LENGTH];
	char sha[2*SHA256_DIGEST_LENGTH+1];
	int i;
	int fd = -1;
	char fname[256];
	const char* path = "static/glue/";
	const char* dl_what = NULL;

	if (glue == NULL)
	{
		ret = -1;
		goto cleanup;
	}

	glue->fd = open(glue->fname, O_RDONLY);
	if (glue->fd < 0)
	{
		fprintf(glue->mem, "\t\"error: cannot open %s: %s.\",\n", glue->fname, strerror(errno));
		glue->code = MHD_HTTP_FORBIDDEN;
		glue->fd   = -1;
		ret = -1;
		goto cleanup;
	}
	fstat(glue->fd, &st);
	buf = (char*) malloc(st.st_size+1);
	buf[st.st_size] = '\0';
	if (read(glue->fd, buf, st.st_size) != (ssize_t)st.st_size)
	{
		fprintf(glue->mem, "\t\"error: cannot read from %s: %s.\",\n", glue->fname, strerror(errno));
		glue->code = MHD_HTTP_FORBIDDEN;
		ret = -1;
		goto cleanup;
	}

	SHA256_Update(&ctx, buf, st.st_size);
	SHA256_Final(tmp, &ctx);
	for (i=0; i<SHA256_DIGEST_LENGTH; i++)
	{
		sprintf(sha+2*i, "%02x", tmp[i]);
	}
	fprintf(glue->mem, "\t\"sha: %s\",\n", sha);

	/* put path / sha into file name */
	snprintf(fname, sizeof(fname)-1, "%s%s.json", path, sha);
	fprintf(glue->mem, "\t\"fname: %s\",\n", fname);

	/* put JSON data into this file */
	fd = open(fname, O_WRONLY|O_CREAT|O_TRUNC, 00644);
	if (fd < 0)
	{
		fprintf(glue->mem, "\t\"error: cannot open %s: %s.\",\n", glue->fname, strerror(errno));
		ret = -1;
		goto cleanup;
	}
	if (write(fd, buf, st.st_size) != st.st_size)
	{
		fprintf(glue->mem, "\t\"error: cannot write to %s: %s.\",\n", glue->fname, strerror(errno));
		ret = -1;
		goto cleanup;
	}


	/* put glued data into cache */
	if (glue->data)
	{
		void* so_handle = NULL;
		int (*so_funcp)(const char*,const char*,const char*,struct dada_data*) = NULL;
		const char* key_space = "dada_cache";
                const char* key_set   = "";
		dl_what = "dada_input_cache_write_data";
		DADA_DL_LOAD_SYMBOL(DADA_DL_PATH_CONST("input/cache"), dl_what, so_funcp, so_handle);
		so_funcp(key_space, key_set, sha, glue->data);
		DADA_DL_CLOSE(so_handle);
		fprintf(glue->mem, "\t\"aero caching: %ld bytes\",\n", glue->data->d[0]*glue->data->d[1]*sizeof(double));

		fprintf(glue->mem, "\t\"URL: /glue/%s\",\n", sha);
	}

cleanup:
	FREE(buf);
	if (glue->fd>0) {close(glue->fd); glue->fd=-1;}
	if (fd>0) {close(fd); fd=-1;}
	return ret;
error:
	DADA_RING_PRINTF("dlopen failed for %s", dl_what);
	goto cleanup;
}

void dada_glue_stxm_free(struct dada_web_post_glue* glue)
{
	int xx, yy, n;

	if (glue == NULL) return;

	if (glue->keys == NULL) return;

	for (yy=0; yy<glue->YY; yy++)
	{
		for (xx=0; xx<glue->XX; xx++)
		{
			n = yy*glue->XX+xx;
			if (glue->keys[n]) {free(glue->keys[n]); glue->keys[n]=NULL;}
		}
	}

	if (glue->keys) {free(glue->keys); glue->keys=NULL;}
}

