#!/bin/sh

# echo 'SELECT "web.key" FROM dada_web' | aql 2>/dev/null | grep -o '[^"]*\/GINIX[^"]*' > /tmp/keys.txt
# echo -n "deleting ..."
# while read key
# do
# 	echo 'DELETE FROM dada_web WHERE PK="'$key'"' | aql 2>/dev/null
# done < /tmp/keys.txt
# 
# echo 'SELECT "data.key" FROM dada_cache' | aql 2>/dev/null | grep -o '\/GINIX[^"]*' > /tmp/keys.txt
# echo -n "deleting ..."
# while read key
# do
# 	echo 'DELETE FROM dada_cache WHERE PK="'$key'"' | aql 2>/dev/null
# done < /tmp/keys.txt
# 
# echo 'SELECT "job.key" FROM dada_job' | aql 2>/dev/null | grep -o '\/GINIX[^"]*' > /tmp/keys.txt
# echo -n "deleting ..."
# while read key
# do
# 	echo 'DELETE FROM dada_job WHERE PK="'$key'"' | aql 2>/dev/null
# done < /tmp/keys.txt
# 
# echo

asinfo -v "truncate:namespace=dada_web"
asinfo -v "truncate:namespace=dada_cache"
asinfo -v "truncate:namespace=dada_cluster"
#asinfo -v "truncate:namespace=dadafs"

