#ifndef _DADA_AERO_H_
#define _DADA_AERO_H_

#include "input/inc/aerospike/aerospike.h"

extern struct dada_backend_aero aero;
extern struct dada_backend_aero_host aero_host;

struct dada_backend_aero_host {
	const char* host;
	int port;
};
struct dada_backend_aero {
	as_config cfg;
	aerospike aer;
	as_error err;
	int hostc;
	struct dada_backend_aero_host* hosts;
};

#endif

